//
//  ProductSearchResultTableViewCell.h
//  Ztore
//
//  Created by ken on 19/7/16.
//  Copyright © ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductSearchResultTableViewCell : UITableViewCell

@property(nonatomic, retain) IBOutlet UILabel *label_text;

- (void)setAttributedText:(NSString *)text searchBarText:(NSString *)searchBarText;

@end
