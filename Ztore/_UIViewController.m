//
//  _UIViewController.m
//  Ztore
//
//  Created by ken on 24/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "RealUtility.h"
#import "ZtoreNavigationBar.h"
#import "_UIViewController.h"
#import "ProductDetailController.h"
#import "ProductSearchBarController.h"
#import "WebViewViewController.h"
#import "MainRegisterViewController.h"
#import "ProductListCollectionViewController.h"
#import "ActionSheetStringPicker.h"
#import "REAPIClient.h"

@implementation _UIViewController {
  UIScrollView *scrollView_kb; // using for custom keyboard
  NSString *pageName;          // for google analytics
}

- (void)viewDidLoad {
    
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  [self.view setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
  self.navigationItem.hidesBackButton = YES;
  [self disableSlidePanGestureForRightMenu];
    
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self setUpNavigationBar];
    
  if (isValid(pageName)) {
        
  [[Common shared] sendGAForpageName:pageName];
      
  }

  DDLogInfo(@"viewWillAppear currentview:-->[%@]", [self class]);
  [self.navigationController.navigationBar setHidden:NO];
  [AppDelegate getAppDelegate].topVisbleViewController = self;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self rac_willDeallocSignal];
    [TSMessage dismissActiveNotification];
    
    for (UIView *view in [AppDelegate getAppDelegate].window.subviews) {
        
        if ([view isKindOfClass:[ZtoreNavigationBar class]]) {
            
            [view removeFromSuperview];
            
        }
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!self.haveCallAPI) {
        
        self.haveCallAPI = YES;
        
        [self callAPI];
        
    }
}

- (void)awakeFromNib {

  [super awakeFromNib];
  self.navigationController.navigationBar.barTintColor = [[Common shared] getCommonRedColor];
  self.navigationController.navigationBar.translucent  = NO;
    
}

- (void)callAPI {
    
}

- (void)setupView {
    
}

- (void)handleErrorFromErrorCode:(NSInteger)errorCode methodName:(NSString *)methodName {
    
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

//-------Google Analytics functions-----------
- (void)setPageName:(NSString *)name {
  pageName = name;
}

- (void)logEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label {
  [self logEventWithCategory:category action:action label:label value:@1];
}

- (void)logEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    
  id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
  [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:action label:label value:value] build]];
    
}
//-------End Google Analytics functions-------

- (void)showToastMessage:(NSString *)msg {
  [self.navigationController.view makeToast:msg];
}

- (void)showToastMessage:(NSString *)msg duration:(NSTimeInterval)duration position:(id)position {
  [self.navigationController.view makeToast:msg duration:duration position:position];
}

- (void)hideToastActivity {
  [self.navigationController.view hideToastActivity];
}

- (void)showLoadingHUD {
  [self showLoadingHUDWithProgress:nil];
}

- (void)showLoadingHUDWithProgress:(NSString *)progress {
  [self hideLoadingHUD];
  self.currentHUD          = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
  self.currentHUD.delegate = self;

  if (progress) {

    self.currentHUD.label.text = progress;

  } else {

    self.currentHUD.label.text = JMOLocalizedString(@"Loading", nil);
  }
    

    if (self.hideLoadingTimer) {
        
        [self.hideLoadingTimer invalidate];
    }
    
    NSTimeInterval timeoout = ([REAPIClient sharedClient].orignalTimeOutInterval) ? [REAPIClient sharedClient].orignalTimeOutInterval : kTimeOutInterval ;
    self.hideLoadingTimer = [NSTimer scheduledTimerWithTimeInterval:timeoout target:self selector:@selector(hideLoadingHUD) userInfo:nil repeats:NO];

}

- (void)hideLoadingHUD {

  if (self.currentHUD) {
    [self.currentHUD hideAnimated:YES];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
  }
}

- (void)loadingFromColor:(UIColor *)color {
    
    [self addDefaultLoading];
    
}

- (void)endCallAPILoading {
    
    [self.loadingView_productList stopAnimating];
    
}

- (void)addDefaultLoading {
    
    if (!isValid(self.loadingView_productList) ) {
        
        // Create and add the Activity Indicator to splashView
        self.loadingView_productList = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingView_productList.alpha                    = 1;
        self.loadingView_productList.hidesWhenStopped         = YES;
        // add the components to the view
        [self.view addSubview:self.loadingView_productList];
        
        [self.loadingView_productList mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self.view.mas_centerX).with.offset(0);
            make.centerY.equalTo(self.view.mas_centerY).with.offset(0);
            
        }];
        
    }
    
    [self.view bringSubviewToFront:self.loadingView_productList];
    [self.loadingView_productList startAnimating];

}

- (void)setUpNavigationBar {
    
}

// Initialize the tansition
- (void)createTransition {
  // self.presentControllerButton is the animatedView used for the transition
  self.transition = [[JTMaterialTransition alloc] initWithAnimatedView:self.presentControllerButton];
}

// Indicate which transition to use when you this controller present a
// controller
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
  self.transition.reverse = NO;

  return self.transition;
}

// Indicate which transition to use when the presented controller is dismissed
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
  self.transition.reverse = YES;

  return self.transition;
}

- (void)backBtnPressed:(id)sender {
    
        [self.navigationController popViewControllerAnimated:YES];

}

- (void)backBtnPressedNotEqualToSearch:(id)sender {
    
    if ([self isPerviousControllerEqualToSearch]) {
        
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 3] animated:YES];
        
    } else {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

- (void)backToMainPageGetCurrentUser:(BOOL)getCurrentUser {
    
    if (getCurrentUser) {
    
    [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self backToMainPage];
            
        }
    }];
        
    } else {
        
        [self backToMainPage];
    }
    
}

- (void)backToMainPage {
    
     [self hideLoadingHUD];
     [self.navigationController popToRootViewControllerAnimated:YES];
     // [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenu object:nil];
     [self endCallAPILoading];
    [[AppDelegate getAppDelegate].productCategoryController showMainPage:YES];
    
}

- (void)backToViewController {

  if ([Common shared].backToViewController != nil) {

    if ([Common shared].backToViewController == [self class]) {

      [Common shared].backToViewController = nil;

    } else {

      [self.navigationController popViewControllerAnimated:NO];
        
        if ([self isKindOfClass:[ProductCategoryController class]]) {
            
            [Common shared].backToViewController = nil;
            
        }
        
    }
      
  }
    
}

- (void)customizeMessageView:(TSMessageView *)messageView {
    
    
}

- (void)popToViewController:(Class)controller {
    
    [Common shared].backToViewController = controller;
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (void)dismissViewController:(Class)controller {
    
    [Common shared].backToViewController = controller;
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)showErrorFromMessage:(NSString *)message controller:(UIViewController *)controller okBtn:(UIAlertAction *)okBtn {
    
    if (message) {
        
        [[AppDelegate getAppDelegate] showMessageBarFromResponse:message];
    }

}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = nil;
    UIFont *font = nil;
    UIColor *textColor = nil;
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];

    text = JMOLocalizedString(@"No Record", nil);
    font = [[Common shared] getNormalFontWithSize:22];
    textColor = [UIColor lightGrayColor];
            
            
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    
    return YES;
}

- (BOOL)isPerviousControllerEqualToSearch {
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2) {
        
        return NO;
        
    } else {
        
        return [[self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2] isKindOfClass:[ProductSearchBarController class]];
        
    }
    
}

- (void)showPrivacyPolicy {
    
    [self.view endEditing:YES];
    WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
    zDollarViewController.titleString = JMOLocalizedString(@"Privacy Policy", nil);
    zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_PrivacyPolicy/mobile" : @"https://www.ztore.com/tc/article/article_PrivacyPolicy/mobile";
    [zDollarViewController setPageName:@"My Account - PrivacyPolicy"];
    [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
    
}

- (void)showTermsOfService {
    
    [self.view endEditing:YES];
    WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
    zDollarViewController.titleString = JMOLocalizedString(@"Terms & Conditions", nil);
    zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_tnc/mobile" : @"https://www.ztore.com/tc/article/article_tnc/mobile";
    [zDollarViewController setPageName:@"My Account - T&C"];
    [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
    
}

#pragma mark - BlueTag delegate

- (void)pressBlueTagBtnFromPromotions:(Promotions *)promotions {
    
    InputListProduct *inputListProduct = [[InputListProduct alloc] init];
    inputListProduct.type = press_blue_tag;
    inputListProduct.sn = promotions.sn;
    [AppDelegate getAppDelegate].searchSelectedString = promotions.label;
    [[Common shared] pushToProductListPageFromInputListProduct:inputListProduct controller:self];
    
}

#pragma mark - SoldOut View delegate

- (void)pressSoldOutBtnFromProduct:(Products *)product {
    
    if (![Common shared].isLogin) {
        
        [[Common shared] notLoginToLoginPage];
        
        
    } else {
        
        [self showLoadingHUD];
        
        if (product.is_notice) {
            
            [[ServerAPI shared] cancelProductNoticeFromProduct_Id:(int)product.id
                                                  completionBlock:^(id response, NSError *error) {
                                                      
                                                      [self hideLoadingHUD];
                                                      
                                                      if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                          
                                                          if ([self isKindOfClass:[ProductDetailController class]] ) {
                                                           
                                                              ProductDetailController *productDetailController = (ProductDetailController *)self;
                                                              product.is_notice = productDetailController.orignalProduct.is_notice = NO;     
                                                              [productDetailController setLabelCartQtyText];
                                                              
                                                          }
                                                          
                                                          if ([self isKindOfClass:[ProductListCollectionViewController class]] ) {
                                                              
                                                              ProductListCollectionViewController *productListCollectionViewController = (ProductListCollectionViewController *)self;
                                                              product.is_notice = NO;
                                                              [productListCollectionViewController.collectionView reloadData];

                                                              
                                                          }
                                                          
                                                      }
                                                  }];
            
            
        } else {
            
            [[ServerAPI shared] addProductNoticeFromProduct_Id:(int)product.id
                                               completionBlock:^(id response, NSError *error) {
                                                   
                                                   [self hideLoadingHUD];
                                                   
                                                   if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                       
                                                       if ([self isKindOfClass:[ProductDetailController class]] ) {
                                                           
                                                           ProductDetailController *productDetailController = (ProductDetailController *)self;
                                                           product.is_notice = productDetailController.orignalProduct.is_notice = YES;
                                                           [productDetailController setLabelCartQtyText];
                                                           
                                                       }
                                                       
                                                       if ([self isKindOfClass:[ProductListCollectionViewController class]] ) {
                                                           
                                                           ProductListCollectionViewController *productListCollectionViewController = (ProductListCollectionViewController *)self;
                                                           product.is_notice = YES;
                                                           [productListCollectionViewController.collectionView reloadData];
                                                           
                                                       }
                                                       
                                                   }
                                               }];
            
        }
        
    }
    
}

#pragma mark - common dropdown menu

- (void)pressChooseTitle:(UIButton *)sender {
    
    CustomTextFieldView *item = (CustomTextFieldView *)sender.superview;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
    [self.view endEditing:YES];
    [item textFieldBegin];
    [item resignFirstResponder];
    
    self.pickerDataArray = item.stringArray;
    
    [ActionSheetCustomPicker showPickerWithTitle:item.textField.attributedPlaceholder.string delegate:self showCancelButton:YES origin:sender];

    if (isValid(self.pickerDataArray)) {
        
        self.selectedPickerData = self.pickerDataArray.firstObject;
        
    }
    
}

#pragma mark - ActionSheetCustomPickerDelegate

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.pickerDataArray.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [self.pickerDataArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedPickerData = [self.pickerDataArray objectAtIndex:row];
    DDLogDebug(@"[self.pickerDataArray objectAtIndex:row] %@", [self.pickerDataArray objectAtIndex:row]);
}

- (void)actionSheetPicker:(AbstractActionSheetPicker *)actionSheetPicker configurePickerView:(UIPickerView *)pickerView {
 
    NSMutableParagraphStyle *labelParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    labelParagraphStyle.alignment = NSTextAlignmentCenter;
    actionSheetPicker.pickerTextAttributes = [@{NSParagraphStyleAttributeName : labelParagraphStyle, NSFontAttributeName : [[Common shared] getBiggerNormalFont]} mutableCopy];
    
}

- (void)actionSheetPickerDidSucceed:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin {
    
    UIButton *sender = origin;
    CustomTextFieldView *item = (CustomTextFieldView *)sender.superview;
    item.textField.text = self.selectedPickerData;
    [item afterDoneFromSelectedValue:self.selectedPickerData];
}

- (void)actionSheetPickerDidCancel:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin {
    
      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
