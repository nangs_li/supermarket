//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "Common.h"
#import "UINavigationController+Utility.h"
#import <QuartzCore/QuartzCore.h>

@implementation UINavigationController (Utility)

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated enableUI:(BOOL)enableUI {
    
    void (^completion)(void) = ^{
        
        self.view.userInteractionEnabled = YES;
        
    };
    
    self.view.userInteractionEnabled = enableUI;
    [CATransaction begin];
    [CATransaction setCompletionBlock:completion];
    [self pushViewController:viewController animated:animated];
    [CATransaction commit];
   
}

@end
