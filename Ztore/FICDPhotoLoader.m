//
//  FICDPhotoLoader.m
//  Ztore
//
//  Created by ken on 28/8/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation FICDPhotoLoader

+ (int)getScreenScale {

  return (int)[[UIScreen mainScreen] scale];
}

+ (NSString *)getPhotoUrlInProduct:(Zoom *)productImage {
  NSString *url = @"";

  switch ([self getScreenScale]) {
  case 1:
    url = productImage.mdpi;
    break;
  case 2:
    url = productImage.xhdpi;
    break;
  case 3:
    url = productImage.xxhdpi;
    break;
  default:
    url = productImage.xhdpi;
    break;
  }

  return url;
}

+ (void)loaderImageForImageView:(UIImageView *)imageView withPhotoUrl:(NSString *)photoUrl {

  [imageView sd_setImageWithURL:[NSURL URLWithString:photoUrl]
               placeholderImage:nil
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                        if (cacheType == 1) {

                          [imageView.layer addAnimation:[CATransition animation] forKey:kCATransition];
                        }

                      }];
}

/*+ (void) loaderImageForImageView:(UIImageView *)imageView
withPhotoUrl:(NSString *)photoUrl withFormatName:(NSString *)formatName{
    [imageView setImage:nil];
    FICDPhoto *photo = [[FICDPhoto alloc] init];
    [photo setSourceImageURL:[NSURL URLWithString:photoUrl]];
   // NSString *formatName = FICDPhotoNormalQualityThumbnail;
    FICImageCacheCompletionBlock completionBlock = ^(id <FICEntity> entity,
NSString *formatName, UIImage *image) {
        //[delegate finishLoadImageFromInternetWithUrl:photoUrl
withImage:image];
        imageView.image = image;
        //[imageView.layer addAnimation:[CATransition animation]
forKey:kCATransition];
    };

    FICImageCache *sharedImageCache = [FICImageCache sharedImageCache];
    BOOL imageExists = [sharedImageCache retrieveImageForEntity:photo
withFormatName:formatName completionBlock:completionBlock];

    if (imageExists == NO) {
        //[delegate readyLoadImageFromInternetWithUrl:photoUrl];
        [self imageCache:sharedImageCache wantsSourceImageForEntity:photo
withFormatName:FICDPhotoNormalQualityThumbnail completionBlock:^(UIImage
*image){
            //completion hadler code on main queue
            //[delegate finishLoadImageFromInternetWithUrl:photoUrl
withImage:image];
            imageView.image = image;
            NSLog(@"new image");
        }];
    }

}

+ (void)imageCache:(FICImageCache *)imageCache
wantsSourceImageForEntity:(id<FICEntity>)entity withFormatName:(NSString
*)formatName completionBlock:(FICImageRequestCompletionBlock)completionBlock {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
0), ^{
        // Fetch the desired source image by making a network request
        NSURL *requestURL = [entity sourceImageURLWithFormatName:formatName];
        UIImage *sourceImage = [self _sourceImageForURL:requestURL];

        FICImageCacheCompletionBlock thisCompletionBlock = ^(id <FICEntity>
entity, NSString *formatName, UIImage *image) {
            completionBlock(image);
        };

        dispatch_async(dispatch_get_main_queue(), ^{
            [imageCache setImage:sourceImage forEntity:entity
withFormatName:formatName completionBlock:thisCompletionBlock];
            //completionBlock(sourceImage);
        });
    });
}

+ (UIImage *)_sourceImageForURL:(NSURL *)imageURL {
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    return image;
}*/
@end
