//
//  NSURL+Utility.m
//  productionreal2
//
//  Created by Ken on 10/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import "UITableViewCell+Utility.h"

@implementation UITableViewCell (Utility)

- (UIEdgeInsets)layoutMargins {

  return UIEdgeInsetsZero;
}

@end
