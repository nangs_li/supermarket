//
//  Address.m
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import "AddressData.h"

@implementation AddressDetail

@end

@implementation AddressData

- (NSString *)getNameFromAddressId:(NSInteger)addressId {

    for (AddressDetail *addressDetail in self.data) {
        
        if (addressDetail.id == addressId) {
            
            return addressDetail.name;
        }
        
        
    }
    
    return nil;
}

@end
