//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "PopUpView.h"

#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation PopUpView

#pragma mark set Up Error MessageView

- (void)setupErrorMessageView {
    
    self.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    self.titleLabel.textColor = [[Common shared] getCommonRedColor];
    self.errorDetailLabel.font = [[Common shared] getSmallContentNormalFont];
    self.errorDetailLabel.textColor = [UIColor blackColor];
    self.errorDetailLabel.text = JMOLocalizedString(@"Please wait, we are retrying..", nil);
    [self runSpinAnimationOnView:self.retryImageView duration:200000 rotations:-0.5 repeat:YES];
    
}

- (void)runSpinAnimationOnView:(UIView *)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    
    CABasicAnimation *rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
