//
//  NSURL+Utility.h
//  productionreal2
//
//  Created by Ken on 10/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITableViewCell (Utility)

- (UIEdgeInsets)layoutMargins;

@end
