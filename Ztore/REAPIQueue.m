//
//  REAPIQueue.m
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REAPIQueue.h"

// Models
#import "REDBAPIQueue.h"

@implementation REAPIQueue

- (REDBAPIQueue *)toREDBObject {
  REDBAPIQueue *apiDBQueue   = [[REDBAPIQueue alloc] init];
  apiDBQueue.queueId         = self.queueId;
  apiDBQueue.httpMethod      = self.httpMethod;
  apiDBQueue.dependingQueues = (RLMArray<REDBAPIQueue * ><REDBAPIQueue> *)[RLMArray arrayWithNSMutableArray:self.dependingQueues objectClass:[REDBAPIQueue class]];
  apiDBQueue.pendingQueues   = (RLMArray<REDBAPIQueue * ><REDBAPIQueue> *)[RLMArray arrayWithNSMutableArray:self.pendingQueues objectClass:[REDBAPIQueue class]];
  apiDBQueue.retryCount      = self.retryCount;

  if (self.response) {
    apiDBQueue.response = [self.response toJSONString];
  }

  apiDBQueue.url       = self.url;
  apiDBQueue.parameter = [self.parameter jsonString];
  apiDBQueue.url       = self.url;
  apiDBQueue.status    = self.status;

  return apiDBQueue;
}

+ (REAPIQueue *)queueWithUrl:(NSString *)url parameter:(NSDictionary *)parameter httpMethod:(REHttpMethod)httpMethod {
  return [REAPIQueue queueWithUrl:url parameter:parameter httpMethod:httpMethod dependencies:nil];
}

+ (REAPIQueue *)queueWithUrl:(NSString *)url parameter:(NSDictionary *)parameter httpMethod:(REHttpMethod)httpMethod dependencies:(NSArray *)dependencies {
  REAPIQueue *queue     = [[REAPIQueue alloc] init];
  queue.queueId         = [[NSUUID UUID] UUIDString];
  queue.httpMethod      = httpMethod;
  queue.url             = url;
  queue.parameter       = [NSDictionary dictionaryWithDictionary:parameter];
  queue.pendingQueues   = [[NSMutableArray alloc] init];
  queue.dependingQueues = [[NSMutableArray alloc] init];

  for (REAPIQueue *apiQueue in dependencies) {
    [queue.pendingQueues addObject:apiQueue];
    [queue.dependingQueues addObject:apiQueue];
  }

  queue.retryCount = 0;
  queue.response   = nil;
  queue.status     = REAPIQueueStatusPending;

  return queue;
}

@end
