 //
//  ProductListCollectionViewController.m
//  Ztore
//
//  Created by ken on 22/7/16.
//  Copyright © ken. All rights reserved.
//

#import "CollectionFooterLoadingView.h"
#import "FICDPhotoLoader.h"
#import "GetCategory.h"
#import "NSError+RENetworking.h"
#import "ProductDetailController.h"
#import "ProductListCollectionViewController.h"
#import "ProductSearchBarController.h"
#import "PromotionHeaderView.h"
#import "PromotionModel.h"
#import "MainRegisterViewController.h"

@interface ProductListCollectionViewController ( ) {
  int result_start;
  UICollectionView *thisCollectionView;
  UICollectionViewFlowLayout *thisCollectionViewFlowLayout;
  GetCategory *productCategory;
  UIViewController *productAdanceSearchViewController;
    
}

@end

static NSString *promotionUrl = nil;
static NSString *cellidentifier = @"ProductCollectionCell";
static float FOOTER_HEIGHT      = 40.f;
static NSString *headCellidentifier = @"PromotionHeaderView";

@implementation ProductListCollectionViewController

@synthesize products = products;

+ (ProductListCollectionViewController *)initViewControllerWithInputListProduct:(InputListProduct *)inputListProduct {
    
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     ProductListCollectionViewController *cvc = [storyboard instantiateViewControllerWithIdentifier:@"ProductListCollectionViewController"];
    cvc.inputListProduct = inputListProduct;
    cvc.inputListProduct.type = inputListProduct.type;
    cvc.products = [[NSMutableArray alloc] init];
    
    return cvc;

}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  result_start                        = 0;
  self.parentController = self;
  [super setPageName:@"Category"];
//  if (inputListProduct.type == parent_category_id) {
//    
//  [[ServerAPI shared] getCategoryFromCategory_Id:inputListProduct.parent_category_id
//                             completionBlock:^(id _Nullable response, NSError *_Nullable error) {
//
//                               if ([[Common shared] checkNotError:error controller:self response:response]) {
//
//                                 [self getProductCategoryCallbackFromGetProductCategory:response];
//                               }
//                             }];
//      
//  }
    
  [self initCollectionView:self.collectionView collectionViewFlowLayout:self.collectionViewFlowLayout];

  (self.inputListProduct.type == mainPage_category) ? nil : [self setUpFiliterBtn];

  [self setUpdateProductQtyNotification];
    
  (self.inputListProduct.type == mainPage_category) ? nil :  [self loadingFromColor:nil];
  self.automaticallyAdjustsScrollViewInsets = NO;
    
}

- (void)viewWillAppear:(BOOL)animated {

  [super viewWillAppear:animated];
  self.navigationItem.hidesBackButton = YES;
    
    if ([self.inputListProduct checkHaveSearchType] || self.inputListProduct.type == press_blue_tag) {
        
    [AppDelegate getAppDelegate].topVisbleViewController = self;
        
    } else {
        
    [AppDelegate getAppDelegate].topVisbleViewController = [AppDelegate getAppDelegate].productCategoryController;
        
    }
    
  DDLogDebug(@"self.collectionView reloadData");
  [self.collectionView reloadData];
  [self enableSlidePanGestureForRightMenu];
    
}

- (void)loadingFromColor:(UIColor *)color {
    
    self.collectionView.hidden = YES;
    [self addDefaultLoading];
    
}

- (void)endCallAPILoading {
    
    self.collectionView.hidden = NO;
    [super endCallAPILoading];
    
}

- (void)addDefaultLoading {
    
    if (!isValid(self.loadingView_productList) ) {
        
        // Create and add the Activity Indicator to splashView
        self.loadingView_productList = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingView_productList.alpha                    = 1;
        self.loadingView_productList.hidesWhenStopped         = YES;
        // add the components to the view
        [self.view addSubview:self.loadingView_productList];
        
        [self.loadingView_productList mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self.view.mas_centerX).with.offset(0);
            make.centerY.equalTo(self.view.mas_centerY).with.offset(-30);
            
        }];
        
    }
    
    [self.view bringSubviewToFront:self.loadingView_productList];
    [self.loadingView_productList startAnimating];
    
}

- (void)callAPI {
    
    if (([self.inputListProduct checkHaveSearchType])) {
        
        [self callgetListProduct];
        
    }
    
    
    if (self.inputListProduct.type == press_blue_tag) {
        
        [self.collectionView setBounces:NO];
          self.collectionView.alwaysBounceVertical = NO;
        
        [[ServerAPI shared] getPromotionInfoFromSn:self.inputListProduct.sn completionBlock:^(id response, NSError *error) {
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                self.promotionModel = response;
                
                Banner *banner = self.promotionModel.data.banner;
                Zoom *image =  banner.image;
                promotionUrl = (isValid(image)) ? [FICDPhotoLoader getPhotoUrlInProduct:image] : nil;
                
                [self callgetListProduct];
                
            }
            
        }];
    
    }
    
}

- (void)callgetListProduct {
    
     [self loadingFromColor:nil];
     self.cleanProducts = YES;
     [self getListProductFromStart:result_start listProduct:self.inputListProduct];
    
}

- (void)initCollectionView:(UICollectionView *)myCollectionView collectionViewFlowLayout:(UICollectionViewFlowLayout *)myCollectionViewFlowLayout {
  thisCollectionView = myCollectionView;
  // thisCollectionView.initialCellTransformBlock = ADLivelyTransformGrow;

    if ([self.inputListProduct checkHaveSearchType] || self.inputListProduct.type == press_blue_tag ) {
    
    [thisCollectionView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(thisCollectionView.superview).with.offset(0); // with is an optional semantic filler
       
    }];
        
    }
    
  thisCollectionViewFlowLayout            = myCollectionViewFlowLayout;
  thisCollectionView.collectionViewLayout = thisCollectionViewFlowLayout;
  thisCollectionView.dataSource           = self;
  thisCollectionView.delegate             = self;
  UINib *nib                              = [UINib nibWithNibName:cellidentifier bundle:nil];
  [thisCollectionView registerNib:nib forCellWithReuseIdentifier:cellidentifier];
  thisCollectionView.alwaysBounceVertical          = YES;
  thisCollectionViewFlowLayout.footerReferenceSize = CGSizeMake(thisCollectionView.frame.size.width, FOOTER_HEIGHT);
  UINib *footerNib                                 = [UINib nibWithNibName:@"CollectionFooterLoadingView" bundle:nil];
  [thisCollectionView registerNib:footerNib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"CollectionFooterLoadingView"];
  [thisCollectionView registerNib:[UINib nibWithNibName:headCellidentifier bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headCellidentifier];
  [thisCollectionView setBackgroundColor:[UIColor clearColor]];
  thisCollectionView.emptyDataSetSource = self;
  thisCollectionView.emptyDataSetDelegate = self;
    
    
  (self.inputListProduct.type == mainPage_category) ? nil : [self setUpMJRefreshWithCollectionView:myCollectionView];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    CGSize size;
    
    if (self.inputListProduct.type == press_blue_tag) {
        
        self.headCell = [[[NSBundle mainBundle] loadNibNamed:headCellidentifier owner:self options:nil] objectAtIndex:0];;
        
        [self.headCell setupViewFromPromotion:self.promotionModel urlLink:promotionUrl];
        
        
        size = CGSizeMake([RealUtility screenBounds].size.width, [self.headCell getTotalHeight]);
        
    } else {
        
        size = CGSizeMake(1, 1);
        
    }
    
    return size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    self.headCell = (PromotionHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headCellidentifier forIndexPath:indexPath];
    
    [self.headCell setupViewFromPromotion:self.promotionModel urlLink:promotionUrl];
 
    self.headCell.hidden = (self.inputListProduct.type == press_blue_tag) ? NO : YES;
    
    return self.headCell;
}

- (void)collectionViewReloadDataWithAnimation {

  [thisCollectionView reloadData];
  [thisCollectionView setContentOffset:CGPointMake(0, [RealUtility screenBounds].size.height / 2) animated:NO];
  [thisCollectionView setContentOffset:CGPointMake(0, 0) animated:NO];
    
}

- (void)setUpMJRefreshWithCollectionView:(UICollectionView *)myCollectionView {

  self.footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{

    thisCollectionViewFlowLayout.footerReferenceSize = CGSizeMake(thisCollectionView.frame.size.width, FOOTER_HEIGHT);

    [self getListProductFromStart:result_start listProduct:self.inputListProduct];

  }];
    
  [self.footer setTitle:@"" forState:MJRefreshStateIdle];
  thisCollectionView.mj_footer = self.footer;
    
}

- (void)getListProductWithtBrands:(NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(NSArray<NSString *> *)productTags selectedSort:(NSString *)selectedSort {

  thisCollectionViewFlowLayout.footerReferenceSize = CGSizeMake(thisCollectionView.frame.size.width, FOOTER_HEIGHT);
    self.productList.selectedProducts_tag = productTags;
  products                = [[NSMutableArray alloc] init];
  self.inputListProduct                    = [[Common shared] getInputListProductWithtBrands:productBrands productSpecialtyTags:productSpecialtyTags productTags:productTags selectedSort:selectedSort inputListProduct:self.inputListProduct];
    
  [[ServerAPI shared] listProductWithAllBrandTagFromInput:self.inputListProduct
                                      completionBlock:^(id _Nullable response, NSError *_Nullable error) {
                                        
                                        [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = NO;
                                        self.collectionView.hidden = NO;
                                          
                                        if ([[Common shared] checkNotError:error controller:self response:response]) {

                                          self.cleanProducts = YES;
                                          [self listProductFromProductListData:response];
                                        }

                                      }];


}

- (void)getListProductFromStart:(int)start listProduct:(InputListProduct *)listProduct {
  
  if (listProduct.parent_category_id != 0) {
        
        [self logEventWithCategory: [NSString stringWithFormat:@"%d", listProduct.parent_category_id]  action:@"cateogry id" label:@""];
      
  }

  listProduct.result_start = start;
  [[ServerAPI shared] listProductWithAllBrandTagFromInput:listProduct
                                      completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                                        [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = NO;
                                          
                                        if ([[Common shared] checkNotError:error controller:self response:response]) {
                                            
                                          [self listProductFromProductListData:response];
                                            
                                        }
                                      
                                      }];
}

- (void)listProductFromProductListData:(ProductListData *)productListData {

//    if (isValid(productListData.data.url_key)) {
//        
//        [self logEventWithCategory:productListData.data.url_key  action:@"url key" label:@""];
//        
//    }
    
    dispatch_async(dispatch_get_main_queue( ), ^{
        
        [thisCollectionView.mj_footer endRefreshing];
        
    });
    
  NSArray *statuscodes     = productListData.statusCodes;
  ProductList *productlist = productListData.data;
  [self endCallAPILoading];
    
  if ([(NSNumber *)statuscodes[0] intValue] == STATUS_OK) {
 
      if (self.cleanProducts) {
          
          self.products = [productlist.products mutableCopy];
          self.cleanProducts = NO;
          
      } else {
          
          [self addProductArray:productlist.products];
      
      }
      
      if (!isValid(self.productList)) {
          
           self.productList = productlist;
          
      } else {
          
      }
      
          
    if (result_start != 0) {

      [thisCollectionView reloadData];

    } else {

      [self collectionViewReloadDataWithAnimation];
        
    }

    if (productlist.products.count != PRODUCT_LIST_LIMIT) {

      thisCollectionView.mj_footer = nil;
        
    } else {
        
        thisCollectionView.mj_footer = self.footer;
    }

   result_start = (int)self.products.count;
      
  } else {

    if ([productListData getErrorCode] == NO_DATA) {

        self.products = [[NSMutableArray alloc] init];
        [thisCollectionView reloadData];
        
    }
      
  }

}

- (void)addProductArray:(NSArray *)productArray {
    
    for (Products *product in productArray) {
        
        [product setUpProductType];
        [self.products addObject:product];
        
    }
    
}

- (void)setUpdateProductQtyNotification {

  [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationUpdateProductListPageUI object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {

    [self.collectionView reloadData];

  }];

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

  return products.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

  return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

  ProductCollectionCell *cell = (ProductCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellidentifier forIndexPath:indexPath];

    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
  if (products.count != 0) {
      
      cell.btn_addToCart.tag = indexPath.row;
      [cell setUpProductCellDataFromProduct:(Products *)products[indexPath.row] mainPage:NO];
      
      cell.blueTag.delegate = [AppDelegate getAppDelegate].productCategoryController;
      cell.delegate = self;
  }

  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//
//  [super logEventWithCategory:@"Product Category" action:@"click product from list" label:productCategory.name];
//
  if (products.count > 0) {
      
      self.collectionView.userInteractionEnabled = NO;
      [[Common shared] pushToProductDetailPageFromProduct:products[indexPath.row]];
      self.collectionView.userInteractionEnabled = YES;

  }
    
}

#pragma mark UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {

    int topinset;
    
    if ([self.inputListProduct checkHaveSearchType]) {
        
        topinset = 8;
        
    } else if (self.inputListProduct.type == press_blue_tag ) {
        
        topinset = 8;
        
    } else {
        
        topinset = 0;
    }
    
    return UIEdgeInsetsMake(topinset, 8, 8, 8);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat cellSpacing    = 8;
  CGRect screenRect      = [[UIScreen mainScreen] bounds];
  CGFloat screenWidth    = screenRect.size.width;
  CGFloat numColumns     = (gt568h) ? 3 : 2; // The total number of columns you want
  CGFloat totalCellSpace = cellSpacing * (numColumns + 1);
  CGFloat width          = (screenWidth - totalCellSpace) / numColumns;
  CGFloat height         = 282; // whatever height you want

  return CGSizeMake(width, height);
}

- (void)setUpFiliterBtn {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.presentControllerButton    = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.presentControllerButton setFrame:CGRectMake(screenRect.size.width - 74, screenRect.size.height - 150, 66, 66)];
    UIImage *btn_image = [UIImage imageNamed:@"ic_filter_list_36pt"];
    [self.presentControllerButton setImage:btn_image forState:UIControlStateNormal];
    [self.view addSubview:self.presentControllerButton];
    [self.presentControllerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(66);
        make.height.mas_equalTo(66);
        make.bottom.equalTo(self.presentControllerButton.superview).with.offset(-26);
        make.right.equalTo(self.presentControllerButton.superview).with.offset(-6);
        
    }];
    [self.presentControllerButton setBackgroundColor:[UIColor whiteColor]];
    [self.presentControllerButton addTarget:self action:@selector(onShow) forControlEvents:UIControlEventTouchDown];
    [self.presentControllerButton setUpCircleButton];
    [self.presentControllerButton drawCricleShadowOpacity];
    [self createTransition];
}

#pragma mark - ProductAdanceSearchMainDelegate delegate

- (void)onShow {
    
       (self.productAdanceSearchView) ? [self.productAdanceSearchView removeFromSuperview] : nil;
        CGRect screenRect                             = [[UIScreen mainScreen] bounds];
        self.productAdanceSearchView = [[ProductAdanceSearchView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
        self.productAdanceSearchView.delegatesa = self;
        productAdanceSearchViewController       = [[UIViewController alloc] init];
        productAdanceSearchViewController.view  = self.productAdanceSearchView;
        [self.productAdanceSearchView setContentProductList:self.productList InputListProduct:self.inputListProduct];
        
        // Indicate you use a custom transition
        [self.productAdanceSearchView setBackgroundColor:[[Common shared] getFilterAnimationColor]];
        productAdanceSearchViewController.modalPresentationStyle = UIModalPresentationCustom;
        productAdanceSearchViewController.transitioningDelegate  = self;
        [self setUpProductAdanceSearchViewAction:self.productAdanceSearchView];
    
    [self presentViewController:productAdanceSearchViewController animated:YES completion:^{
        
        [[Common shared] sendGAForpageName:@"Category - Fitlering"];
        
        [self.productAdanceSearchView checkProductCount];
    }];
   
}

- (void)setUpProductAdanceSearchViewAction:(ProductAdanceSearchView *)productAdanceSearchView {
    
    [[self.productAdanceSearchView.closeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
        [productAdanceSearchViewController dismissViewControllerAnimated:YES completion:nil];
        
    }];
}

- (void)setUpNavigationBar {
    
    if ([self.inputListProduct checkHaveSearchType] || self.inputListProduct.type == press_blue_tag) {
        
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar mainMenuBarType];
    [self.ztoreNavigationBar.leftMenuImage setImage:[UIImage imageNamed:@"ic_arrow_back_white"]];
    [self.ztoreNavigationBar.rightMenuImage setImage:[UIImage imageNamed:@"ic_shopping_cart_white_24pt"]];
    self.ztoreNavigationBar.rightMenuImage.hidden = self.ztoreNavigationBar.leftMenuImage.hidden = NO;
        
         self.ztoreNavigationBar.titleLabel.text = (isValid([AppDelegate getAppDelegate].searchSelectedString)) ? [AppDelegate getAppDelegate].searchSelectedString : JMOLocalizedString(@"Menu", nil);
            
    self.ztoreNavigationBar.rightMenuImage2.hidden = self.ztoreNavigationBar.rightBtn2.hidden = self.inputListProduct.type == press_blue_tag;
   [self.ztoreNavigationBar.rightMenuImage2 setImage:[UIImage imageNamed:@"ic_clear_white_3x"]];
   self.ztoreNavigationBar.rightMenuImage2.tintColor = [UIColor whiteColor];
    
   [self.ztoreNavigationBar.rightBtn2 addTarget:self action:@selector(rightBtn2Press) forControlEvents:UIControlEventTouchDown];
        
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(leftBtnPress) forControlEvents:UIControlEventTouchDown];
        
    [self.ztoreNavigationBar.rightBtn addTarget:self action:@selector(rightBtnPress) forControlEvents:UIControlEventTouchDown];
        
    }
    
}

- (void)rightBtn2Press {
 
     [self backBtnPressed:self.ztoreNavigationBar.rightBtn2];
    
}

- (void)leftBtnPress {
    
     [self backBtnPressedNotEqualToSearch:self.ztoreNavigationBar.leftBtn];
}

- (void)rightBtnPress {
    
     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenRightMenu object:nil];
    
}

#pragma mark - ProductAdanceSearchViewDelegate delegate

- (void)applySelectedProductBrands:(NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(NSArray<NSString *> *)productTags selectedSort:(NSString *)sort {
    
    [self loadingFromColor:nil];
    
    [productAdanceSearchViewController dismissViewControllerAnimated:YES completion:^{
       
          [self getListProductWithtBrands:productBrands productSpecialtyTags:productSpecialtyTags productTags:productTags selectedSort:sort];
        
    }];
    
}

@end
