//
//  ProductAdanceSearchTableViewHeader.m
//  Ztore
//
//  Created by ken on 27/7/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductAdanceSearchTableViewHeader.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProductAdanceSearchTableViewHeader

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSString *)accessibilityLabel {
    return self.textLabel.text;
}

- (void)setLoading:(BOOL)loading {
    if (loading != _loading) {
        _loading = loading;
        [self _updateDetailTextLabel];
    }
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {
    if (expansionStyle != _expansionStyle) {
        _expansionStyle = expansionStyle;
        [self _updateDetailTextLabel];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self _updateDetailTextLabel];
        self.backgroundColor = [UIColor yellowColor];
    }
    
    return self;
}

- (void)_updateDetailTextLabel {
    if (self.isLoading) {
        self.detailTextLabel.text = @"Loading data";
    } else {
        switch (self.expansionStyle) {
            case UIExpansionStyleExpanded:
                self.detailTextLabel.text = @"Click to collapse";
                break;
            case UIExpansionStyleCollapsed:
                self.detailTextLabel.text = @"Click to expand";
                break;
        }
    }
}

- (void)setExpandImageHidden:(BOOL)hidden {

  [self.expandLabel setHidden:hidden];
}

- (void)runSpinAnimationOnViewDuration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        [self.imageView_right setTransform:CGAffineTransformRotate(self.imageView_right.transform, M_PI_2 * rotations)];
        
    } completion:nil];
    
}

@end
