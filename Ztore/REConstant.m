//
//  REConstant.m
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REConstant.h"

@implementation REConstant

NSTimeInterval const kTransitionDuration = 0.35;
NSTimeInterval const kAnimationDuration  = 0.25;
#pragma mark searchBar
NSTimeInterval const kSearchBarInputDelayDuration = 0.3;

@end
