
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

@interface LeftMenuHeaderView : UIView
@property(weak, nonatomic) IBOutlet UILabel *loginLabel;
@property(weak, nonatomic) IBOutlet UILabel *myFavouriteLabel;
@property(weak, nonatomic) IBOutlet UILabel *homeLabel;
@property(weak, nonatomic) IBOutlet UIButton *loginBtn;
@property(weak, nonatomic) IBOutlet UIButton *favouriteBtn;
@property(weak, nonatomic) IBOutlet UIButton *homeBtn;
@property(weak, nonatomic) IBOutlet UIImageView *favouriteView;
@property(weak, nonatomic) IBOutlet UIImageView *homeView;
@property(weak, nonatomic) IBOutlet UIImageView *accountView;

- (void)setupView;

@end
