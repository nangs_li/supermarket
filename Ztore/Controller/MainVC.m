//
//  MainVC.m
//  testProject
//
//  Created by artur on 2/14/14.
//  Copyright (c) 2014 artur. All rights reserved.
//

#import "Constants.h"
#import "MainVC.h"
#import "RealUtility.h"
#import "RightMenuVC.h"

@interface MainVC ( )

@end

@implementation MainVC

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  [self.view drawShadowOpacity];
  [self setMenuAction];

  self.slideMenuDelegate = self;
}

- (NSIndexPath *)initialIndexPathForLeftMenu {

  return [NSIndexPath indexPathForRow:1 inSection:0];
}

- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath {

  return @"ProductCategorySegue";
}

- (NSString *)segueIdentifierForIndexPathInRightMenu:(NSIndexPath *)indexPath {
  NSString *identifier = @"";

  switch (indexPath.row) {
  case 0:
    identifier = @"firstSegue";
    break;
  case 1:
    identifier = @"secondSegue";
    break;
  case 2:
    identifier = @"thirdSegue";
    break;
  }

  return identifier;
}

- (CGFloat)leftMenuWidth {

  return [UIScreen mainScreen].bounds.size.width - 60;
}

- (CGFloat)rightMenuWidth {

  return [UIScreen mainScreen].bounds.size.width;
}

- (void)rightMenuWillOpen {

    [[AppDelegate getAppDelegate].rightMenuVC.myTableView reloadData];
    
}

- (void)rightMenuDidOpen {
    
    [AppDelegate getAppDelegate].rightMenuIsOpen = YES;
    
    if (![AppDelegate getAppDelegate].haveAPIProcessing) {
        
        [[AppDelegate getAppDelegate].rightMenuVC getShoppingCart];
        
    }
    
    if (!isValid([Common shared].deliveryAddressData)) {
        
        [[ServerAPI shared] listCurrentUserAddressCompletionBlock:^(id response, NSError *error) {
            
            
        }];
        
    }
    
    [[Common shared] sendGAForpageName:@"Shopping Cart"];
    
}

- (void)rightMenuWillClose {
    
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUpdateProductListPageUI object:nil];
  [[AppDelegate getAppDelegate].productCategoryController reloadAllCollectionView];
  [AppDelegate getAppDelegate].rightMenuIsOpen = NO;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadProductDetailUI object:nil];
    
}

- (void)rightMenuDidClose {
    
    
}

- (AMPrimaryMenu)primaryMenu {

  return AMPrimaryMenuLeft;
}

- (BOOL)deepnessForLeftMenu {

  return NO;
}

- (void)leftMenuWillOpen {

  [AppDelegate getAppDelegate].leftMenuIsOpen = YES;

}

- (void)leftMenuDidOpen {
    
     [[Common shared] sendGAForpageName:@"Leftmenu"];
    
}

- (void)leftMenuDidClose {

  [AppDelegate getAppDelegate].leftMenuIsOpen = NO;
    
  if ([AppDelegate getAppDelegate].didPressLeftMenu) {
        
        [AppDelegate getAppDelegate].didPressLeftMenu = NO;
        [[AppDelegate getAppDelegate].productCategoryController getCategoryAPIFromLevel2Category_ID:[[AppDelegate getAppDelegate].Level2_childrenProduct.related_id integerValue]];
        
  }
    
}

- (void)setMenuAction {

    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationCloseLeftMenu object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self closeLeftMenuAnimated:NO];
        
    }];
  
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationCloseLeftMenuHaveAnimation object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self closeLeftMenuAnimated:YES];
        
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationOpenOrCloseLeftMenu object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {

    ([AppDelegate getAppDelegate].leftMenuIsOpen) ? [self closeLeftMenuAnimated:YES] : [self openLeftMenuAnimated:YES];

    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationCloseRightMenu object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self closeRightMenuAnimated:NO] ;
        
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationOpenRightMenu object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self openRightMenuAnimated:YES] ;
        
    }];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationCloseRightMenuHaveAnimation object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self closeRightMenuAnimated:YES] ;
        
    }];

}

@end
