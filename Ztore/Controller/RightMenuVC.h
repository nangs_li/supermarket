//
//  RightMenuVC.h
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AMSlideMenuRightTableViewController.h"
#import "ServerAPI.h"

#import <MBProgressHUD/MBProgressHUD.h>
#import "DeliveryFeePopUpView.h"
#import "CustomCancelLabel.h"
#import "SlidingOrderTableViewCell.h"

@class SlidingOrderFooterCell;

@interface RightMenuVC : AMSlideMenuRightTableViewController <MBProgressHUDDelegate, CustomCancelLabelDelegate, MyFavouriteCellDelegate> {
  int selectedIndex;
  int headerHeight;
  int footerHeight;
  int footerMaxHeight;
  int rowHeight;
  int commonFontSize;

}

@property(strong, nonatomic) SlidingOrderFooterCell *slidingOrderFooterCell;
@property(assign, nonatomic) BOOL lockUI;
@property(strong, nonatomic) MBProgressHUD *currentHUD;
@property(strong, nonatomic) ZtoreNavigationBar *ztoreNavigationBar;
@property(strong, nonatomic) ShoppingCartPopUpView *shoppingCartPopUpView;
@property(strong, nonatomic) NSTimer *hideLoadingTimer;
@property(assign, nonatomic) BOOL havePressZdollarExpandBtn;
@property(assign, nonatomic) BOOL reloadFooterView;
@property(assign, nonatomic) BOOL hideFooterView;
@property(strong, nonatomic) UITableView *myTableView;
@property(strong, nonatomic) DeliveryFeePopUpView *deliveryFeePopUpView;

- (void)showLoadingHUD;
- (void)hideLoadingHUD;
- (void)pressExpandBtn;
- (void)addProductAPiCallbackFromAddProductReturnShoppingCartModel:(AddProductReturnShoppingCartModel *)cart;
- (void)getShoppingCart;

@end
