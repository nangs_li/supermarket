//
//  LeftMenuVC.m
//  Ztore
//
//  Created by ken on 2/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ExpandingCell.h"
#import "LeftMenuVC.h"
#import "SlidingMenuStaticCell.h"
#import "MainRegisterViewController.h"
#import "FXBlurView.h"
#import "MyAccountViewController.h"
#import "UIScrollView+VGParallaxHeader.h"
#import "MyFavouriteViewController.h"

@interface LeftMenuVC ( )

@property(nonatomic, strong) NSMutableIndexSet *expandableSections;

@end

@implementation LeftMenuVC

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  [AppDelegate getAppDelegate].leftMenuVC = self;
    
  _expandableSections = [NSMutableIndexSet indexSet];
  selectedIndex       = -1;
  productCatHeight    = 56;
  superCatX           = 19;
  subCatX             = 39;
  cellDivide          = 1;
  numberOfSection     = 1;
  headerHeight        = 56;

  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && ![UIApplication sharedApplication].isStatusBarHidden) {
    self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
  }

  [self.tableView setSeparatorColor:[UIColor whiteColor]];
  // init menu data
  headerTitles = [NSArray arrayWithObjects:JMOLocalizedString(@"my_menu", nil), JMOLocalizedString(@"category", nil), nil];
  mymenuImages = [NSArray arrayWithObjects:@"icon-menu", @"icon-menu", @"icon-menu", nil];
  mymenuNames  = [NSArray arrayWithObjects:JMOLocalizedString(@"my_wish_list", nil), JMOLocalizedString(@"my_account", nil), JMOLocalizedString(@"on_sale", nil), nil];

  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && ![UIApplication sharedApplication].isStatusBarHidden) {
  }
  self.navigationController.navigationBar.backgroundColor = [UIColor redColor];

  if (UI_USER_INTERFACE_IDIOM( ) != UIUserInterfaceIdiomPad) {
    // The device is an iPhone or iPod touch.
    [self setFixedStatusBar];
  }
    

  [self.myTableView mas_remakeConstraints:^(MASConstraintMaker *make) {

    make.top.equalTo(self.myTableView.superview).with.offset(0);
    make.left.equalTo(self.myTableView.superview).with.offset(0);
    make.height.mas_equalTo([RealUtility screenBounds].size.height);
    make.width.mas_equalTo([UIScreen mainScreen].bounds.size.width - 60);

  }];
    
    [self.myTableView setBackgroundColor:[UIColor clearColor]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"menu_bg"] blurredImageWithRadius:10.0f iterations:1 tintColor:[UIColor clearColor]]];
    UIView *view = [[UIView alloc] init];
    [imageView addSubview:view];
    [view matchSizeWithSuperViewFromAutoLayout];
    view.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8f];
    [self.myTableView setBackgroundView:imageView];
    [self.view setBackgroundColor:[[Common shared] getCommonRedColor]];
    [self setUpTableViewHeader];
    [self setMenuAction];

}

- (void)setUpTableViewHeader {
 
    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"LeftMenuHeaderView" owner:self options:nil] objectAtIndex:0];
    [self.headerView setupView];
    
    [self.myTableView setParallaxHeaderView:self.headerView mode:VGParallaxHeaderModeFill height:140];
    [self.myTableView shouldPositionParallaxHeader];
    
    [[self.headerView.loginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        
        [Common shared].isLogin ? [self myAccountTapped] : [self loginLabelTapped];
    }];

    [[self.headerView.favouriteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        
        
        if (![Common shared].isLogin) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenu object:nil];
            [[Common shared] notLoginToLoginPage];
            
        } else {
        
        MyFavouriteViewController *myFavouriteViewController = [[MyFavouriteViewController alloc] initWithNibName:@"MyFavouriteViewController" bundle:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenu object:nil];
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:myFavouriteViewController animated:YES enableUI:NO];

        }
       
    }];

    [[self.headerView.homeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        
        [[AppDelegate getAppDelegate].productCategoryController showMainPage:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenuHaveAnimation object:nil];
        
    }];

}

- (void)setUpTableViewFooterFromHeight:(int)height {
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.myTableView.frame.size.width, height)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    self.myTableView.tableFooterView = footerView;
    
}

- (void)getMenu {
    
    [[ServerAPI shared] getCategoryTreeCompletion:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            ProductCategoryModel *productCategoryModel = (ProductCategoryModel *)response;
            
            proCats = productCategoryModel.data;
            
            if ((isValid(proCats))) {
                
                [self setUpTableViewFooterFromHeight:self.myTableView.frame.size.height - 20 - proCats.count * productCatHeight];
                
            }
            
            [self.myTableView reloadData];
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {

  [super didReceiveMemoryWarning];
}

#pragma mark - SetupView

- (void)setFixedStatusBar {

  self.view                 = [[UIView alloc] initWithFrame:self.view.bounds];
  self.view.backgroundColor = self.myTableView.backgroundColor;
  [self.view addSubview:self.myTableView];
}

- (void)loginLabelTapped {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenu object:nil];
    [[Common shared] notLoginToRegisterPage];
}

- (void)myAccountTapped {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenu object:nil];
    [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:[[MyAccountViewController alloc] init] animated:YES enableUI:NO];
}

#pragma mark - SLExpandableTableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  NSArray<Children> *children = proCats[section].children;

  return children.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return proCats.count;
}

#pragma mark - TableViewDatasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  static NSString *cellIdentifier = @"ExpandingCell";

  ExpandingCell *cell = (ExpandingCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {

    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ExpandingCell" owner:self options:nil];
    cell         = [nib objectAtIndex:0];
  }

  ProductCategoryData *productCategoryData = proCats[indexPath.section];
  Children *children                       = (Children *)productCategoryData.children[indexPath.row];
  cell.expandImage.hidden                  = YES;
  [cell setUpSubCellFromProductChildren:children indexPath:indexPath];

  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  return productCatHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

  ProductCategoryData *productCategoryData            = proCats[indexPath.section];
  Children *children                                  = productCategoryData.children[indexPath.row];
  [AppDelegate getAppDelegate].Level2_childrenProduct = children;
  [AppDelegate getAppDelegate].productCategoryController.mainScrollView.hidden = YES;
  [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar.titleLabel.text = productCategoryData.name;
  [AppDelegate getAppDelegate].didPressLeftMenu = YES;
  [[AppDelegate getAppDelegate].productCategoryController showMainPage:NO];
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseLeftMenuHaveAnimation object:nil];
  [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar.titleLabel.text = ([AppDelegate getAppDelegate].Level2_childrenProduct.name == nil) ? JMOLocalizedString(@"Menu", nil) : [AppDelegate getAppDelegate].Level2_childrenProduct.name;
  [[AppDelegate getAppDelegate].productCategoryController loadingFromColor:nil];
    
}

#pragma mark - SLExpandableTableViewDatasource

- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {

  return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
  return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {

  static NSString *cellIdentifier = @"ExpandingCell";

  ExpandingCell *cell = (ExpandingCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {

    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ExpandingCell" owner:self options:nil];
    cell         = [nib objectAtIndex:0];
  }

  if ([self.myTableView isSectionExpanded:section]) {

    [cell setUpSelectedCellFromProductCategoryData:proCats[section] indexPath:[NSIndexPath indexPathForRow:0 inSection:section]];

  } else {

    [cell setUpCellFromProductCategoryData:proCats[section] indexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
  }

  return cell;
}

#pragma mark - SLExpandableTableViewDelegate

- (void)tableView:(SLExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue( ), ^{
    [self.expandableSections addIndex:section];
    [tableView expandSection:section animated:YES];
  });
}

- (void)tableView:(SLExpandableTableView *)tableView didCollapseSection:(NSUInteger)section animated:(BOOL)animated {

  [self.expandableSections removeIndex:section];
}

- (void)tableView:(SLExpandableTableView *)tableView willExpandSection:(NSUInteger)section animated:(BOOL)animated {

  [self.myTableView reloadDataAndResetExpansionStates:YES];
}

- (void)tableView:(SLExpandableTableView *)tableView willCollapseSection:(NSUInteger)section animated:(BOOL)animated {

  ExpandingCell *cell = (ExpandingCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
  [cell setUpCellFromProductCategoryData:proCats[section] indexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
  
}

- (void)setMenuAction {
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationReloadLeftMenu object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self.headerView setupView];
        
    }];
    
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // This must be called in order to work
    [scrollView shouldPositionParallaxHeader];
    
    self.headerView.alpha = scrollView.parallaxHeader.progress;
    
    // This is how you can implement appearing or disappearing of sticky view
    [scrollView.parallaxHeader.stickyView setAlpha:scrollView.parallaxHeader.progress];
}

@end
