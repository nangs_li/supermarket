//
//  RightMenuVC.m
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "NSError+RENetworking.h"
#import "ProductDetailController.h"
#import "RealUtility.h"
#import "RightMenuVC.h"
#import "ShoppingCartModel.h"
#import "SlidingOrderTableViewCell.h"
#import "CustomTextFieldView.h"
#import "RegisterViewController.h"
#import "ShoppingCartPopUpView.h"
#import "PromotionCode.h"
#import "PaymentSuccessViewController.h"
#import "DeliveryAddressViewController.h"
#import "MainRegisterViewController.h"
#import "SlidingOrderFooterCell.h"

@interface RightMenuVC ( )

@end

@implementation RightMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];

    headerHeight             = 44;
    footerHeight             = 120;
    rowHeight                = 120;
    commonFontSize           = 10;
    footerMaxHeight          = 400;
    
    [self setUpTableView];
    [self setUpNavigationBar];
    [self setUpViewForFooterInSection];
    [self setShoppingCartNotDataNotification];
    [AppDelegate getAppDelegate].rightMenuVC = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
     [self backToViewController];
}

- (void)backToViewController {
    
    if ([Common shared].backToViewController != nil) {
        
        if ([Common shared].backToViewController == [self class]) {
            
            [Common shared].backToViewController = nil;
            
        } else {
            
            [self.navigationController popViewControllerAnimated:NO];
        }
        
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.myTableView.userInteractionEnabled = YES;
    
}

- (void)setUpTableView {
    
    self.myTableView          = [[UITableView alloc] init];
    self.view                 = [[UIView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = self.myTableView.backgroundColor;
    [self.view addSubview:self.myTableView];
    self.myTableView.delegate                             = self;
    self.myTableView.dataSource                           = self;
    self.myTableView.allowsMultipleSelectionDuringEditing = NO;
    [self.myTableView matchSizeWithSuperViewNavigationBarFromAutoLayoutTop:64 bottom:-footerHeight];
}

- (void)setUpNavigationBar {
    self.ztoreNavigationBar = [Common getRightMenuBar];
    [self.view addSubview:self.ztoreNavigationBar];
    [self.ztoreNavigationBar matchSizeWithNavigationBarAutoLayout];
    self.ztoreNavigationBar.titleLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Shopping Cart", nil), [NSString stringWithFormat: @"%ld", (long)[Common shared].shoppingCartData.products.count]];
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
}

- (void)setUpViewForFooterInSection {
    
    NSArray *nib                = [[NSBundle mainBundle] loadNibNamed:@"SlidingOrderFooterCell" owner:self options:nil];
    self.slidingOrderFooterCell = [nib objectAtIndex:0];
    self.slidingOrderFooterCell.alpha = 0;
    [self.view addSubview:self.slidingOrderFooterCell];
    [self.slidingOrderFooterCell matchSizeWithSuperViewBottomFromHeight:footerHeight];
    [self.slidingOrderFooterCell.expandButton addTarget:self action:@selector(pressExpandBtn) forControlEvents:UIControlEventTouchDown];
    [self.slidingOrderFooterCell.btn_zdollarTitle addTarget:self action:@selector(pressZdollarExpandBtn) forControlEvents:UIControlEventTouchDown];
    self.slidingOrderFooterCell.useZdollarView.cancelBtnDelegate = self;
    self.slidingOrderFooterCell.deliveryFeeView.cancelBtnDelegate = self;
    self.slidingOrderFooterCell.useZdollarView.titleLabel.userInteractionEnabled = YES;
    [self.slidingOrderFooterCell.btn_checkout addTarget:self action:@selector(pressCheckOutBtn:) forControlEvents:UIControlEventTouchDown];
    self.slidingOrderFooterCell.layer.masksToBounds = NO;
    self.slidingOrderFooterCell.layer.shadowOffset  = CGSizeMake(0, 0);
    self.slidingOrderFooterCell.layer.shadowRadius  = 2;
    self.slidingOrderFooterCell.layer.shadowOpacity = 0.3;
    self.havePressZdollarExpandBtn = NO;
    [self pressExpandBtn];
    self.slidingOrderFooterCell.btn_checkout.userInteractionEnabled = NO;
    
}

- (void)pressExpandBtn {
    
    int currentFooterMaxHeight = (self.havePressZdollarExpandBtn) ? footerMaxHeight : [self getHideZdollarViewHeight];
    
    int newFooterViewHeight = (self.slidingOrderFooterCell.frame.size.height == footerHeight) ? currentFooterMaxHeight : footerHeight;
    
    [self.slidingOrderFooterCell mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(newFooterViewHeight);
    }];
    
    self.hideFooterView = (newFooterViewHeight == footerHeight) ? YES : NO;

    if (self.hideFooterView) {
        
    } else {
        
    }
    
        [self.slidingOrderFooterCell.expandButton setImage:(self.hideFooterView) ? [UIImage imageNamed:@"ic_keyboard_arrow_up_3x"] : [UIImage imageNamed:@"ic_keyboard_arrow_down_3x"]  forState:UIControlStateNormal];
    
    self.slidingOrderFooterCell.label_OrderDetail.alpha = (self.hideFooterView) ? 1 : 0 ;
    self.slidingOrderFooterCell.label_ProductSubtotal.alpha = self.slidingOrderFooterCell.label_ProductPrice.alpha = (self.hideFooterView) ? 0 : 1 ;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         [self.slidingOrderFooterCell.superview layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                     }];

    
}

- (void)pressZdollarExpandBtn {
   
    self.havePressZdollarExpandBtn = !self.havePressZdollarExpandBtn;
    
    int newFooterViewHeight = (self.slidingOrderFooterCell.frame.size.height == footerMaxHeight) ? [self getHideZdollarViewHeight] : footerMaxHeight;
    
    [self.slidingOrderFooterCell.btn_zdollarTitle setImage:(self.havePressZdollarExpandBtn) ?  [self imageWithColor:[UIColor clearColor]] : [UIImage imageNamed:@"ic_keyboard_arrow_down_3x"] forState:UIControlStateNormal];
    [self.slidingOrderFooterCell mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(newFooterViewHeight);
    }];
    
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         [self.slidingOrderFooterCell.superview layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                    
                     }];

    
}

- (int)getHideZdollarViewHeight {
    
    return (footerMaxHeight - self.slidingOrderFooterCell.zdollarViewCount * 33) - 5 ;
    
}

- (void)pressUsePromoBtn:(UIButton *)sender {
    
    if (!self.shoppingCartPopUpView) {
        
        self.shoppingCartPopUpView = [[[NSBundle mainBundle] loadNibNamed:@"ShoppingCartPopUpView" owner:self options:nil] objectAtIndex:0];
        [self.shoppingCartPopUpView setupView];
        [self.view addSubview:self.shoppingCartPopUpView];
        [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
        
        [[self.shoppingCartPopUpView.useBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
            
            
            [self.shoppingCartPopUpView.usePromoCodeTextField checkValid];
            
            if (self.shoppingCartPopUpView.usePromoCodeTextField.textField.text.length > 0) {
                
                self.reloadFooterView = YES;
                [self showLoadingHUD];
                
                [self setPromoCode:self.shoppingCartPopUpView.usePromoCodeTextField.textField.text];
                
            }
            
        }];
        
        [[self.shoppingCartPopUpView.backBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
            
            [TSMessage dismissActiveNotification];
            
            if (self.shoppingCartPopUpView) {

                [self.shoppingCartPopUpView fadeOutCompletion:^(id response, NSError *error) {
                [self.shoppingCartPopUpView removeFromSuperview];
                
                }];
                
            }
            
        }];
        
    } else {
        
        [self.view addSubview:self.shoppingCartPopUpView];
        [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
        [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
        
    }
    
    
}

- (void)pressCancelPromoBtn:(UIButton *)sender {
    
    [self setPromoCode:nil];
    
}

- (void)pressCheckOutBtn:(UIButton *)sender {
    
    if ([[Common shared].shoppingCartData checkIsNotEnoughStock]) {
        
        [[AppDelegate getAppDelegate] showMessageBarFromResponse:JMOLocalizedString(@"Please remove the items first which are not available.", nil)];
        
    } else {
    
    if ([Common shared].isLogin) {
     
        if ([[Common shared].shoppingCartData.delivery_fee intValue] > 0) {
        
            [self showDeliveryFeePopUpView];
            
        } else {
            
            [self pushToDeliveryAddresPage];
         }

    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseRightMenu object:nil];
        [[Common shared] notLoginToLoginPage];
   
    }
        
    }
    
}

- (void)pushToDeliveryAddresPage {
    
    if ([[Common shared].userAccont.data.mobile isEqualToString:@""] || [[Common shared].userAccont.data.first_name isEqualToString:@""] || [[Common shared].userAccont.data.last_name isEqualToString:@""] || [Common shared].userAccont.data.title == 0) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseRightMenu object:nil];
        RegisterViewController *registerViewController = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:registerViewController animated:YES enableUI:NO];
        
    } else {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseRightMenu object:nil];
        DeliveryAddressViewController *deliveryAddressViewController = [[DeliveryAddressViewController alloc] initWithNibName:@"DeliveryAddressViewController" bundle:nil];
        deliveryAddressViewController.editType = EditInCheckOut;
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:deliveryAddressViewController animated:YES enableUI:NO];
        
    }

}

- (void)getShoppingCart {
    
   [self getShoppingCartUse_zdollar:[[[Common shared] valueForKey:@"use_zdollar"] boolValue] use_default:NO];
    
}

- (void)setPromoCode:(NSString *)promoCode {
    
     __weak __typeof__(self) weakSelf = self;
    
    [[ServerAPI shared] setPromotionCode:promoCode completionBlock:^(id response, NSError *error) {
        
        [self hideLoadingHUD];

        if ([response getErrorCode] == PROMO_CODE_INVALID) {
            
            [self.shoppingCartPopUpView.usePromoCodeTextField setErrorStatus];;
            
        } else if ([[Common shared] checkNotError:error controller:weakSelf response:response]) {
            
            [weakSelf getShoppingCartUse_zdollar:[[[Common shared] valueForKey:@"use_zdollar"] boolValue] use_default:NO];
            
        }
        
    }];
}

- (void)setZdollar:(BOOL)use_zdollar {
    
    [[ServerAPI shared] setUseZdollar:use_zdollar completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self getShoppingCartUse_zdollar:use_zdollar use_default:NO];
        }
        
        
    }];
}

- (void)getShoppingCartUse_zdollar:(BOOL)use_zdollar use_default:(BOOL)use_default {
    
    [[ServerAPI shared] cartAddProductFromProductArraysReturnCart:YES onlyGetShoppingCart:YES completionBlock:^(id response, NSError *error) {

        [self hideLoadingHUD];
        self.slidingOrderFooterCell.btn_checkout.userInteractionEnabled = YES;
        self.lockUI = NO;
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self addProductAPiCallbackFromAddProductReturnShoppingCartModel:response];
          
        }
        
    }];
    
}

- (void)addProductAPiCallbackFromAddProductReturnShoppingCartModel:(AddProductReturnShoppingCartModel *)cart {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUpdateProductListPageUI object:nil];
    [[AppDelegate getAppDelegate].productCategoryController reloadAllCollectionView];
    
    if (cart.data.cart.error_code == NO_DATA) {
        
         [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShoppingCartNotData object:nil];
        
    } else if ([cart getErrorCode] == STATUS_OK) {
        
//        cart.data.products = [[Common shared] getFreebieTypeProductFromProduct:cart.data.products];
        
        [Common shared].shoppingCartData = cart.data.cart;

        [self.slidingOrderFooterCell setupDataFromCart:[Common shared].shoppingCartData rightMenuVC:self userAccountData:[Common shared].userAccont.data haveCheckOutBtn:YES] ;
        
            
        footerMaxHeight = self.slidingOrderFooterCell.viewHeight;
        
        if (self.reloadFooterView) {
            
            self.reloadFooterView = NO;
            
            [self.slidingOrderFooterCell mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.height.mas_equalTo(self.slidingOrderFooterCell.viewHeight);
            }];
            
            [self.slidingOrderFooterCell.superview layoutIfNeeded];
            
        }
        
        self.slidingOrderFooterCell.hidden = NO;
        self.ztoreNavigationBar.titleLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Shopping Cart", nil), [NSString stringWithFormat: @"%ld", (long)[Common shared].shoppingCartData.products.count]];
        
        if (self.shoppingCartPopUpView) {
            [self.shoppingCartPopUpView removeFromSuperview];
        }
        
        [self.myTableView reloadData];
        
    }
    
}

- (void)backBtnPressed:(id)target {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseRightMenuHaveAnimation object:nil];
    
}

#pragma mark CustomCancelLabelDelegate

- (void)pressCancelBtnFromLabelType:(LabelType)labelType {
    
    self.reloadFooterView = YES;
    
    if (labelType == UseZDollar) {
        
        [self setZdollar:NO];
        
    }
    
    if (labelType == UsePromoCode || labelType == UseZDollarCode) {
     
        [self setPromoCode:nil];
        
    }
    
}

- (void)pressUseBtnFromLabelType:(LabelType)labelType {
    
    if (labelType == UseZDollar) {
        
       [self setZdollar:YES];
        
    }
    
    if (labelType == UsePromoCode || labelType == UseZDollarCode) {
        
        [self pressUsePromoBtn:nil];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Common shared].shoppingCartData == nil ? 0 : [Common shared].shoppingCartData.products.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Products *product               = (Products *)[Common shared].shoppingCartData.products[indexPath.row];
    static NSString *cellIdentifier = @"SlidingOrderTableViewCell";
    SlidingOrderTableViewCell *cell = (SlidingOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
        
    [cell setUpOrderCellFromProduct:product ProductCellType:ShoppingCartType];
  
    
    cell.upBtn.tag   = indexPath.row;
    cell.downBtn.tag = indexPath.row;
    cell.tag         = indexPath.row;
    cell.delegate    = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return rowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Products *product = [Common shared].shoppingCartData.products[indexPath.row];
    
    if (!product.isFreeProduct) {
        
        tableView.userInteractionEnabled = NO;
        [[Common shared] pushToProductDetailPageFromProduct:product];
        tableView.userInteractionEnabled = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCloseRightMenu object:nil];

    }

}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)showLoadingHUD {
    [self showLoadingHUDWithProgress:nil];
}

- (void)showLoadingHUDWithProgress:(NSString *)progress {
    
    [self hideLoadingHUD];
    self.currentHUD          = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.currentHUD.delegate = self;
    
    if (progress) {
        
        self.currentHUD.label.text = progress;
        
    } else {
        
        self.currentHUD.label.text = JMOLocalizedString(@"Loading", nil);
    }
    
    if (self.hideLoadingTimer) {
        
        [self.hideLoadingTimer invalidate];
    }
    
    self.hideLoadingTimer = [NSTimer scheduledTimerWithTimeInterval:kTimeOutInterval target:self selector:@selector(hideLoadingHUD) userInfo:nil repeats:NO];
    
}

- (void)hideLoadingHUD {
    
    if (self.currentHUD) {
        [self.currentHUD hideAnimated:YES];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
}

- (void)setShoppingCartNotDataNotification {
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationShoppingCartNotData object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        for (id key in [Common shared].productDict) {
            
            Products *products = [[Common shared].productDict objectForKey:key];
            products.qty = products.cart_qty = 0;
            
        }
        
        [Common shared].productStockArray  = [[NSMutableArray alloc] init];
        [Common shared].productChangeArray = [[NSMutableArray alloc] init];
        
        self.ztoreNavigationBar.titleLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Shopping Cart", nil), [NSString stringWithFormat: @"0"]];
        [Common shared].shoppingCartData = nil;
        self.slidingOrderFooterCell.hidden = YES;
        [self.myTableView reloadData];
        
    }];
    
}

- (void)deleteBtnPressedFromCell:(SlidingOrderTableViewCell *)cell product:(Products *)product {
    
    [self showLoadingHUD];
    [cell cleanProductStock];
    [cell.addProductObject btnFinishPressd:self];
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, 24.0f, 24.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext( );
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext( );
    UIGraphicsEndImageContext( );
    
    return image;
}

#pragma mark - Delivery Fee PopUp View

- (void)showDeliveryFeePopUpView {
    
    if (!self.deliveryFeePopUpView) {
        
        self.deliveryFeePopUpView = [[[NSBundle mainBundle] loadNibNamed:@"DeliveryFeePopUpView" owner:self options:nil] objectAtIndex:0];
        [self.deliveryFeePopUpView setupDeliveryFreePopUpViewFromDeliveryFree:[Common shared].shoppingCartData.delivery_fee limitPrice:[Common shared].shoppingCartData.free_shipping_min_order_amount];
        [[AppDelegate getAppDelegate].window addSubview:self.deliveryFeePopUpView];
        [self.deliveryFeePopUpView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        [self.deliveryFeePopUpView matchSizeWithSuperViewFromAutoLayout];
        
    }

    [[self.deliveryFeePopUpView.useBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        if (self.deliveryFeePopUpView) {
            
        [self.deliveryFeePopUpView removeFromSuperview];
        self.deliveryFeePopUpView = nil;
            
        }
        
        [self pushToDeliveryAddresPage];
        
    }];
    
    [[self.deliveryFeePopUpView.backBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        [TSMessage dismissActiveNotification];
        
        if (self.deliveryFeePopUpView) {
            
            [self.deliveryFeePopUpView fadeOutCompletion:^(id response, NSError *error) {
                
                [self.deliveryFeePopUpView removeFromSuperview];
                self.deliveryFeePopUpView = nil;
                
            }];
            
        }
        
    }];

    
}

@end
