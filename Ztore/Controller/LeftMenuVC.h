//
//  LeftMenuVC.h
//  Ztore
//
//  Created by ken on 2/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AMSlideMenuLeftTableViewController.h"
#import "SLExpandableTableView.h"
#import "ServerAPI.h"
#import <UIKit/UIKit.h>
#import "LeftMenuHeaderView.h"

@interface LeftMenuVC : AMSlideMenuLeftTableViewController <SLExpandableTableViewDatasource, SLExpandableTableViewDelegate, UIScrollViewDelegate> {
  int selectedIndex;
  NSArray<ProductCategoryData *> *proCats;
  int productCatHeight;
  int superCatX;
  int subCatX;
  int cellDivide;
  int numberOfSection;
  int headerHeight;
  NSArray *mymenuImages;
  NSArray *mymenuNames;
  NSArray *headerTitles;
}

@property(strong, nonatomic) IBOutlet SLExpandableTableView *myTableView;
@property(strong, nonatomic) ZtoreNavigationBar *navigationBar;
@property(strong, nonatomic) LeftMenuHeaderView *headerView;

- (void)getMenu;

@end
