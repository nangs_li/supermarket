//
//  REBackgroundAPIClient.m
//  real-v2-ios
//
//  Created by Alex Hung on 25/5/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "REAPIQueue.h"
#import "REBackgroundAPIClient.h"
#import "REDatabase.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>

@interface REBackgroundAPIClient ( )

@property(nonatomic, assign) BOOL isLooping;

@end

@implementation REBackgroundAPIClient

- (void)commonSetup {
  [super commonSetup];
  self.isLooping                         = NO;
  __weak REBackgroundAPIClient *weakSelf = self;

  [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {

    if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
      [weakSelf loopPendingQueue];
    } else {
      weakSelf.isLooping = NO;
    }

  }];
}

- (REAPIQueue *_Nonnull)post:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter {
  return [self addAPIQueueWithHttpMethod:REHttpMethodPost URL:url paramter:parameter];
}

- (REAPIQueue *_Nonnull)get:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter {
  return [self addAPIQueueWithHttpMethod:REHttpMethodGet URL:url paramter:parameter];
}

- (REAPIQueue *_Nonnull)post:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter dependencies:(NSArray<REAPIQueue *> *_Nullable)dependencies {
  return [self addAPIQueueWithHttpMethod:REHttpMethodPost URL:url paramter:parameter dependencies:dependencies];
}

- (REAPIQueue *_Nonnull)get:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter dependencies:(NSArray<REAPIQueue *> *_Nullable)dependencies {
  return [self addAPIQueueWithHttpMethod:REHttpMethodGet URL:url paramter:parameter dependencies:dependencies];
}

- (REAPIQueue *_Nonnull)addAPIQueueWithHttpMethod:(REHttpMethod)method URL:(NSString *_Nonnull)url paramter:(NSDictionary *_Nonnull)parameter {
  return [REAPIQueue queueWithUrl:url parameter:parameter httpMethod:method dependencies:nil];
}

- (REAPIQueue *)addAPIQueueWithHttpMethod:(REHttpMethod)method URL:(NSString *)url paramter:(NSDictionary *)parameter dependencies:(NSArray<REAPIQueue *> *)dependencies {
  REAPIQueue *queue = [REAPIQueue queueWithUrl:url parameter:parameter httpMethod:method dependencies:dependencies];
  [[REDatabase sharedInstance] createOrUpdateAPIQueue:queue];
  [self loopPendingQueue];

  return queue;
}

- (void)loopPendingQueue {
  if (!self.isLooping && [REAPIClient networkReachable]) {
    self.isLooping            = YES;
    NSArray *pendingAPIQueues = [[REDatabase sharedInstance] getPendingAPIQueues];

    if (pendingAPIQueues.count > 0) {
      [self sendRequestInPendingQueue:pendingAPIQueues fromIndex:0];
    } else {
      self.isLooping = NO;
    }
  }
}

- (void)sendRequestInPendingQueue:(NSArray *)pendingQueue fromIndex:(NSInteger)index {

  if (pendingQueue.count > index) {
    REAPIQueue *queue = pendingQueue[index];

    if (queue) {
      __weak REBackgroundAPIClient *weakSelf = self;
      NSString *url                          = queue.url;
      NSDictionary *parameter                = queue.parameter;
      NSInteger nextIndex                    = index + 1;

      if (queue.status == REAPIQueueStatusFail) {
        queue.retryCount++;
        [[REDatabase sharedInstance] createOrUpdateAPIQueue:queue];
      }

      REAPICommonBlock completion = ^(REServerResponseModel *response, NSError *error) {
        if (!error) {
          queue.response = response;
          queue.status   = REAPIQueueStatusSuccess;
          [[REDatabase sharedInstance] createOrUpdateAPIQueue:queue];
          [[REDatabase sharedInstance] updatePendingQueuesWithAPIQueue:queue];
        } else {
          queue.status = REAPIQueueStatusFail;
          [[REDatabase sharedInstance] createOrUpdateAPIQueue:queue];
        }

        [weakSelf sendRequestInPendingQueue:pendingQueue fromIndex:nextIndex];
      };

      if (queue.httpMethod == REHttpMethodPost) {
        [self post:url parameter:parameter requestID:queue.queueId completion:completion progress:nil];
      } else {
        [self get:url parameter:parameter requestID:queue.queueId completion:completion progress:nil];
      }
    }

  } else {
    [self reloopPendingQueue];
  }
}

- (void)reloopPendingQueue {
  self.isLooping = NO;
  [self loopPendingQueue];
}

@end
