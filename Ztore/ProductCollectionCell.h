//
//  OnSaleCollectionCell.h
//  Ztore
//
//  Created by ken on 7/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductList.h"
#import <UIKit/UIKit.h>
#import "EHHorizontalViewCell.h"
#import "BlueTag.h"
#import "AddProductObject.h"

@protocol ProductCollectionCellDelegate <NSObject>

- (void)pressSoldOutBtnFromProduct:(Products *)product;

@end

@interface ProductCollectionCell : EHHorizontalViewCell <AddProductObjectDelegate>

@property(weak, nonatomic) IBOutlet UIImageView *imageView_product;
@property(weak, nonatomic) IBOutlet UILabel *label_brand;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductName;
@property(weak, nonatomic) IBOutlet UILabel *label_volume;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductActualPricePrices;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductOrginalPrices;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductPrices;
@property(weak, nonatomic) IBOutlet UIButton *btn_addToCart;
@property(weak, nonatomic) IBOutlet UIView *lineView;
@property(weak, nonatomic) IBOutlet UIView *addProductView;
@property(weak, nonatomic) IBOutlet UILabel *labelQty;
@property(nonatomic) CGPoint clickedLocation;
@property(strong, nonatomic) Products *product;
@property(weak, nonatomic) IBOutlet UIButton *downBtn;
@property(weak, nonatomic) IBOutlet UIButton *upBtn;
@property(strong, nonatomic) IBOutlet UIImageView *increaseBtnImage;
@property(strong, nonatomic) IBOutlet UIImageView *decreaseBtnImage;
@property(strong, nonatomic) AddProductObject *addProductObject;
@property(assign, nonatomic) BOOL isMainPageCell;
@property(weak, nonatomic) BlueTag *blueTag;
@property(weak, nonatomic) IBOutlet UIView *soldOutView;
@property(weak, nonatomic) IBOutlet UIButton *soldOutBtn;
@property(strong, nonatomic) id<ProductCollectionCellDelegate> delegate;

- (void)setUpProductCellDataFromProduct:(Products *)product mainPage:(BOOL)mainPage;

@end
