//
//  ProductCategory.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AutoCompleteProduct.h"

@implementation AutoCompleteProduct

@end

@implementation AutoCompleteProductData

- (id)getObjectFromIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        
        return [self.brands objectAtIndex:indexPath.row];
        
    } else  if (indexPath.section == 1) {
        
        return [self.products objectAtIndex:indexPath.row];
        
    } else  if (indexPath.section == 2) {
        
        return [self.tags objectAtIndex:indexPath.row];
    }
    
    return nil;
    
}

- (NSString *)getNameFromIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        AutoCompleteBrands *brands = [self.brands objectAtIndex:indexPath.row];
        
        return brands.name;
        
    } else  if (indexPath.section == 1) {
        
        Products *product = [self.products objectAtIndex:indexPath.row];
        
        return product.name;

        
    } else  if (indexPath.section == 2) {
        
        return [self.tags objectAtIndex:indexPath.row];

    }
    
    return nil;
    
}

- (NSUInteger)getObjectCountFromSection:(NSInteger)section {
    
    if (section == 0) {
        
        return self.brands.count;
        
    } else  if (section == 1) {
        
       return self.products.count;
        
    } else  if (section == 2) {
        
        return self.tags.count;
    }
    
    return 0;
    
}

@end

@implementation AutoCompleteBrands

@end

@implementation AutoCompleteProducts

@end
