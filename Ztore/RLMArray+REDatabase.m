//
//  RLMArray+REDatabase.m
//  real-v2-ios
//
//  Created by Derek Cheung on 25/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "RLMArray+REDatabase.h"

// Models
#import "REObject.h"

@implementation RLMArray (REDatabase)

+ (RLMArray *)arrayWithNSMutableArray:(NSMutableArray *)array objectClass:(Class)objectClass {

  NSString *className = NSStringFromClass(objectClass);
  RLMArray *output    = [[RLMArray alloc] initWithObjectClassName:className];

  for (REObject *object in array) {

    [output addObject:[object toREDBObject]];
  }

  return output;
}

@end
