//
//  ProductList.m
//  Ztore
//
//  Created by ken on 17/9/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductList.h"

@implementation ProductList

- (void)checkEmptyArray {
    
    self.products_brand = (self.products_brand.count == 0 || self.products_brand == nil) ? [[NSMutableArray<Products_Brand> alloc] init] : self.products_brand;
    self.products_tag = (self.products_tag.count == 0 || self.products_tag == nil) ? [[NSMutableArray alloc] init] : self.products_tag;
    
}

- (NSArray<Products_Specialty_Tag> *)getIsActivedSpecialtyTagArray {

  NSMutableArray<Products_Specialty_Tag> *specialtyTagArray = [[NSMutableArray<Products_Specialty_Tag> alloc] init];

  for (Products_Specialty_Tag *productSpecialtyTag in self.products_specialty_tag) {

    if (productSpecialtyTag.is_active) {

      [specialtyTagArray addObject:productSpecialtyTag];
    }
  }

  return [specialtyTagArray copy];
}

- (ProductList *)getReSetSeletedProductList {

  self.selectedProducts_tag = nil;

  for (Products_Brand *brand in self.products_brand) {

    brand.selected = NO;
  }

  for (Products_Specialty_Tag *specialty in self.products_specialty_tag) {

    specialty.selected = NO;
  }

  return self;
}

@end

@implementation ProductListData

@end

@implementation ProductListArray

@end

@implementation FeatureProductListArray

@end

@implementation Products

+ (JSONKeyMapper *)keyMapper {

  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"desc" : @"description" }];
}

- (void)setUpProductType {

  if (self.cart_qty > self.cart_freebie_qty && self.cart_freebie_qty != 0) {

    self.isCanAddFreeProduct = YES;
    self.isFreeProduct = NO;

  } else if (self.cart_qty == self.cart_freebie_qty && self.cart_freebie_qty != 0) {

    self.isFreeProduct = YES;
    self.isCanAddFreeProduct = NO;
      
  }
}

- (void)setUpHideButton {

  if (self.isFreeProduct) {

  } else {

    if (self.stock_qty > self.cart_qty + self.cart_freebie_qty) {

      self.hideupbtn = NO;

    } else {

      self.hideupbtn = YES;
    }

    if (self.cart_qty == 0) {

      self.hidedownbtn = YES;

    } else {

      self.hidedownbtn = NO;
    }
  }
}

- (NSString *)getProductIdString {

  return [NSString stringWithFormat:@"%ld", (long)self.id];
}

- (BOOL)isNotEnoughStock {
    
    return (self.cart_qty > self.stock_qty || self.stock_qty == 0);
    
}

- (NSAttributedString *)getDesc {
    
    NSString *descString = @"";
    
    if (isValid(self.desc)) {
        
    self.desc = [self.desc stringByReplacingOccurrencesOfString:@"<br>" withString:@"abcdefghijklionmop"];
    NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[self.desc dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                          NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                     documentAttributes:nil
                                                                  error:nil];
    self.desc = [[attr string] stringByReplacingOccurrencesOfString:@"abcdefghijklionmop" withString:@"\n"];
    self.desc = [self.desc stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    descString = self.desc;
        
    }
    
    return [[NSAttributedString alloc] initWithString:descString];
    
}

- (Products *)getGlobalProduct {
    
    return ([[Common shared] getProductFromProductId:self.id]) ? [[Common shared] getProductFromProductId:self.id] : self;
}

@end

@implementation Image

- (Zoom *)changeToZoomClass {

  Zoom *zoom  = [[Zoom alloc] init];
  zoom.mdpi   = self.mdpi;
  zoom.xhdpi  = self.xhdpi;
  zoom.hdpi   = self.hdpi;
  zoom.xxhdpi = self.xxhdpi;

  return zoom;
}

@end

@implementation Price

@end

@implementation Promotions

@end

@implementation Products_Brand

@end

@implementation Products_Specialty_Tag

@end
