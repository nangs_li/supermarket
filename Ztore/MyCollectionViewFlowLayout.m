//
//  ProductCategory.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "MyCollectionViewFlowLayout.h"
#import "ProductCategoryController.h"

@implementation MyCollectionViewFlowLayout

- (void)awakeFromNib {
    
    [super awakeFromNib];

}

- (CGFloat)pageWidth {
    
    if (self.collectionView.tag == 0) {
        
        return MainPage_TopScrollViewCellHeight + MinSpaceBetweenCell;
        
    } else {
        
        return MainPage_ScrollViewCellHeight + MinSpaceBetweenCell;
    }

}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity {
    
    CGFloat rawPageValue = self.collectionView.contentOffset.x / self.pageWidth;
    CGFloat currentPage = (velocity.x > 0.0) ? floor(rawPageValue) : ceil(rawPageValue);
    CGFloat nextPage = (velocity.x > 0.0) ? ceil(rawPageValue) : floor(rawPageValue);
    
    BOOL pannedLessThanAPage = fabs(1 + currentPage - rawPageValue) > 0.5;
    BOOL flicked = fabs(velocity.x) > [self flickVelocity];
    
    if (pannedLessThanAPage && flicked) {
        proposedContentOffset.x = nextPage * self.pageWidth;
    } else {
        proposedContentOffset.x = round(rawPageValue) * self.pageWidth;
    }
    
    return proposedContentOffset;
}

- (CGFloat)flickVelocity {
    
    return 0.3;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewLayoutAttributes *layoutAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    
    if (!layoutAttributes) {
        
        layoutAttributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    }
    
    return layoutAttributes;
}

@end
