//
//  REDBObject.m
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REDBObject.h"

@implementation REDBObject

- (nonnull REObject *)toREObject {

  @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];

  return nil;
}

@end
