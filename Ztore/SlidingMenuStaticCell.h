//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlidingMenuStaticCell : UITableViewCell

@property(strong, nonatomic) IBOutlet UIImageView *imgv_icon;
@property(strong, nonatomic) IBOutlet UILabel *label_name;

@end
