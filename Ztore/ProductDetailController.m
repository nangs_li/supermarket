//
//  ProductDetailController.m
//  Ztore
//
//  Created by ken on 4/8/16.
//  Copyright © ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "FullImageViewController.h"
#import "MainVC.h"
#import "PageViewImageController.h"
#import "ProductDetailController.h"
#import "RightMenuVC.h"
#import "UIScrollView+VGParallaxHeader.h"
#import <QuartzCore/QuartzCore.h>
#import "MainRegisterViewController.h"

@interface ProductDetailController ( ) <UIScrollViewDelegate> {
    
    int productImageHeight;
    int cartViewWidth;
    int addCartImageSize;
    int expandButtonSize;
    float fullWidth;
    int lineSpace;
    int textX;
    int posty;
    float textWidth;
    CGFloat productDescViewMaxHeight;
    
}

@end

@implementation ProductDetailController

int imagePageViewCurIdx = 0;

- (void)viewDidLoad {

  [super viewDidLoad];

  productImageHeight           = 258;
  cartViewWidth    = 150;
  addCartImageSize = 66;
  expandButtonSize = 30;
  [self setUpdateProductQtyNotification];
  [self setUpParallaxHeader];
  [self loadingFromColor:nil];
  [super setPageName:@"Product"];
  [self setAllObject];
    
}

- (void)getProductDetail {
  
  [self loadingFromColor:nil];
    
  [[ServerAPI shared] getProductFromProduct_Id:self.productId
                           completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                             [self endCallAPILoading];

                             if ([[Common shared] checkNotError:error controller:self response:response]) {

                               [self getProductCallbackFromProductDetail:response];
                                 
                             }

                           }];
}

- (void)reloadProductDetail {
    
    if (isValid(self.product)) {
        
        [self setLabelCartQtyText];
        [self hiddenAddReduceCartViewsAnimation:NO];
        
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    
  [super viewWillAppear:animated];
  [self enableSlidePanGestureForRightMenu];
  self.navigationItem.hidesBackButton = YES;
  [self reloadProductDetail];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self cleanAllObject];
   
}

- (void)setUpNavigationBar {

  self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
  [self.ztoreNavigationBar productDetailBarType];
  self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Product Detail", nil);
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressedNotEqualToSearch:) forControlEvents:UIControlEventTouchDown];
  [self.ztoreNavigationBar.rightMenuImage setImage:[UIImage imageNamed:@"ic_shopping_cart_white_24pt"]];
  [self.ztoreNavigationBar.rightBtn addTarget:self action:@selector(rightBtnPress) forControlEvents:UIControlEventTouchDown];

}

- (void)rightBtnPress {
    
      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenRightMenu object:nil];
    
}

- (void)setUpdateProductQtyNotification {
    
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadProductDetail) name:kNotificationReloadProductDetailUI object:nil];
    
}

- (void)addToMyWishList:(UIButton *)sender {

  if (self.product.is_exist_in_wish_list) {

    [[ServerAPI shared] userListDeleteProductFromProduct_Id:self.productId
                                        completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                                          if ([[Common shared] checkNotError:error controller:self response:response]) {

                                            [self addToMyWishListCallbackFromProductAddWishList:response];
                                          }

                                        }];

  } else {

    [[ServerAPI shared] userListAddProductFromProduct_Id:self.productId
                                     completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                                       if ([[Common shared] checkNotError:error controller:self response:response]) {
                                           
                                         [self addToMyWishListCallbackFromProductAddWishList:response];
                                       }

                                     }];
  }
    
}

- (void)addToMyWishListCallbackFromProductAddWishList:(ProductAddWishList *)productAddWishList {

  if ([(NSNumber *)productAddWishList.statusCodes[0] intValue] == STATUS_OK) {

    if (self.product.is_exist_in_wish_list) {

      [super logEventWithCategory:@"Wish List" action:@"Remove from wish list" label:self.product.name value:[NSNumber numberWithInteger:self.product.id]];
      self.product.is_exist_in_wish_list = false;
      [super showToastMessage:JMOLocalizedString(@"This product removed from wish list", nil)];

    } else {

      [super logEventWithCategory:@"Wish List" action:@"Add to wish list" label:self.product.name value:[NSNumber numberWithInteger:self.product.id]];
      self.product.is_exist_in_wish_list = true;
      [super showToastMessage:JMOLocalizedString(@"This product added to wish list", nil)];
    }

      [self setUpBtnLike];
      
      [Common shared].favouriteList = nil;
      
      [[ServerAPI shared] userListListProductCompletionBlock:^(id response, NSError *error) {
          
          [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpTableViewDataFromMyTableView object:nil userInfo:nil];
          
      }];
      
  } }

- (void)setUpBtnLike {
    
     [self.btn_like setImage:[UIImage imageNamed:self.product.is_exist_in_wish_list ? @"ic_favorite_white_24pt" : @"ic_favorite_border_white_24pt"] forState:UIControlStateNormal];
    
}

#pragma AddProductObjectDelegate

- (void)addProductAPIWithAddCount:(int)addCount {

  [[Common shared] addProductQtyFromProduct:self.product addCount:addCount];

  [self setLabelCartQtyText];

  [self hiddenAddReduceCartViewsAnimation:YES];

  [self disableButtonGeaterThanStockQty];

}

- (void)setLabelCartQtyText {

  self.product = [[Common shared] setQtyLabelTextFromProduct:self.product label:self.label_cartQty];
  [self disableButtonGeaterThanStockQty];
    
    if (self.product.stock_qty == 0) {
        
        self.view_soldOut.hidden = NO;
        self.btn_addCartImage.hidden = YES;
        self.view_cart.hidden = YES;
        [self.view_soldOut setUpSoldoutViewFromProduct:self.product];
        
    } else {
        
        self.view_soldOut.hidden = YES;
       // [self hiddenAddReduceCartViewsAnimation:YES];
    }
    
}

- (void)invalidAllTimer {
    
    if (self.addProductObject.increaseProductTimer) {
        
        [self.addProductObject.increaseProductTimer invalidate];
    }
    
    if (self.addProductObject.decreaseProductTimer) {
        
        [self.addProductObject.decreaseProductTimer invalidate];
    }
    
    self.product.pressDownbtn = NO;
    self.product.pressUpbtn   = NO;
    
}

- (void)getProductCallbackFromProductDetail:(ProductDetailModel *)productDetailModel {

  if ([(NSNumber *)productDetailModel.statusCodes[0] intValue] == STATUS_OK) {

    self.productDetailModel = productDetailModel;
    self.product = productDetailModel.data;
    [self.product setUpProductType];

    [self setupView];
      
      if (isValid(productDetailModel.data.sn)) {
          
          [self logEventWithCategory:productDetailModel.data.sn action:@"product sn" label:@""];
          
      }
      
  } else {
      
  }
    
}

- (void)disableButtonGeaterThanStockQty {

  self.upBtn.alpha                     = (self.product.hideupbtn == YES) ? 0.3 : 1;
  self.downBtn.alpha                  = (self.product.hidedownbtn == YES) ? 0.3 : 1;
  self.upBtn.userInteractionEnabled    = (self.product.hideupbtn == YES) ? NO : YES;
  self.downBtn.userInteractionEnabled = (self.product.hidedownbtn == YES) ? NO : YES;
}

- (void)hiddenAddReduceCartViewsAnimation:(BOOL)animation {

  int qty = [(NSString *)self.label_cartQty.text intValue];

  if (qty == 0) {

    [self invalidAllTimer];

    [self.view_cart mas_updateConstraints:^(MASConstraintMaker *make) {

      make.top.equalTo(self.btn_addCartImage).with.offset(0); // with is an optional semantic filler
      make.trailing.equalTo(self.btn_addCartImage).with.offset(0);
      make.height.mas_equalTo(addCartImageSize);
      make.width.mas_equalTo(addCartImageSize);

    }];

    [self.btn_addCartImage setHidden:NO];
    [self.upBtn setHidden:YES];
    [self.label_cartQty setHidden:YES];

  } else {

    [self.view_cart mas_updateConstraints:^(MASConstraintMaker *make) {

      make.top.equalTo(self.btn_addCartImage).with.offset(0); // with is an optional semantic filler
      make.trailing.equalTo(self.btn_addCartImage).with.offset(0);
      make.height.mas_equalTo(addCartImageSize);
      make.width.mas_equalTo(cartViewWidth);

    }];

    [self.btn_addCartImage setHidden:YES];
    [self.upBtn setHidden:NO];
    [self.label_cartQty setHidden:NO];
  }

  [self.view_cart setNeedsUpdateConstraints];

  [self.view_cart updateConstraintsIfNeeded];
  float animationTime = (animation) ? 0.5 : 0;
  [UIView animateWithDuration:animationTime
      animations:^{

        [self.view_cart.superview layoutIfNeeded];
      }
      completion:^(BOOL finished) {
        [self.btn_addCartImage setHidden:qty > 0];
      }];
}

- (void)expendProductDescription:(UIButton *)sender {

  CGRect tempFrame      = self.label_productDesc.frame;
  CGRect tempFrame2     = self.view_tagList.frame;
  CGRect tempFrame3     = self.view_description.frame;
  CGFloat adjustY       = productDescViewMaxHeight - tempFrame.size.height - expandButtonSize - GL_COMMON_MARGIN;
  tempFrame.size.height = productDescViewMaxHeight;
  tempFrame2.origin.y += adjustY;
  tempFrame3.size.height += adjustY;

  [self.label_productDesc setNumberOfLines:0];

  [UIView animateWithDuration:0.6f
                   animations:^{
                     [self.label_productDesc setFrame:tempFrame];
                     [self.view_tagList setFrame:tempFrame2];
                     [self.view_description setFrame:tempFrame3];
                     [self.view_description.layer addSublayer:[[Common shared] getNormalBreakLineWithFrame:CGRectMake(0, self.view_tagList.frame.origin.y, self.view_description.frame.size.width, 0.5f)]];
                     [self.view_expandDesc setHidden:TRUE];

                     // setup scroll view
                     CGSize adjustSize             = self.scrollView_parent.contentSize;
                     CGFloat fullProductDescHeight = productDescViewMaxHeight - 44;

                     // sum of scrollView subView height > contentSize height
                     // then add
                     // can not scroll size
                     if (self.topViewContainer.frame.size.height + GL_COMMON_MARGIN + self.view_description.frame.size.height > [RealUtility screenBounds].size.height - 64) {

                       adjustSize.height += fullProductDescHeight;
                     }

                       
                     [self.scrollView_parent setContentSize:adjustSize];
                   }];
}

- (void)onSelectTagButton:(UIButton *)sender {
    
    [Common shared].tagString = self.product.tags[sender.tag];
    [Common shared].order_by_String = @"brand.sort_order:asc";

    if (isValid([Common shared].tagString)) {
        
        [self endCallAPILoading];
        InputListProduct *inputListProduct                    = [[InputListProduct alloc] init];
        inputListProduct.type = search_tags;
        inputListProduct.tags                    = @[[Common shared].tagString];
        [AppDelegate getAppDelegate].searchSelectedString = [Common shared].tagString;
        [[Common shared] pushToProductListPageFromInputListProduct:inputListProduct controller:self];
        [Common shared].tagString = nil;
        [Common shared].order_by_String = nil;
        
    }

    
}

- (void)openFullImageView:(UIView *)sender {
  NSMutableArray *imageUrls = [[NSMutableArray alloc] initWithCapacity:self.product.images.count];

  for (int i = 0; i < self.product.images.count; i++) {

    ProductImages *imageGroup = self.product.images[i];
    [imageUrls addObject:[FICDPhotoLoader getPhotoUrlInProduct:imageGroup.zoom]];
  }
  FullImageViewController *targetController = [[FullImageViewController alloc] init];
  targetController.imageUrls                = imageUrls;
  [self presentViewController:targetController animated:YES completion:nil];
}

- (void)setupView {

  fullWidth = self.view.frame.size.width;
  lineSpace = 5;
  textX     = GL_COMMON_MARGIN;
  posty     = 10;
  textWidth = fullWidth - 10;

  [self setLabelCartQtyText];
  [self setUpSwipeView];
  [self setUpPageControl];

  if (self.topViewContainer) {

    [self.topViewContainer removeFromSuperview];
  }

  if (self.view_cart) {

    [self.view_cart removeFromSuperview];
  }

  if (self.view_description) {

    [self.view_description removeFromSuperview];
  }

  // init UIView for store the image swipe view, labels
  self.topViewContainer = [[UIView alloc] init];

  // set brand label
  self.label_brand = [[UILabel alloc] initWithFrame:CGRectMake(textX, posty, textWidth, 22)];
  [self.label_brand setUpTitleLabelFromText:self.product.brand];
  [self.label_brand sizeToFit];
  posty += self.label_brand.frame.size.height;
  [self.topViewContainer addSubview:self.label_brand];

  // set product label
  self.label_productName = [[UILabel alloc] initWithFrame:CGRectMake(textX, posty, textWidth, 22)];
  [self.label_productName setText:self.product.name];
  //[label_brand setFont:[[Common shared] getIta2licFont]];
  [self.label_productName setNumberOfLines:2];
  [self.label_productName setFont:[[Common shared] getBiggerNormalFont]];
  [self.label_productName sizeToFit];
  posty += self.label_productName.frame.size.height + lineSpace;
  [self.topViewContainer addSubview:self.label_productName];

  // set country label
  self.label_productCountry = [[UILabel alloc] initWithFrame:CGRectMake(textX, posty, textWidth, 22)];
  [self.label_productCountry setText:[NSString stringWithFormat:@"%@: %@", JMOLocalizedString(@"country", nil), self.product.country]];
  [self.label_productCountry setFont:[[Common shared] getContentNormalFont]];
  [self.label_productCountry setTextColor:[[Common shared] getCommonGrayColor]];
  [self.label_productCountry sizeToFit];
  posty += self.label_productCountry.frame.size.height + lineSpace;
  [self.topViewContainer addSubview:self.label_productCountry];

  // set volume label
  self.label_volume = [[UILabel alloc] initWithFrame:CGRectMake(textX, posty, 30, 22)];
  [self.label_volume setText:[self.product.volume isEqualToString:@""] ? @"nil" : self.product.volume];
  [self.label_volume setFont:[[Common shared] getContentNormalFont]];
  [self.label_volume setTextColor:[[Common shared] getCommonGrayColor]];
  [self.label_volume sizeToFit];
  posty += self.label_volume.frame.size.height + lineSpace;
  [self.topViewContainer addSubview:self.label_volume];

  // set actual price label
  self.label_actualPrice           = [[UILabel alloc] init];
  self.label_actualPrice.font      = [[Common shared] getBiggerBoldTitleFont];
  self.label_actualPrice.textColor = [[Common shared] getCommonRedColor];
  self.label_actualPrice.text      = [[Common shared] getStringFromPriceString:self.product.price.promotion_price];
  [self.label_actualPrice setNumberOfLines:1];
  [self.label_actualPrice setTextAlignment:NSTextAlignmentLeft];
  [self.label_actualPrice sizeToFit];
  [self.label_actualPrice setFrame:CGRectMake(textX, posty, self.label_actualPrice.frame.size.width, self.label_actualPrice.frame.size.height)];
  [self.topViewContainer addSubview:self.label_actualPrice];

  // set orginal price label
  self.label_orginalPrice      = [[UILabel alloc] init];
  self.label_orginalPrice.font = [[Common shared] getContentNormalFont];
  [self.label_orginalPrice setTextColor:[[Common shared] getCommonGrayColor]];
  [[Common shared] setStrikethroughForLabel:self.label_orginalPrice withText:[[Common shared] getStringFromPriceString:self.product.price.standard_price]];
  [self.label_orginalPrice setNumberOfLines:1];
  [self.label_orginalPrice setTextAlignment:NSTextAlignmentLeft];
  [self.label_orginalPrice sizeToFit];
  [self.label_orginalPrice setFrame:CGRectMake(self.label_actualPrice.frame.size.width + textX * 2, posty, self.label_orginalPrice.frame.size.width, self.label_actualPrice.frame.size.height)];
  [self.topViewContainer addSubview:self.label_orginalPrice];

  if (self.product.price.promotion_price == self.product.price.standard_price) {
    [self.label_orginalPrice setHidden:TRUE];
    [self.label_actualPrice setTextColor:[UIColor blackColor]];
  }

  posty += self.label_actualPrice.frame.size.height + lineSpace * 2;

  // add break line
  [self.topViewContainer.layer addSublayer:[[Common shared] getNormalBreakLineWithFrame:CGRectMake(0, posty + 6, self.view.frame.size.width, 0.5f)]];

  // set like button
  self.btn_like = [[UIButton alloc] initWithFrame:CGRectMake(textX, posty + 14, 200, 44)];
  [self.btn_like setTitle:JMOLocalizedString(@"addToWishList", nil) forState:UIControlStateNormal animation:NO];
  [self.btn_like setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
  [self setUpBtnLike];
  [self.btn_like setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
  [self.btn_like.titleLabel setFont:[[Common shared] getNormalFont]];
  self.btn_like.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    

    if ([Common shared].isLogin) {
        
         [self.btn_like addTarget:self action:@selector(addToMyWishList:) forControlEvents:UIControlEventTouchDown];
        
    } else {
        
        [self.btn_like addTarget:self action:@selector(moveToMainRegisterViewController:) forControlEvents:UIControlEventTouchDown];
        
    }
    
  [self.topViewContainer addSubview:self.btn_like];
  [self.topViewContainer setBackgroundColor:[[Common shared] getCommonGrayBackgroundColor]];

  posty += self.btn_like.frame.size.height;

  // add topview container to scroll view
  [self.topViewContainer setFrame:CGRectMake(0, 0, fullWidth, posty + 20 - 1)];
  [self.scrollView_parent addSubview:self.topViewContainer];
  self.scrollView_parent.delegate = self;
  // set Add to cart image button
  self.btn_addCartImage = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - textX - addCartImageSize, [self.topViewContainer getViewYAddHeight] - addCartImageSize / 2 , addCartImageSize, addCartImageSize)];
  [self.btn_addCartImage setBackgroundColor:[[Common shared] getCommonRedColor]];
  [self.btn_addCartImage setImage:[UIImage imageNamed:@"ic_add_shopping_cart_white"] forState:UIControlStateNormal];
  self.btn_addCartImage.tag                = 1;
  self.btn_addCartImage.layer.cornerRadius = addCartImageSize / 2;
  [self.btn_addCartImage drawTopShadow];
  self.btn_addCartImage.hidden = YES;

  // set Add to cart button/Reduce from cart button
  self.view_cart = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - textX - addCartImageSize, self.btn_addCartImage.frame.origin.y, addCartImageSize, addCartImageSize)];

  self.view_cart.layer.cornerRadius  = addCartImageSize / 2;
  self.view_cart.layer.masksToBounds = YES;
  [self.view_cart drawCricleShadowOpacity];
  [self.view_cart setBackgroundColor:[[Common shared] getCommonRedColor]];
  self.downBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, cartViewWidth / 3, addCartImageSize)];
  [self.downBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
  [self.downBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  self.downBtn.tag             = -1;
  self.downBtn.titleLabel.font = [[Common shared] getNormalFontWithSize:30];
  [self.downBtn setImage:[UIImage imageNamed:@"ic_remove_white_3x"] forState:UIControlStateNormal];
  [self.view_cart addSubview:self.downBtn];

  self.label_cartQty = [[UILabel alloc] initWithFrame:CGRectMake(self.downBtn.frame.size.width + 1, 0, cartViewWidth / 3, addCartImageSize)];
  [self.label_cartQty setTextAlignment:NSTextAlignmentCenter];
  [self.label_cartQty setTextColor:[UIColor whiteColor]];
  self.label_cartQty.font = [[Common shared] getBiggerNormalFont];
  [self.view_cart addSubview:self.label_cartQty];

  self.upBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.downBtn.frame.size.width * 2 + 1, 0, cartViewWidth / 3, addCartImageSize)];
  [self.upBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
  [self.upBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [self.upBtn setImage:[UIImage imageNamed:@"ic_add_white_3x"] forState:UIControlStateNormal];
  self.upBtn.tag             = 1;
  self.upBtn.titleLabel.font = [[Common shared] getNormalFontWithSize:30];
  [self.upBtn drawCricleShadowOpacity];
  [self.view_cart addSubview:self.upBtn];

  // set Product description and product tag
  self.view_description = [self bottomDescriptionView];
    
  [self.view_description setFrame:CGRectMake(textX, GL_COMMON_MARGIN, self.view_description.frame.size.width, self.view_description.frame.size.height)];
  UIView *greyBackGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, [self.topViewContainer getViewYAddHeight] + 1, [RealUtility screenBounds].size.width, 20000)];
  [greyBackGroundView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
  [greyBackGroundView addSubview:self.view_description];
  [self.scrollView_parent addSubview:greyBackGroundView];

    if (!isValid(self.product.tags) && !isValid(self.product.desc)) {
        
        self.view_description.hidden = YES;
        
    } else {
        
        self.view_description.hidden = NO;
        
    }
    
  posty += GL_COMMON_MARGIN;
  posty += self.view_description.frame.size.height;

  // add add/reduce cart widget for overlap
  [self.scrollView_parent addSubview:self.btn_addCartImage];
  [self.scrollView_parent addSubview:self.view_cart];

  [self.scrollView_parent bringSubviewToFront:self.btn_addCartImage];
  [self.view_cart mas_makeConstraints:^(MASConstraintMaker *make) {

    make.top.equalTo(self.btn_addCartImage.mas_top).with.offset(0); // with is an optional semantic filler
    make.right.equalTo(self.btn_addCartImage.mas_right).with.offset(0);
    make.height.mas_equalTo(addCartImageSize);

  }];

    self.view_soldOut = [[[NSBundle mainBundle] loadNibNamed:@"BlueTag" owner:self options:nil] objectAtIndex:0];
    [self.view_soldOut setUpSoldoutViewFromProduct:self.product];
    [self.scrollView_parent addSubview:self.view_soldOut];
    self.view_soldOut.delegate = self;
    
    [self.view_soldOut mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.btn_addCartImage.mas_top).with.offset(0); // with is an optional semantic filler
        make.right.equalTo(self.btn_addCartImage.mas_right).with.offset(0);
        make.bottom.equalTo(self.btn_addCartImage.mas_bottom).with.offset(0);
        
    }];

  // setup scroll view  contentSizeHeight = sum of contentSize view
  CGFloat contentSizeHeight = self.topViewContainer.frame.size.height + GL_COMMON_MARGIN + self.view_description.frame.size.height;

  // if [RealUtility screenBounds].size.height - 64 > contentSize height then
  // add can not scroll size
  if (contentSizeHeight < [RealUtility screenBounds].size.height - 64) {

    contentSizeHeight = contentSizeHeight + ([RealUtility screenBounds].size.height - 64 - contentSizeHeight);
  }

  CGSize adjustSize = CGSizeMake([self.scrollView_parent frame].size.width, contentSizeHeight);
  [self.scrollView_parent setContentSize:adjustSize];
  [self.scrollView_parent setBackgroundColor:[UIColor whiteColor]];

  // setup touch event for showing full image view
  [[Common shared] addClickEventForView:self.view_images withTarget:self action:@"openFullImageView:"];
  [self.scrollView_parent.parallaxHeader setBackgroundColor:[UIColor whiteColor]];
  [self setUpAllBtn];
    
    
  [self.blueTag setUpFromPromotions:self.product.promotions.firstObject];
    
  [self setLabelCartQtyText];
  [self hiddenAddReduceCartViewsAnimation:NO];
  [self disableButtonGeaterThanStockQty];
    
  // draw shadow
  [self.topViewContainer drawTopShadow];
  [self.view_description drawTopShadow];
  [self.scrollView_parent shouldPositionParallaxHeader];
    
}

- (void)moveToMainRegisterViewController:(UIButton *)sender {
    
    [[Common shared] notLoginToLoginPage];
    
}

- (void)setUpAllBtn {
    
    self.addProductObject = [[AddProductObject alloc] init];
    self.addProductObject.delegate = self;
    self.addProductObject.upBtn = self.upBtn;
    self.addProductObject.downBtn = self.downBtn;
    self.addProductObject.btn_addToCart = self.btn_addCartImage;
    [self.addProductObject setUpAllAction];

}

- (void)setUpSwipeView {

  // setup Images Swipe view

  //[topViewContainer addSubview:self.view_images];
  posty += 0 + lineSpace;

  if ([self.pageViewController_images view]) {

    [[self.pageViewController_images view] removeFromSuperview];
  }

  self.pageViewController_images            = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
  self.pageViewController_images.dataSource = self;
  self.pageViewController_images.delegate   = self;
  [[self.pageViewController_images view] setFrame:[self.view_images bounds]];

  PageViewImageController *initImageViewController = [self imageViewControllerAtIndex:0];

  NSArray *viewControllers = [NSArray arrayWithObject:initImageViewController];
  [self.pageViewController_images setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
  [self addChildViewController:self.pageViewController_images];
  [self.view_images addSubview:[self.pageViewController_images view]];
  [self.pageViewController_images didMoveToParentViewController:self];
  [self.pageViewController_images.view matchSizeWithSuperViewFromAutoLayout];
 
    if (self.product.stock_qty > 0) {
     
        if (self.product.promotions.count > 0) {
            
            self.blueTagArray = [[NSMutableArray alloc] init];
            
            BlueTag *perviousSwipeView;
            
            for (Promotions *promotion in self.product.promotions) {
                
                
                BlueTag *blueTag = [[[NSBundle mainBundle] loadNibNamed:@"BlueTag" owner:self options:nil] objectAtIndex:0];
              
                [self.blueTagArray addObject:blueTag];
                blueTag.hidden = NO;
                [self.scrollView_parent addSubview:blueTag];
                [blueTag mas_makeConstraints:^(MASConstraintMaker *make) {
                    
                    if (perviousSwipeView) {
                        
                        make.top.equalTo(perviousSwipeView.mas_bottom).with.offset(6);
                        
                    }
                    
                    make.left.equalTo(self.scrollView_parent.mas_left).with.offset(6);
                    
                }];
                
                [blueTag setUpFromPromotions:promotion];
                
                perviousSwipeView = blueTag;
                blueTag.delegate = self;
            }
            
            [perviousSwipeView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.equalTo(self.scrollView_parent.mas_bottom).with.offset(-9);
                
            }];
            
        }
        
    } else {
        
        self.blueTagArray = [[NSMutableArray alloc] init];
        
        BlueTag *perviousSwipeView;
        
            BlueTag *blueTag = [[[NSBundle mainBundle] loadNibNamed:@"BlueTag" owner:self options:nil] objectAtIndex:0];
            [self.blueTagArray addObject:blueTag];
            blueTag.hidden = NO;
            [self.scrollView_parent addSubview:blueTag];
            [blueTag mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.left.equalTo(self.scrollView_parent.mas_left).with.offset(6);
                
            }];
            
            [blueTag setUpNotStockCase];
            
            perviousSwipeView = blueTag;
            blueTag.delegate = self;
        
        [perviousSwipeView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.scrollView_parent.mas_bottom).with.offset(-9);
            
        }];
        
        
    }
     
    [self.view setNeedsLayout];
    
}

- (void)setUpPageControl {

  NSArray *subviews = self.pageViewController_images.view.subviews;

  for (int i = 0; i < [subviews count]; i++) {

    if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {

      self.pageControl_images = (UIPageControl *)[subviews objectAtIndex:i];
    }
  }

  self.pageControl_images.numberOfPages                 = self.product.images.count;
  self.pageControl_images.currentPage                   = 0;
  self.pageControl_images.pageIndicatorTintColor        = [UIColor lightGrayColor];
  self.pageControl_images.currentPageIndicatorTintColor = [[Common shared] getCommonRedColor];
  [self.pageControl_images setBackgroundColor:[UIColor clearColor]];
  [self.pageControl_images setNeedsLayout];
    
}

- (void)setUpParallaxHeader {

  //  y : 30    image height : 200    space : 6     control height : 37
  [self.scrollView_parent setParallaxHeaderView:self.view_images mode:VGParallaxHeaderModeFill height:200 + 37 + 6 + 30];
  [self.scrollView_parent shouldPositionParallaxHeader];
  [self.scrollView_parent.parallaxHeader setFrame:CGRectMake(self.scrollView_parent.parallaxHeader.frame.origin.x, self.scrollView_parent.parallaxHeader.frame.origin.y, self.scrollView_parent.parallaxHeader.frame.size.width, self.scrollView_parent.parallaxHeader.frame.size.height)];
  [self.scrollView_parent.parallaxHeader setBackgroundColor:[UIColor clearColor]];
    
  
}

- (UIView *)bottomDescriptionView {

  CGFloat postX     = GL_COMMON_MARGIN;
  CGFloat postY     = GL_COMMON_MARGIN;
  CGFloat viewWidth = self.view.frame.size.width - postX * 4;

  UIView *view_description = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - postX * 2, GL_COMMON_HEIGHT)];
  [view_description setBackgroundColor:[[Common shared] getCommonGrayBackgroundColor]];

  // set Description title Label
  UILabel *label_title = [[UILabel alloc] initWithFrame:CGRectMake(postX, postY, 100, 22)];
  [label_title setText:JMOLocalizedString(@"productDescription", nil)];
  [label_title setFont:[[Common shared] getNormalFont]];
  [label_title sizeToFit];
  [view_description addSubview:label_title];

  postY += label_title.frame.size.height + GL_COMMON_MARGIN;

  // set Product description label
  self.label_productDesc = [[UILabel alloc] initWithFrame:CGRectMake(postX, postY, viewWidth, 44)];

  [self.label_productDesc setAttributedText:[self.product getDesc]];
  [self.label_productDesc setTextColor:[[Common shared] getCommonGrayColor]];
  [self.label_productDesc setFont:[[Common shared] getContentNormalFont]];
  [self.label_productDesc setNumberOfLines:0];
  [self.label_productDesc sizeToFit];
  productDescViewMaxHeight = self.label_productDesc.frame.size.height;
  [self.label_productDesc setNumberOfLines:2];
  [self.label_productDesc sizeToFit];
  [view_description addSubview:self.label_productDesc];

  postY += self.label_productDesc.frame.size.height + GL_COMMON_MARGIN;

  if (productDescViewMaxHeight >= 44) {

    // set Product description expand button
    self.view_expandDesc = [[UIView alloc] initWithFrame:CGRectMake(postX, postY, viewWidth, expandButtonSize)];

    [self.view_expandDesc.layer addSublayer:[[Common shared] getNormalBreakLineWithFrame:CGRectMake(0, self.view_expandDesc.frame.size.height / 2, self.view_expandDesc.frame.size.width, 0.3f)]];
    self.btn_productDesc = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, expandButtonSize, expandButtonSize)];
    [self.btn_productDesc setImage:[UIImage imageNamed:@"ic_arrow_drop_down_circle_3x"] forState:UIControlStateNormal];
    [self.btn_productDesc.titleLabel setFont:[[Common shared] getContentNormalFont]];
    self.btn_productDesc.center = CGPointMake(CGRectGetMidX(self.view_expandDesc.bounds), self.btn_productDesc.center.y);
    [self.btn_productDesc addTarget:self action:@selector(expendProductDescription:) forControlEvents:UIControlEventTouchDown];
    [self.view_expandDesc addSubview:self.btn_productDesc];
    [view_description addSubview:self.view_expandDesc];
    postY += GL_COMMON_MARGIN + self.view_expandDesc.frame.size.height;
  }

    
    if (isValid(self.product.tags)) {
        
        self.view_tagList = [[UIView alloc] initWithFrame:CGRectMake(postX, postY, viewWidth, GL_COMMON_HEIGHT)];
        self.view_tagList = [[Common shared] tagView:self.view_tagList setTagsList:self.product.tags setSelectedTagsList:nil addTarget:self action:@selector(onSelectTagButton:)];
        [view_description addSubview:self.view_tagList];
        postY += self.view_tagList.frame.size.height + GL_COMMON_MARGIN;

        
    } 

  [view_description setFrame:CGRectMake(view_description.frame.origin.x, view_description.frame.origin.y, view_description.frame.size.width, postY)];

  return view_description;
}

- (void)setAllObject {
    
    self.parentController                                = self;
    [AppDelegate getAppDelegate].productDetailController = self;
    [self.addProductObject.increaseProductTimer invalidate];
    [self.addProductObject.decreaseProductTimer invalidate];
    [self setUpdateProductQtyNotification];
    
}

- (void)cleanAllObject {
    
    self.parentController                                = nil;
    [AppDelegate getAppDelegate].productDetailController = nil;
    [self.addProductObject.increaseProductTimer invalidate];
    [self.addProductObject.decreaseProductTimer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)callAPI {
    
    if ([Common shared].autoCompleteProducts) {
        
        [[ServerAPI shared] getProductByUrlKey:[Common shared].autoCompleteProducts.url_key completionBlock:^(id response, NSError *error) {
            
            [Common shared].autoCompleteProducts = nil;
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                ProductDetailModel *productdetailmodel = response;
                self.productId = (int)productdetailmodel.data.id;
                [self getProductDetail];

            }
            
        }];
        
        
    } else {
        
          [self getProductDetail];
          [self reloadProductDetail];
            
    }

}

- (PageViewImageController *)imageViewControllerAtIndex:(int)index {

  PageViewImageController *childViewController = [[PageViewImageController alloc] init];
  childViewController.index                    = index;
  childViewController.pageHeight               = productImageHeight;
  ProductImages *imageGroup                    = self.product.images[index];
  childViewController.imageUrl                 = [FICDPhotoLoader getPhotoUrlInProduct:imageGroup.zoom];
  [childViewController.view setBackgroundColor:[UIColor whiteColor]];

  return childViewController;
}

- (void)didReceiveMemoryWarning {

  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - PageViewControllerDelegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
  int index = [(PageViewImageController *)viewController index];

  if (index == 0) {

    return nil;
  }
  index--;

  return [self imageViewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
  int index = [(PageViewImageController *)viewController index];
  index++;

  if (index == self.product.images.count) {
    return nil;
  }

  return [self imageViewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {

  for (int i = 0; i < pendingViewControllers.count; i++) {
    imagePageViewCurIdx = [(PageViewImageController *)[pendingViewControllers objectAtIndex:i] index];

  }
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {

  if (completed) {

    [self.pageControl_images setCurrentPage:imagePageViewCurIdx];
  }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {

  return self.product.images.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {

  return 0;
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  
  // This must be called in order to work
  [scrollView shouldPositionParallaxHeader];
    
  self.view_images.alpha = scrollView.parallaxHeader.progress;

  // This is how you can implement appearing or disappearing of sticky view
  [scrollView.parallaxHeader.stickyView setAlpha:scrollView.parallaxHeader.progress];
}

@end
