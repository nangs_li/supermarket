//
//  RealApiClient.m
//  productionreal2
//
//  Created by Ken on 18/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

// Framework
#import <AFNetworking/AFNetworking.h>

// Category
#import "NSError+RENetworking.h"

@implementation REAPIClient

#pragma mark - init method

+ (instancetype)sharedClient {

  static NSMutableDictionary *_sharedClients = nil;

  if (_sharedClients == nil) {
    _sharedClients = [NSMutableDictionary dictionary];
  }

  REAPIClient *sharedClient = nil;

  @synchronized(self) {
    NSString *instanceClass = NSStringFromClass(self);

    // Looking for existing instance
    sharedClient = [_sharedClients objectForKey:instanceClass];

    // If there's no instance – create one and add it to the dictionary
    if (sharedClient == nil) {
      sharedClient = [[self alloc] init];
      [_sharedClients setObject:sharedClient forKey:instanceClass];
      [sharedClient commonSetup];
    }
  }

  return sharedClient;
}

- (void)commonSetup {
    
  self.manager                    = [AFHTTPSessionManager manager];
  self.manager.requestSerializer  = [AFJSONRequestSerializer serializer];
  self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
  [self setShortTimeOutInterval];
  self.manager.responseSerializer.acceptableContentTypes = [self.manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
}

- (void)setLongTimeOutInterval {
    
    self.orignalTimeOutInterval = (double)kTimeOutInterval * 4;
    [self.manager.requestSerializer setTimeoutInterval:kTimeOutInterval * 4];
    
}

- (void)setShortTimeOutInterval {
    
    self.orignalTimeOutInterval = (double)kTimeOutInterval;
    [self.manager.requestSerializer setTimeoutInterval:kTimeOutInterval];
    
}

#pragma mark - common tool

+ (BOOL)networkReachable {
  return [AFNetworkReachabilityManager sharedManager].reachable;
}

#pragma mark - make request

- (NSURLSessionDataTask *)requestWithHttpMethod:(REHttpMethod)method url:(NSString *)url parameter:(NSDictionary *)parameter requestID:(NSString *)requestID completion:(REAPICommonBlock)completionBlock progress:(REAPIProgressBlock)progressBlock {

  return [self requestWithHttpMethod:method url:url parameter:parameter requestID:requestID retryTimes:0 completion:completionBlock progress:(REAPIProgressBlock)progressBlock];
}

- (NSURLSessionDataTask *)requestWithHttpMethod:(REHttpMethod)method url:(NSString *)url parameter:(NSDictionary *)parameter requestID:(nullable NSString *)requestID retryTimes:(NSInteger)retryTimes completion:(REAPICommonBlock)completionBlock progress:(REAPIProgressBlock)progressBlock {

  // generate requestID for every request
  NSString *myRequestID = [[NSUUID UUID] UUIDString];

  if (requestID && [requestID valid]) {

    myRequestID = requestID;
  }

  NSMutableDictionary *myParameter = [NSMutableDictionary dictionaryWithDictionary:parameter];
  [myParameter setObject:myRequestID forKey:@"UniqueKey"];

  [self logRequestStartedWithHttpMethod:method url:url parameter:myParameter requestID:myRequestID];

  // check network failure
  if (![REAPIClient networkReachable]) {

    NSError *outputError = [NSError networkUnreachableError];
    [self logRequestEndedWithURL:url response:nil error:outputError requestID:myRequestID];

    if (completionBlock) {
      completionBlock(nil, outputError);
    }

    return nil;
  }

  // http success request
  __weak typeof(self) weakSelf = self;
  void (^success)(NSURLSessionDataTask *, id) = ^(NSURLSessionDataTask *task, id responseObject) {

    if (completionBlock) {

      completionBlock(responseObject, nil);
    }
  };

  // http failure request
  void (^failure)(NSURLSessionDataTask *, NSError *) = ^(NSURLSessionDataTask *task, NSError *error) {

    [weakSelf logRequestEndedWithURL:[task.originalRequest.URL absoluteString] response:nil error:error requestID:myRequestID];

    if (retryTimes >= 1 && error.code != kStatusCodeInternalServerError) {
      [weakSelf requestWithHttpMethod:method url:url parameter:myParameter requestID:myRequestID retryTimes:retryTimes - 1 completion:completionBlock progress:progressBlock];

    } else {

      if (completionBlock) {
        completionBlock(nil, error);
      }
    }
  };

  NSURLSessionDataTask *task = nil;

  switch (method) {
  case REHttpMethodPost:
    task = [self.manager POST:url parameters:myParameter progress:nil success:success failure:failure];
    break;
  case REHttpMethodGet:
    task = [self.manager GET:url parameters:myParameter progress:nil success:success failure:failure];
    break;
  default:
    task = [self.manager POST:url parameters:myParameter progress:nil success:success failure:failure];
    break;
  }

  return task;
}

- (nullable NSURLSessionDataTask *)post:(nonnull NSString *)url parameter:(nonnull NSDictionary *)parameter requestID:(nullable NSString *)requestID completion:(nullable REAPICommonBlock)completionBlock progress:(nullable REAPIProgressBlock)progressBlock {

  return [self requestWithHttpMethod:REHttpMethodPost url:url parameter:parameter requestID:requestID completion:completionBlock progress:(REAPIProgressBlock)progressBlock];
}

- (nullable NSURLSessionDataTask *)post:(nonnull NSString *)url parameter:(nonnull NSDictionary *)parameter requestID:(nullable NSString *)requestID retryTimes:(NSInteger)retryTimes completion:(nullable REAPICommonBlock)completionBlock progress:(nullable REAPIProgressBlock)progressBlock {

  return [self requestWithHttpMethod:REHttpMethodPost url:url parameter:parameter requestID:requestID retryTimes:retryTimes completion:completionBlock progress:progressBlock];
}

- (nullable NSURLSessionDataTask *)get:(nonnull NSString *)url parameter:(nonnull NSDictionary *)parameter requestID:(nullable NSString *)requestID completion:(nullable REAPICommonBlock)completionBlock progress:(nullable REAPIProgressBlock)progressBlock {

  return [self requestWithHttpMethod:REHttpMethodGet url:url parameter:parameter requestID:requestID completion:completionBlock progress:progressBlock];
}

- (nullable NSURLSessionDataTask *)get:(nonnull NSString *)url parameter:(nonnull NSDictionary *)parameter requestID:(nullable NSString *)requestID retryTimes:(NSInteger)retryTimes completion:(nullable REAPICommonBlock)completionBlock progress:(nullable REAPIProgressBlock)progressBlock {

  return [self requestWithHttpMethod:REHttpMethodGet url:url parameter:parameter requestID:requestID retryTimes:retryTimes completion:completionBlock progress:progressBlock];
}

#pragma mark - Loggings

- (void)logRequestStartedWithHttpMethod:(REHttpMethod)method url:(NSString *)url parameter:(NSDictionary *)parameter requestID:(NSString *)requestID {

  NSMutableString *logString = [[NSMutableString alloc] init];

  NSString *methodString;

  switch (method) {
  case REHttpMethodGet:
    methodString = @"GET";
    break;
  case REHttpMethodPost:
    methodString = @"POST";
    break;
  default:
    methodString = @"POST";
    break;
  }

  [logString appendString:@"---Request started---\n"];
  [logString appendFormat:@"Request ID: %@\n", requestID];
  [logString appendFormat:@"URL: %@\n", url];
  [logString appendFormat:@"RequestMethod: %@\n", methodString];
  [logString appendFormat:@"Parameters: %@\n", parameter];
  [logString appendString:@"---Request started---"];

  // DDLogAPI(@"%@", logString);
}

- (void)logRequestEndedWithURL:(NSString *)url response:(REServerResponseModel *)response error:(NSError *)error requestID:(NSString *)requestID {

  NSMutableString *logString = [[NSMutableString alloc] init];

  [logString appendString:@"---Request ended---\n"];
  [logString appendFormat:@"Request ID: %@\n", requestID];
  [logString appendFormat:@"Status: %@\n", error ? @"Fail" : @"Success"];
  [logString appendFormat:@"URL: %@\n", url];

  if (response) {

    [logString appendFormat:@"Response: %@\n", [response toDictionary]];
  }

  if (error) {

    [logString appendFormat:@"ErrorCode: %d\n", (int)error.code];
    [logString appendFormat:@"ErrorMessage: %@\n", error.localizedDescription];
  }
  [logString appendString:@"---Request ended---"];

  //  DDLogAPI(@"%@", logString);
}

@end
