//
//  User.h
//  Ztore
//
//  Created by ken on 24/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAccountData : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, assign) NSInteger rank_id;

@property(nonatomic, copy) NSString *zdollar;

@property(nonatomic, copy) NSString *contact_no;

@property(nonatomic, assign) BOOL is_referred;

@property(nonatomic, copy) NSString *mobile;

@property(nonatomic, assign) NSInteger bonus_point;

@property(nonatomic, copy) NSString *company;

@property(nonatomic, assign) NSInteger title;

@property(nonatomic, assign) BOOL is_friend;

@property(nonatomic, copy) NSString *rank_name;

@property(nonatomic, copy) NSString *first_name;

@property(nonatomic, copy) NSString *birthday;

@property(nonatomic, assign) NSInteger reg_date;

@property(nonatomic, copy) NSString *referral_user_id;

@property(nonatomic, copy) NSString *last_name;

@property(nonatomic, assign) NSInteger sn;

@property(nonatomic, copy) NSString *email;

@property(nonatomic, copy) NSString *referral_code;

@property(nonatomic, copy) NSString *fb_user_id;

@end

@interface UserAccount : REObject

@property(nonatomic, strong) UserAccountData *data;

@end
