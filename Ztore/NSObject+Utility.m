//
//  NSObject+Utility.m
//  productionreal2
//
//  Created by Ken on 18/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import "NSObject+Utility.h"

@implementation NSObject (Utility)

- (BOOL)valid {
  BOOL valid = YES;

  if ([self isKindOfClass:[NSNull class]] || self == nil) {
    valid = NO;
  }

  if ([self isKindOfClass:[NSString class]]) {

    NSString *string = (NSString *)self;
    string           = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    valid            = string && string.length > 0;

  } else if ([self isKindOfClass:[NSDictionary class]]) {

    NSDictionary *dict = (NSDictionary *)self;
    valid              = dict && dict.allKeys.count > 0;

  } else if ([self isKindOfClass:[NSArray class]]) {

    NSArray *array = (NSArray *)self;
    valid          = array && array.count;
  }

  return valid;
}

@end
