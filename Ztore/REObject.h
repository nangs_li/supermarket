//
//  REObject.h
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class REDBObject;

@interface REObject : JSONModel

@property(nonatomic, strong) NSArray *statusCodes;
@property(nonatomic, strong) NSDictionary *jsonDict;

- (nonnull REDBObject *)toREDBObject; // Abstract method. Must be implemented in
                                      // subclasses if you need to save it into
                                      // Database
- (NSInteger)getErrorCode;

@end
