//
//  REObject.m
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REObject.h"

@implementation REObject

- (nonnull REDBObject *)toREDBObject {

  return nil;
}

- (NSInteger )getErrorCode {
    
    NSArray *statusCodes     = self.statusCodes;
    
    if (statusCodes.firstObject) {
        
        if ([(NSNumber *)statusCodes[0] intValue] == STATUS_ERROR) {
            
            if (statusCodes.count > 1) {
                
             return [(NSNumber *)statusCodes[1] integerValue];
                
            }
        }
    }
    
    return STATUS_OK;
}

#pragma mark - Override

+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  return YES;
}

@end
