//
//  PageViewImageController.m
//  Ztore
//
//  Created by ken on 15/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "PageViewImageController.h"

@interface PageViewImageController ( )

@end

@implementation PageViewImageController

const int imageY        = 30;
const int imageHeight   = 200;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

  if (self) {
  }

  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  NSLog(@"index is %@", [NSString stringWithFormat:@"Screen #%d", self.index]);

  if (self.isFullScreen) {
    [self showFullImageView];
  } else {
    float xPosition = 0.0;
    xPosition       = ([UIScreen mainScreen].bounds.size.width - imageHeight) / 2;

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPosition, imageY, imageHeight, imageHeight)];
    imageView.contentMode  = UIViewContentModeScaleAspectFill;

    [FICDPhotoLoader loaderImageForImageView:imageView withPhotoUrl:self.imageUrl];
    // imageView.image = image;
    [self.view addSubview:imageView];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  // 4
  CGRect scrollViewFrame           = self.scrollView.frame;
  CGFloat scaleWidth               = scrollViewFrame.size.width / self.scrollView.contentSize.width;
  CGFloat scaleHeight              = scrollViewFrame.size.height / self.scrollView.contentSize.height;
  CGFloat minScale                 = MIN(scaleWidth, scaleHeight);
  self.scrollView.minimumZoomScale = minScale;

  // 5
  self.scrollView.maximumZoomScale = 5.0f;
  self.scrollView.zoomScale        = minScale;

  // 6
  [self centerScrollViewContents];
}

- (void)showFullImageView {
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  [self.scrollView setCenter:self.view.center];
  self.scrollView.delegate    = self;
  self.scrollView.contentSize = self.view.frame.size;

  float xPosition            = 0.0;
  xPosition                  = ([UIScreen mainScreen].bounds.size.width - imageHeight) / 2;
  CGSize size                = self.view.frame.size;
  self.imageView             = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, size.width - 60, size.width - 60)];
  self.imageView.contentMode = UIViewContentModeScaleAspectFit;
  [self.imageView setCenter:self.view.center];
  [FICDPhotoLoader loaderImageForImageView:self.imageView withPhotoUrl:self.imageUrl];
  // imageView.image = image;
  [self.scrollView addSubview:self.imageView];
  [self.view addSubview:self.scrollView];

  UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
  doubleTapRecognizer.numberOfTapsRequired    = 2;
  doubleTapRecognizer.numberOfTouchesRequired = 1;
  [self.scrollView addGestureRecognizer:doubleTapRecognizer];

  UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
  twoFingerTapRecognizer.numberOfTapsRequired    = 1;
  twoFingerTapRecognizer.numberOfTouchesRequired = 2;
  [self.scrollView addGestureRecognizer:twoFingerTapRecognizer];
    
}

- (UIImage *)scaleImage:(UIImage *)image toResolution:(int)resolution {
  CGFloat width  = image.size.width;
  CGFloat height = image.size.height;
  CGRect bounds  = CGRectMake(0, 0, width, height);

  // if already at the minimum resolution, return the orginal image, otherwise
  // scale
  if (width <= resolution && height <= resolution) {
    return image;

  } else {
    CGFloat ratio = width / height;

    if (ratio > 1) {
      bounds.size.width  = resolution;
      bounds.size.height = bounds.size.width / ratio;
    } else {
      bounds.size.height = resolution;
      bounds.size.width  = bounds.size.height * ratio;
    }
  }

  UIGraphicsBeginImageContext(bounds.size);
  [image drawInRect:CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height)];
  UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext( );
  UIGraphicsEndImageContext( );

  return imageCopy;
}

- (void)centerScrollViewContents {
  CGSize boundsSize    = self.scrollView.bounds.size;
  CGRect contentsFrame = self.imageView.frame;

  if (contentsFrame.size.width < boundsSize.width) {
    contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
  } else {
    contentsFrame.origin.x = 0.0f;
  }

  if (contentsFrame.size.height < boundsSize.height) {
    contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
  } else {
    contentsFrame.origin.y = 0.0f;
  }

  self.imageView.frame = contentsFrame;
}

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer *)recognizer {
  // 1
  CGPoint pointInView = [recognizer locationInView:self.imageView];

  // 2
  CGFloat newZoomScale = self.scrollView.zoomScale * 1.5f;
  newZoomScale         = MIN(newZoomScale, self.scrollView.maximumZoomScale);

  // 3
  CGSize scrollViewSize = self.scrollView.bounds.size;

  CGFloat w = scrollViewSize.width / newZoomScale;
  CGFloat h = scrollViewSize.height / newZoomScale;
  CGFloat x = pointInView.x - (w / 2.0f);
  CGFloat y = pointInView.y - (h / 2.0f);

  CGRect rectToZoomTo = CGRectMake(x, y, w, h);

  // 4
  [self.scrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer *)recognizer {
  // Zoom out slightly, capping at the minimum zoom scale specified by the
  // scroll view
  CGFloat newZoomScale = self.scrollView.zoomScale / 1.5f;
  newZoomScale         = MAX(newZoomScale, self.scrollView.minimumZoomScale);
  [self.scrollView setZoomScale:newZoomScale animated:YES];
}

#pragma mark - ScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
  // Return the view that you want to zoom
  return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
  // The scroll view has zoomed, so you need to re-center the contents
  [self centerScrollViewContents];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
