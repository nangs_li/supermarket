//
//  ProductAdanceSearchView.h
//  Ztore
//
//  Created by ken on 25/7/16.
//  Copyright © ken. All rights reserved.
//

#import "Common.h"
#import "DLRadioButton.h"
#import "SLExpandableTableView.h"
#import <UIKit/UIKit.h>
#import "productlist.h"

@protocol ProductAdanceSearchViewDelegate
- (void)applySelectedProductBrands:(NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(NSArray<NSString *> *)productTags selectedSort:(NSString *)sort;

@end

@interface ProductAdanceSearchView : UIView <SLExpandableTableViewDatasource, SLExpandableTableViewDelegate, UIScrollViewDelegate>
@property(weak, nonatomic) IBOutlet UILabel *label_title;
@property(strong, nonatomic) UIView *view;
@property(weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property(weak, nonatomic) IBOutlet UIButton *resetBtn;
@property(weak, nonatomic) IBOutlet UIButton *submitBtn;
@property(weak, nonatomic) IBOutlet UITableView *tableView_sorting;
@property(strong, nonatomic) IBOutlet SLExpandableTableView *tableView_content;
@property(assign, nonatomic) id<ProductAdanceSearchViewDelegate> delegatesa;
@property(weak, nonatomic) IBOutlet DLRadioButton *radioBtnGroup;
@property(strong, nonatomic) NSMutableArray *radioBtnGroupArray;
@property(weak, nonatomic) IBOutlet UIButton *closeBtn;
@property(nonatomic, strong) NSMutableIndexSet *expandableSections;
@property(nonatomic, strong) ProductList *productList;
@property(nonatomic, strong) InputListProduct *inputListProduct;
@property(strong, nonatomic) NSTimer *checkProductCountTimer;
@property(strong, nonatomic) UIActivityIndicatorView *btnLoading;

- (void)setContentProductList:(ProductList *)productList InputListProduct:(InputListProduct *)inputListProduct;
- (void)checkProductCount;

@end
