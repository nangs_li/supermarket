//
//  ProductAdanceSearchView.m
//  Ztore
//
//  Created by ken on 25/7/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductAdanceSearchSortingTableViewCell.h"
#import "ProductAdanceSearchTableViewCell.h"
#import "ProductAdanceSearchTableViewHeader.h"
#import "ProductAdanceSearchView.h"
#import "ServerAPI.h"

@implementation ProductAdanceSearchView {

  NSArray *contentTitles;
  NSArray *contentData;
  NSArray *sortingTypes;
  NSArray *sortingTypeValues;
  UIView *view_tagList;
  NSString *selectedSort;
  NSMutableArray *isOpenOptionListCollection; // o=brand list, 1=specialty list
    
}

static int COMMON_CELL_HEIGHT = 50;

- (id)initWithFrame:(CGRect)frame {

  self = [super initWithFrame:frame];

  if (self) {

    [self initObjectNLayout];
  }

  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];

  if (self) {
    [self initObjectNLayout];
  }

  return self;
}

- (void)initObjectNLayout {

  NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProductAdanceSearchView" owner:self options:nil];
  // self.bounds = self.view.bounds;
  self.view = [nib objectAtIndex:0];
  [self addSubview:self.view];
  self.view.translatesAutoresizingMaskIntoConstraints = NO;

  [self.view matchSizeWithSuperViewFromAutoLayout];
  [self layoutIfNeeded];
  [self.tableView_content setBackgroundColor:[UIColor clearColor]];
  [self.tableView_sorting setBackgroundColor:[UIColor clearColor]];

  // add button to top bar as tap menu
  NSDictionary *attributes = [NSDictionary dictionaryWithObject:[[Common shared] getNormalFont] forKey:NSFontAttributeName];
  [self.segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
  [self.segmentedControl addTarget:self action:@selector(segmentedControlAction:) forControlEvents:UIControlEventValueChanged]; 

    self.label_title.text = JMOLocalizedString(@"Filtering", nil);
    [self.segmentedControl setTitle:JMOLocalizedString(@"Filtering", nil) forSegmentAtIndex:0];
    [self.segmentedControl setTitle:JMOLocalizedString(@"Sorting", nil) forSegmentAtIndex:1];
  contentTitles                     = [NSArray arrayWithObjects:JMOLocalizedString(@"brand_name", nil), JMOLocalizedString(@"specialty", nil), @"", nil];
  self.tableView_content.dataSource = self;
  self.tableView_content.delegate   = self;

  view_tagList = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView_content.frame.size.width, COMMON_CELL_HEIGHT)];

  // init static variables for sorting table
  sortingTypes                      = [NSArray arrayWithObjects:JMOLocalizedString(@"sort_price_asc", nil), JMOLocalizedString(@"sort_price_desc", nil), JMOLocalizedString(@"sort_brand_asc", nil), JMOLocalizedString(@"sort_brand_desc", nil), nil];
  sortingTypeValues                 = [NSArray arrayWithObjects:@"price:asc", @"price:desc", @"brand:asc", @"brand:desc", nil];
      DDLogDebug(@"[contentTitles count] %@", JMOLocalizedString(@"sort_price_asc", nil));
 
    self.tableView_sorting.dataSource = self;
  self.tableView_sorting.delegate   = self;
  [self.resetBtn setTitle:JMOLocalizedString(@"Reset", nil) forState:UIControlStateNormal animation:NO];
  [self.resetBtn setUpSmallCornerBorderButton];
  self.resetBtn.backgroundColor = [UIColor clearColor];
  [self.resetBtn addTarget:self action:@selector(resetBtnPressed) forControlEvents:UIControlEventTouchDown];
  [self.submitBtn setTitle:JMOLocalizedString(@"Submit", nil) forState:UIControlStateNormal animation:NO] ;
  self.resetBtn.titleLabel.font = [[Common shared] getContentNormalFont];
 
  [self.submitBtn setUpSmallCornerBorderButton];
  self.submitBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];

    self.segmentedControl.tintColor = [[Common shared] getCommonRedColor];
    
}

- (IBAction)confirmToSearch:(id)sender {

  if (self.productList) {

  [self.delegatesa applySelectedProductBrands:contentData[0] productSpecialtyTags:contentData[1] productTags:[self getProductTagsStringArray] selectedSort:[self getSelectedSort]];
  
  }
    
}

- (NSString *)getSelectedSort {
    
    DLRadioButton *selectedRadioButton = [self.radioBtnGroup selectedButton];
    selectedSort                       = sortingTypeValues[selectedRadioButton.tag];
    
    return selectedSort;
}

- (NSMutableArray *)getProductTagsStringArray {
    
    NSMutableArray *productTags = [[NSMutableArray alloc] init];
    
    for (UIView *subviews in view_tagList.subviews) {
        
        for (UIButton *button in subviews.subviews) {
            
            if ([button.accessibilityHint isEqualToString:@"Selected"]) {
                
                [productTags addObject:contentData[2][button.tag]];
            }
        }
    }

    return productTags;
    
}

- (void)segmentedControlAction:(UISegmentedControl *)sender {

  NSInteger selectedSegment     = sender.selectedSegmentIndex;
  self.tableView_content.hidden = (selectedSegment == 0) ? NO : YES;
  self.tableView_sorting.hidden = (selectedSegment == 1) ? NO : YES;
}

- (void)onSelectTagButton:(UIButton *)button {
    
  button.accessibilityHint = (button.accessibilityHint == nil ) ? @"Selected" : nil ;
    
  if ([button.accessibilityHint isEqualToString:@"Selected"]) {
      
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[[Common shared] getTagColor]];
      
  } else {
      
    [button setTitleColor:[[Common shared] getTagColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
      
  }
    
    [self callCheckProductCountTimer];
}

- (void)resetBtnPressed {

  for (DLRadioButton *radioButton in self.radioBtnGroup.otherButtons) {

    [radioButton setSelected:NO];
  }

  [self setContentProductList:[self.productList getReSetSeletedProductList] InputListProduct:self.inputListProduct];
    
    ProductAdanceSearchTableViewHeader *cell = (ProductAdanceSearchTableViewHeader *)[self.tableView_content cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    CGAffineTransform transform1 = CGAffineTransformMakeTranslation(0, 0);
    [cell.imageView_right setTransform:transform1];
    ProductAdanceSearchTableViewHeader *cell2 = (ProductAdanceSearchTableViewHeader *)[self.tableView_content cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [cell2.imageView_right setTransform:transform1];
    
    [self callCheckProductCountTimer];
    
}

- (void)checkAnimationFromIndexPath:(NSIndexPath *)indexPath selected:(BOOL)selected animation:(BOOL)animation {

  ProductAdanceSearchTableViewCell *cell = (ProductAdanceSearchTableViewCell *)[self.tableView_content cellForRowAtIndexPath:indexPath];
  [cell checkAnimationSelected:selected animated:animation];
    
    [self callCheckProductCountTimer];
    
}

- (void)callCheckProductCountTimer {
    
        if (self.checkProductCountTimer) {
            
            [self.checkProductCountTimer invalidate];
        }
        
        self.checkProductCountTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(checkProductCount) userInfo:nil repeats:NO];
        [self setSubmitBtnUserEnable:NO];
}

- (void)checkProductCount {
    
    if (self.productList) {
        
        InputListProduct *inputListProduct                    = [[Common shared] getInputListProductWithtBrands:contentData[0] productSpecialtyTags:contentData[1] productTags:[self getProductTagsStringArray] selectedSort:[self getSelectedSort] inputListProduct:self.inputListProduct];
        [[ServerAPI shared] getProductCountFromInput:inputListProduct
                                     completionBlock:^(id _Nullable response, NSError *_Nullable error) {
                                         
                                         
                                         if ([[Common shared] checkNotError:error controller:[AppDelegate getAppDelegate].topVisbleViewController response:response]) {
                                             
                                             CardID *cardID = response;
                                             NSString *submitBtnString = [NSString stringWithFormat:JMOLocalizedString(@"Submit (%@)", nil), cardID.data];
                                             [self.submitBtn setTitle:submitBtnString forState:UIControlStateNormal animation:NO];
                                             [self setSubmitBtnUserEnable:![cardID.data isEqualToString:@"0"]];
                                             [self.btnLoading stopAnimating];
                                             
                                         }
                                         
                                     }];
    }

}

- (void)setSubmitBtnUserEnable:(BOOL)userEnable {
    
    if (!userEnable) {
        
        self.submitBtn.userInteractionEnabled = NO;
        self.submitBtn.backgroundColor = [UIColor lightGrayColor];
        self.submitBtn.layer.borderColor   = [UIColor clearColor].CGColor;
        
        if (!isValid(self.btnLoading)) {
        
        self.btnLoading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGFloat halfButtonHeight = self.submitBtn.bounds.size.height / 2;
        CGFloat buttonWidth = self.submitBtn.bounds.size.width;
        self.btnLoading.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
        [self.submitBtn addSubview:self.btnLoading];
            
        }
        
        [self.btnLoading startAnimating];
        
    } else {
        
        [self.submitBtn setUpSmallCornerBorderButton];
        self.submitBtn.userInteractionEnabled = YES;
        [self.btnLoading stopAnimating];
        
    }
}

- (void)onSelectFilterSection:(UITapGestureRecognizer *)sender {

  UIView *view                        = sender.view;
  NSInteger section                   = view.tag;
  isOpenOptionListCollection[section] = [(NSNumber *)isOpenOptionListCollection[section] boolValue] ? [NSNumber numberWithBool:FALSE] : [NSNumber numberWithBool:TRUE];

  if ([(NSNumber *)isOpenOptionListCollection[section] boolValue]) {
      
    NSInteger anotherSection                   = section == 0 ? 1 : 0;
    isOpenOptionListCollection[anotherSection] = [NSNumber numberWithBool:FALSE];
      
  }
    
  [self.tableView_content reloadData];
}

- (void)setContentProductList:(ProductList *)productList InputListProduct:(InputListProduct *)inputListProduct {
    
  self.productList = productList;
  self.inputListProduct = inputListProduct;
  [self.productList checkEmptyArray];
  contentData = [NSArray arrayWithObjects:productList.products_brand, [productList getIsActivedSpecialtyTagArray], productList.products_tag, nil];
  isOpenOptionListCollection = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:FALSE], [NSNumber numberWithBool:FALSE], nil];
  selectedSort               = nil;
  [self segmentedControlAction:self.segmentedControl];
  view_tagList = [[Common shared] tagView:view_tagList setTagsList:self.productList.products_tag setSelectedTagsList:self.productList.selectedProducts_tag addTarget:self action:@selector(onSelectTagButton:)];
 
  [self.tableView_content reloadData];
  [self.tableView_sorting reloadData];
    
}

#pragma mark - SLExpandableTableViewDatasource

- (BOOL)tableView:(SLExpandableTableView *)tableView canExpandSection:(NSInteger)section {

  return YES;
}

- (BOOL)tableView:(SLExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {

  return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {

  static NSString *cellIdentifier = @"ProductAdanceSearchTableViewHeader";

  ProductAdanceSearchTableViewHeader *cell = (ProductAdanceSearchTableViewHeader *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
    cell         = [nib objectAtIndex:0];
  }

  cell.label_title.text      = contentTitles[section];
  cell.label_title.font      = [[Common shared] getNormalFont];
  cell.label_title.textColor = [[Common shared] getCommonRedColor];
  cell.tag                   = section;

  if (section == 2) {

    cell = [[ProductAdanceSearchTableViewHeader alloc] initWithFrame:view_tagList.frame];
    [cell addSubview:view_tagList];
    [cell setBackgroundColor:[UIColor whiteColor]];
  }

  cell.selectionStyle = UITableViewCellSelectionStyleNone;

  return cell;
}

#pragma mark - SLExpandableTableViewDelegate

- (void)tableView:(SLExpandableTableView *)tableView downloadDataForExpandableSection:(NSInteger)section {

  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue( ), ^{
    [self.expandableSections addIndex:section];
    [tableView expandSection:section animated:YES];
  });
}

- (void)tableView:(SLExpandableTableView *)tableView didCollapseSection:(NSUInteger)section animated:(BOOL)animated {

  [self.expandableSections removeIndex:section];
}

- (void)tableView:(SLExpandableTableView *)tableView willExpandSection:(NSUInteger)section animated:(BOOL)animated {

    ProductAdanceSearchTableViewHeader *cell = (ProductAdanceSearchTableViewHeader *)[self.tableView_content cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    [cell runSpinAnimationOnViewDuration:0.5 rotations:-1 repeat:0];
    
}

- (void)tableView:(SLExpandableTableView *)tableView willCollapseSection:(NSUInteger)section animated:(BOOL)animated {
    
    ProductAdanceSearchTableViewHeader *cell = (ProductAdanceSearchTableViewHeader *)[self.tableView_content cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    [cell runSpinAnimationOnViewDuration:0.5 rotations:1 repeat:0];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  if (tableView == self.tableView_content) {

    if (indexPath.section != 2) {

      static NSString *cellIdentifier = @"ProductAdanceSearchTableViewCell";

      ProductAdanceSearchTableViewCell *cell = (ProductAdanceSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

      if (cell == nil) {

        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        [cell setUpTick];
      }

      if (indexPath.section == 0) {

        Products_Brand *brand = contentData[indexPath.section][indexPath.row - 1];
        [cell.label_option setText:brand.name];
        [cell checkAnimationSelected:brand.selected animated:NO];

      } else if (indexPath.section == 1) {

        Products_Specialty_Tag *specialty = contentData[indexPath.section][indexPath.row - 1];
        [cell.label_option setText:specialty.name];
        [cell checkAnimationSelected:specialty.selected animated:NO];
          
      }

      cell.label_option.font = [[Common shared] getNormalFont];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;

      return cell;

    } else {

      UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:view_tagList.frame];
      cell.selectionStyle   = UITableViewCellSelectionStyleNone;

      return cell;
    }

  } else {

    static NSString *cellIdentifier               = @"ProductAdanceSearchSortingTableViewCell";
    ProductAdanceSearchSortingTableViewCell *cell = (ProductAdanceSearchSortingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
      cell         = [nib objectAtIndex:0];
    }

    [cell.label_left setText:sortingTypes[indexPath.row]];
    cell.label_left.font = [[Common shared] getNormalFont];

    if (indexPath.row == 0) {

      self.radioBtnGroupArray = [[NSMutableArray alloc] init];
    }

    cell.radioBtn.tag = indexPath.row;
    [self.radioBtnGroupArray addObject:cell.radioBtn];
    self.radioBtnGroup.otherButtons = [self.radioBtnGroupArray copy];
    cell.selectionStyle             = UITableViewCellSelectionStyleNone;
    [cell drawShadowOpacity];
    cell.radioBtn.selected = (indexPath.row == 0 && self.radioBtnGroup.selectedButton == nil) ? YES : cell.radioBtn.selected;

    return cell;
  }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  if (tableView == self.tableView_content) {

    DDLogDebug(@"[contentTitles count] %lu", (unsigned long)[contentTitles count]);

    return [contentTitles count];

  } else {

    return 1;
  }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  if (tableView == self.tableView_content) {

      return section == 2 ? 1 : [self getContentDataCountFromSection:section] + 1;

  } else {

    return [sortingTypes count];
  }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  if (indexPath.section == 2) {

      if (contentData.count == 0) {
          
          return 0;
      }
      
    return (((NSArray *)contentData[2]).count == 0) ? 0 : view_tagList.frame.size.height;

  } else {

    return COMMON_CELL_HEIGHT;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView == self.tableView_content) {
 
    if (indexPath.section == 0) {
        
        Products_Brand *brand = contentData[indexPath.section][indexPath.row - 1];
        brand.selected = !brand.selected;
        [self checkAnimationFromIndexPath:indexPath selected:brand.selected animation:YES];
        
        
    } else if (indexPath.section == 1) {
        
        Products_Specialty_Tag *specialty = contentData[indexPath.section][indexPath.row - 1];
        specialty.selected = !specialty.selected;
        [self checkAnimationFromIndexPath:indexPath selected:specialty.selected animation:YES];
        
    }
    
    } else {
        
        ProductAdanceSearchSortingTableViewCell *cell = (ProductAdanceSearchSortingTableViewCell *)[self.tableView_sorting cellForRowAtIndexPath:indexPath];
       [cell.radioBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    }


}

- (NSUInteger)getContentDataCountFromSection:(NSInteger)section {
 
    
    NSUInteger contentDataCount;
    
    if (!contentData) {
        
        contentDataCount = 0;
        
    }
    
    if (contentData.count > section) {
        
        contentDataCount =  [contentData[section] count];
        
    } else {
        
         contentDataCount = 0;
    }
    
    return contentDataCount;
    
}

@end
