//
//  AgentListSectionHeaderView.h
//  productionreal2
//
//  Created by Alex Hung on 8/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZtoreNavigationBar : UIView

#pragma mark property
@property(weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(weak, nonatomic) IBOutlet UIButton *leftBtn;
@property(weak, nonatomic) IBOutlet UIButton *rightBtn;
@property(weak, nonatomic) IBOutlet UIButton *rightBtn2;
@property(weak, nonatomic) IBOutlet UIImageView *rightMenuImage2;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIButton *voiceBtn;
@property(weak, nonatomic) IBOutlet UILabel *leftMenuTitle;
@property(weak, nonatomic) IBOutlet UIImageView *leftMenuImage;
@property(weak, nonatomic) IBOutlet UIImageView *rightMenuImage;
@property(weak, nonatomic) IBOutlet UIButton *clearBtn;
@property(weak, nonatomic) IBOutlet UIView *contentView;
@property(weak, nonatomic) IBOutlet UIButton *usePromoBtn;
@property(weak, nonatomic) IBOutlet UIImageView *ztoreWhiteLogoImageView;

#pragma mark CheckOutBar
@property(weak, nonatomic) IBOutlet UIView *checkOutBarView;
@property(weak, nonatomic) IBOutlet UIButton *oneBtn;
@property(weak, nonatomic) IBOutlet UIButton *twoBtn;
@property(weak, nonatomic) IBOutlet UIButton *threeBtn;
@property(weak, nonatomic) IBOutlet UIButton *fourBtn;
@property(weak, nonatomic) IBOutlet UIImageView *oneImageView;
@property(weak, nonatomic) IBOutlet UIImageView *twoImageView;
@property(weak, nonatomic) IBOutlet UIImageView *threeImageView;

- (id)initFromXib;

#pragma mark Navigation Bar
- (ZtoreNavigationBar *)rightMenuBar;
- (ZtoreNavigationBar *)mainMenuBar;
- (ZtoreNavigationBar *)productSearchBarType;
- (ZtoreNavigationBar *)rightMenuBarType;
- (ZtoreNavigationBar *)productDetailBarType;
- (ZtoreNavigationBar *)leftMenuBarType;
- (ZtoreNavigationBar *)mainMenuBarType;
- (ZtoreNavigationBar *)oneCheckOutBarType;
- (ZtoreNavigationBar *)twoCheckOutBarType;
- (ZtoreNavigationBar *)threeCheckOutBarType;
- (ZtoreNavigationBar *)fourCheckOutBarType;
- (ZtoreNavigationBar *)loginMenuBarType;

@end
