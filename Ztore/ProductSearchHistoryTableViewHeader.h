//
//  ProductSearchHistoryTableViewHeader.h
//  Ztore
//
//  Created by ken on 18/7/16.
//  Copyright © ken. All rights reserved.
//

#import "Common.h"
#import "Constants.h"
#import <UIKit/UIKit.h>

@interface ProductSearchHistoryTableViewHeader : UIView

@property(nonatomic, retain) UILabel *label_leftText;

+ (ProductSearchHistoryTableViewHeader *)initHeader;

- (void)setLabelHidden:(BOOL)hidden;

- (void)setLabelText:(NSString *)text;

@end
