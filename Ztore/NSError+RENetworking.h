//
//  NSError+RENetworking.h
//  real-v2-ios
//
//  Created by Derek Cheung on 24/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

// Constant
static NSString *const kStatusMessageInternalServerError = @"We are currently upgrading our service. Please try again later.";
static NSString *const kStatusMessageNetworkFailError    = @"Your network has failed.";
static NSString *const kStatusMessageTimeOutError        = @"Time Out Error";
#pragma mark - API Success Block Success Status
static NSInteger const kStatusCodeSuccess = 0;

/** lt will use "We are currently upgrading our service. Please try again later." message **/ // See RealAPI.pdf
#pragma mark - API Error Block Pop Up Error Status
static NSInteger const kStatusCodeInternalServerError = kCFURLErrorBadServerResponse;

static NSInteger const kStatusCodeNetworkFailError = kCFURLErrorNotConnectedToInternet;

static NSInteger const kStatusCodeTimeOutError = kCFURLErrorTimedOut;

static NSInteger const STATUS_OK = 1;

static NSInteger const STATUS_ERROR = 0;

static NSInteger const STATUS_RETRY = -1;

static NSInteger const STATUS_DONOTHING = -2;

//API Framework error

static NSInteger const PHP_ERROR = 10000;

static NSInteger const ACCESS_DENIED = 10001;

static NSInteger const UPDATE_SESSION_KEY = 10002;

static NSInteger const REQUIRED_LOGIN = 10003;

static NSInteger const API_CLASS_NOT_FOUND = 10004;

static NSInteger const API_METHOD_NOT_FOUND = 10005;

static NSInteger const API_REQUIRED_PARAMS = 10006;

//API classes logic error

static NSInteger const NO_DATA = 20001;

static NSInteger const EMAIL_NOT_MATCH = 20002;

static NSInteger const PASSWORD_NOT_MATCH = 20003;

static NSInteger const USER_NOT_EXIST = 20004;

static NSInteger const KEY_INVALID = 20005;

static NSInteger const REFERRAL_ALREADY_POINTED = 20006;

static NSInteger const REFERRAL_CODE_INVALID = 20007;

static NSInteger const REFERRAL_INVALID = 20008;

static NSInteger const NO_REFERRAL = 20009;

static NSInteger const ALREADY_TRIGGERED = 20010;

static NSInteger const CART_NO_PRODUCT = 20011;

static NSInteger const ADDRESS_NOT_EXIST = 20012;

static NSInteger const ZDOLLAR_OVER_LIMIT = 20013;

static NSInteger const LIST_NO_PRODUCT = 20014;

static NSInteger const SHIPPING_CODE_INVALID = 20015;

static NSInteger const PAYMENT_CODE_INVALID = 20016;

static NSInteger const DELIVERY_TIME_INVALID = 20017;

static NSInteger const SEND_EMAIL_FAIL = 20018;

static NSInteger const CREATE_ORDER_FAIL = 20019;

static NSInteger const BRAINTREE_ERROR = 20020;

static NSInteger const INVITATION_CODE_INVALID = 20021;

static NSInteger const INVITATION_INVALID = 20022;

static NSInteger const NOT_ALLOWED_TYPE = 20023;

static NSInteger const PROMO_CODE_INVALID = 20024;

static NSInteger const EMAIL_OVER_LIMIT = 20025;

static NSInteger const PROMO_CODE_OVER_USE_LIMIT = 20026;

static NSInteger const PROMOTION_EXPIRED = 20027;

static NSInteger const PROMO_CODE_NOT_REACH_CONDITION_PRICE = 20028;

static NSInteger const PRODUCT_INACTIVE = 20029;

static NSInteger const PRODUCT_INSTOCK = 20030;

static NSInteger const ORDER_RATING_EMPTY = 20031;

static NSInteger const ORDER_RATING_RATED = 20032;

static NSInteger const PROMO_CODE_UNABLE_FOR_REFERRAL_DISCOUNT = 20033;

static NSInteger const ARTICLE_LANGUAGE_NO_DATA = 20034;

static NSInteger const PROMOTION_FREEBIE_OOS = 20035;

static NSInteger const PROMO_CODE_USED_BY_OTHER = 20036;

static NSInteger const IS_VALIDATED = 20037;

static NSInteger const PROMO_CODE_CONDITION_PRODUCT_NOT_EXIST = 20038;

static NSInteger const PROMO_CODE_FIRST_ORDER_ONLY = 20039;

static NSInteger const INCORRECT_CREDIT_CARD_PROMO_CODE = 20040;

static NSInteger const ORDER_RENEW = 20041;

static NSInteger const ORDER_DUPLICATE = 20042;

static NSInteger const CART_NOT_AVAILABLE = 20043;

static NSInteger const PAYMENT_NOT_AVAILABLE = 20045;

static NSInteger const UNKWOUN_ERROR = 987654321;

//API classes field error

static NSInteger const INVALID = 30001;

static NSInteger const USED = 30002;

#pragma mark - API Error Block Not Pop Up Error Status
static NSString *const kREAPIErrorDomain = @"co.real.api.error";

@interface NSError (RENetworking)

+ (NSError *)createError:(NSInteger)errorCode;

+ (NSError *)networkUnreachableError;

+ (NSError *)internalServerError;

+ (NSError *)timeoutError;

@end
