//
//  REEnvConstant.m
//  real-v2-ios
//
//  Created by Derek Cheung on 17/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REEnvConstant.h"

@implementation REEnvConstant

#ifdef DEBUG

BOOL const kDebugMode = YES;

NSString *const kServerAddress = @"http://dev.real.co:8888/v2";

DDLogLevel const ddLogLevel = LOG_LEVEL_DEBUG;

#else

BOOL const kDebugMode = NO;

NSString *const kServerAddress = @"http://dev.real.co:8888/v2";

DDLogLevel const ddLogLevel = LOG_LEVEL_DEBUG;

#endif

NSString *const kGoogleAPIKey = @"AIzaSyCD898qHbBz5BwB6xcud0auqCI5CXx5hQQ";

@end
