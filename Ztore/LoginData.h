//
//  REAPIQueue.h
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REObject.h"

@class DBLoginData;

@interface LoginData : REObject

@property(strong, nonatomic) NSString *id;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *password;

- (DBLoginData *)toREDBObject;

@end
