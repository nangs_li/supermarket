//
//  REAPIManager.m
//  real-v2-ios
//
//  Created by Derek Cheung on 26/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAPIManager.h"
#import "REAddessSearchResponseModel.h"
#import "REAgentListingResponseModel.h"
#import "REAgentProfileResponseModel.h"

// API Group
static NSString *const kAPIGroupAdministration = @"/Admin";
static NSString *const kAPIGroupListing        = @"/Listing";

// API Method
static NSString *const kAPIMethodLogin           = @"Login";
static NSString *const kAPIMethodAddressSearch   = @"AddressSearch";
static NSString *const kAPIMethodAgentProfileGet = @"AgentProfileGet";
static NSString *const kAPIMethodAgentListingGet = @"AgentListingGet";

@implementation REAPIManager

#pragma Login

+ (nonnull NSURLSessionDataTask *)loginWithSocialNetworkType:(RESocialNetworkType)socialNetworkType
                                                    userName:(nonnull NSString *)userName
                                                      userID:(nonnull NSString *)userID
                                                       email:(nonnull NSString *)email
                                                    photoURL:(nullable NSString *)photoURL
                                                 accessToken:(nullable NSString *)accessToken
                                                  completion:(nullable REAPICommonBlock)completionBlock
                                                    progress:(nullable REAPIProgressBlock)progressBlock {

  NSString *url = [REAPIManager urlStringWithGroup:kAPIGroupAdministration];

  NSMutableDictionary *jsonDict = [NSMutableDictionary new];

  [jsonDict setObject:@(1) forKey:@"deviceType"];
#pragma mark DC - need to put the real device token
  [jsonDict setObject:@"deviceToken" forKey:@"deviceToken"];
  [jsonDict setObject:@(socialNetworkType) forKey:@"socialNetworkType"];
  [jsonDict setObject:userName forKey:@"userName"];
  [jsonDict setObject:userID forKey:@"userID"];
  [jsonDict setObject:email forKey:@"email"];

  if (photoURL && [photoURL valid]) {

    [jsonDict setObject:photoURL forKey:@"photoURL"];
  }

  if (accessToken && [accessToken valid]) {

    [jsonDict setObject:accessToken forKey:@"accessToken"];
  }
#pragma mark DC - need to put the real lang
  [jsonDict setObject:@"en" forKey:@"lang"];

  return [[REAPIClient sharedClient] post:url parameter:jsonDict requestID:nil retryTimes:3 completion:completionBlock progress:progressBlock];
}

+ (nonnull NSURLSessionTask *)addressSearchWithPlaceId:(nullable NSString *)placeId completion:(nullable REAPICommonBlock)completionBlock {

  NSString *url = [REAPIManager urlStringWithGroup:kAPIGroupListing];

  NSMutableDictionary *paramter = [[self parameterWithMethod:kAPIMethodAddressSearch] mutableCopy];
  [paramter setObject:placeId forKey:@"PlaceID"];

  NSURLSessionTask *task = [[REAPIClient sharedClient] post:url
                                                  parameter:paramter
                                                  requestID:nil
                                                 completion:^(id _Nullable response, NSError *_Nullable error) {

                                                   if (!error) {
                                                     NSError *parseError                = nil;
                                                     REAddessSearchResponseModel *model = [[REAddessSearchResponseModel alloc] initWithDictionary:response error:&parseError];

                                                     if (completionBlock) {
                                                       completionBlock(model, parseError);
                                                     }

                                                   } else {

                                                     if (completionBlock) {
                                                       completionBlock(nil, error);
                                                     }
                                                   }

                                                 }
                                                   progress:nil];

  return task;
}

+ (nonnull NSURLSessionTask *)agentProfileGetWithMemberIDs:(nonnull NSArray<AgentObject> *)memberIDs completion:(nullable REAPICommonBlock)completionBlock {
  NSString *url                  = [REAPIManager urlStringWithGroup:kAPIGroupListing];
  NSMutableDictionary *paramter  = [[self parameterWithMethod:kAPIMethodAgentProfileGet] mutableCopy];
  paramter                       = [self AddCommonParameterInDict:paramter];
  NSMutableArray *memberIDsArray = [[NSMutableArray alloc] init];

  for (AgentObject *agentObject in memberIDs) {

    [memberIDsArray addObject:@(agentObject.MID)];
  }

  [paramter setObject:memberIDsArray forKey:@"RetrieveList"];

  NSURLSessionTask *task = [[REAPIClient sharedClient] post:url
                                                  parameter:paramter
                                                  requestID:nil
                                                 completion:^(id _Nullable response, NSError *_Nullable error) {

                                                   if (!error) {
                                                     NSError *parseError                = nil;
                                                     REAgentProfileResponseModel *model = [[REAgentProfileResponseModel alloc] initWithDictionary:response error:&parseError];

                                                     if (completionBlock) {
                                                       completionBlock(model, parseError);
                                                     }

                                                   } else {

                                                     if (completionBlock) {
                                                       completionBlock(nil, error);
                                                     }
                                                   }

                                                 }
                                                   progress:nil];

  return task;
}

+ (nonnull NSURLSessionTask *)agentLisingGetWithMemberIDs:(nonnull NSArray<AgentObject> *)listingIDs completion:(nullable REAPICommonBlock)completionBlock {
  NSString *url = [REAPIManager urlStringWithGroup:kAPIGroupListing];

  NSMutableDictionary *paramter   = [[self parameterWithMethod:kAPIMethodAgentListingGet] mutableCopy];
  paramter                        = [self AddCommonParameterInDict:paramter];
  NSMutableArray *listingIDsArray = [[NSMutableArray alloc] init];

  for (AgentObject *agentObject in listingIDs) {

    [listingIDsArray addObject:@(agentObject.LID)];
  }

  [paramter setObject:listingIDsArray forKey:@"RetrieveList"];

  NSURLSessionTask *task = [[REAPIClient sharedClient] post:url
                                                  parameter:paramter
                                                  requestID:nil
                                                 completion:^(id _Nullable response, NSError *_Nullable error) {

                                                   if (!error) {
                                                     NSError *parseError                = nil;
                                                     REAgentListingResponseModel *model = [[REAgentListingResponseModel alloc] initWithDictionary:response error:&parseError];

                                                     if (completionBlock) {
                                                       completionBlock(model, parseError);
                                                     }

                                                   } else {

                                                     if (completionBlock) {
                                                       completionBlock(nil, error);
                                                     }
                                                   }

                                                 }
                                                   progress:nil];

  return task;
}

#pragma mark - Helpers

+ (nonnull NSDictionary *)demoGoogleAddress {

  NSDictionary *addressDict = @{ @"formatted_address" : @"United States", @"address_components" : @[ @{@"long_name" : @"United states", @"short_name" : @"US", @"types" : @[ @"country", @"political" ]} ], @"place_id" : @"ChIJCzYy5IS16lQRQrfeQ5K5Oxw" };

  return addressDict;
}

+ (nonnull NSDictionary *)parameterWithMethod:(NSString *)method {

  NSMutableDictionary *inputDict = [[NSMutableDictionary alloc] init];
  [inputDict setObject:method forKey:@"ApiName"];

  return inputDict;
}

+ (nonnull NSString *)urlStringWithGroup:(nonnull NSString *)group {
  return [NSString stringWithFormat:@"%@%@", kServerAddress, group];
}

+ (NSMutableDictionary *)AddCommonParameterInDict:(NSMutableDictionary *)dict {

  [dict setObject:@"b7620441-fb00-4b95-9e73-3666a19bfacb" forKey:@"APIToken"];
  [dict setObject:@532532 forKey:@"MemberID"];

  return dict;
}

@end
