//
//  ProductAdanceSearchSortingTableViewCell.h
//  Ztore
//
//  Created by ken on 29/7/16.
//  Copyright © ken. All rights reserved.
//

#import "DLRadioButton.h"
#import <UIKit/UIKit.h>

@interface ProductAdanceSearchSortingTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *label_left;
@property(weak, nonatomic) IBOutlet UIButton *btn_right;
@property(weak, nonatomic) IBOutlet DLRadioButton *radioBtn;

@end
