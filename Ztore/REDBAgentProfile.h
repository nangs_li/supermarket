//
//  REAgentProfile.h
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  strongright © 2016 ken. All rights reserved.
//

#import "REDBObject.h"

@class Languages, Experiences, REAgentProfile;

RLM_ARRAY_TYPE(DBLanguages)

@interface DBLanguages : REDBObject

@property(nonatomic, strong) NSString *Language;

@property(nonatomic, assign) int LangIndex;

- (Languages *)toREObject;

@end

RLM_ARRAY_TYPE(DBExperiences)

@interface DBExperiences : REDBObject

@property(nonatomic, assign) int IsCurrentJob;

@property(nonatomic, strong) NSString *EndDate;

@property(nonatomic, strong) NSString *Title;

@property(nonatomic, strong) NSString *Company;

@property(nonatomic, strong) NSString *StartDate;

- (Experiences *)toREObject;

@end

RLM_ARRAY_TYPE(DBSpecialties)

@interface DBSpecialties : REDBObject

@property(nonatomic, strong) NSString *Specialty;

- (DBSpecialties *)toREObject;

@end

RLM_ARRAY_TYPE(DBPastClosings)

@interface DBPastClosings : REDBObject

@property(nonatomic, assign) int PropertyType;

@property(nonatomic, strong) NSString *SoldPrice;

@property(nonatomic, strong) NSString *State;

@property(nonatomic, strong) NSString *SoldDateString;

@property(nonatomic, strong) NSString *Address;

@property(nonatomic, strong) NSString *Country;

@property(nonatomic, strong) NSString *SoldDate;

@property(nonatomic, assign) int Position;

@property(nonatomic, assign) int ID;

@property(nonatomic, assign) int SpaceType;

- (DBPastClosings *)toREObject;

@end

RLM_ARRAY_TYPE(REDBAgentProfile)

@interface REDBAgentProfile : REDBObject

@property(nonatomic, assign) int FollowerCount;

@property(nonatomic, strong) RLMArray<DBSpecialties *><DBSpecialties> *Specialties;

@property(nonatomic, assign) int Version;

@property(nonatomic, assign) int FollowingCount;

@property(nonatomic, strong) NSString *LicenseNumber;

@property(nonatomic, assign) int IsFollowing;

@property(nonatomic, strong) RLMArray<DBLanguages *><DBLanguages> *Languages;

@property(nonatomic, strong) NSString *QBID;

@property(nonatomic, assign) int MemberType;

@property(nonatomic, strong) NSString *PhotoURL;

@property(nonatomic, assign) int MemberID;

@property(nonatomic, strong) NSString *MemberName;

@property(nonatomic, strong) RLMArray<DBExperiences *><DBExperiences> *Experiences;

@property(nonatomic, strong) RLMArray<DBPastClosings *><DBPastClosings> *PastClosings;

- (REAgentProfile *)toREObject;

@end
