//
//  RealUtility.h
//  real-v2-ios
//
//  Created by Li Ken on 5/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

typedef void (^PHAssetBlock)(UIImage *result, NSDictionary *info);

typedef enum { AgentProfileStatusNormal = 0, AgentProfileStatusPreview, AgentProfileStatusEdit } AgentProfileStatus;

typedef enum { RealRemoteNotificationTypeOther = 0, RealRemoteNotificationTypeFollow, RealRemoteNotificationTypeChat, RealRemoteNotificationTypeInviteToFollowSuccess, RealRemoteNotificationTypeConnectUserSuccess, RealRemoteNotificationTypeConnectAgentSuccess } RealRemoteNotificationType;

typedef enum { ActivityLogTypeFollow = 0, ActivityLogTypeUnfollow, ActivityLogTypeInviteToFollow, ActivityLogTypeServerUnknown, ActivityLogTypeIsConnected } ActivityLogType;

typedef enum { ActivityLogViewTypeFollow = 0, ActivityLogViewTypeFollowing } ActivityLogViewType;

typedef enum { QBUUserRelationMute = 0, QBUUserRelationBlock } QBUUserRelation;

typedef enum {
  ChatFeatureTypeUnknown = 0,
  ChatFeatureTypeAudioCall,
  ChatFeatureTypeVideoCall,
  ChatFeatureTypeAudioAttachment,
  ChatFeatureTypeVideoAttachment,
} ChatFeatureType;

typedef enum { TutorialTypeSearch = 0, TutorialTypeCreatePost, TutorialTypeInviteToFollow, TutorialTypeConnectAgent } TutorialType;

typedef enum { ItemViewStyleWhite = 0, ItemViewStyleBlack } ItemViewStyle;

typedef enum {
  AgentStatusNotAgent = 0,
  AgentStatusWithoutListing,
  AgentStatusWithListing,
} AgentStatus;

#define MaxBlurRadius 100.0f
#define MaxImageBlurRadius 20.0f
#define expandOffsetY 44.0f
#define profileHeaderMinHeight 118.0f
#define profileActionHeaderMinHeight 158.0f
#define gt568h [RealUtility screenBounds].size.height > 568.0
#define RealGreenColor [UIColor colorWithRed:0 / 255.0f green:165 / 255.0f blue:80 / 255.0f alpha:1.0f]
#define RealLightBlueColor [UIColor colorWithRed:26 / 255.0f green:197 / 255.0f blue:236 / 255.0f alpha:1.0f]
#define RealBlueColor [UIColor colorWithRed:0 / 255.0f green:164 / 255.0f blue:233 / 255.0f alpha:1.0f]
#define RealDarkBlueColor [UIColor colorWithRed:50 / 255.0f green:60 / 255.0f blue:75 / 255.0f alpha:1.0]
#define RealNavBlueColor [UIColor colorWithRed:66 / 255.0f green:77 / 255.0f blue:93 / 255.0f alpha:1.0]
#define UnfollowRedColor [UIColor colorWithRed:231 / 255.0f green:89 / 255.0f blue:93 / 255.0f alpha:1.0]
#define NotificationRedColor [UIColor colorWithRed:255 / 255.0f green:39 / 255.0f blue:0 / 255.0f alpha:1.0]
#define RealPhoneNumberBlueColor [UIColor colorWithRed:18 / 255.0f green:144 / 255.0f blue:255 / 255.0f alpha:1.0]
#define RealNavBarHeightWithPageControl 80.0
#define RealNavBarHeight 64.0
#define RealUploadPhotoSize CGSizeMake(1000, 1000)

@interface RealUtility : NSObject

+ (CGRect)screenBounds;
+ (void)getImageBy:(PHAsset *)asset size:(CGSize)size handler:(PHAssetBlock)handler;
+ (NSString *)getMonthNameByIndex:(int)month;
+ (NSString *)getDecimalFormatByString:(NSString *)numberString withUnit:(NSString *)unit;

@end
