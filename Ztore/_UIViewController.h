//
//  _UIViewController.h
//  Ztore
//
//  Created by ken on 24/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ZtoreNavigationBar.h"
#import <Google/Analytics.h>
#import <JTMaterialTransition.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <UIKit/UIKit.h>
#import "TSMessage.h"
#import "TSMessageView.h"
#import "MesssageBar.h"
#import "ShoppingCartPopUpView.h"
#import "UIViewController+AMSlideMenu.h"
#import "BlueTag.h"
#import "ActionSheetCustomPicker.h"
#import "ShoppingCartPopUpView.h"

@interface _UIViewController : UIViewController <UIViewControllerAnimatedTransitioning, MBProgressHUDDelegate, TSMessageViewProtocol, UITextFieldDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, BlueTagDelegate, ActionSheetCustomPickerDelegate>

@property(nonatomic) JTMaterialTransition *transition;
@property(nonatomic) UIButton *presentControllerButton;
@property(nonatomic) UISearchBar *searchBar;
@property(strong, nonatomic) MBProgressHUD *currentHUD;
@property(assign, nonatomic) BOOL lockUI;
@property(strong, nonatomic) ZtoreNavigationBar *ztoreNavigationBar;
@property(strong, nonatomic) ShoppingCartPopUpView *shoppingCartPopUpView;
@property(strong, nonatomic) NSMutableAttributedString *termText;
@property(strong, nonatomic) NSTimer *hideLoadingTimer;
@property(strong, nonatomic) UIActivityIndicatorView *loadingView_productList;
@property(assign, nonatomic) BOOL haveCallAPI;

#pragma mark Common PickerView
@property(strong, nonatomic) NSArray *pickerDataArray;
@property(strong, nonatomic) NSString *selectedPickerData;

#pragma mark call API

- (void)callAPI;

#pragma mark GA

- (void)setPageName:(NSString *)name;
- (void)logEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label;
- (void)logEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

#pragma mark popup Toast and AlertView
- (void)showToastMessage:(NSString *)msg;
- (void)showToastMessage:(NSString *)msg duration:(NSTimeInterval)duration position:(id)position;
- (void)hideToastActivity;
- (void)showErrorFromMessage:(NSString *)message controller:(UIViewController *)controller okBtn:(UIAlertAction *)okBtn;
- (void)createTransition;

#pragma mark view

- (void)setupView;
- (void)setUpNavigationBar;

#pragma mark loading

- (void)showLoadingHUDWithProgress:(NSString *)progress;
- (void)showLoadingHUD;
- (void)hideLoadingHUD;
- (void)loadingFromColor:(UIColor *)color;
- (void)endCallAPILoading;

#pragma mark back method

- (void)backBtnPressed:(id)sender;
- (void)backBtnPressedNotEqualToSearch:(id)sender;
- (void)backToMainPageGetCurrentUser:(BOOL)getCurrentUser;
- (void)backToViewController;
- (BOOL)isPerviousControllerEqualToSearch;
- (void)popToViewController:(Class)controller;
- (void)dismissViewController:(Class)controller;
- (void)backToMainPage;

#pragma mark handleError method
- (void)handleErrorFromErrorCode:(NSInteger)errorCode methodName:(NSString *)methodName;


#pragma mark showPrivacyPolicy and TermsOfService method

- (void)showPrivacyPolicy;
- (void)showTermsOfService;

#pragma mark BlueTag Deletgate
- (void)pressBlueTagBtnFromPromotions:(Promotions *)promotions;

#pragma mark - BlueTag SoldOut View delegate
- (void)pressSoldOutBtnFromProduct:(Products *)product;

#pragma mark - dropDown Menu
- (void)pressChooseTitle:(UIButton *)sender;

@end
