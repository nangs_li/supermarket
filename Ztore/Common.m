//
//  Common.m
//  Ztore
//
//  Created by ken on 22/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "NSError+RENetworking.h"
#import "ProductSearchBarController.h"
#import "RightMenuVC.h"
#import "PaymentViewController.h"
#import "AddCreditCardViewController.h"
#import "RightMenuVC.h"
#import "DeliveryAddressViewController.h"
#import "TimeslotViewController.h"
#import "ConfirmOrderViewController.h"
#import "_UIViewController.h"
#import "ProductListCollectionViewController.h"
#import "CreateUserViewController.h"
#import "MyTableViewController.h"
#import "MainRegisterViewController.h"
#import "LoginViewController.h"

@implementation Common {
  NSUserDefaults *userDefaults;
}

+ (instancetype)shared {
  static id instance_ = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance_ = [[self alloc] init];
  });

  return instance_;
}

- (id)init {

  self                    = [super init];
  userDefaults            = [NSUserDefaults standardUserDefaults];
  self.productDict        = [[NSMutableDictionary alloc] init];
  self.productStockArray  = [[NSMutableArray alloc] init];
  self.productChangeArray = [[NSMutableArray alloc] init];
  self.specialyRequest = [SpecialyRequest shared];
    
  return self;
}

- (void)setValue:(id)value forKey:(NSString *)key {

  [userDefaults setValue:value forKey:key];
  [userDefaults synchronize];
}

- (id)valueForKey:(NSString *)key {

  return [userDefaults valueForKey:key];
}

- (NSString *)getCurrentLanguage {

  NSString *code = [[LanguagesManager sharedInstance] getDefaultLanguage];

  return [code containsString:@"en"] ? @"en" : @"tc";
}

- (BOOL)isEnLanguage {
    
    return [[[Common shared] getCurrentLanguage] isEqualToString:@"en"];
}

- (int)addProductQtyFromProduct:(Products *)product addCount:(int)addCount {

  product.pressUpbtn   = (addCount > 0) ? YES : NO;
  product.pressDownbtn = (addCount > 0) ? NO : YES;

  if (product.stock_qty >= (int)product.cart_qty + addCount) {

    [self setProductQtyChangeFromProductId:product.id ProductQtyChange:(NSInteger)addCount];
      
  }

  int qty = (int)product.cart_qty + addCount;

  if (qty >= 0 && qty <= product.stock_qty) {

    product.cart_qty = qty;
    [[Common shared] setProduct:product];

  }

  return (int)product.cart_qty + addCount;
}

- (Products *)setQtyLabelTextFromProduct:(Products *)product label:(UILabel *)label {

  Products *productObject = [product getGlobalProduct];

  product.cart_qty         = productObject.cart_qty;
  product.stock_qty        = productObject.stock_qty;
  product.cart_freebie_qty = productObject.cart_freebie_qty;

  [productObject setUpHideButton];
  product.hideupbtn   = productObject.hideupbtn;
  product.hidedownbtn = productObject.hidedownbtn;

    if (product.isFreeProduct) {
        
        label.text = [NSString stringWithFormat:@"%li", (long)(product.cart_freebie_qty)];
        
    } else {
        
         label.text = [NSString stringWithFormat:@"%li", (long)(product.cart_qty - product.cart_freebie_qty)];
    }
    
    return product;
}

- (void)setProductQtyChangeFromProductId:(NSInteger)productId ProductQtyChange:(NSInteger)ProductQtyChange {

  NSMutableDictionary *dictionary;

  for (NSMutableDictionary *dict in self.productChangeArray) {

    if ([[dict objectForKey:@"productId"] integerValue] == productId) {

      dictionary = dict;
    }
  }

  if (dictionary) {

    NSInteger oldProductQtyChange = [[dictionary objectForKey:@"productChangeQty"] integerValue];
    [dictionary removeObjectForKey:@"productChangeQty"];
    [dictionary setObject:[NSString stringWithFormat:@"%ld", (long)oldProductQtyChange + ProductQtyChange] forKey:@"productChangeQty"];

  } else {

    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%ld", (long)productId], @"productId", [NSString stringWithFormat:@"%ld", (long)ProductQtyChange], @"productChangeQty", nil];

    [self.productChangeArray addObject:dict];
  }
    
}

- (Products *)getProductFromProductId:(NSInteger)productId {

  return [self.productDict objectForKey:[NSString stringWithFormat:@"%ld", (long)productId]];
}

- (void)setProduct:(Products *)product {

  if ([self.productDict objectForKey:[product getProductIdString]]) {

    [self.productDict removeObjectForKey:[product getProductIdString]];
  }

  [self.productDict addEntriesFromDictionary:@{[product getProductIdString] : product}];
}

- (void)addClickEventForView:(UIView *)view withTarget:(UIViewController *)target action:(NSString *)action {
    
  UITapGestureRecognizer *viewReongnizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:NSSelectorFromString(action)];
  viewReongnizer.numberOfTapsRequired    = 1;
  [view setUserInteractionEnabled:YES];
  [view addGestureRecognizer:viewReongnizer];
    
}

- (void)setStrikethroughForLabel:(UILabel *)label withText:(NSString *)text {
  NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
  [attributeString addAttribute:NSStrikethroughStyleAttributeName value:@1 range:NSMakeRange(0, [attributeString length])];
  [label setAttributedText:attributeString];
}

- (UIColor *)getCommonPinkRedColor {
    
    return [UIColor colorWithRed:230 / 255.0 green:0 / 255.0 blue:75 / 255.0 alpha:1.0];
}

- (UIColor *)getCommonAlphaWhiteColor {

  return [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
}

- (UIColor *)getCommonRedColor {

  return [self getColorWithRed:181 greed:10 blue:71];
}

- (UIColor *)getCommonGrayColor {

  return [self getColorWithRed:162 greed:162 blue:162];
}

- (UIColor *)getCommonGrayBackgroundColor {

  return [self getColorWithRed:247 greed:247 blue:247];
}

- (UIColor *)getFilterAnimationColor {

  return [UIColor colorWithRed:(245 / 255.0) green:(246 / 255.0) blue:(247 / 255.0) alpha:0.95f];
}

- (UIColor *)getProductDetailDescGrayBackgroundColor {

  return [self getColorWithRed:238 greed:238 blue:238];
}

- (UIColor *)getTableViewSeparatorGreyBackgroundColor {

  return [self getColorWithRed:234 greed:237 blue:236];
}

- (UIColor *)getTagColor {

  return [self getColorWithRed:185 greed:146 blue:103];
}

- (UIColor *)getLineColor {

  return [self getColorWithRed:219 greed:219 blue:219];
}

- (UIColor *)getShoppingCartLineColor {
    
    return [self getColorWithRed:229 greed:229 blue:229];
}

- (UIColor *)getColorWithRed:(int)red greed:(int)green blue:(int)blue {

  return [UIColor colorWithRed:(red / 255.0) green:(green / 255.0) blue:(blue / 255.0) alpha:1.0f];
}

- (UIColor *)getPressIncreseButtonColor {

  return [UIColor colorWithRed:241.0 / 255. green:241.0 / 255. blue:241.0 / 255. alpha:1];
}

- (UIColor *)getTextFieldPlaceHolderColor {
    
    return [UIColor colorWithRed:211.0 / 255.0 green:211.0 / 255. blue:211.0 / 255. alpha:1];
}

- (UIColor *)getSoldOutButtonTitleColor {
    
    return [UIColor colorWithRed:144.0 / 255.0 green:144.0 / 255. blue:144.0 / 255. alpha:1];
}

- (UIFont *)getBiggerNormalFont {

    return [self fontWithName:@"Open Sans" size:DEFAULT_FONT_SIZE + 8];

}

- (UIFont *)getBiggerBoldTitleFont {

  return [self fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE + 8];
}

- (UIFont *)getNormalTitleFont {

  return [self fontWithName:@"Open Sans" size:DEFAULT_FONT_SIZE + 2];
}

- (UIFont *)getNormalBoldTitleFont {

  return [self fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE + 2];
}

- (UIFont *)getNormalFont {

  return [self fontWithName:@"Open Sans" size:DEFAULT_FONT_SIZE];
}

- (UIFont *)getNormalBoldFont {

  return [self fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE];
}

- (UIFont *)getContentNormalFont {

  return [self fontWithName:@"Open Sans" size:DEFAULT_FONT_SIZE - 1];
}

- (UIFont *)getSmallContentBoldFont {

  return [self fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE - 3];
}

- (UIFont *)getSmallContentNormalFont {

  return [self fontWithName:@"Open Sans" size:DEFAULT_FONT_SIZE - 3];
}

- (UIFont *)getContentBoldFont {

  return [self fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE - 1];
}

- (UIFont *)getNormalFontWithSize:(int)size {

  return [self fontWithName:@"Open Sans" size:size];
}

- (UIFont *)getBoldFontWithSize:(int)size {
    
  return [self fontWithName:@"OpenSans-Bold" size:size];
}

- (UIFont *)fontWithName:(NSString *)name size:(int)size {
    
    return  [UIFont fontWithName:name size:(gt568h) ? size : size - 4];
    
}

- (NSString *)getStringFromPrice:(float)price {

  return [NSString stringWithFormat:@"$%.1f", price];
}

- (NSString *)getStringFromPriceString:(NSString *)price {

  return (price) ? [NSString stringWithFormat:@"$%.2f", [price doubleValue]] : @"$0";
}

- (CALayer *)getNormalBreakLineWithFrame:(CGRect)frame {
  CALayer *line = [CALayer layer];
  [line setFrame:frame];
  [line setBackgroundColor:[self getLineColor].CGColor];

  return line;
}

- (NSMutableArray<Products> *)getFreeProductArrayFromProductArray:(NSArray<Products> *)productArray {

  NSMutableArray<Products> *array = [[NSMutableArray<Products> alloc] init];

  for (Products *product in productArray) {

    if (product.cart_qty > product.cart_freebie_qty && product.cart_freebie_qty != 0) {

      Products *productObject           = [product copy];
      productObject.isCanAddFreeProduct = YES;
      productObject.isFreeProduct = NO;
        
        if (isValid(productObject)) {
            
        [array addObject:productObject];
            
        }

      Products *product2 = product;

      product2.isFreeProduct = YES;
      product2.isCanAddFreeProduct = NO;

        if (isValid(product2)) {
            
      [array addObject:product2];
            
        }

    } else if (product.cart_qty == product.cart_freebie_qty && product.cart_freebie_qty != 0) {

      product.isFreeProduct = YES;
      product.isFreeProduct = NO;
        
      [array addObject:product];

    } else {

      [array addObject:product];
    }
  }

  return array;
}

- (Products *)createNewProductFrom:(nullable Products *)product {
    
    Products *productcopy = [[Products alloc] init];
    productcopy.meta_keyword = product.meta_keyword;
    productcopy.category_ids = product.category_ids;
    productcopy.order9 = product.order9;
    productcopy.is_notice = product.is_notice;
    productcopy.volume = product.volume;
    productcopy.url_key = product.url_key;
    productcopy.country = product.country;
    productcopy.default_category_path = product.default_category_path;
    productcopy.stock_qty = product.stock_qty;
    productcopy.image = product.image;
    productcopy.sn = product.sn;
    productcopy.zdollar_percentage = product.zdollar_percentage;
    productcopy.tags = product.tags;
    productcopy.is_new = product.is_new;
    productcopy.brand = product.brand;
    productcopy.images = product.images;
    productcopy.cart_qty = product.cart_qty;
    productcopy.cart_freebie_qty = product.cart_freebie_qty;
    productcopy.name = product.name;
    productcopy.meta_description = product.meta_description;
    productcopy.brand_id = product.brand_id;
    productcopy.default_category_id = product.default_category_id;
    productcopy.id = product.id;
    productcopy.cart_available = product.cart_available;
    productcopy.promotions = product.promotions;
    productcopy.shop = product.shop;
    productcopy.is_exist_in_wish_list = product.is_exist_in_wish_list;
    productcopy.category_paths = product.category_paths;
    productcopy.purchase_quota = product.purchase_quota;
    productcopy.price = product.price;
    productcopy.is_hot = product.is_hot;
    productcopy.desc = product.desc;
    productcopy.available = product.available;
    productcopy.freebie_qty = product.freebie_qty;
    productcopy.subtotal = product.subtotal;
    productcopy.discounted_subtotal = product.discounted_subtotal;
    productcopy.qty = product.qty;
    productcopy.barcode = product.barcode;
    productcopy.is_visible = product.is_visible;
    productcopy.hideupbtn = product.hideupbtn;
    productcopy.hidedownbtn = product.hidedownbtn;
    productcopy.pressUpbtn = product.pressUpbtn;
    productcopy.pressDownbtn = product.pressDownbtn;
    productcopy.isFreeProduct = product.isFreeProduct;
    productcopy.isCanAddFreeProduct = product.isCanAddFreeProduct;
    
    return productcopy;
    
}

- (NSArray<Products> *)getFreebieTypeProductFromProduct:(NSArray<Products> *)products {
    
    NSMutableArray<Products> *array = [[NSMutableArray<Products> alloc] init];
    
    for (Products *product in products) {
        
        Promotions *freebiePromotion = nil;
        
        for (Promotions *promotion in product.promotions) {
            
            if ([promotion.type isEqualToString:@"freebie"]) {
                
                freebiePromotion = promotion;
                
            }
            
        }
        
        if (isValid(freebiePromotion)) {
            
            Products *productcopy = [self createNewProductFrom:product];
            productcopy.cart_freebie_qty = [freebiePromotion.qty integerValue];
            productcopy.isFreeProduct = YES;
            productcopy.isCanAddFreeProduct = NO;
            productcopy.meta_keyword = product.meta_keyword;
            
            if (isValid(productcopy)) {
                
                [array addObject:productcopy];
                
            }
            
            product.cart_qty = product.cart_qty - productcopy.cart_freebie_qty;
            product.isFreeProduct = NO;
            product.isCanAddFreeProduct = YES;
        }
        
        if (product.cart_qty > 0) {
         
             [array addObject:product];
            
        }
        
        freebiePromotion = nil;
        
    }
    
    return [array copy];
}

- (UIView *)tagView:(UIView *)view_tagList setTagsList:(NSArray *)tags setSelectedTagsList:(NSArray *)selectedTagsList addTarget:(id)target action:(SEL)selector {

  // First create tags view then calculate tags cell height for render tableView
  CGFloat maxWidth        = view_tagList.frame.size.width - GL_TAG_MARGIN * 2;
  int positionX           = 0;
  int positionY           = GL_TAG_MARGIN;
  int currentWidth        = 0;
  bool isFirstItem        = YES;
  NSMutableArray *tagRows = [[NSMutableArray alloc] init];
  UIView *view_row        = [[UIView alloc] initWithFrame:CGRectMake(GL_TAG_MARGIN, positionY, maxWidth, GL_TAG_HEIGHT)];
  for (int i = 0; i < [tags count]; i++) {

    CGFloat curItemWidth = 0; // use for calculating the final row width
    if (isFirstItem) {
      isFirstItem = NO;
    } else {
      positionX += GL_TAG_MARGIN;
      currentWidth += GL_TAG_MARGIN;
      curItemWidth += GL_TAG_MARGIN;
    }
    UIButton *label_tag = [[UIButton alloc] initWithFrame:CGRectMake(positionX, 0, 100, GL_TAG_HEIGHT)];

    // set on click event and set button tag index
    [label_tag setTag:i];
    [label_tag addTarget:target action:selector forControlEvents:UIControlEventTouchDown];
    [label_tag.titleLabel setFont:[self getContentNormalFont]];
    [label_tag setTitle:tags[i] forState:UIControlStateNormal animation:NO];
    [label_tag setTitleColor:[self getTagColor] forState:UIControlStateNormal];
    [label_tag sizeToFit];
    [label_tag setFrame:CGRectMake(label_tag.frame.origin.x, label_tag.frame.origin.y, label_tag.frame.size.width + GL_TAG_MARGIN * 2, GL_TAG_HEIGHT)];

    for (NSString *selectedTag in selectedTagsList) {

      label_tag.accessibilityHint = ([selectedTag isEqualToString:tags[i]]) ? @"Selected" : nil;
    }

    if ([label_tag.accessibilityHint isEqualToString:@"Selected"]) {

      [label_tag setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
      [label_tag setBackgroundColor:[self getTagColor]];

    } else {

      [label_tag setTitleColor:[self getTagColor] forState:UIControlStateNormal];
      [label_tag setBackgroundColor:[UIColor whiteColor]];
    }

    label_tag.layer.borderColor  = [self getTagColor].CGColor;
    label_tag.layer.borderWidth  = 1.0f;
    label_tag.layer.cornerRadius = 5.0f;
    currentWidth += label_tag.frame.size.width;
    curItemWidth += label_tag.frame.size.width;

    if (currentWidth > maxWidth) {
      // store the current line to array
      [view_row setFrame:CGRectMake(view_row.frame.origin.x, view_row.frame.origin.y, currentWidth - curItemWidth, view_row.frame.size.height)];
      view_row.center = CGPointMake(CGRectGetMidX(view_tagList.bounds), view_row.center.y);
      [tagRows addObject:view_row];
      // create new line if not enough space
      positionY += GL_TAG_HEIGHT + GL_TAG_MARGIN;
      positionX = 0;
      view_row  = [[UIView alloc] initWithFrame:CGRectMake(GL_TAG_MARGIN, positionY, maxWidth, GL_TAG_HEIGHT)];
      [label_tag setFrame:CGRectMake(positionX, 0, label_tag.frame.size.width, label_tag.frame.size.height)];
      currentWidth = label_tag.frame.size.width;
      positionX    = currentWidth;
      [view_row addSubview:label_tag];
    } else {
      // add the tag item to current line
      [view_row addSubview:label_tag];
      positionX += label_tag.frame.size.width;

      if (i == [tags count] - 1) {
        // store the last line to array
        [view_row setFrame:CGRectMake(view_row.frame.origin.x, view_row.frame.origin.y, currentWidth, view_row.frame.size.height)];
        view_row.center = CGPointMake(CGRectGetMidX(view_tagList.bounds), view_row.center.y);
        [tagRows addObject:view_row];
      }
    }
  }

  // Create a view to render each row of tags
  view_tagList = [[UIView alloc] initWithFrame:CGRectMake(view_tagList.frame.origin.x, view_tagList.frame.origin.y, view_tagList.frame.size.width, [tagRows count] * GL_TAG_HEIGHT + ([tagRows count] + 1) * GL_TAG_MARGIN)];

  for (int i = 0; i < [tagRows count]; i++) {
    [view_tagList addSubview:tagRows[i]];
  }

  return view_tagList;
}

- (BOOL)checkNotError:(NSError *)error controller:(UIViewController *)controller response:(id)response {

    NSString *message;
    NSInteger errorCode = 1;
    
    // server have  not response case maybe network error
    
  if (isValid(error)) {
      
    errorCode = error.code;
      
      if (errorCode ==  kStatusCodeNetworkFailError) {
          
          message = JMOLocalizedString(kStatusMessageNetworkFailError, nil);
          
          
      } else if (errorCode == kStatusCodeInternalServerError){
          message = JMOLocalizedString(kStatusMessageInternalServerError, nil);
          
          
      } else if (errorCode == kStatusCodeTimeOutError){
          
          message = JMOLocalizedString(kStatusMessageNetworkFailError, nil);
          
      } else {
          
          errorCode = UNKWOUN_ERROR;
          message = JMOLocalizedString(@"Sorry，We have some unknown bug", nil);
          
      }
      
      [[AppDelegate getAppDelegate] showPopUpViewFromErrorMessage:message];
      
  } else {

      // server have response case
       [[AppDelegate getAppDelegate] hidePopUpView];
      
      if ([response isKindOfClass:[REObject class]]) {

        REObject *responseObject = (REObject *)response;
        errorCode = [responseObject getErrorCode];

      }

        if (errorCode == NO_DATA) {
        
        if ([controller isKindOfClass:[_UIViewController class]]) {
            
            _UIViewController *viewController = (_UIViewController *)controller;
            [viewController hideLoadingHUD];
            [viewController endCallAPILoading];
            
        }
        
            message = nil;
  
    } else if (errorCode == PROMO_CODE_INVALID){
        
            message = JMOLocalizedString(@"This promotion code is invalid", nil);
            

    } else if (errorCode == BRAINTREE_ERROR) {
        
        NSError *error;
        CardID *cardID = response;
        WrongCardID *wrongCardID = [[WrongCardID alloc] initWithDictionary:cardID.jsonDict error:&error];
        message = wrongCardID.data.firstObject;
        
    } else if (errorCode == USED){
        
        message = JMOLocalizedString(@"email is used", nil);
    
    } else if (errorCode == PASSWORD_NOT_MATCH){
        
        message = JMOLocalizedString(@"Please enter a valid password", nil);
        
        
    } else if (errorCode == EMAIL_NOT_MATCH){
        
        message = JMOLocalizedString(@"Account login information does not correct", nil);
        
        
    } else if (errorCode == INVALID){
        
        message = JMOLocalizedString(@"Please enter a valid information", nil);
        
    } else if (errorCode == ACCESS_DENIED){
        
        message = JMOLocalizedString(@"ACCESS_DENIED MAY BE ENCRYPTION ERROR", nil);;
        
    } else if (errorCode == USER_NOT_EXIST){
        
        message = JMOLocalizedString(@"Your email is not registered.", nil);;
        
    } else if (errorCode == PRODUCT_INSTOCK){
        
        message = JMOLocalizedString(@"This product has restocked,please refresh", nil);;
        
    } else if (errorCode == PROMO_CODE_NOT_REACH_CONDITION_PRICE){
        
        if ([response isKindOfClass:[CardID class]]) {
            
            CardID *cardID = response;
            
            NSString *string = [NSString stringWithFormat:JMOLocalizedString(@"This code is only valid upon $%@ purchase.", nil), cardID.data];
            
            message = string;
            
        }
        
    } else if (errorCode == STATUS_OK){
        
        message = nil;
        
    } else {
        
         message = JMOLocalizedString(@"Sorry，We have some unknown bug", nil);
        
    }
      
  if (message) {

      
      if ([controller isKindOfClass:[_UIViewController class]]) {
          
          _UIViewController *viewController = (_UIViewController *)controller;
          [viewController hideLoadingHUD];
         
          
      }
      
      if ([controller isKindOfClass:[RightMenuVC class]]) {
          
              RightMenuVC *viewController = (RightMenuVC *)controller;
              [viewController hideLoadingHUD];
    }
      
      [[AppDelegate getAppDelegate] showMessageBarFromResponse:message];
      
      
  } else {
      
       [[AppDelegate getAppDelegate] hidePopUpView];
      
  }
    
      
  // hide small loading
  if ([controller isKindOfClass:[_UIViewController class]]) {
      
       _UIViewController *viewController = (_UIViewController *)controller;
      [viewController endCallAPILoading];
        
  }

  }
    
   return !isValid(message);
}

- (BOOL)notShowNOResultPageFromController:(UIViewController *)controller {

  return ([controller isKindOfClass:[ProductListCollectionViewController class]] || [controller isKindOfClass:[RightMenuVC class]] || [controller isKindOfClass:[ProductSearchBarController class]] || [controller isKindOfClass:[DeliveryAddressViewController class]] || [controller isKindOfClass:[MyTableViewController class]] || [controller isKindOfClass:[PaymentViewController class]] || [controller isKindOfClass:[MyTableViewController class]]);
}

- (BOOL)checkNotEnoughStockFromResponse:(id)response {

    BOOL notEnoughStock = NO;
    
  REObject *responseObject = (REObject *)response;
  NSDictionary *dict       = [responseObject.statusCodes objectAtIndex:1];
  NSArray *array           = [dict objectForKey:@"data"];
  
    if ([response getErrorCode] == STATUS_OK) {
        
    } else {
    
    
  for (NSDictionary *dataStatusDict in array) {

    if ([(NSNumber *)[dataStatusDict objectForKey:@"status"] boolValue] == FALSE) {

        notEnoughStock = YES;

    }
  }
        
  }
    
    return notEnoughStock;
}

- (int)getConsignee_titleIdFromConsignee_title:(NSString *)consignee_title {
    
    if ([consignee_title isEqualToString:@"Mr."]||([consignee_title isEqualToString:@"先生"])) {
        
        return 1;
    }
    
    if ([consignee_title isEqualToString:@"Mrs."]||([consignee_title isEqualToString:@"夫人"])) {
        
        return 2;
    }
    
    if ([consignee_title isEqualToString:@"Miss"]||([consignee_title isEqualToString:@"小姐"])) {
        
        return 3;
    }
    
    if ([consignee_title isEqualToString:@"Ms."]||([consignee_title isEqualToString:@"女士"])) {
        
        return 4;
    }
    
    return 1;
    
}

- (NSString *)getConsigneeTitleFromTitle:(NSInteger )title {
    
    switch (title) {
            
        case 1:
            
            return JMOLocalizedString(@"Mr.", nil);
            break;
            
        case 2:
            
            return JMOLocalizedString(@"Mrs.", nil);
            break;
            
        case 3:
            
            return JMOLocalizedString(@"Miss", nil);
            break;
            
        case 4:
            
            return JMOLocalizedString(@"Ms.", nil);
            break;
            
        default:
            
            return @"";
            break;
    }
}

- (void)setUpTickFromCheckBox:(BEMCheckBox *)checkBox {
    
    checkBox.tintColor        = [UIColor lightGrayColor];
    checkBox.onTintColor      = [UIColor clearColor];
    checkBox.onFillColor      = [[Common shared] getCommonRedColor];
    checkBox.onCheckColor     = [UIColor whiteColor];
    checkBox.onAnimationType  = BEMAnimationTypeStroke;
    checkBox.offAnimationType = BEMAnimationTypeFill;
    checkBox.boxType          = BEMBoxTypeSquare;
    [checkBox setOn:NO animated:NO];
    [checkBox reload];
    
}

- (void)cleanAllCheckOutData {
    
    self.selectedDeliveryAddress = nil;
    self.selectedTimeArray = [[NSMutableArray alloc] init];
    self.timeslotArray1 = [[NSMutableArray alloc] init];
    self.timeslotArray2 = [[NSMutableArray alloc] init];
    self.timeslotArray3 = [[NSMutableArray alloc] init];
    self.timeslotArray4 = [[NSMutableArray alloc] init];
    self.timeslotArray5 = [[NSMutableArray alloc] init];
    self.timeslotArray6 = [[NSMutableArray alloc] init];
    self.timeslotArray7 = [[NSMutableArray alloc] init];
    self.timeslotData = nil;
    self.menuItems = [[NSMutableArray alloc] init];
    self.selectedTimeslot = nil;
    self.selectedDayBtn = nil;
    self.specialyRequest = [SpecialyRequest shared];
    self.backToViewController = nil;
    self.shoppingCartData = nil;
    self.customerCardArray = nil;
    self.selectedCard = nil;
    self.selectedCardnew = nil;
    self.is_store_card = NO;
    self.clientToken = nil;
    [self.specialyRequest initString];
    
    for (id key in self.productDict) {
        
        Products *products = [self.productDict objectForKey:key];
        products.qty = products.cart_qty = 0;
        
    }
    
    self.productStockArray  = [[NSMutableArray alloc] init];
    self.productChangeArray = [[NSMutableArray alloc] init];
    self.zDollarModel = nil;
    self.recentPurchaseList = nil;
    self.orderHistory = nil;
    self.favouriteList = nil;
    [AppDelegate getAppDelegate].timeslotViewController = nil;
    self.shoppingCartData = nil;
    
}

+ (ZtoreNavigationBar *)getRightMenuBar {

  return [[ZtoreNavigationBar alloc] rightMenuBar];
}

+ (ZtoreNavigationBar *)getMainMenuBar {

  return [[ZtoreNavigationBar alloc] mainMenuBar];
}

- (InputListProduct *)getInputListProductWithtBrands:(NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(NSArray<NSString *> *)productTags selectedSort:(NSString *)selectedSort inputListProduct:(InputListProduct *)inputListProduct {
    
    InputListProduct *input = [[InputListProduct alloc] init];
    input.type = inputListProduct.type;
    
    NSMutableArray *brand_idsArray = [[NSMutableArray alloc] init];
    
    for (Products_Brand *brand in productBrands) {
        
        if (brand.selected) {
            
            [brand_idsArray addObject:[NSNumber numberWithInt:(int)brand.id]];
        }
    }
    
    input.brand_ids = brand_idsArray;
    
    NSMutableArray *tagsArray = [[NSMutableArray alloc] init];
    
    [tagsArray addObjectsFromArray:productTags];
    
    for (Products_Specialty_Tag *specialty in productSpecialtyTags) {
        
        if (specialty.selected) {
            
            [tagsArray addObject:specialty.name];
        }
    }
    
    input.tags = tagsArray;
    
    if (selectedSort != nil) {
        
        input.order_by = [NSArray arrayWithObject:selectedSort];
    }
    
    if (inputListProduct.type == parent_category_id) {
        
        input.parent_category_id =  [[AppDelegate getAppDelegate].Level2_childrenProduct.related_id intValue];
        
    } else if (inputListProduct.type == search_brand_ids) {
        
        [brand_idsArray addObjectsFromArray:inputListProduct.brand_ids];
        input.brand_ids = brand_idsArray;
        
    } else if (inputListProduct.type == search_tags) {
        
        [tagsArray addObjectsFromArray:inputListProduct.tags];
        input.tags = tagsArray;
        
    }
    
    input.sn = inputListProduct.sn;
    
    return input;
}

- (void)getZDollarModelCompletionBlock:(nullable REAPICommonBlock)completionBlock {

 if (!isValid([Common shared].zDollarModel)) {
    
    [[ServerAPI shared] getUserZDollarLogCompletionBlock:completionBlock];
    
}
    
}

- (void)getFavouriteListCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
 if (!isValid([Common shared].favouriteList)) {
    
    [[ServerAPI shared] userListListProductCompletionBlock:completionBlock];
    
}

}

- (void)getRecentPurchaseListCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
if (!isValid([Common shared].recentPurchaseList)) {
    
    [[ServerAPI shared] userListListRecentlyPurchasedProductCompletionBlock:completionBlock];
    
}

}

- (void)getDeliveryAddressDataCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
 if (!isValid([Common shared].deliveryAddressData)) {
    
    [[ServerAPI shared] listCurrentUserAddressCompletionBlock:completionBlock];
    
}
    
}

- (void)getOrderHistoryCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
  if (!isValid([Common shared].orderHistory)) {
    
    [[ServerAPI shared] listCurrentUserPaidOrderCompletionBlock:completionBlock];
    
}

}

- (void)pushToProductDetailPageFromProduct:(Products *)product {

    if (!product.isFreeProduct) {
        
        UIStoryboard *storyboard               = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [AppDelegate getAppDelegate].productDetailController = (ProductDetailController *)[storyboard instantiateViewControllerWithIdentifier:@"ProductDetailController"];
        [AppDelegate getAppDelegate].productDetailController.productId        = (int)product.id;
        [AppDelegate getAppDelegate].productDetailController.orignalProduct   = product;
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:[AppDelegate getAppDelegate].productDetailController animated:YES enableUI:NO];

   }
    
}

- (void)pushToProductListPageFromInputListProduct:(InputListProduct *)InputListProduct controller:(_UIViewController *)controller {
    
    ProductListCollectionViewController *collection;
    collection = [ProductListCollectionViewController initViewControllerWithInputListProduct:InputListProduct];
    collection.parentController                     = controller;
    collection.cleanProducts = YES;
    [collection loadingFromColor:nil];
    [controller.navigationController pushViewController:collection animated:YES enableUI:NO];

}

- (void)sendGAForpageName:(NSString *)pageName {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:pageName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)sendGAEcommerceForPaymentSuccess:(PaymentSuccess *)paymentSuccess {
    
    if ([(NSNumber *)paymentSuccess.statusCodes[0] intValue] == STATUS_OK) {
         
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:[paymentSuccess.data getProductIdString]           // (NSString) Transaction ID
                                                     affiliation:@"Ztore"         // (NSString) Affiliation
                                                         revenue:[paymentSuccess.data.discounted_subtotal toNSNmuer]                  // (NSNumber) Order revenue (including tax and shipping)
                                                             tax:@0                  // (NSNumber) Tax
                                                        shipping:[paymentSuccess.data.delivery_fee toNSNmuer]                      // (NSNumber) Shipping
                                                    currencyCode:@"HKD"] build]];        // (NSString) Currency code
    
    
    for (Products *product in paymentSuccess.data.products) {
    
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *price = [f numberFromString:product.discounted_subtotal];
        
    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:[paymentSuccess.data getProductIdString]         // (NSString) Transaction ID
                                                                name:product.name  // (NSString) Product Name
                                                                 sku:product.sn            // (NSString) Product SKU
                                                            category:@""  // (NSString) Product category
                                                               price:price              // (NSNumber) Product price
                                                            quantity:[NSNumber numberWithInteger:product.qty]              // (NSInteger) Product quantity
                                                        currencyCode:@"HKD"] build]];
     }
        
    }
    
}

- (void)notLoginToRegisterPage {
    
    if (![Common shared].isLogin) {
        
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:[[MainRegisterViewController alloc] initWithNibName:@"MainRegisterViewController" bundle:nil] animated:YES enableUI:NO];
        
    }
    
}

- (void)notLoginToLoginPage {
    
    if (![Common shared].isLogin) {
        
        MainRegisterViewController *mainRegisterViewController = [[MainRegisterViewController alloc] initWithNibName:@"MainRegisterViewController" bundle:nil];
        [mainRegisterViewController addCommondRedView];
        [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:mainRegisterViewController animated:NO enableUI:NO];
         [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil] animated:YES enableUI:NO];
        
    }
    
}

+ (CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize {
    
    CGSize sizeOfText = [aString boundingRectWithSize:aSize
                                              options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                           attributes:[NSDictionary dictionaryWithObject:aFont
                                                                                  forKey:NSFontAttributeName]
                                              context:nil].size;
    
    return ceilf(sizeOfText.height);
    
}

- (BOOL)isLogin {
    
    if ([userDefaults boolForKey:@"isLogin"]) {
        
       return YES;
        
    } else {
        
       return NO;
        
    }
}

- (void)setIsLogin:(BOOL)isLogin {
    
    [userDefaults setBool:isLogin forKey:@"isLogin"];
    [userDefaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadLeftMenu object:nil];

}

@end
