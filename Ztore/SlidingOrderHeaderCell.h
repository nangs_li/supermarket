//
//  SlidingOrderHeaderCell.h
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlidingOrderHeaderCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *label_title;
@property(weak, nonatomic) IBOutlet UIButton *btn_delete;
@property(weak, nonatomic) IBOutlet UIButton *backBtn;

@end
