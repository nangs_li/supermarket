//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DeliveryAddress

@end

@interface DeliveryAddress : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *consignee;

@property(nonatomic, copy) NSString *contact_no;

@property(nonatomic, assign) NSInteger country_id;

@property(nonatomic, copy) NSString *mobile;

@property(nonatomic, assign) NSInteger district_id;

@property(nonatomic, copy) NSString *area;

@property(nonatomic, assign) NSInteger area_id;

@property(nonatomic, copy) NSString *address;

@property(nonatomic, assign) BOOL is_no_lift;

@property(nonatomic, copy) NSString *region;

@property(nonatomic, copy) NSString *district;

@property(nonatomic, assign) BOOL is_default;

@property(nonatomic, assign) NSInteger region_id;

@property(nonatomic, assign) NSInteger consignee_title;

@property(nonatomic, copy) NSString *country;

@property(nonatomic, copy) NSString *area_code;

@end

@interface DeliveryAddressData : REObject

@property(nonatomic, strong) NSArray<DeliveryAddress> *data;

@end
