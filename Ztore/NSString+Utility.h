//
//  NSString+Utility.h
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

- (NSString *)trim;
    
- (BOOL)isValidEmail;

- (NSNumber *)toNSNmuer;

- (NSString *)removeHtmlCode;

@end
