//
//  AgentListSectionHeaderView.m
//  productionreal2
//
//  Created by Ken on 8/10/15.
//  Copyright © 2015 Ken. All rights reserved.
//

#import "Common.h"
#import "ZtoreNavigationBar.h"

@implementation ZtoreNavigationBar

- (id)initFromXib {
    
    self                             = [[[NSBundle mainBundle] loadNibNamed:@"ZtoreNavigationBar" owner:self options:nil] objectAtIndex:0];
    self.contentView.backgroundColor = [[Common shared] getCommonRedColor];
    
    return self;
}

- (ZtoreNavigationBar *)rightMenuBar {
    
    ZtoreNavigationBar *ztoreNavigationBar  = [self initFromXib];
    ztoreNavigationBar.leftBtn.hidden       = NO;
    ztoreNavigationBar.leftMenuImage.hidden = NO;
    ztoreNavigationBar.titleLabel.hidden    = NO;
    ztoreNavigationBar.titleLabel.font      = [[Common shared] getNormalTitleFont];
    ztoreNavigationBar.usePromoBtn.hidden   = NO;
    [ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Use Promo Code", nil) forState:UIControlStateNormal animation:NO];
    
    return ztoreNavigationBar;
}

- (ZtoreNavigationBar *)mainMenuBar {
    
    ZtoreNavigationBar *ztoreNavigationBar    = [self initFromXib];
    ztoreNavigationBar.leftBtn.hidden         = NO;
    ztoreNavigationBar.leftMenuImage.hidden   = NO;
    ztoreNavigationBar.titleLabel.hidden      = NO;
    ztoreNavigationBar.rightBtn.hidden        = NO;
    ztoreNavigationBar.rightMenuImage.hidden  = NO;
    ztoreNavigationBar.rightBtn2.hidden       = NO;
    ztoreNavigationBar.rightMenuImage2.hidden = NO;
    ztoreNavigationBar.titleLabel.font        = [[Common shared] getNormalTitleFont];
    [ztoreNavigationBar.leftMenuImage setImage:[UIImage imageNamed:@"newMenu"]];
    [ztoreNavigationBar.rightMenuImage setImage:[UIImage imageNamed:@"ic_shopping_cart_white_24pt"]];
    
    return ztoreNavigationBar;
}

- (UIImage *)dotImage {
    
    return [UIImage imageNamed:@"step_dot"];
}

- (ZtoreNavigationBar *)productSearchBarType {
    
    [self refreshNavigationBar];
    self.searchBar.hidden     = NO;
    self.voiceBtn.hidden      = NO;
    self.leftBtn.hidden       = NO;
    self.leftMenuImage.hidden = NO;
    
    return self;
}

- (ZtoreNavigationBar *)rightMenuBarType {
    
    [self refreshNavigationBar];
    self.titleLabel.hidden    = NO;
    self.titleLabel.font      = [[Common shared] getNormalTitleFont];
    self.usePromoBtn.hidden   = NO;
    [self.usePromoBtn setTitle:JMOLocalizedString(@"Use Promo Code", nil) forState:UIControlStateNormal animation:NO];
    
    return self;
}

- (ZtoreNavigationBar *)productDetailBarType {
    
    [self refreshNavigationBar];
    self.titleLabel.hidden     = NO;
    self.rightBtn.hidden       = NO;
    self.rightMenuImage.hidden = NO;
    self.titleLabel.font       = [[Common shared] getNormalTitleFont];
    
    return self;
}

- (ZtoreNavigationBar *)leftMenuBarType {
    
    [self refreshNavigationBar];
    self.leftMenuTitle.hidden = NO;
    self.leftMenuTitle.font   = [[Common shared] getNormalTitleFont];
    
    return self;
}

- (ZtoreNavigationBar *)mainMenuBarType {
    
    [self refreshNavigationBar];
    self.titleLabel.hidden      = NO;
    self.rightBtn.hidden        = NO;
    self.rightMenuImage.hidden  = NO;
    self.rightBtn2.hidden       = NO;
    self.rightMenuImage2.hidden = NO;
    self.titleLabel.font        = [[Common shared] getNormalTitleFont];
    [self.leftMenuImage setImage:[UIImage imageNamed:@"newMenu"]];
    [self.rightMenuImage setImage:[UIImage imageNamed:@"ic_shopping_cart_white_24pt"]];
    [self.rightMenuImage2 setImage:[UIImage imageNamed:@"ic_search_white_24pt"]];
    
    return self;
}

- (ZtoreNavigationBar *)oneCheckOutBarType {
    
    [self checkOutBarType];
    [self.oneBtn setUpCheckOutBiggerButtonFromTitleText:@"1"];
    [self.oneImageView setImage:[self dotImage]];
    self.oneImageView.backgroundColor = [UIColor clearColor];
    [self.twoImageView setImage:[self dotImage]];
    self.twoImageView.backgroundColor = [UIColor clearColor];
    [self.threeImageView setImage:[self dotImage]];
    self.threeImageView.backgroundColor = [UIColor clearColor];
    self.usePromoBtn.hidden = NO;
    
    return self;
}

- (ZtoreNavigationBar *)twoCheckOutBarType {
    
    [self checkOutBarType];
    [self.twoBtn setUpCheckOutBiggerButtonFromTitleText:@"2"];
    [self.oneBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.oneImageView setImage:nil];
    self.oneImageView.backgroundColor = [UIColor whiteColor];
    [self.twoImageView setImage:[self dotImage]];
    self.twoImageView.backgroundColor = [UIColor clearColor];
    [self.threeImageView setImage:[self dotImage]];
    self.threeImageView.backgroundColor = [UIColor clearColor];

    return self;
}

- (ZtoreNavigationBar *)threeCheckOutBarType {
    
    [self checkOutBarType];
    [self.threeBtn setUpCheckOutBiggerButtonFromTitleText:@"3"];
    [self.oneBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.twoBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.oneImageView setImage:nil];
    self.oneImageView.backgroundColor = [UIColor whiteColor];
    [self.twoImageView setImage:nil];
    self.twoImageView.backgroundColor = [UIColor whiteColor];
    [self.threeImageView setImage:[self dotImage]];
    self.threeImageView.backgroundColor = [UIColor clearColor];
    
    return self;
}

- (ZtoreNavigationBar *)fourCheckOutBarType {
    
    [self checkOutBarType];
    [self.fourBtn setUpCheckOutBiggerButtonFromTitleText:@"4"];
    [self.oneBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.twoBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.threeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.oneImageView setImage:nil];
    self.oneImageView.backgroundColor = [UIColor whiteColor];
    [self.twoImageView setImage:nil];
    self.twoImageView.backgroundColor = [UIColor whiteColor];
    [self.threeImageView setImage:nil];
    self.threeImageView.backgroundColor = [UIColor whiteColor];
    
    return self;
}

- (ZtoreNavigationBar *)checkOutBarType {
    
    [self refreshNavigationBar];
    self.checkOutBarView.hidden          = NO;
    self.checkOutBarView.backgroundColor = [UIColor clearColor];
    [self.oneBtn setUpCheckOutButtonFromTitleText:@"1"];
    [self.twoBtn setUpCheckOutButtonFromTitleText:@"2"];
    [self.threeBtn setUpCheckOutButtonFromTitleText:@"3"];
    [self.fourBtn setUpCheckOutButtonFromTitleText:@"4"];
    self.titleLabel.hidden = YES;
    self.usePromoBtn.hidden = YES;
    
    return self;
}

- (ZtoreNavigationBar *)loginMenuBarType {
    
    [self refreshNavigationBar];
    self.usePromoBtn.titleLabel.font = [[Common shared] getContentNormalFont];
    self.titleLabel.hidden = NO;
    self.usePromoBtn.hidden = NO;
    [self.usePromoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return self;
}

- (void)refreshNavigationBar {
    
    [self.contentView hideAllSubView];
    [self.contentView removeAllSubViewAction];
    [self.leftBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.contentView setBackgroundColor:[[Common shared] getCommonRedColor]];
    [self.leftMenuImage setImage:[UIImage imageNamed:@"ic_arrow_back_white"]];
    [AppDelegate getAppDelegate].mainPageNavigationController.navigationBar.barTintColor = [[Common shared] getCommonRedColor];
    [self showLeftBtn];
    
}

- (void)showLeftBtn {
    
    self.leftBtn.hidden                  = NO;
    self.leftMenuImage.hidden            = NO;

}

@end
