//
//  REAgentProfile.m
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentProfile.h"
#import "REDBAgentProfile.h"

@implementation REAgentProfile

//- (REAgentProfile *)toREDBObject {
//    REAgentProfile *dbAgentProfile = [[REAgentProfile alloc] init];
//    dbGooglePlaceObejct.placeId = self.placeId;
//    dbGooglePlaceObejct.name = self.name;
//
//    return dbGooglePlaceObejct;
//}

- (REDBAgentProfile *)toREDBObject {
  REDBAgentProfile *dbAgentProfile = [[REDBAgentProfile alloc] init];
  dbAgentProfile.FollowerCount     = self.FollowerCount;
  dbAgentProfile.Specialties       = (RLMArray<DBSpecialties * ><DBSpecialties> *)[RLMArray arrayWithNSMutableArray:self.Specialties objectClass:[DBSpecialties class]];
  dbAgentProfile.Version           = self.Version;
  dbAgentProfile.FollowingCount    = self.FollowingCount;
  dbAgentProfile.LicenseNumber     = self.LicenseNumber;
  dbAgentProfile.IsFollowing       = self.IsFollowing;
  dbAgentProfile.Languages         = (RLMArray<DBLanguages * ><DBLanguages> *)[RLMArray arrayWithNSMutableArray:self.Languages objectClass:[DBLanguages class]];
  dbAgentProfile.QBID              = self.QBID;
  dbAgentProfile.MemberType        = self.MemberType;
  dbAgentProfile.PhotoURL          = self.PhotoURL;
  dbAgentProfile.MemberID          = self.MemberID;
  dbAgentProfile.MemberName        = self.MemberName;
  dbAgentProfile.Experiences       = (RLMArray<DBExperiences * ><DBExperiences> *)[RLMArray arrayWithNSMutableArray:self.Experiences objectClass:[DBExperiences class]];
  dbAgentProfile.PastClosings      = (RLMArray<DBPastClosings * ><DBPastClosings> *)[RLMArray arrayWithNSMutableArray:self.PastClosings objectClass:[DBPastClosings class]];

  return dbAgentProfile;
}

- (NSString *)languageTagString {
  NSString *tagString  = @"";
  NSMutableArray *tags = [[NSMutableArray alloc] init];

  for (Languages2 *lang in self.Languages) {
    if ([lang.Language valid]) {
      [tags addObject:lang.Language];
    }
  }

  tagString = [tags componentsJoinedByString:@"・"];

  return tagString;
}

- (NSString *)specialtyTagString {
  NSString *tagString  = @"";
  NSMutableArray *tags = [[NSMutableArray alloc] init];

  for (Specialties *specialty in self.Specialties) {
    if ([specialty.Specialty valid]) {
      [tags addObject:specialty.Specialty];
    }
  }

  tagString = [tags componentsJoinedByString:@"・"];

  return tagString;
}

@end

@implementation Languages2

- (DBLanguages *)toREDBObject {
  DBLanguages *dbLanguages = [[DBLanguages alloc] init];
  dbLanguages.Language     = self.Language;
  dbLanguages.LangIndex    = self.LangIndex;

  return dbLanguages;
}

@end

@implementation Experiences

- (DBExperiences *)toREDBObject {
  DBExperiences *dbExperiences = [[DBExperiences alloc] init];
  dbExperiences.IsCurrentJob   = self.IsCurrentJob;
  dbExperiences.EndDate        = self.EndDate;
  dbExperiences.Title          = self.Title;
  dbExperiences.Company        = self.Company;
  dbExperiences.StartDate      = self.StartDate;

  return dbExperiences;
}

- (NSString *)getPeriodString {
  NSDateFormatter *dateFormat = [NSDateFormatter localeDateFormatter];
  NSDate *startDate         = [dateFormat dateFromString:self.StartDate];
  NSDate *endDate           = [dateFormat dateFromString:self.EndDate];
  NSString *startDateString = [dateFormat stringFromDate:startDate];
  NSString *endDateString   = [dateFormat stringFromDate:endDate];

  return [NSString stringWithFormat:@"%@ - %@", startDateString, endDateString];
}

@end

@implementation Specialties

- (DBSpecialties *)toREDBObject {
  DBSpecialties *dbSpecialties = [[DBSpecialties alloc] init];
  dbSpecialties.Specialty      = self.Specialty;

  return dbSpecialties;
}

@end

@implementation PastClosings

- (DBPastClosings *)toREDBObject {
  DBPastClosings *dbPastClosings = [[DBPastClosings alloc] init];
  dbPastClosings.PropertyType    = self.PropertyType;
  dbPastClosings.SoldDate        = self.SoldDate;
  dbPastClosings.State           = self.State;
  dbPastClosings.SoldDateString  = self.SoldDateString;
  dbPastClosings.Address         = self.Address;
  dbPastClosings.Country         = self.Country;
  dbPastClosings.SoldDate        = self.SoldDate;
  dbPastClosings.Position        = self.Position;
  dbPastClosings.ID              = self.ID;
  dbPastClosings.SpaceType       = self.SpaceType;

  return dbPastClosings;
}

@end
