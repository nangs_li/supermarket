//
//  REAPIManager.h
//  real-v2-ios
//
//  Created by Derek Cheung on 26/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

// Controllers
#import "REBackgroundAPIClient.h"

@interface REAPIManager : NSObject

+ (nonnull NSURLSessionDataTask *)loginWithSocialNetworkType:(RESocialNetworkType)socialNetworkType
                                                    userName:(nonnull NSString *)userName
                                                      userID:(nonnull NSString *)userID
                                                       email:(nonnull NSString *)email
                                                    photoURL:(nullable NSString *)photoURL
                                                 accessToken:(nullable NSString *)accessToken
                                                  completion:(nullable REAPICommonBlock)completionBlock
                                                    progress:(nullable REAPIProgressBlock)progressBlock;

+ (nonnull NSURLSessionTask *)addressSearchWithPlaceId:(nullable NSString *)placeId completion:(nullable REAPICommonBlock)completionBlock;

+ (nonnull NSURLSessionTask *)agentProfileGetWithMemberIDs:(nonnull NSArray *)memberIDs completion:(nullable REAPICommonBlock)completionBlock;

+ (nonnull NSURLSessionTask *)agentLisingGetWithMemberIDs:(nonnull NSArray *)listingIDs completion:(nullable REAPICommonBlock)completionBlock;

@end
