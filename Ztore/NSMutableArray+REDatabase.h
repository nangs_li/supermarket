//
//  NSMutableArray+REDatabase.h
//  real-v2-ios
//
//  Created by Derek Cheung on 25/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

// Framework
#import <Realm/Realm.h>

@interface NSMutableArray (REDatabase)

+ (NSMutableArray *)arrayWithRLMArray:(RLMArray *)array;

@end
