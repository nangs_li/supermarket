
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "MainPageBottomView.h"

@interface MainPageBottomView : UIView

@property(weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property(weak, nonatomic) IBOutlet UIImageView *bottomImageView;
@property(weak, nonatomic) IBOutlet UIButton *bottomBtn;

@end
