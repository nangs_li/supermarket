//
//  NSString+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface UIButton (Utility)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing;
- (void)setUpDefaultTitleFontSize;
- (void)setUpDefaultDetailTextFontSize;
- (void)setUpAutoScaleButton;
- (void)setUpCircleButton;
- (void)setUpLeftMenuButton;
- (void)setUpLeftSelectedMenuButton;
- (void)setUpCornerBorderButton;
- (void)setUpSmallCornerBorderButton;
- (void)setUpForgetPasswordBtn;
- (void)setCancelUseBtnFromTitleText:(NSString *)titleText;
- (void)setAttributedBtnFromTitleText:(NSString *)titleText;
- (void)makeRightImageView;
- (void)setUpGreyCircleButton;
- (void)setUpWhiteBottomBtnFromTitleText:(NSString *)titleText;
- (void)setUpCheckOutButtonFromTitleText:(NSString *)titleText;
- (void)setUpCheckOutBiggerButtonFromTitleText:(NSString *)titleText;
- (void)setUpSelectedDayBtn;
- (void)setTitle:(NSString *)title forState:(UIControlState)state animation:(BOOL)animation;
- (void)setUpWhiteBorderButton;
- (void)setUpRedBorderButton;
- (void)setAttributedTitle:(NSAttributedString *)title forState:(UIControlState)state animation:(BOOL)animation;

@end
