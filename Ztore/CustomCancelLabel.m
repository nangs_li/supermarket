//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "CustomCancelLabel.h"
#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomCancelLabel

#pragma mark App LifeCycle

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self commonInit];
        
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self commonInit];
        
    }
    
    return self;
}

- (void)commonInit {
    
    [[NSBundle mainBundle] loadNibNamed:@"CustomCancelLabel" owner:self options:nil];
    
    [self addSubview:self.contentView];
    [self.contentView matchSizeWithSuperViewFromAutoLayout];
    
}

- (void)setUpViewFromTitleText:(NSString *)titleText feeText:(NSString *)feeText labelType:(LabelType)labelType haveCancelBtn:(BOOL)haveCancelBtn discount:(Discounts *)discount haveCheckOutBtn:(BOOL)haveCheckOutBtn {
    
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    self.titleLabel.font           = [[Common shared] getContentNormalFont];
    self.titleLabel.text = titleText;
    self.titleLabel.textColor = [UIColor blackColor];
    self.feeLabel.font           = [[Common shared] getContentBoldFont];
    self.feeLabel.text = feeText;
    self.feeLabel.textColor = [UIColor blackColor];
    [self.cancelBtn setCancelUseBtnFromTitleText:JMOLocalizedString(@"Cancel Use", nil)];
    self.cancelBtn.hidden = !haveCancelBtn;
    self.labelType = labelType;

    if (haveCancelBtn) {
        
        if (isValid(discount)) {
            
            if ([discount.code isEqualToString:@"FRIEND"]) {
                
                self.cancelBtn.hidden = YES;
                
            } else {
                
                self.cancelBtn.hidden = NO;
                
            }
            
        } else {
            
            self.cancelBtn.hidden = NO;
        }
        
    } else {
        
        self.cancelBtn.hidden = YES;
        
        for (UIGestureRecognizer *recognizer in self.titleLabel.gestureRecognizers) {
            
            [self.titleLabel removeGestureRecognizer:recognizer];
            
        }
        
        if (labelType == UsePromoCode || labelType == UseZDollar) {
         
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(pressUseBtn:)];
            self.titleLabel.userInteractionEnabled = YES;
            [self.titleLabel addGestureRecognizer:tapGesture];
            [self setAttributeTextFromLabel:self.titleLabel underline:(haveCheckOutBtn) ? YES : NO text:titleText];

        } else {
            
            [self setAttributeTextFromLabel:self.titleLabel underline:NO text:titleText];
            
        }
    }
    
    if (!haveCheckOutBtn) {
        
        self.cancelBtn.hidden = YES;
    }
    
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    if ([self.cancelBtnDelegate respondsToSelector:@selector(pressCancelBtnFromLabelType:)]) {
        [self.cancelBtnDelegate pressCancelBtnFromLabelType:self.labelType];
    }
    
}

- (void)setAttributeTextFromLabel:(UILabel *)label underline:(BOOL)underline text:(NSString *)text {
    
    UIColor *color = (underline) ? [[Common shared] getCommonRedColor] :  [UIColor blackColor];
    
    NSDictionary *attrDict           = @{NSFontAttributeName : [[Common shared] getContentNormalFont], NSForegroundColorAttributeName : color};
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:text attributes:attrDict];
    
    if (underline) {
        
        [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [text length])];
        
    }
    
    [label setAttributedText:title];
    
}

- (void)pressUseBtn:(UITapGestureRecognizer *)tapGesture {
    
    if ([self.cancelBtnDelegate respondsToSelector:@selector(pressUseBtnFromLabelType:)]) {
        [self.cancelBtnDelegate pressUseBtnFromLabelType:self.labelType];
    }

}

@end
