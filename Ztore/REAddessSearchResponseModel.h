//
//  REAddessSearchResponseModel.h
//  real-v2-ios
//
//  Created by Ken on 15/7/2016.
//  strongright © 2016 ken. All rights reserved.
//

#import "REAgentListing.h"
#import "REAgentProfile.h"
#import "REServerResponseModel.h"

@protocol ListingAddress

@end

@interface ListingAddress : REObject

@property(nonatomic, strong) NSString *Address;

@property(nonatomic, strong) NSString *Lang;

@end

@protocol AgentObject

@end

@interface AgentObject : REObject

@property(nonatomic, assign) int LID;

@property(nonatomic, strong) NSArray<ListingAddress> *Address;

@property(nonatomic, assign) int LVersion;

@property(nonatomic, assign) int MVersion;

@property(nonatomic, assign) int MID;

@end

@interface REAddessSearchResponseModel : REServerResponseModel

@property(nonatomic, strong) NSString *Record;

@property(nonatomic, strong) NSMutableArray<REAgentProfile *> *AgentProfiles;

@property(nonatomic, strong) NSMutableArray<REAgentListing *> *AgentListings;

@property(nonatomic, strong) NSMutableArray<AgentObject> *FullRecordSet;

@property(nonatomic, strong) NSString *List;

- (NSMutableArray<NSString *> *)mIDArrayFromFullRecordSet;

- (NSMutableArray<NSString *> *)LIDArrayFromFullRecordSet;

@end