//
//  REDBAPIQueue.m
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REDBAPIQueue.h"

// Models
#import "REAPIQueue.h"

@implementation REDBAPIQueue

+ (NSString *)primaryKey {
  return @"queueId";
}

+ (NSArray *)indexedProperties {
  return @[ @"queueId" ];
}

- (REAPIQueue *)toREObject {
  REAPIQueue *apiQueue     = [[REAPIQueue alloc] init];
  apiQueue.queueId         = self.queueId;
  apiQueue.httpMethod      = self.httpMethod;
  apiQueue.dependingQueues = [NSMutableArray arrayWithRLMArray:self.dependingQueues];
  apiQueue.pendingQueues   = [NSMutableArray arrayWithRLMArray:self.pendingQueues];
  apiQueue.retryCount      = self.retryCount;
  NSError *error;
  apiQueue.response  = [[REServerResponseModel alloc] initWithString:self.response error:&error];
  apiQueue.url       = self.url;
  apiQueue.parameter = [NSDictionary dictionaryFromJsonString:self.parameter];
  apiQueue.status    = self.status;

  return apiQueue;
}

@end
