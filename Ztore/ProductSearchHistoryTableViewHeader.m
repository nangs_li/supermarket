//
//  ProductSearchHistoryTableViewHeader.m
//  Ztore
//
//  Created by ken on 18/7/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductSearchHistoryTableViewHeader.h"

@implementation ProductSearchHistoryTableViewHeader

@synthesize label_leftText;

- (id)initWithFrame:(CGRect)frame {
    
  self = [super initWithFrame:frame];

  if (self) {
      
    label_leftText           = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, frame.size.width, frame.size.height)];
    [self addSubview:label_leftText];
      
  }

  return self;
}

- (void)setLabelHidden:(BOOL)hidden {
    
    label_leftText.text      = JMOLocalizedString(@"CLEAR RECENT SEARCHES", nil);
    label_leftText.textColor = [[Common shared] getCommonGrayColor];
    label_leftText.font      = [[Common shared] getSmallContentNormalFont];
    [self setHidden:hidden];
    [self setBackgroundColor:[[Common shared] getTableViewSeparatorGreyBackgroundColor]];
    
}

- (void)setLabelText:(NSString *)text {
    
    label_leftText.text      = text;
    label_leftText.font      = [[Common shared] getSmallContentNormalFont];
    label_leftText.textColor = [[Common shared] getCommonRedColor];
    [self setBackgroundColor:[[Common shared] getTableViewSeparatorGreyBackgroundColor]];
    
}

+ (ProductSearchHistoryTableViewHeader *)initHeader {
    
    return [[ProductSearchHistoryTableViewHeader alloc] initWithFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, 36)];
}

@end
