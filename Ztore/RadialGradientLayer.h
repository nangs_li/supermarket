//
//  RadialGradientLayer.h
//  productionreal2
//
//  Created by Ken on 17/11/2015.
//  Copyright © 2015 Ken. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface RadialGradientLayer : CALayer

@property(nonatomic, strong) NSArray *locations;
@property(nonatomic, strong) NSArray *scales;

@end
