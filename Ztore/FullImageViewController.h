//
//  FullImageViewController.h
//  Ztore
//
//  Created by ken on 24/9/15.
//  Copyright (c) ken. All rights reserved.
//

#import "_UIViewController.h"

@interface FullImageViewController : _UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property(retain, nonatomic) UIPageViewController *pageViewController_images;
@property(strong, nonatomic) UIPageControl *pageControl_images;
@property(strong, nonatomic) NSArray *imageUrls;

@end
