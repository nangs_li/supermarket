

//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductList.h"
#import <Foundation/Foundation.h>
@class Products;

@protocol ProductPrice

@end

@interface ProductPrice : REObject

@property(nonatomic, copy) NSString *rank_price;

@property(nonatomic, copy) NSString *standard_price;

@property(nonatomic, copy) NSString *promotion_price;

@end

@protocol Zoom

@end

@interface Zoom : REObject

@property(nonatomic, copy) NSString *xhdpi;

@property(nonatomic, copy) NSString *mdpi;

@property(nonatomic, copy) NSString *hdpi;

@property(nonatomic, copy) NSString *xxhdpi;

@end

@protocol Normal

@end

@interface Normal : REObject

@property(nonatomic, copy) NSString *xhdpi;

@property(nonatomic, copy) NSString *mdpi;

@property(nonatomic, copy) NSString *hdpi;

@property(nonatomic, copy) NSString *xxhdpi;

@end

@protocol ProductImages

@end

@interface ProductImages : REObject

@property(nonatomic, strong) Zoom *zoom;

@property(nonatomic, strong) Normal *normal;

@end

@protocol Products

@end

@interface ProductDetailModel : REObject

@property(nonatomic, strong) Products *data;

@end

@interface ProductDetailArray : REObject

@property(nonatomic, strong) NSArray<Products> *data;

- (void)hideAllPressAction;

- (NSArray<Products> *)removeAllNotAvailableProduct;

@end

@interface ProductAddWishList : REObject

@property(nonatomic, strong) NSString *data;

@end
