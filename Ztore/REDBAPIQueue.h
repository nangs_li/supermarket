//
//  REDBAPIQueue.h
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REDBObject.h"

@class REAPIQueue;

RLM_ARRAY_TYPE(REDBAPIQueue)

@interface REDBAPIQueue : REDBObject

@property(strong, nonatomic) NSString *queueId;
@property(assign, nonatomic) REHttpMethod httpMethod;
@property(strong, nonatomic) RLMArray<REDBAPIQueue *><REDBAPIQueue> *dependingQueues;
@property(strong, nonatomic) RLMArray<REDBAPIQueue *><REDBAPIQueue> *pendingQueues;
@property(assign, nonatomic) NSInteger retryCount;
@property(strong, nonatomic) NSString *response;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSString *parameter;
@property(assign, nonatomic) REAPIQueueStatus status;

- (REAPIQueue *)toREObject;

@end