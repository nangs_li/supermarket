//
//  Common.m
//  Ztore
//
//  Created by ken on 22/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AddProductObject.h"

@implementation AddProductObject
//
//+ (instancetype)shared {
//  static id instance_ = nil;
//  static dispatch_once_t onceToken;
//  dispatch_once(&onceToken, ^{
//    instance_ = [[self alloc] init];
//  });
//
//  return instance_;
//}

- (id)init {

  self                    = [super init];

  return self;
}

- (void)upBtnPressed:(id)sender {
    
    [self invalidAllTimer];
    [self increaseProducTimer:nil];
    
}

- (void)btnFinishPressd:(id)sender {
    
    [self invalidAllTimer];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCallAddProductAPIFromProductListChangeProductQty object:nil];
    
}

- (void)downBtnPressed:(id)sender {
    
    [self invalidAllTimer];
    [self decreaseProducTimer:nil];
    
}

- (void)addToCartPressed:(id)sender {
    
    [self upBtnPressed:self];
    [self btnFinishPressd:self];
    
}

- (void)increaseProducTimer:(NSTimer *)theTimer {
    
    [self addProductAPIWithAddCount:1];
}

- (void)decreaseProducTimer:(NSTimer *)theTimer {
    
    [self addProductAPIWithAddCount:-1];
}

- (void)invalidAllTimer {
    
    if ([self.delegate respondsToSelector:@selector(invalidAllTimer)]) {
        [self.delegate invalidAllTimer];
    }
}

- (void)addProductAPIWithAddCount:(int)addCount {
    
    if ([self.delegate respondsToSelector:@selector(addProductAPIWithAddCount:)]) {
        [self.delegate addProductAPIWithAddCount:addCount];
    }
    
}

- (void)upBtnLongPress:(UILongPressGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        DDLogDebug(@"UIGestureRecognizerStateBegan");
        [self invalidAllTimer];
        self.increaseProductTimer = [NSTimer scheduledTimerWithTimeInterval:INCREASE_PRODUCT_SPEED target:self selector:@selector(increaseProducTimer:) userInfo:[NSString stringWithFormat:@"%ld", (long)1] repeats:YES];
        
    } else if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateFailed) {
        
        DDLogDebug(@"UIGestureRecognizerStateEnded");
        [self btnFinishPressd:self];
        
    }
}

- (void)downBtnLongPress:(UILongPressGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        DDLogDebug(@"UIGestureRecognizerStateBegan");
        [self invalidAllTimer];
        self.increaseProductTimer = [NSTimer scheduledTimerWithTimeInterval:INCREASE_PRODUCT_SPEED target:self selector:@selector(decreaseProducTimer:) userInfo:[NSString stringWithFormat:@"%ld", (long) - 1] repeats:YES];
        
    } else if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateFailed) {
        
        DDLogDebug(@"UIGestureRecognizerStateEnded");
        [self btnFinishPressd:self];
        
    }
    
}

- (void)setUpAllAction {
    
    [self.upBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.upBtn addTarget:self action:@selector(upBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.upBtn addTarget:self action:@selector(btnFinishPressd:) forControlEvents:UIControlEventTouchUpInside];
    UILongPressGestureRecognizer *upBtnLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(upBtnLongPress:)];
    [self.upBtn addGestureRecognizer:upBtnLongPress];
    [self.downBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.downBtn addTarget:self action:@selector(downBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.downBtn addTarget:self action:@selector(btnFinishPressd:) forControlEvents:UIControlEventTouchUpInside];
    UILongPressGestureRecognizer *downBtnLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(downBtnLongPress:)];
    [self.downBtn addGestureRecognizer:downBtnLongPress];
    [self.btn_addToCart removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.btn_addToCart addTarget:self action:@selector(addToCartPressed:) forControlEvents:UIControlEventTouchUpInside];
    
}

@end
