//
//  Common.h
//  Ztore
//
//  Created by ken on 22/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Constants.h"
#import "ProductList.h"

@class AddProductObject;

@protocol AddProductObjectDelegate <NSObject>

- (void)addProductAPIWithAddCount:(int)addCount;
- (void)invalidAllTimer;

@end

@interface AddProductObject : NSObject

@property(strong, nonatomic) NSTimer *increaseProductTimer;
@property(strong, nonatomic) NSTimer *decreaseProductTimer;
@property(strong, nonatomic) id<AddProductObjectDelegate> delegate;
@property(strong, nonatomic) UIButton *downBtn;
@property(strong, nonatomic) UIButton *upBtn;
@property(strong, nonatomic) UIButton *btn_addToCart;

- (void)setUpAllAction;
- (void)btnFinishPressd:(id)sender;

@end
