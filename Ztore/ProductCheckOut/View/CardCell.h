//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeliveryAddress.h"
#import "CustomerCard.h"
#import "CustomTextFieldView.h"

@interface CardCell : UITableViewCell <UITextFieldDelegate>

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *lineLabel;
@property(weak, nonatomic) IBOutlet UIView *selectedView;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UIButton *editBtn;
@property(weak, nonatomic) IBOutlet UIView *cardView;
@property(strong, nonatomic) CustomTextFieldView *cardTextField;
@property(weak, nonatomic) IBOutlet UIView *greyLine;

- (void)setUpCardCellFromCustomerCard:(CustomerCard *)customerCard selected:(BOOL)selected;

@end
