//
//  SlidingOrderFooterCell.h
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ShoppingCartModel.h"
#import <UIKit/UIKit.h>
#import "SlidingOrderFooterCell.h"

@interface ConfirmOrderHeaderCell : UIView

@property(weak, nonatomic) IBOutlet UIView *editSpecialyRequestView;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestEditLabel;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestDetailLabel;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestUserInputLabel;
@property(weak, nonatomic) IBOutlet UIButton *specialyRequestEditBtn;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestLabel;

@property(weak, nonatomic) IBOutlet UIView *deliveryAddressView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *userInputAddressLabel;
@property(weak, nonatomic) IBOutlet UILabel *addressLabel;
@property(weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property(weak, nonatomic) IBOutlet UIButton *editBtn;
@property(weak, nonatomic) IBOutlet UILabel *label_TitleText;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryAddressTop;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *specialyRequestLabelBottom;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *specialyRequestUserInputBottom;

@property(strong, nonatomic) SlidingOrderFooterCell *slidingOrderFooterCell;

- (void)setupDataFromCart:(ShoppingCartData *)cart;

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress selected:(BOOL)selected;

- (void)setUpSpecialyRequestView;

- (void)setUpSpecialyRequestViewFromPaymentData:(PaymentSuccess *)paymentData;

@end
