//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ApplePayCell.h"
@import PassKit;

@implementation ApplePayCell
//
//- (id)initWithCoder:(NSCoder *)aDecoder {
//    
//    self = [super initWithCoder:aDecoder];
//    
//    if (self) {
//        
//        [self createApplePayBtn];
//    }
//    
//    return self;
//}

- (void)createApplePayBtn {
    
    BOOL pkPaymentButtonAvailable = NO;
    pkPaymentButtonAvailable = [PKPaymentButton class] != nil;
    
    if (pkPaymentButtonAvailable) {
        
        if (!isValid(self.applePayBtn)) {
            
            self.applePayBtn = [PKPaymentButton buttonWithType:PKPaymentButtonTypePlain style:PKPaymentButtonStyleBlack];
            [self addSubview:self.applePayBtn];
            
            [self.applePayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.equalTo(self.mas_centerY).with.offset(0);
                make.left.equalTo(self.mas_left).with.offset(24);
                
            }];
            
            [self.applePayBtn addTarget:self action:@selector(pressApplePayBtn) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
    }
    
}

- (void)pressApplePayBtn {
    
    if ([self.delegate respondsToSelector:@selector(applePayBtnPressedFromCell:)]) {
        [self.delegate applePayBtnPressedFromCell:self];
    }
    
}

@end
