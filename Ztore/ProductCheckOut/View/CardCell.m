//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "CardCell.h"

@implementation CardCell

- (void)setUpCardCellFromCustomerCard:(CustomerCard *)customerCard selected:(BOOL)selected {

    self.selectedView.hidden = !selected;
    [self.tickImageView setImage:(selected) ? [UIImage imageNamed:@"ic_done"] : [UIImage imageNamed:@"ic_done_grey"]];
    
    self.tickImageView.hidden = self.selectedView.hidden = NO;
    
    if (selected) {
        
        [self.tickImageView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
        [self.selectedView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
    } else if (!selected) {
        
        self.tickImageView.alpha = self.selectedView.alpha = 0;
        
    }

        self.cardTextField.textField.text = (selected) ? @"" : self.cardTextField.textField.text;
    NSString *cardNumber = [NSString stringWithFormat:@"%@ %@", customerCard.cardType, customerCard.maskedNumber];
    [self.titleLabel setUpTitleLabelFromText:cardNumber];

    if (self.cardView.subviews.count == 0) {
           self.cardTextField                       = [self.cardView addCustomTextFieldView];
        [self.cardTextField setFieldText:JMOLocalizedString(@"VERIFICATION CODE", nil) errorText:JMOLocalizedString(@"Please enter code", nil)];
        self.cardTextField.textField.delegate = self;
        [self.cardTextField setTextFieldCVCNumberType];
    }
  
    self.cardTextField.textField.text = [Common shared].selectedCard.cvc;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.greyLine.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.cardTextField.textField) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        int numberMaxLength = 0;
        numberMaxLength = (textField == self.cardTextField.textField) ? 4 : numberMaxLength;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return newLength <= numberMaxLength;
        
    } else {
        
        return YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
     [Common shared].selectedCard.cvc = self.cardTextField.textField.text;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
