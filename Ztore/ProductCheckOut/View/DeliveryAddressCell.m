//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "DeliveryAddressCell.h"

@implementation DeliveryAddressCell

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress selected:(BOOL)selected {

    [self.titleLabel setUpTitleLabelFromText:[NSString stringWithFormat:JMOLocalizedString(@"%@ Address", nil), deliveryAddress.consignee]];
    [self.lineLabel setBackgroundColor:[[Common shared] getLineColor]];
    self.tickImageView.hidden = self.selectedView.hidden = NO;
    
    if (selected) {
        
        [self.tickImageView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
        [self.selectedView fadeInCompletion:^(id response, NSError *error) {
            
        }];
    
    } else if (!selected) {
        
        self.tickImageView.alpha = self.selectedView.alpha = 0;
    
    }
    
  self.addressLabel.font                               = [[Common shared] getNormalFont];
  self.addressLabel.text                               = [NSString stringWithFormat:@"%@,%@,%@", deliveryAddress.country, deliveryAddress.district, deliveryAddress.area];
  self.userInputAddressLabel.font                      = [[Common shared] getNormalFont];
  self.userInputAddressLabel.text                      = deliveryAddress.address;
  self.phoneLabel.font                                 = [[Common shared] getNormalFont];
  self.phoneLabel.text                                 = [NSString stringWithFormat:@"%@ %@", JMOLocalizedString(@"Phone:", nil), deliveryAddress.mobile];
    
}

@end
