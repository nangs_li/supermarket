//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeliveryAddress.h"

@interface DeliveryAddressCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *userInputAddressLabel;
@property(weak, nonatomic) IBOutlet UILabel *addressLabel;
@property(weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property(weak, nonatomic) IBOutlet UIView *lineLabel;
@property(weak, nonatomic) IBOutlet UIView *selectedView;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UIButton *editBtn;

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress selected:(BOOL)selected;

@end
