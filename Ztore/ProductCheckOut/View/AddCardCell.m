//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AddCardCell.h"

@implementation AddCardCell

- (void)setUpAddCardCellSelected:(BOOL)selected {
    
    [self.titleLabel setUpTitleLabelFromText:JMOLocalizedString(@"Add a new payment credit card", nil)];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.greyLine.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    [self.tickImageView setImage:[UIImage imageNamed:@"ic_add_3x"]];
    self.backgroundColor = [UIColor whiteColor];
    
}

- (void)setUpCardCellFromCustomerCard:(CustomerCard *)customerCard selected:(BOOL)selected {
    
     [self.tickImageView setImage:(selected) ? [UIImage imageNamed:@"ic_done"] : [UIImage imageNamed:@"ic_done_grey"]];
    self.selectedView.hidden = !selected;
    self.tickImageView.hidden = NO;
    
    if (selected) {
        
        [self.tickImageView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
        [self.selectedView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
    }

    NSString *cardNumber = [NSString stringWithFormat:@"%@ %@", customerCard.cardType, customerCard.maskedNumber];
    [self.titleLabel setUpTitleLabelFromText:cardNumber];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.greyLine.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    self.backgroundColor = [UIColor whiteColor];
    
}

@end
