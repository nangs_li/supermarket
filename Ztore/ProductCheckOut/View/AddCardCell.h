//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeliveryAddress.h"

@interface AddCardCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *selectedView;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UIView *greyLine;

- (void)setUpAddCardCellSelected:(BOOL)selected;
- (void)setUpCardCellFromCustomerCard:(CustomerCard *)customerCard selected:(BOOL)selected;

@end
