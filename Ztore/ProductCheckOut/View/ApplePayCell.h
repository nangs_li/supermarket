//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ApplePayCell;

@protocol ApplePayCellDelegate <NSObject>

- (void)applePayBtnPressedFromCell:(ApplePayCell *)cell;

@end

@interface ApplePayCell : UITableViewCell

@property(strong, nonatomic) id<ApplePayCellDelegate> delegate;
@property(strong, nonatomic) UIButton *applePayBtn;

- (void)createApplePayBtn;

@end
