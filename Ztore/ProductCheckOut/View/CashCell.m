//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "CashCell.h"

@implementation CashCell

- (void)setUpCashCellSelected:(BOOL)selected {

    [self.tickImageView setImage:(selected) ? [UIImage imageNamed:@"ic_done"] : [UIImage imageNamed:@"ic_done_grey"]];
    
    self.tickImageView.hidden = self.selectedView.hidden = NO;
    
    if (selected) {
        
        [self.tickImageView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
        [self.selectedView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        
    } 

    self.greyLine.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    self.selectedView.hidden = !selected;
    [self.titleLabel setUpTitleLabelFromText:JMOLocalizedString(@"Cash On Delivery", nil)];
    self.waringLabel.textColor = [UIColor blackColor];
    self.waringLabel.text = JMOLocalizedString(@"No change, Hong Kong Dollar only. The upper limit of purchase is HK$1000.", nil);
    self.waringLabel.font = [[Common shared] getSmallContentNormalFont];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
