//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "DeliveryAddressFooterCell.h"

@implementation DeliveryAddressFooterCell

- (void)awakeFromNib {
  // Initialization code
     [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)setUpDeliveryAddress {
    
    [self.titleLabel setUpTitleLabelFromText:JMOLocalizedString(@"Add New Delivery Address", nil)];
    
}

@end
