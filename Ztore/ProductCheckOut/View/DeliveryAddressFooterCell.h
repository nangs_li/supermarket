//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryAddressFooterCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setUpDeliveryAddress;

@end
