//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeliveryAddress.h"

@interface CashCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *selectedView;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UILabel *waringLabel;
@property(weak, nonatomic) IBOutlet UIView *greyLine;

- (void)setUpCashCellSelected:(BOOL)selected;

@end
