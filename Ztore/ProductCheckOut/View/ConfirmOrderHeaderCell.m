//
//  SlidingOrderFooterCell.m
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ConfirmOrderHeaderCell.h"

@implementation ConfirmOrderHeaderCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress selected:(BOOL)selected {
    
    self.label_TitleText.text       = JMOLocalizedString(@"Confirm Your Order", nil);
    self.label_TitleText.font       = [[Common shared] getBiggerNormalFont];
    [self.titleLabel setUpTitleLabelFromText:[NSString stringWithFormat:JMOLocalizedString(@"%@ Address", nil), deliveryAddress.consignee]];
    self.addressLabel.font          = [[Common shared] getNormalFont];
    self.addressLabel.text          = [NSString stringWithFormat:@"%@,%@,%@", deliveryAddress.country, deliveryAddress.district, deliveryAddress.area];
    self.userInputAddressLabel.font = [[Common shared] getNormalFont];
    self.userInputAddressLabel.text = deliveryAddress.address;
    self.phoneLabel.font            = [[Common shared] getNormalFont];
    self.phoneLabel.text            = [NSString stringWithFormat:@"%@ %@", JMOLocalizedString(@"Phone:", nil), deliveryAddress.mobile];
    
}

- (void)setUpSpecialyRequestView {
    
    [self.specialyRequestEditLabel setUpTitleLabelFromText:[[Common shared].selectedTimeslot getFullDateAndTimeString]];
    
    if ([Common shared].specialyRequest.specialyRequestUserInput.length > 0 || [Common shared].specialyRequest.isChoiceString1 || [Common shared].specialyRequest.isChoiceString2 || [Common shared].specialyRequest.isChoiceString3) {
        
        self.specialyRequestDetailLabel.text    = [[Common shared].specialyRequest getChoiceString];
        self.specialyRequestUserInputLabel.text = [[Common shared].specialyRequest getUserInputString];
        self.specialyRequestLabel.text          = [Common shared].specialyRequest.specialyRequestString;
        
    } else {
        
        self.specialyRequestLabel.text = nil;
        self.specialyRequestUserInputBottom.constant = 0;
        self.specialyRequestLabelBottom.constant = 12;
        
    }
    
    self.specialyRequestLabel.font          = [[Common shared] getSmallContentBoldFont];
    self.specialyRequestDetailLabel.font    = [[Common shared] getSmallContentNormalFont];
    self.specialyRequestUserInputLabel.font = [[Common shared] getSmallContentNormalFont];
}

- (void)setUpSpecialyRequestViewFromPaymentData:(PaymentSuccess *)paymentData {
    
    self.editBtn.hidden = self.specialyRequestEditBtn.hidden = YES;
    [self.specialyRequestEditLabel setUpTitleLabelFromText:[paymentData.data getFullDateAndTimeString]];
    
    if (![paymentData.data.remark isEqualToString:@""]) {
        
         self.specialyRequestLabel.text          = [Common shared].specialyRequest.specialyRequestString;
        self.specialyRequestUserInputLabel.text = paymentData.data.remark;
        
    } else {
        
        self.specialyRequestLabel.text = nil;
        self.specialyRequestUserInputLabel.text = nil;
        self.specialyRequestUserInputBottom.constant = 0;
        self.specialyRequestLabelBottom.constant = 12;
    }
 
    self.specialyRequestLabel.font          = [[Common shared] getSmallContentBoldFont];
    self.specialyRequestDetailLabel.font    = [[Common shared] getSmallContentNormalFont];
    self.specialyRequestUserInputLabel.font = [[Common shared] getSmallContentNormalFont];
    self.specialyRequestDetailLabel.hidden = YES;
  
}

- (void)setupDataFromCart:(ShoppingCartData *)cart {

    self.backgroundColor              = [[Common shared] getProductDetailDescGrayBackgroundColor];
    
    if (self.slidingOrderFooterCell) {
        
        [self.slidingOrderFooterCell removeFromSuperview];
    }
    
   NSArray *nib                = [[NSBundle mainBundle] loadNibNamed:@"SlidingOrderFooterCell" owner:self options:nil];
   self.slidingOrderFooterCell = [nib objectAtIndex:0];
   [self addSubview:self.slidingOrderFooterCell];
    
    if ([cart isKindOfClass:[PaymentSuccessData class]]) {
        
        PaymentSuccessData *paymentSuccessObject = (PaymentSuccessData *)cart;
        
        [self.slidingOrderFooterCell setupDataFromCart:cart rightMenuVC:nil userAccountData:paymentSuccessObject.user haveCheckOutBtn:NO] ;
        
        self.deliveryAddressTop.active = NO;
        
        [self.slidingOrderFooterCell mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.mas_top).with.offset(6);
            make.left.equalTo(self.mas_left).with.offset(0);
            make.right.equalTo(self.mas_right).with.offset(0);
            make.height.mas_equalTo(self.slidingOrderFooterCell.viewHeight);
            
            [self.deliveryAddressView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(self.slidingOrderFooterCell.mas_bottom).with.offset(6);
                
            }];
            
        }];
        
    } else {
        
    [self.slidingOrderFooterCell setupDataFromCart:cart rightMenuVC:nil userAccountData:[Common shared].userAccont.data haveCheckOutBtn:NO] ;
        
    [self.slidingOrderFooterCell mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.editSpecialyRequestView.mas_bottom).with.offset(6);
        make.left.equalTo(self.mas_left).with.offset(0);
        make.right.equalTo(self.mas_right).with.offset(0);
        make.height.mas_equalTo(self.slidingOrderFooterCell.viewHeight);
        
    }];
        
    }
    
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
     self.specialyRequestLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.specialyRequestLabel.frame);
    self.specialyRequestEditLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.specialyRequestEditLabel.frame);
    self.specialyRequestUserInputLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.specialyRequestUserInputLabel.frame);
    self.specialyRequestDetailLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.specialyRequestDetailLabel.frame);
    
}

@end
