
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import "CustomTextFieldView.h"
#import "TimeslotData.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface TimeslotViewController : _UIViewController <UIScrollViewDelegate>

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIButton *dateBtn;
@property(weak, nonatomic) IBOutlet UIView *line;
@property(weak, nonatomic) IBOutlet UIButton *timeBtn;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel1;
@property(weak, nonatomic) IBOutlet UILabel *toDayLabel;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel2;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel3;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel4;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel5;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel6;
@property(weak, nonatomic) IBOutlet UILabel *dayLabel7;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn1;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn2;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn3;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn4;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn5;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn6;
@property(weak, nonatomic) IBOutlet UIButton *dayBtn7;
@property(weak, nonatomic) IBOutlet UIView *addSpecialyRequestView;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestLabel;
@property(weak, nonatomic) IBOutlet UIButton *specialyRequestBtn;
@property(weak, nonatomic) IBOutlet UIView *editSpecialyRequestView;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestEditLabel;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestDetailLabel;
@property(weak, nonatomic) IBOutlet UILabel *specialyRequestUserInputLabel;
@property(weak, nonatomic) IBOutlet UIButton *specialyRequestEditBtn;
@property(weak, nonatomic) IBOutlet UIButton *nextBtn;
@property(weak, nonatomic) IBOutlet UIView *dateView;
@property(weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *dateViewTop;
@property(strong, nonatomic) Timeslot *firstMonthTimeSlot;
@property(strong, nonatomic) Timeslot *secondMonthTimeSlot;
@property(assign, nonatomic) BOOL hideDateView;

@end
