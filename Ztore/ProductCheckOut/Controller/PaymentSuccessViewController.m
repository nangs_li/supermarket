//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "PaymentSuccessViewController.h"
#import "OrderHistoryViewController.h"

@implementation PaymentSuccessViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
  [[Common shared] cleanAllCheckOutData];
  [super setPageName:@"Checkout - Successful"];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Back Shopping", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Order Record", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];

}

- (void)setupView {
    
    self.tickImageView.layer.cornerRadius = self.tickImageView.frame.size.width / 2;
    self.tickImageView.clipsToBounds = YES;
    self.paymentSuccessLabel.font            = [[Common shared] getNormalFontWithSize:24];
    self.paymentSuccessLabel.textColor       = [[Common shared] getCommonRedColor];
    self.paymentSuccessLabel.text = JMOLocalizedString(@"Thank you for your payment", nil);
    self.orderNoLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Order Number: %@", nil), [Common shared].paymentSuccess.data.order_sn];
    self.orderNoLabel.font = [[Common shared] getNormalFontWithSize:18];
    
    NSDictionary *termNormalAttributeDict =  @{ NSForegroundColorAttributeName : [UIColor blackColor] };
    NSMutableAttributedString *termString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", JMOLocalizedString(@"You can check the details at", nil)] attributes:termNormalAttributeDict];
    NSMutableAttributedString *policyLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"Order Record", nil) attributes:@{ @"Order Record" : @(YES),NSForegroundColorAttributeName:  [[Common shared] getCommonRedColor],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:policyLinkString];
    
    NSMutableAttributedString *termString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", JMOLocalizedString(@" or ", nil)] attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString2];
    NSMutableAttributedString *serviceLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"Continue Shopping", nil) attributes:@{ @"Continue Shopping" : @(YES),NSForegroundColorAttributeName:  [[Common shared] getCommonRedColor],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:serviceLinkString];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    [termString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [termString length])];
    self.termText = termString;
    [self.linkTextView setAttributedText:self.termText];
    self.linkTextView.font = [[Common shared] getNormalFont];
    UITapGestureRecognizer *textViewTapGesureeRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTapped:)];
    [self.linkTextView addGestureRecognizer:textViewTapGesureeRecognizer];
    self.commentLabel.font = [[Common shared] getNormalFont];
    self.commentLabel.text = JMOLocalizedString(@"You may also review your shopping experience with Ztore in . Order Record after receiving the goods. Thank you.", nil);
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    [self.linkTextView setViewHeight:([[Common shared] isEnLanguage]) ? 70 : 150 ];
    self.linkTextViewLeading.constant = self.linkTextViewTrailing.constant = (gt568h) ? 80 : 40 ;
        
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id didTapTermOfService = [self.termText attribute:@"Order Record" atIndex:characterIndex effectiveRange:&range];
        id didTapPrivacyPolicy = [self.termText attribute:@"Continue Shopping" atIndex:characterIndex effectiveRange:&range];
        // Handle as required...
        if (didTapPrivacyPolicy) {
            [self backBtnPressed:self];
            
        } else if (didTapTermOfService) {
            
            [self showTermsOfService];
            
        }
        
    }
}

- (void)showTermsOfService {
    
    [self.view endEditing:YES];
    [self pressUsePromoBtn:self];
    
}

- (void)backBtnPressed:(id)sender {
    
    [self backToMainPageGetCurrentUser:NO];
    
}

- (void)pressUsePromoBtn:(id)sender {
    
    OrderHistoryViewController *orderHistoryViewController = [[OrderHistoryViewController alloc] initWithNibName:@"OrderHistoryViewController" bundle:nil];
    orderHistoryViewController.orderHistoryType = PaymentSuccessType;
    [self.navigationController pushViewController:orderHistoryViewController animated:YES enableUI:NO];
    
}

@end
