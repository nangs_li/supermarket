//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "SpecialyRequestViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation SpecialyRequestViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  [self setupView];
  [super setPageName:@"Checkout - Special Requirement"];
    
}

- (void)setupView {

  self.automaticallyAdjustsScrollViewInsets = NO;
  self.view.backgroundColor                 = [[Common shared] getProductDetailDescGrayBackgroundColor];

  self.specialyRequestTextField =  [self.specialyRequestView addCustomTextFieldView];
  [self.specialyRequestTextField setFieldText:JMOLocalizedString(@"Requests", nil) errorText:JMOLocalizedString(@"Please enter first name", nil)];
  [self.specialyRequestTextField setPlaceHolderFromText:JMOLocalizedString(@"If you have any requests, please fill in this column. We will try our best to help you.", nil)];
  self.specialyRequestTextField.textField.delegate              = self;
  self.specialyRequestTextField.backgroundColor                 = [UIColor clearColor];
  [[Common shared] setUpTickFromCheckBox:self.choiceView1];
  [[Common shared] setUpTickFromCheckBox:self.choiceView2];
  [[Common shared] setUpTickFromCheckBox:self.choiceView3];
  self.choiceString1.text = [Common shared].specialyRequest.choiceString1;
  self.choiceString2.text = [Common shared].specialyRequest.choiceString2;
  self.choiceString3.text = [Common shared].specialyRequest.choiceString3;

    [[self.choiceBtn1 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
         [self.choiceView1 setOn:!self.choiceView1.on animated:YES];
        
    }];
    
    [[self.choiceBtn2 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.choiceView2 setOn:!self.choiceView2.on animated:YES];
    }];
    
    [[self.choiceBtn3 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.choiceView3 setOn:!self.choiceView3.on animated:YES];
    }];
    
    
  [self.nextBtn setUpSmallCornerBorderButton];
  self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
  self.nextBtn.backgroundColor = [[Common shared] getCommonRedColor];
  [self.nextBtn setTitle:JMOLocalizedString(@"Confirm", nil) forState:UIControlStateNormal animation:NO];
  [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [Common shared].specialyRequest.isChoiceString1          = self.choiceView1.on;
    [Common shared].specialyRequest.isChoiceString2          = self.choiceView2.on;
    [Common shared].specialyRequest.isChoiceString3          = self.choiceView3.on;
    [Common shared].specialyRequest.specialyRequestUserInput = self.specialyRequestTextField.textField.text;
    [self backBtnPressed:self];
  }];

   [self reloadChoiceView];
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [Common getRightMenuBar];
  [self.ztoreNavigationBar rightMenuBarType];
    [self.view addSubview:self.ztoreNavigationBar];
    [self.ztoreNavigationBar matchSizeWithNavigationBarAutoLayout];
  self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Special Requirement", nil);
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = NO;
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Clear", nil) forState:UIControlStateNormal animation:NO];
       [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(usePromoBtnPress) forControlEvents:UIControlEventTouchDown];

}

- (void)usePromoBtnPress {
 
    [Common shared].specialyRequest.isChoiceString1          = NO;
    [Common shared].specialyRequest.isChoiceString2          = NO;
    [Common shared].specialyRequest.isChoiceString3          = NO;
    [Common shared].specialyRequest.specialyRequestUserInput = nil;
    [self reloadChoiceView];
    
}

- (void)reloadChoiceView {
    
    self.specialyRequestTextField.textField.text = [Common shared].specialyRequest.specialyRequestUserInput;
    self.choiceView1.on                          = [Common shared].specialyRequest.isChoiceString1;
    self.choiceView2.on                          = [Common shared].specialyRequest.isChoiceString2;
    self.choiceView3.on                          = [Common shared].specialyRequest.isChoiceString3;
    [self.choiceView1 reload];
    [self.choiceView2 reload];
    [self.choiceView3 reload];
    
}

- (void)backBtnPressed:(id)target {

  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    
    if (range.length + range.location > textField.text.length) {
        
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return newLength <= 200;
}

@end
