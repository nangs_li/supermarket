
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "DeliveryAddress.h"

typedef NS_ENUM (NSInteger, DeliveryAddressType) {
    Add,
    Edit
};

@interface AddDeliveryAddressViewController : _UIViewController <UIScrollViewDelegate>

@property(strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIView *titleView;
@property(strong, nonatomic) IBOutlet UIView *firstNameView;
@property(strong, nonatomic) IBOutlet UIView *address1View;
@property(strong, nonatomic) IBOutlet UIView *address2View;
@property(strong, nonatomic) IBOutlet UIView *address3View;
@property(strong, nonatomic) IBOutlet UIView *addressDetailView;
@property(strong, nonatomic) IBOutlet BEMCheckBox *checkView;
@property(strong, nonatomic) IBOutlet UIButton *checkBtn;
@property(strong, nonatomic) IBOutlet UILabel *checkLabel;
@property(strong, nonatomic) IBOutlet UILabel *liftWarning;
@property(strong, nonatomic) IBOutlet UIView *phoneView;
@property(strong, nonatomic) IBOutlet UIView *OtherPhoneView;
@property(strong, nonatomic) CustomTextFieldView *titleTextField;
@property(strong, nonatomic) CustomTextFieldView *firstNameTextField;
@property(strong, nonatomic) CustomTextFieldView *address1TextField;
@property(strong, nonatomic) CustomTextFieldView *address2TextField;
@property(strong, nonatomic) CustomTextFieldView *address3TextField;
@property(strong, nonatomic) CustomTextFieldView *addressDetailTextField;
@property(strong, nonatomic) CustomTextFieldView *phoneTextField;
@property(strong, nonatomic) CustomTextFieldView *OtherPhoneTextField;
@property(strong, nonatomic) IBOutlet UILabel *waringLabel;
@property(strong, nonatomic) IBOutlet UIButton *nextBtn;
@property(strong, nonatomic) DeliveryAddress *deliveryAddress;
@property(assign, nonatomic) DeliveryAddressType deliveryAddressType;

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress;

@end
