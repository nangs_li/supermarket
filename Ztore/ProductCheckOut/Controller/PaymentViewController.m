//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "DeliveryAddressCell.h"
#import "DeliveryAddressFooterCell.h"
#import "PaymentViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "TimeslotViewController.h"
#import "DeliveryAddressViewController.h"
#import "AddCreditCardViewController.h"
#import "CashCell.h"
#import "AddCardCell.h"
#import <Braintree/BraintreeCard.h>
#import <Braintree/BTCard.h>
#import "BraintreePayPal.h"
#import "PaymentSuccessViewController.h"
#import "ConfirmOrderViewController.h"
#import "RightMenuVC.h"

@import PassKit;

@implementation PaymentViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.hidden = YES;
    self.canMakePaymentsUsingNetworks = [self canMakePaymentsUsingNetworks];
    self.addRowCount = (self.canMakePaymentsUsingNetworks) ? 4 : 3;
    [super setPageName:@"Checkout - Payment"];
    [self setUpNextBtn];
    [self loadingFromColor:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [[REAPIClient sharedClient] setLongTimeOutInterval];

}

- (void)viewWillDisappear:(BOOL)animated {
    
    [[REAPIClient sharedClient] setShortTimeOutInterval];
    
}

- (void)callAPI {
    
    [self setupView];
    [self callCustomerCardAPI];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar fourCheckOutBarType];
    
    [self.ztoreNavigationBar.oneBtn addTarget:self action:@selector(oneBtnPressed) forControlEvents:UIControlEventTouchDown];
    
    [self.ztoreNavigationBar.twoBtn addTarget:self action:@selector(oneBtnPressed) forControlEvents:UIControlEventTouchDown];
    
    [self.ztoreNavigationBar.threeBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];

}

- (void)oneBtnPressed {
    
      [self popToViewController:[DeliveryAddressViewController class]];
    
}

- (void)twoBtnPressed {
    
    [self popToViewController:[TimeslotViewController class]];
    
}

- (void)setupView {
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    [[self.titleBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.view endEditing:YES];
        
    }];
    
    self.titleLabel.text = JMOLocalizedString(@"Choose Payment Method", nil);
    self.tableView.delegate                   = self;
    self.tableView.dataSource                 = self;
    self.view.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    self.tableView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    [self setUpNextBtn];
    self.totalFreeLabel.text = JMOLocalizedString(@"Total", nil);
    self.totalFreeLabel.font = [[Common shared] getSmallContentNormalFont];
    self.totalFreeLabel.textColor = [[Common shared] getCommonRedColor];
    self.totalFree.text =   [[Common shared] getStringFromPriceString:[Common shared].shoppingCartData.total_price];
    self.totalFree.font = [[Common shared] getNormalFontWithSize:30];
    
    [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        if (self.selectedIndexPath!= nil) {
            
        if (self.selectedIndexPath.row == [self cashCellRow]) {
            
             [self showLoadingHUD];
            
            [[ServerAPI shared] createOrderWithCoDPaymentCompletionBlock:^(id response, NSError *error) {
                
                [self hideLoadingHUD];
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                    
                     [Common shared].paymentSuccess = response;
                     [self pushPaymentSuccessViewController];
                } else {
                    
                    [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"createOrder"];
                }
                
            }];
            
        } else if (self.selectedIndexPath.row == [self addCardCellRow]) {
            
            
        } else {
            
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];

            if ([cell isKindOfClass:[CardCell class]]) {
            
                CardCell *selectedCardCell = (CardCell *)cell;
                
            if ([selectedCardCell.cardTextField checkValid] && [Common shared].selectedCard) {
                
                [self showLoadingHUD];
                
                                [[ServerAPI shared] verifyExistCreditCardFromCard_id:[Common shared].selectedCard.id cvv:selectedCardCell.cardTextField.textField.text CompletionBlock:^(id response, NSError *error) {
                                    
                                    if ([[Common shared] checkNotError:error controller:self response:response]) {
                                        
                                    CardID *cardID = response;
                                    [Common shared].selectedCard.cardholderName = [Common shared].selectedCard.cardholderName;
                                    [Common shared].selectedCard.id = cardID.data;
                                    [Common shared].selectedCard.cvc = selectedCardCell.cardTextField.textField.text;
                                    [Common shared].is_store_card = true;
                                    
                                    [[ServerAPI shared] createOrderWithPaymentByPaymentMethodCompletionBlock:^(id response, NSError *error) {
                                        
                                        [self hideLoadingHUD];
                                        
                                        if ([[Common shared] checkNotError:error controller:self response:response]) {
                                            
                                            [Common shared].paymentSuccess = response;
                                            [self pushPaymentSuccessViewController];
                                        } else {
                                            
                                            [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"createOrder"];
                                        }
                                        
                                    }];
                                        
                                    };
                                } ];
                                
                
            }
            
                
  
        } else {
            
            
        }
            
        }
            
        } else {
            
             [self showErrorFromMessage:JMOLocalizedString(@"Please Select Payment Method", nil) controller:self okBtn:nil];
          
        }
    }];

}

- (void)setUpNextBtn {
    
    [self.nextBtn setTitle:JMOLocalizedString(@"Next", nil) forState:UIControlStateNormal animation:NO];
    [self.nextBtn setUpSmallCornerBorderButton];
    self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    self.nextBtn.backgroundColor = [[Common shared] getCommonRedColor];
    
}

- (void)backToViewController {
    

        if ([Common shared].backToViewController == [PaymentSuccessViewController class]) {
            
            [Common shared].backToViewController = nil;
            
            [self pushPaymentSuccessViewController];
            
        } else {
            
            [super backToViewController];
            
        }

    
    
}

- (void)pushPaymentSuccessViewController {

     [self.navigationController pushViewController:[[PaymentSuccessViewController alloc] init] animated:YES enableUI:NO];
    
}

#pragma mark - ApplePay

- (void)applePayBtnPressedFromCell:(ApplePayCell *)cell {
    
    [self tappedApplePay:cell.applePayBtn];
}

- (void)tappedApplePay:(id)sender {
    
    self.btAPIClient =  (self.btAPIClient == nil) ? [[BTAPIClient alloc] initWithAuthorization:[Common shared].clientToken] : self.btAPIClient;
    self.applePayClient =  (self.applePayClient == nil) ?  [[BTApplePayClient alloc] initWithAPIClient:self.btAPIClient] : self.applePayClient;
    [self.applePayClient paymentRequest:^(PKPaymentRequest *paymentRequest, NSError *error) {
        
        if (error) {
             DDLogDebug(@"error : %@", error.localizedDescription);
            
            return;
        }
        
        // Requiring PKAddressFieldPostalAddress crashes Simulator
        paymentRequest.merchantIdentifier = @"merchant.com.ztore.ztore";
        paymentRequest.requiredBillingAddressFields = PKAddressFieldNone;
        paymentRequest.currencyCode = @"HKD";
        paymentRequest.countryCode = @"HK";

        paymentRequest.requiredShippingAddressFields = PKAddressFieldNone;
        paymentRequest.paymentSummaryItems = @[
                                               [PKPaymentSummaryItem summaryItemWithLabel:@"Ztore HK Limited" amount:[NSDecimalNumber decimalNumberWithString:[Common shared].shoppingCartData.total_price]]
                                               ];
        
        paymentRequest.merchantCapabilities = PKMerchantCapability3DS;
        
        if ([paymentRequest respondsToSelector:@selector(setShippingType:)]) {
            paymentRequest.shippingType = PKShippingTypeDelivery;
        }
        
        PKPaymentAuthorizationViewController *viewController = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
        viewController.delegate = self;
        
        if (!self.canMakePaymentsUsingNetworks) {
            NSLog(@"没有绑定支付卡");
            return;
        }
        
        [self presentViewController:viewController animated:YES completion:nil];
    }];
    
}

#pragma mark PKPaymentAuthorizationViewControllerDelegate

- (void)paymentAuthorizationViewController:(__unused PKPaymentAuthorizationViewController *)controller
                   didSelectShippingMethod:(PKShippingMethod *)shippingMethod
                                completion:(void (^)(PKPaymentAuthorizationStatus, NSArray<PKPaymentSummaryItem *> *))completion {
    
    PKPaymentSummaryItem *testItem = [PKPaymentSummaryItem summaryItemWithLabel:@"SOME ITEM" amount:[NSDecimalNumber decimalNumberWithString:@"10"]];
    
    if ([shippingMethod.identifier isEqualToString:@"fast"]) {
        completion(PKPaymentAuthorizationStatusSuccess,
                   @[
                     testItem,
                     [PKPaymentSummaryItem summaryItemWithLabel:@"SHIPPING" amount:shippingMethod.amount],
                     [PKPaymentSummaryItem summaryItemWithLabel:@"BRAINTREE" amount:[testItem.amount decimalNumberByAdding:shippingMethod.amount]],
                     ]);
    } else if ([shippingMethod.identifier isEqualToString:@"fail"]) {
        completion(PKPaymentAuthorizationStatusFailure, @[testItem]);
    } else {
        completion(PKPaymentAuthorizationStatusSuccess, @[testItem]);
    }
}

- (void)paymentAuthorizationViewController:(__unused PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus status))completion {
    DDLogDebug(@"Apple Pay Did Authorize Payment");
    
    [self.applePayClient tokenizeApplePayPayment:payment completion:^(BTApplePayCardNonce *tokenizedApplePayPayment, NSError *error) {
        if (error) {
             DDLogDebug(@"error : %@", error.localizedDescription);
            completion(PKPaymentAuthorizationStatusFailure);
        } else {
        
            [self showLoadingHUD];
            
            [[ServerAPI shared] createOrderWithPaymentMethodFromPaymentCode:@"APPLEPAY" paymentMethodNonce:tokenizedApplePayPayment.nonce completionBlock:^(id response, NSError *error) {
                
                [self hideLoadingHUD];
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                    
                    [Common shared].paymentSuccess = response;
                    [self pushPaymentSuccessViewController];
                } else {
                    
                    [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"createOrder"];
                }
                
            }];
         
            completion(PKPaymentAuthorizationStatusSuccess);
        }
    }];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)paymentAuthorizationViewControllerWillAuthorizePayment:(__unused PKPaymentAuthorizationViewController *)controller {
    DDLogDebug(@"Apple Pay will Authorize Payment");
}

- (BOOL)canMakePaymentsUsingNetworks {
    
    NSArray *supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkMasterCard,PKPaymentNetworkVisa,PKPaymentNetworkChinaUnionPay];

    if (![PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:supportedNetworks]) {
        NSLog(@"没有绑定支付卡");
    }

    return [PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:supportedNetworks];

}

#pragma mark - PayPal

- (IBAction)customPayPalButtonTapped:(id)sender {
    
    [self showLoadingHUD];
    
    self.btAPIClient =  (self.btAPIClient == nil) ? [[BTAPIClient alloc] initWithAuthorization:[Common shared].clientToken] : self.btAPIClient;
    BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.btAPIClient];
    payPalDriver.viewControllerPresentingDelegate = self;

    [payPalDriver authorizeAccountWithCompletion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error) {
        
        if (tokenizedPayPalAccount) {
            
            [[ServerAPI shared] createOrderWithPaymentMethodFromPaymentCode:@"PAYPAL" paymentMethodNonce:tokenizedPayPalAccount.nonce completionBlock:^(id response, NSError *error) {
                
                 [self hideLoadingHUD];
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                    
                    [Common shared].paymentSuccess = response;
                    [self pushPaymentSuccessViewController];
                } else {
                    
                    [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"createOrder"];
                }
                
            }];

            
            DDLogDebug(@"tokenizedPayPalAccount : %@", tokenizedPayPalAccount);

        } else if (error) {
            
             [self hideLoadingHUD];
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                            style:0 handler:^(UIAlertAction *action) {
                                                                
                                                                
                                                            }];
            
            [self showErrorFromMessage:isValid(error.localizedDescription) ? error.localizedDescription : @"PayPalError" controller:self okBtn:okBtn];
            DDLogDebug(@"error : %@", error.localizedDescription);
            
        } else {
            
             [self hideLoadingHUD];
             DDLogDebug(@"Canceled 🔰");
        }
    }];

}

#pragma mark - BTViewControllerPresentingDelegate

- (void)paymentDriver:(__unused id)driver requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)paymentDriver:(__unused id)driver requestsDismissalOfViewController:(UIViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)callCustomerCardAPI {
    
    [[ServerAPI shared] getCustomerCardCompletionBlock:^(id response, NSError *error) {
        
        self.tableView.hidden = NO;
        
          if ([[Common shared] checkNotError:error controller:self response:response]) {

                  if ([response getErrorCode] == NO_DATA) {
                      
                      [Common shared].customerCardArray = [[CardArray alloc] init];
                      [Common shared].customerCardArray.data = [[NSArray<CustomerCard> alloc] init];
                      self.tableView.hidden = NO;
                      [self.tableView reloadData];
                      
                  } else if ([response getErrorCode] == STATUS_OK) {
                      
                      [Common shared].customerCardArray = response;
                      [self.tableView reloadData];
                      
                  }
              
    
              
          } else {
              
              [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"getCustomerCard"];
          }

        
    }];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldEnd];
}

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSUInteger cardArrayCount = [Common shared].customerCardArray.data.count;
    
    return (cardArrayCount) ?  cardArrayCount + self.addRowCount : self.addRowCount;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    
    if (indexPath.row == [self cashCellRow]) {
        
        static NSString *cellIdentifier = @"CashCell";
        CashCell *cell       = (CashCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
        }

        [cell setUpCashCellSelected:(indexPath == self.selectedIndexPath)];
        
         return cell;
        
    } else if (indexPath.row == [self addCardCellRow]) {
        
        static NSString *cellIdentifier = @"AddCardCell";
        AddCardCell *cell       = (AddCardCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
        }
        
        [cell setUpAddCardCellSelected:(indexPath == self.selectedIndexPath)];
        cell.hidden = ([Common shared].customerCardArray.data.count >= 3) ? YES : NO;
        
         return cell;
        
    } else if (indexPath.row == [self paypalRow]) {
        
        static NSString *cellIdentifier = @"AddCardCell";
        AddCardCell *cell       = (AddCardCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
        }
        
        [cell setUpAddCardCellSelected:(indexPath == self.selectedIndexPath)];
        [cell.titleLabel setUpTitleLabelFromText:@"PayPal"];
        
        return cell;
        
    } else if (indexPath == self.selectedIndexPath) {
        
        static NSString *cellIdentifier = @"CardCell";
        CardCell *cell       = (CardCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
        }
        
        [cell setUpCardCellFromCustomerCard:[[Common shared].customerCardArray.data objectAtIndex:indexPath.row] selected:(indexPath == self.selectedIndexPath)];
        
            [cell.editBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
            [cell.editBtn addTarget:self action:@selector(editBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
        return cell;
        
    } else if (self.canMakePaymentsUsingNetworks && (indexPath.row == [self applePlayRow])) {
            
                static NSString *cellIdentifier = @"ApplePayCell";
                ApplePayCell *cell       = (ApplePayCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                
                if (cell == nil) {
     
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
                cell         = [nib objectAtIndex:0];
                [cell createApplePayBtn];
                cell.delegate = self;
                    
                }
        
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
                return cell;
   
        } else {
        
        
        static NSString *cellIdentifier = @"AddCardCell";
        AddCardCell *cell       = (AddCardCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
        }
        
        [cell setUpCardCellFromCustomerCard:[[Common shared].customerCardArray.data objectAtIndex:indexPath.row] selected:(indexPath == self.selectedIndexPath)];
        
        cell.hidden = NO;
        
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self cashCellRow]) {
        
        if ([[Common shared].shoppingCartData.discounted_subtotal intValue] > 1000) {
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                            style:0 handler:^(UIAlertAction *action) {
                                                                
                                                                
                                                            }];
            
            [self showErrorFromMessage:JMOLocalizedString(@"The upper limit of purchase is HK$1000", nil) controller:self okBtn:okBtn];
            
        } else {
        
        self.selectedIndexPath = indexPath;
        [Common shared].selectedCard = nil;
            
        }
 
    } else {
        
        if ([[Common shared].shoppingCartData.discounted_subtotal intValue] > 10000) {
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                            style:0 handler:^(UIAlertAction *action) {
                                                                
                                                                
                                                            }];
            
        [self showErrorFromMessage:JMOLocalizedString(@"The upper limit of purchase is HK$10000", nil) controller:self okBtn:okBtn];
            
        } else {
        
        if (indexPath.row == [self addCardCellRow]) {
    
        [self.navigationController presentViewController:[[AddCreditCardViewController alloc] init] animated:YES completion:nil];
        
    } else if (indexPath.row == [self paypalRow]) {
        
         [self customPayPalButtonTapped:self];
        
    } else if (self.canMakePaymentsUsingNetworks && (indexPath.row == [self applePlayRow])) {
        
        
    } else {
        
        self.selectedIndexPath = indexPath;
        [Common shared].selectedCard = [[Common shared].customerCardArray.data objectAtIndex:indexPath.row];
        
    }
            
    }
    
    }
    
    [self.view endEditing:YES];
    [self.tableView reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat height = 96;
    
    if (indexPath.row == [self cashCellRow]) {
        
        height = 143;
        
    } else if (indexPath.row == [self addCardCellRow]) {
        
        height = ([Common shared].customerCardArray.data.count >= 3)  ? 1 : 96;
        
    } else if (indexPath == self.selectedIndexPath) {
        
        height = 169;
        
    } else if (indexPath.row == [self applePlayRow]) {
        
        height = 96;
    }
    
  return height;
}

- (NSInteger)addCardCellRow {
    
    return [Common shared].customerCardArray.data.count;
}

- (NSInteger)cashCellRow {
    
     return [Common shared].customerCardArray.data.count + 1;
}

- (NSInteger)applePlayRow {
    
    return (self.canMakePaymentsUsingNetworks) ? [Common shared].customerCardArray.data.count + 3 : 42483284328  ;
}

- (NSInteger)paypalRow {
    
    return [Common shared].customerCardArray.data.count + 2;
    
}

- (void)editBtnPressed:(id)sender {
   
   [self showLoadingHUD];  
    
  [[ServerAPI shared] deleteCustomerCardFromCardId:[Common shared].selectedCard.id CompletionBlock:^(id response, NSError *error) {
      
      if ([[Common shared] checkNotError:error controller:self response:response]) {
       
          self.selectedIndexPath = nil;
          [Common shared].selectedCard = nil;
          [self callCustomerCardAPI];

      }
      
  }];
    
}

- (void)handleErrorFromErrorCode:(NSInteger)errorCode methodName:(NSString *)methodName {
    
    NSString *message;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                    style:0 handler:^(UIAlertAction *action) {
                                                        
                                                        
                                                    }];
    
    if (errorCode == CART_NOT_AVAILABLE) {
        message = JMOLocalizedString(@"Shopping cart no product", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                                [self popToViewController:[RightMenuVC class]];
                                             
                                         }];
        
    } else if (errorCode == ADDRESS_NOT_EXIST){
        message = JMOLocalizedString(@"Delivery Address not exist", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                              [self popToViewController:[DeliveryAddressViewController class]];
                                             
                                             
                                         }];
    } else if (errorCode == DELIVERY_TIME_INVALID){
        message = JMOLocalizedString(@"Delivery time invalid", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                               [self popToViewController:[TimeslotViewController class]];
                                             
                                             
                                         }];
    } else if (errorCode == CREATE_ORDER_FAIL){
        
        message = JMOLocalizedString(@"Create order fail", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                    [self popToViewController:[ConfirmOrderViewController class]];
                                             
                                             
                                         }];
    }
    
    [self showErrorFromMessage:message controller:self okBtn:okBtn];
}

- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    
}

- (void)appSwitcher:(id)appSwitcher didPerformSwitchToTarget:(BTAppSwitchTarget)target {
    
}

@end
