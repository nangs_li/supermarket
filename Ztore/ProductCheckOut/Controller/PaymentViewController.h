
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import "CardCell.h"
#import <Braintree/BraintreeCore.h>
#import <BraintreeApplePay.h>
#import "ApplePayCell.h"

@interface PaymentViewController : _UIViewController <UIScrollViewDelegate, UITableViewDelegate,UITableViewDataSource,BTViewControllerPresentingDelegate,BTAppSwitchDelegate,PKPaymentAuthorizationViewControllerDelegate, ApplePayCellDelegate>

@property(strong, nonatomic) IBOutlet UIButton *nextBtn;
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) IBOutlet UILabel *totalFreeLabel;
@property(strong, nonatomic) IBOutlet UILabel *totalFree;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(strong, nonatomic) NSIndexPath *selectedIndexPath;
@property(weak, nonatomic) IBOutlet UIButton *titleBtn;
@property(strong, nonatomic) BTAPIClient *btAPIClient;
@property(strong, nonatomic) BTApplePayClient *applePayClient;
@property(assign, nonatomic) BOOL canMakePaymentsUsingNetworks;
@property(assign, nonatomic) int addRowCount;

@end
