
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface SpecialyRequestViewController : _UIViewController <UIScrollViewDelegate>

@property(weak, nonatomic) IBOutlet UIButton *nextBtn;
@property(weak, nonatomic) IBOutlet UIView *specialyRequestView;
@property(weak, nonatomic) IBOutlet BEMCheckBox *choiceView1;
@property(weak, nonatomic) IBOutlet UILabel *choiceString1;
@property(weak, nonatomic) IBOutlet UIButton *choiceBtn1;
@property(weak, nonatomic) IBOutlet BEMCheckBox *choiceView2;
@property(weak, nonatomic) IBOutlet UIButton *choiceBtn2;
@property(weak, nonatomic) IBOutlet UILabel *choiceString2;
@property(weak, nonatomic) IBOutlet BEMCheckBox *choiceView3;
@property(weak, nonatomic) IBOutlet UILabel *choiceString3;
@property(weak, nonatomic) IBOutlet UIButton *choiceBtn3;
@property(strong, nonatomic) CustomTextFieldView *specialyRequestTextField;

@end
