
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"

typedef NS_ENUM(NSInteger, EditType) { EditInCheckOut, EditInMyAccount};

@interface DeliveryAddressViewController : _UIViewController <UIScrollViewDelegate, UITableViewDelegate,UITableViewDataSource>

@property(strong, nonatomic) IBOutlet UIButton *nextBtn;
@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(assign, nonatomic) EditType editType;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(strong, nonatomic) NSTimer *selectDeliveryAddressTimer;

@end
