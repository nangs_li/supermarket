//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "EmailLoginViewController.h"
#import "MainRegisterViewController.h"
#import "ForgetPasswordViewController.h"

@implementation EmailLoginViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupView];
    self.view.backgroundColor = self.centerView.backgroundColor = [[Common shared] getCommonRedColor];
    [super setPageName:@"Login with Email"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar loginMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
    
}

- (void)setupView {
    
    self.titleLabel.text = JMOLocalizedString(@"Login with Email", nil);
     [self.titleLabel setFont:[[Common shared] getNormalFontWithSize:33]];
     [self.bottomBtn setUpWhiteBottomBtnFromTitleText:JMOLocalizedString(@"Login", nil)];
    
    self.emailTextField = [self.emailView addCustomTextFieldView];
    self.emailView.backgroundColor = [UIColor clearColor];
    [self.emailTextField setTextFieldLoginEmailType];
    self.emailTextField.textField.delegate              = self;
    self.passwordTextField = [self.passwordView addCustomTextFieldView];
    self.passwordView.backgroundColor = [UIColor clearColor];
    [self.passwordTextField setTextFieldPasswordType];
    self.passwordTextField.textField.delegate              = self;
    
    [self.forgetPasswordBtn setUpForgetPasswordBtn];
    [self.forgetPasswordBtn setTitle:JMOLocalizedString(@"Forget Password", nil) forState:UIControlStateNormal animation:NO];
    [self.forgetPasswordBtn sizeToFit];

    [self.forgetPasswordBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(self.forgetPasswordBtn.frame.size.width + 12);
        
    }];

    
    [self.view setNeedsLayout];
    
    [[self.forgetPasswordBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.navigationController pushViewController:[[ForgetPasswordViewController alloc] init] animated:YES enableUI:NO];
        
    }];

    
    [[self.bottomBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
    
        [self.emailTextField checkValid];
        [self.passwordTextField checkValid];
        
        if ([self.emailTextField checkValid]&&[self.passwordTextField checkValid]) {
        
        [self loadingFromColor:nil];
            
        [[ServerAPI shared] loginWithEmail:self.emailTextField.textField.text password:self.passwordTextField.textField.text completionBlock:^(id response, NSError *error) {
                           
                           if ([[Common shared] checkNotError:error controller:self response:response]) {
                               
                               [self backToMainPageGetCurrentUser:YES];
                               
                           }
                           
                       }];
            
             }
    
    }];


}

- (void)pressUsePromoBtn:(id)sender {
    
      [self popToViewController:[MainRegisterViewController class]];
    
}

@end
