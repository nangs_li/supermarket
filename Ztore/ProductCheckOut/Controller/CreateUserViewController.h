
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"

@interface CreateUserViewController : _UIViewController
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *emailView;
@property(weak, nonatomic) IBOutlet UIView *passwordView;
@property(strong, nonatomic) CustomTextFieldView *emailTextField;
@property(strong, nonatomic) CustomTextFieldView *passwordTextField;
@property(weak, nonatomic) IBOutlet UITextView *termTextView;
@property(weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property(weak, nonatomic) IBOutlet UIView *centerView;

@end
