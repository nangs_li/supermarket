
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface LoginViewController : _UIViewController <FBSDKLoginButtonDelegate>
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property(weak, nonatomic) IBOutlet UIButton *emailLoginBtn;
@property(weak, nonatomic) IBOutlet UIView *centerView;

@end
