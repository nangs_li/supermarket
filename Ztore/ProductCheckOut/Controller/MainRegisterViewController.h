
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface MainRegisterViewController : _UIViewController <FBSDKLoginButtonDelegate>

@property(weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property(weak, nonatomic) IBOutlet UIImageView *ztoreImageView;
@property(weak, nonatomic) IBOutlet UIButton *facebookLoginBtn;
@property(weak, nonatomic) IBOutlet UIButton *createUserBtn;
@property(weak, nonatomic) IBOutlet UITextView *termTextView;
@property(weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property(strong, nonatomic) UIView *commondRedView;

- (void)addCommondRedView;

@end
