//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MainRegisterViewController.h"
#import "LoginViewController.h"
#import "CreateUserViewController.h"
#import "FXBlurView.h"
#import "FacebookLogin.h"

@implementation MainRegisterViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupView];
    [super setPageName:@"Register"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [[AppDelegate getAppDelegate].mainPageNavigationController setNavigationBarHidden:NO];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    
     [self removeCommondRedView];
    
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [Common getRightMenuBar];
    [self.view addSubview:self.ztoreNavigationBar];
    [self.ztoreNavigationBar matchSizeWithNavigationBarAutoLayout];
    [self.ztoreNavigationBar loginMenuBarType];
    [self.ztoreNavigationBar.usePromoBtn setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
    [self.ztoreNavigationBar setBackgroundColor:[UIColor clearColor]];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backToMainPage) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.contentView setBackgroundColor:[UIColor clearColor]];

}

- (void)setupView {
    
    [self.bgImageView setImage:[[UIImage imageNamed:@"menu_bg"] blurredImageWithRadius:10.0f iterations:1 tintColor:[UIColor clearColor]]];
    self.welcomeLabel.text = JMOLocalizedString(@"Welcome to", nil);
     [self.facebookLoginBtn setTitle:JMOLocalizedString(@"Register with Facebook", nil) forState:UIControlStateNormal animation:NO];
    self.facebookLoginBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
    [self.createUserBtn setUpRedBorderButton];
    [self.createUserBtn setTitle:JMOLocalizedString(@"New User", nil) forState:UIControlStateNormal];
     self.createUserBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
    
    [[self.createUserBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
    
        [self.navigationController pushViewController:[[CreateUserViewController alloc] init] animated:YES enableUI:NO];
        
    }];
    
    UIColor *textColor = [UIColor blackColor];
    NSDictionary *termNormalAttributeDict =  @{ NSForegroundColorAttributeName : textColor };
    NSMutableAttributedString *termString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", JMOLocalizedString(@"I have READ and UNDERSTAND Ztore ", nil)] attributes:termNormalAttributeDict];
    NSMutableAttributedString *policyLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"General Terms & Conditions", nil) attributes:@{ @"General Terms & Conditions" : @(YES),NSForegroundColorAttributeName:  textColor,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:policyLinkString];
    
    NSMutableAttributedString *termString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", JMOLocalizedString(@" as well as ", nil)] attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString2];
    NSMutableAttributedString *serviceLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"Privacy Policy", nil) attributes:@{ @"Privacy Policy" : @(YES),NSForegroundColorAttributeName:  textColor,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:serviceLinkString];
    NSMutableAttributedString *termString3 = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@", and AGREE the use of my personal data for direct marketing purpose stated in the Privacy Policy.", nil) attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString3];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    [termString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [termString length])];
    self.termText = termString;
    [self.termTextView setAttributedText:self.termText];
    UITapGestureRecognizer *textViewTapGesureeRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTapped:)];
    [self.termTextView addGestureRecognizer:textViewTapGesureeRecognizer];
    [self.termTextView setViewHeight:([[Common shared] isEnLanguage]) ? 70 : 150 ];
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.readPermissions = @[@"email"];
    loginButton.delegate = self;
    [[self.facebookLoginBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    }];
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id didTapTermOfService = [self.termText attribute:@"General Terms & Conditions" atIndex:characterIndex effectiveRange:&range];
        id didTapPrivacyPolicy = [self.termText attribute:@"Privacy Policy" atIndex:characterIndex effectiveRange:&range];
        // Handle as required...
        if (didTapPrivacyPolicy) {
            [self showPrivacyPolicy];
            
        } else if (didTapTermOfService) {
            
            [self showTermsOfService];
            
        }
        
    }
}

- (void)pressUsePromoBtn:(id)sender {
    
    [self.navigationController pushViewController:[[LoginViewController alloc] init] animated:YES enableUI:NO];
}

#pragma mark FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
   
    if (result.token.tokenString) {
    
    [self showLoadingHUD];
    
    
    [[ServerAPI shared] fbLoginWithAccessToken:result.token.tokenString completionBlock:^(id response, NSError *error) {
        
        [self hideLoadingHUD];
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            FacebookLogin *fbLogin = response;
            
            if (fbLogin.data.confirm_password != nil) {
                
                if (!self.shoppingCartPopUpView) {
                    
                    self.shoppingCartPopUpView = [[[NSBundle mainBundle] loadNibNamed:@"ShoppingCartPopUpView" owner:self options:nil] objectAtIndex:0];
                    [self.shoppingCartPopUpView setupView];
                    [self.view addSubview:self.shoppingCartPopUpView];
                    [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
                        
                    }];
                    [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
                    
                    [[self.shoppingCartPopUpView.useBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
                        
                        [[ServerAPI shared] connectFbAccountWithEmail:fbLogin.data.confirm_password password:self.shoppingCartPopUpView.usePromoCodeTextField.textField.text accessToken:result.token.tokenString completionBlock:^(id response, NSError *error) {
                            
                             if ([[Common shared] checkNotError:error controller:self response:response]) {
                                 
                                  [self backToMainPageGetCurrentUser:NO];
                                 
                             }
                            
                        }];
                        
                    }];
                    
                    self.shoppingCartPopUpView.titleLabel.text = JMOLocalizedString(@"Your email has been registered on Ztore before, simply enter your password to link it with your existing Ztore account.", nil);
                    self.shoppingCartPopUpView.usePromoCodeTextField.fieldLabel.text = JMOLocalizedString(@"Password", nil);
                    self.shoppingCartPopUpView.usePromoCodeTextField.textField.secureTextEntry = YES;
                    self.shoppingCartPopUpView.backBtn.hidden = YES;
                    
                } else {
                    
                    [self.view addSubview:self.shoppingCartPopUpView];
                    [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
                        
                    }];
                    [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
                    
                }
                
            } else {
                
                [self backToMainPageGetCurrentUser:YES];
                
            }

        }
        
    }];
        
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

- (void)addCommondRedView {

    self.commondRedView = [[UIView alloc] init];
    [self.commondRedView setBackgroundColor:[[Common shared] getCommonRedColor]];
    [self.view addSubview:self.commondRedView];
    [self.commondRedView matchSizeWithSuperViewFromAutoLayout];
    
}

- (void)removeCommondRedView {
    
    if (self.commondRedView) {
        
        [self.commondRedView removeFromSuperview];
        
    }

}

@end
