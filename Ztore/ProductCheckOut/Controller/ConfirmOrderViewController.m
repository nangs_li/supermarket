//
//  RightMenuVC.m
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "NSError+RENetworking.h"
#import "ProductDetailController.h"
#import "RealUtility.h"
#import "RightMenuVC.h"
#import "ShoppingCartModel.h"
#import "SlidingOrderTableViewCell.h"
#import "CustomTextFieldView.h"
#import "ConfirmOrderViewController.h"
#import "DeliveryAddressViewController.h"
#import "TimeslotViewController.h"
#import "ConfirmOrderHeaderCell.h"
#import "OrderRatingViewController.h"

@interface ConfirmOrderViewController ( )

@end

@implementation ConfirmOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    headerHeight             = 44;
    footerHeight             = 120;
    rowHeight                = 120;
    commonFontSize           = 10;
    
    [self setUpViewForFooterInSection];
    [self setUpNextBtn];
    [self loadingFromColor:nil];
    [super setPageName:@"Checkout - Confirm"];
    self.myTableView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self backToViewController];

    self.nextBtn.hidden = (self.orderType == History) ? YES : NO;
    
}

- (void)callAPI {
    
    if (self.orderType == Current) {
        
        [self getClientToken];
        
        [[ServerAPI shared] cartAddProductFromProductArraysReturnCart:YES onlyGetShoppingCart:YES completionBlock:^(id response, NSError *error) {
            
            [self hideLoadingHUD];
            self.lockUI = NO;
            self.myTableView.hidden = NO;
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                if ([response getErrorCode] == STATUS_OK) {
                    
                    AddProductReturnShoppingCartModel *cart = (AddProductReturnShoppingCartModel *)response;
//                    cart.data.products = [[Common shared] getFreebieTypeProductFromProduct:cart.data.products];
                    [Common shared].shoppingCartData          = cart.data.cart;
                    [self.slidingOrderFooterCell setFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, 999)];
                    [self.slidingOrderFooterCell setUpDeliveryAddress:[Common shared].selectedDeliveryAddress selected:YES];
                    [self.slidingOrderFooterCell setUpSpecialyRequestView];
                    [self.slidingOrderFooterCell setupDataFromCart:[Common shared].shoppingCartData];
                    [self.slidingOrderFooterCell setNeedsLayout];
                    
                    self.myTableView.tableHeaderView = self.slidingOrderFooterCell;
                    
                    
                    self.myTableView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
                    [self.myTableView reloadData];
                    
                } else {
                    
                    if ([response getErrorCode] == NO_DATA) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShoppingCartNotData object:nil];
                        [Common shared].shoppingCartData = nil;
                        [self.slidingOrderFooterCell setupDataFromCart:[Common shared].shoppingCartData];
                        [self.myTableView reloadData];
                    }
                }

            }
            
            
        }];
        
    }
    
    if (self.orderType == History) {
        
        self.nextBtn.hidden = YES;
        [[ServerAPI shared] getCurrentUserPaidOrderFromOrder_id:self.orderId completionBlock:^(id response, NSError *error) {
            
            self.myTableView.hidden = NO;
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                self.paymentdata = response;
                self.paymentdata.data.rating.rating_comment = [self.paymentdata.data.rating.rating_comment removeHtmlCode];
                
                if ([self.paymentdata isKindOfClass:[PaymentSuccess class]]) {
                    
                    for (Products *product in self.paymentdata.data.products) {
                        product.cart_qty         = product.qty;
                        product.cart_freebie_qty = product.freebie_qty;
                        [product setUpProductType];
                    }
                    
                    PaymentSuccess *paymentData = (PaymentSuccess *)response;
                    
                    if ([(NSNumber *)paymentData.statusCodes[0] intValue] == STATUS_OK) {
                        
                        paymentData.data.products = [[Common shared] getFreebieTypeProductFromProduct:paymentData.data.products];
                        [self.slidingOrderFooterCell setFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, 999)];
                        [self.slidingOrderFooterCell setUpDeliveryAddress:(DeliveryAddress *)paymentData.data.address selected:YES];
                        [self.slidingOrderFooterCell setUpSpecialyRequestViewFromPaymentData:paymentData];
                        [self.slidingOrderFooterCell setupDataFromCart:(ShoppingCartData *)paymentData.data];
                        
                        [self.view setNeedsLayout];
                        
                        self.ztoreNavigationBar.titleLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Order#%@", nil), paymentData.data.order_sn];
                        self.myTableView.tableHeaderView = self.slidingOrderFooterCell;
                        self.myTableView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
                        [self.myTableView reloadData];
                        
                    } else {
                        
                        if ([paymentData getErrorCode] == NO_DATA) {
                            
                            [self.myTableView reloadData];
                        }
                    }

                    
                }
                
            }
            
        }];
        
        [self.myTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.myTableView.superview).with.offset(0);
            
        }];
    }

}

- (void)setUpNextBtn {
    
    [self.nextBtn setTitle:JMOLocalizedString(@"Next", nil) forState:UIControlStateNormal animation:NO];
    [self.nextBtn setUpSmallCornerBorderButton];

    [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        PaymentViewController *paymentViewController = [[PaymentViewController alloc] init];
        
        [self.navigationController pushViewController:paymentViewController animated:YES enableUI:NO];
        
    }];

    
}

- (void)setUpNavigationBar {

    if (self.orderType == Current) {

  self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

  [self.ztoreNavigationBar threeCheckOutBarType];
  [self.ztoreNavigationBar.oneBtn addTarget:self action:@selector(oneBtnPress) forControlEvents:UIControlEventTouchDown];
  [self.ztoreNavigationBar.twoBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
        
    }
    
      if (self.orderType == History) {
          
          self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
          [self.ztoreNavigationBar rightMenuBarType];
          [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
          [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Rating", nil) forState:UIControlStateNormal];
          self.ztoreNavigationBar.titleLabel.text = (self.paymentdata) ? [NSString stringWithFormat:JMOLocalizedString(@"Order#%@", nil), self.paymentdata.data.order_sn] : @"";
          [[self.ztoreNavigationBar.usePromoBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

                  OrderRatingViewController *orderRatingViewController = [[OrderRatingViewController alloc] init];
                  orderRatingViewController.paymentdata = self.paymentdata;
                  [self.navigationController pushViewController:orderRatingViewController animated:YES enableUI:NO];
              
          }];

          
      }
    
}

- (void)oneBtnPress {
    
     [self popToViewController:[DeliveryAddressViewController class]];
    
}

- (void)setUpViewForFooterInSection {
    
    if (self.orderType == Current) {
    
    static NSString *cellIdentifier = @"ConfirmOrderHeaderCell";
    
     if (self.slidingOrderFooterCell == nil) {
        NSArray *nib                = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        self.slidingOrderFooterCell = [nib objectAtIndex:0];
    }
    
    [[self.slidingOrderFooterCell.editBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        [self popToViewController:[DeliveryAddressViewController class]];
        
    }];

    
    [[self.slidingOrderFooterCell.specialyRequestEditBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
        
    }
    
     if (self.orderType == History) {
         
         static NSString *cellIdentifier = @"ConfirmOrderHeaderCell";
         
         if (self.slidingOrderFooterCell == nil) {
             NSArray *nib                = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
             self.slidingOrderFooterCell = [nib objectAtIndex:0];
         }

         
     }
}

- (void)getClientToken {
    
    [[ServerAPI shared] getBraintreeClientTokenCompletionBlock:^(id response, NSError *error) {
        
        if ([response isKindOfClass:[ClientToken class]]) {
      
              [Common shared].clientToken = ((ClientToken *)response).data;
            
        }
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.orderType == Current) {
        
       return [Common shared].shoppingCartData == nil ? 0 : [Common shared].shoppingCartData.products.count;
        
    }
    
    if (self.orderType == History) {
        
       return self.paymentdata == nil ? 0 : self.paymentdata.data.products.count;
        
    }
    
    return [Common shared].shoppingCartData == nil ? 0 : [Common shared].shoppingCartData.products.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Products *product               = (self.orderType == History) ? (Products *)self.paymentdata.data.products[indexPath.row] : (Products *)[Common shared].shoppingCartData.products[indexPath.row];
    
    static NSString *cellIdentifier = @"SlidingOrderTableViewCell";
    SlidingOrderTableViewCell *cell = (SlidingOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    [cell setUpOrderCellFromProduct:product ProductCellType: (self.orderType == History) ? ConfirmOrderHistoryType : ConfirmOrderType];
    cell.upBtn.hidden = YES;
    cell.downBtn.hidden = YES;
    cell.upBtn.tag   = indexPath.row;
    cell.downBtn.tag = indexPath.row;
    cell.tag         = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return rowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)viewDidLayoutSubviews {
    
    UIView *header = self.myTableView.tableHeaderView;
    
    [header setNeedsLayout];
    [header layoutIfNeeded];
    
    CGFloat height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = header.frame;
    
    frame.size.height = height;
    header.frame = frame;
    
    int viewHeight;
    
    if (self.orderType == Current) {
        
    viewHeight = self.slidingOrderFooterCell.slidingOrderFooterCell.frame.origin.y + self.slidingOrderFooterCell.slidingOrderFooterCell.viewHeight + 6;
        
    } else {
        
    viewHeight = [self.slidingOrderFooterCell.editSpecialyRequestView getViewYAddHeight] + 6;
        
    }
    
    
    if (self.slidingOrderFooterCell.frame.size.height != viewHeight) {
    
    [self.slidingOrderFooterCell setFrame:CGRectMake(0, 0, [RealUtility screenBounds].size.width, viewHeight)];
        
    }
    
    self.myTableView.tableHeaderView = header;
    
}

@end
