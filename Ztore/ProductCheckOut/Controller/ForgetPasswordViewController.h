
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"

@interface ForgetPasswordViewController : _UIViewController

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIView *emailView;
@property(strong, nonatomic) CustomTextFieldView *emailTextField;
@property(weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property(weak, nonatomic) IBOutlet UIView *centerView;

@end
