//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AddCreditCardViewController.h"

#import "AddressData.h"
#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>
#import <Braintree/BraintreeCore.h>
#import <Braintree/BraintreeCard.h>
#import <Braintree/BTCard.h>
#import "RightMenuVC.h"
#import "PaymentSuccessViewController.h"
#import "TimeslotViewController.h"
#import "ConfirmOrderViewController.h"

@implementation AddCreditCardViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    [self setupView];
    [super setPageName:@"Checkout - Add Credit Card"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [[REAPIClient sharedClient] setLongTimeOutInterval];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [[REAPIClient sharedClient] setShortTimeOutInterval];
    
}

- (void)setupView {
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.cardNameTextField = [self.cardNameView addCustomTextFieldView];
    [self.cardNameTextField setFieldText:JMOLocalizedString(@"CARDHOLDER NAME", nil) errorText:JMOLocalizedString(@"Please enter cardholder name", nil)];
    [self.cardNameTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter cardholder name", nil)];
    self.cardNameTextField.textField.delegate = self;
    
    self.cardNumberTextField = [self.cardNumberView addCustomTextFieldView];
    [self.cardNumberTextField setFieldText:JMOLocalizedString(@"CARD NUMBER", nil) errorText:JMOLocalizedString(@"Please enter card number", nil)];
    [self.cardNumberTextField setTextFieldCardNumberType];
    [self.cardNumberTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter card number", nil)];
    self.cardNumberTextField.textField.delegate              = self;
    
    self.expirationYearTextField  = [self.expirationYearView addCustomTextFieldView];
    [self.expirationYearTextField createDropDownMenuFromStringArray:@[@"2017", @"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",@"2027",@"2028",@"2029",@"2030"] placeholder:JMOLocalizedString(@"Please choose year", nil) dropDownMenutype:ExpirationYear];
    [self.expirationYearTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
    [self.expirationYearTextField setFieldText:JMOLocalizedString(@"EXPIRATION DATE", nil) errorText:JMOLocalizedString(@"Please choose year", nil)];
    self.expirationYearTextField.textField.delegate = self;
    self.expirationMonthTextField = [self.expirationMonthView addCustomTextFieldView];
    [self.expirationMonthTextField createDropDownMenuFromStringArray:@[ @"01", @"02", @"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12"] placeholder:JMOLocalizedString(@"Please choose month", nil) dropDownMenutype:ExpirationMonth];
    [self.expirationMonthTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
    [self.expirationMonthTextField setFieldText:@"" errorText:JMOLocalizedString(@"Please choose month", nil)];
    self.expirationMonthTextField.textField.delegate = self;
    self.cvcTextField                       = [self.cvcView addCustomTextFieldView];
    [self.cvcTextField setFieldText:JMOLocalizedString(@"VERIFICATION CODE", nil) errorText:JMOLocalizedString(@"Please enter code", nil)];
    self.cvcTextField.textField.delegate = self;
    [self.cvcTextField setTextFieldCVCNumberType];
    
    [self setUpTick];
    [self.checkBtn addTarget:self action:@selector(checkAnimationSelected) forControlEvents:UIControlEventTouchDown];
    [self.nextBtn setTitle:JMOLocalizedString(@"Save", nil) forState:UIControlStateNormal animation:NO];
    [self.nextBtn setUpSmallCornerBorderButton];
    self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    self.nextBtn.backgroundColor = [[Common shared] getCommonRedColor];
    
    [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.cardNameTextField checkValid];
        [self.cardNumberTextField checkValid];
        [self.expirationYearTextField checkValid];
        [self.expirationMonthTextField checkValid];
        [self.cvcTextField checkValid];
        
        if ([self.cardNameTextField checkValid] && [self.cardNumberTextField checkValid] && [self.expirationYearTextField checkValid] && [self.expirationMonthTextField checkValid] && [self.cvcTextField checkValid]) {
            
             [self showLoadingHUD];
            
                BTAPIClient  *apiClient = [[BTAPIClient alloc] initWithAuthorization:[Common shared].clientToken];
                BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:apiClient];
                BTCard *card = [[BTCard alloc] initWithNumber:self.cardNumberTextField.textField.text
                                              expirationMonth:self.expirationMonthTextField.textField.text
                                               expirationYear:self.expirationYearTextField.textField.text
                                                          cvv:self.cvcTextField.textField.text];
                
                [cardClient tokenizeCard:card completion:^(BTCardNonce *tokenized, NSError *error) {
                    
                    if (error == nil) {
                        
                        
                        [[ServerAPI shared] verifyNewCreditCardFromPayment_method_nonce:tokenized.nonce cardholder_name:self.cardNameTextField.textField.text CompletionBlock:^(id response, NSError *error) {
                            
                             if ([[Common shared] checkNotError:error controller:self response:response]) {
                            CardID *cardID = response;
                            [Common shared].selectedCardnew = [[CustomerCard alloc] init];
                            [Common shared].selectedCardnew.cardholderName = self.cardNameTextField.textField.text;
                            [Common shared].selectedCardnew.id = cardID.data;
                            [Common shared].selectedCardnew.cvc = self.cvcTextField.textField.text;
                            [Common shared].is_store_card = self.checkView.on;
                            
                             [[ServerAPI shared] createOrderWithPaymentWithNewCustomerCompletionBlock:^(id response, NSError *error) {
                                 
                                 [self hideLoadingHUD];
                                 
                                 if ([[Common shared] checkNotError:error controller:self response:response]) {
                                     
                                     [Common shared].paymentSuccess = response;
                                     [self dismissViewController:[PaymentSuccessViewController class]];
                                     
                                 } else {
                                     
                                     [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"createOrder"];
                                 }
                                 
                             }];
                                 
                             } 
                        } ];
                        
                    } else {
                        
                        [self hideLoadingHUD];
                    }
                    
                }];
    
        }
        
    }];
    
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [Common getRightMenuBar];
    [self.view addSubview:self.ztoreNavigationBar];
    [self.ztoreNavigationBar matchSizeWithNavigationBarAutoLayout];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Add a new payment credit card", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    self.ztoreNavigationBar.usePromoBtn.hidden = YES;
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(usePromoBtn) forControlEvents:UIControlEventTouchDown];
    
}

- (void)usePromoBtn {
    
    [[ServerAPI shared] deleteCurrentUserAddressFromAddressId:(int)self.deliveryAddress.id completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self backBtnPressed:self];
        }
        
    }];
    
}

- (void)backBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
    [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
    [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.cardNumberTextField.textField || textField == self.cvcTextField.textField) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        int numberMaxLength = 0;
        numberMaxLength = (textField == self.cardNumberTextField.textField) ? 16 : numberMaxLength;
        numberMaxLength = (textField == self.cvcTextField.textField) ? 4 : numberMaxLength;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return newLength <= numberMaxLength;
        
    } else {
        
        return YES;
    }
}

- (void)setUpTick {
    
    [[Common shared] setUpTickFromCheckBox:self.checkView];
    self.checkLabel.font    = [[Common shared] getNormalTitleFont];
    self.checkLabel.text    = JMOLocalizedString(@"Save the credit card information in my account (Not include Card Verification No.)", nil);
    self.checkLabel.textColor    = [UIColor blackColor];
    
}

- (void)checkAnimationSelected {
    
    self.checkView.tintColor = [UIColor lightGrayColor];
    [self.checkView setOn:!self.checkView.on animated:YES];
    
}

- (void)handleErrorFromErrorCode:(NSInteger)errorCode methodName:(NSString *)methodName {
 
    NSString *message;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                    style:0 handler:^(UIAlertAction *action) {
                                                        
                                                        
                                                    }];
    
    if (errorCode == CART_NOT_AVAILABLE) {
        message = JMOLocalizedString(@"Shopping cart no product", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                                [self dismissViewController:[RightMenuVC class]];
                                             
                                             
                                         }];
        
    } else if (errorCode == ADDRESS_NOT_EXIST){
        message = JMOLocalizedString(@"Delivery Address not exist", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                                [self dismissViewController:[DeliveryAddressViewController class]];
                                             
                                         }];
    } else if (errorCode == DELIVERY_TIME_INVALID){
        message = JMOLocalizedString(@"Delivery time invalid", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                                [self dismissViewController:[TimeslotViewController class]];
                                   

                                         }];
        
    } else if (errorCode == CREATE_ORDER_FAIL){
        
        message = JMOLocalizedString(@"Create order fail", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                               [self dismissViewController:[ConfirmOrderViewController class]];
                                             
                                             
                                         }];
    }
 
    [self showErrorFromMessage:message controller:self okBtn:okBtn];
}

@end
