
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "DeliveryAddress.h"

@interface AddCreditCardViewController : _UIViewController <UIScrollViewDelegate>

@property(strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIView *cardNameView;
@property(strong, nonatomic) IBOutlet UIView *cardNumberView;
@property(strong, nonatomic) IBOutlet UIView *expirationYearView;
@property(strong, nonatomic) IBOutlet UIView *expirationMonthView;
@property(strong, nonatomic) IBOutlet UIView *cvcView;
@property(strong, nonatomic) IBOutlet BEMCheckBox *checkView;
@property(strong, nonatomic) IBOutlet UIButton *checkBtn;
@property(strong, nonatomic) IBOutlet UILabel *checkLabel;
@property(strong, nonatomic) CustomTextFieldView *cardNameTextField;
@property(strong, nonatomic) CustomTextFieldView *cardNumberTextField;
@property(strong, nonatomic) CustomTextFieldView *expirationYearTextField;
@property(strong, nonatomic) CustomTextFieldView *expirationMonthTextField;
@property(strong, nonatomic) CustomTextFieldView *cvcTextField;

@property(strong, nonatomic) IBOutlet UIButton *nextBtn;
@property(strong, nonatomic) DeliveryAddress *deliveryAddress;

@end
