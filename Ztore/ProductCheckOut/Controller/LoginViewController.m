//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "LoginViewController.h"
#import "MainRegisterViewController.h"
#import "EmailLoginViewController.h"
#import "FacebookLogin.h"

@implementation LoginViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
  self.view.backgroundColor = self.centerView.backgroundColor = [[Common shared] getCommonRedColor];
  [super setPageName:@"Login"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar loginMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backToMainPage) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];

}

- (void)setupView {
    
    self.titleLabel.text = JMOLocalizedString(@"Choose Login Method", nil);
     [self.titleLabel setFont:[[Common shared] getNormalFontWithSize:33]];
    [self.facebookLoginBtn setTitle:JMOLocalizedString(@"Sign in with Facebook", nil) forState:UIControlStateNormal animation:NO];
    self.facebookLoginBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
    self.emailLoginBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
    [self.emailLoginBtn setTitle:JMOLocalizedString(@"Login with Email", nil) forState:UIControlStateNormal animation:NO];
    
    [self.emailLoginBtn setUpWhiteBorderButton];
    
    [[self.emailLoginBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.navigationController pushViewController:[[EmailLoginViewController alloc] init] animated:YES enableUI:NO];
        
    }];
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.readPermissions = @[@"email"];
    loginButton.delegate = self;
    [[self.facebookLoginBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
         [loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        
    }];
    
}

- (void)pressUsePromoBtn:(id)sender {
    
   [self popToViewController:[MainRegisterViewController class]];
    
}

#pragma mark FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error {
    
    if (result.token.tokenString) {
        
        [self showLoadingHUD];
        
        
        [[ServerAPI shared] fbLoginWithAccessToken:result.token.tokenString completionBlock:^(id response, NSError *error) {
            
            [self hideLoadingHUD];
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                FacebookLogin *fbLogin = response;
                
                if (fbLogin.data.confirm_password != nil) {
                    
                    if (!self.shoppingCartPopUpView) {
                        
                        self.shoppingCartPopUpView = [[[NSBundle mainBundle] loadNibNamed:@"ShoppingCartPopUpView" owner:self options:nil] objectAtIndex:0];
                        [self.shoppingCartPopUpView setupView];
                        [self.view addSubview:self.shoppingCartPopUpView];
                        [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
                            
                        }];
                        [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
                        
                        [[self.shoppingCartPopUpView.useBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
                            
                            [[ServerAPI shared] connectFbAccountWithEmail:fbLogin.data.confirm_password password:self.shoppingCartPopUpView.usePromoCodeTextField.textField.text accessToken:result.token.tokenString completionBlock:^(id response, NSError *error) {
                                
                                if ([[Common shared] checkNotError:error controller:self response:response]) {
                                    
                                    [self backToMainPageGetCurrentUser:NO];
                                    
                                }
                                
                            }];
                            
                        }];
                        
                        self.shoppingCartPopUpView.titleLabel.text = JMOLocalizedString(@"Your email has been registered on Ztore before, simply enter your password to link it with your existing Ztore account.", nil);
                        self.shoppingCartPopUpView.usePromoCodeTextField.fieldLabel.text = JMOLocalizedString(@"Password", nil);
                        self.shoppingCartPopUpView.usePromoCodeTextField.textField.secureTextEntry = YES;
                        self.shoppingCartPopUpView.backBtn.hidden = YES;
                        
                    } else {
                        
                        [self.view addSubview:self.shoppingCartPopUpView];
                        [self.shoppingCartPopUpView fadeInCompletion:^(id response, NSError *error) {
                            
                        }];
                        [self.shoppingCartPopUpView matchSizeWithSuperViewFromAutoLayout];
                        
                    }
                    
                } else {
                    
                    [self backToMainPageGetCurrentUser:YES];
                }
                
            }
            
        }];
        
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

@end
