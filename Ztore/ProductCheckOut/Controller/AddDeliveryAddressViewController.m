//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AddDeliveryAddressViewController.h"

#import "AddressData.h"
#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>

@implementation AddDeliveryAddressViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  
}

- (void)callAPI {
    
    [self setupView];
}

- (void)setupView {

  self.automaticallyAdjustsScrollViewInsets = NO;
  self.titleTextField                       =  [self.titleView addCustomTextFieldView];
  [self.titleTextField setFieldText:JMOLocalizedString(@"Title*", nil) errorText:JMOLocalizedString(@"Please choose title", nil)];
  [self.titleTextField createDropDownMenuFromStringArray:@[JMOLocalizedString(@"Mr.", nil), JMOLocalizedString(@"Mrs.", nil), JMOLocalizedString(@"Miss", nil), JMOLocalizedString(@"Ms.", nil)] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:Title];
  self.titleTextField.textField.delegate = self;
  [self.titleTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
  self.firstNameTextField =   [self.firstNameView addCustomTextFieldView];;
  [self.firstNameTextField setFieldText:JMOLocalizedString(@"First Name*", nil) errorText:JMOLocalizedString(@"Please enter first name", nil)];
  [self.firstNameTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter first name", nil)];
  self.firstNameTextField.textField.delegate              = self;
  self.address1TextField                                  =  [self.address1View addCustomTextFieldView];

  if (self.deliveryAddressType == Add) {

    [[ServerAPI shared] listRegionFromCountry_id:1
                             completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                               if ([[Common shared] checkNotError:error controller:self response:response]) {

                                 self.address1TextField.addressData = response;
                                 [self.address1TextField createDropDownMenuFromStringArray:[self getStringArrayFromAddressData:response] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:AddressDetail1];
                                 self.address1TextField.textField.delegate = self;
                                 [self.address1TextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
                               }

                             }];
  }

  [self.address1TextField setFieldText:JMOLocalizedString(@"Area*", nil) errorText:JMOLocalizedString(@"Please choose area", nil)];
  self.address2TextField =  [self.address2View addCustomTextFieldView];
  [self.address2TextField setFieldText:JMOLocalizedString(@"Area*", nil) errorText:JMOLocalizedString(@"Please choose area", nil)];
  self.address2TextField.textField.delegate = self;
  [self.address2TextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
  [self setupGetAddressDetail2Notification];
  self.address3TextField = [self.address3View addCustomTextFieldView];
  [self.address3TextField setFieldText:JMOLocalizedString(@"Area*", nil) errorText:JMOLocalizedString(@"Please choose area", nil)];
  self.address3TextField.textField.delegate = self;
  [self.address3TextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
  [self setupGetAddressDetail3Notification];
  self.addressDetailTextField =  [self.addressDetailView addCustomTextFieldView];
  [self.addressDetailTextField setFieldText:JMOLocalizedString(@"Full Address*", nil) errorText:JMOLocalizedString(@"* Tung Chung and Ma Wan areas are restricted to limited delivery time. Islands and restricted districts are currently unavailable for delivery service.", nil)];
  [self.addressDetailTextField setPlaceHolderFromText:JMOLocalizedString(@"Street,Building and Floor", nil)];
  self.addressDetailTextField.textField.delegate              = self;
  [self setUpTick];
  [self.checkBtn addTarget:self action:@selector(checkAnimationSelected) forControlEvents:UIControlEventTouchDown];
  self.phoneTextField =  [self.phoneView addCustomTextFieldView];
  [self.phoneTextField setFieldText:JMOLocalizedString(@"Phone*", nil) errorText:JMOLocalizedString(@"Please enter phone", nil)];
    [self.phoneTextField setTextFieldNumberType];
  [self.phoneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter phone", nil)];
  self.phoneTextField.textField.delegate              = self;
  self.OtherPhoneTextField                            = [self.OtherPhoneView addCustomTextFieldView];
  [self.OtherPhoneTextField setFieldText:JMOLocalizedString(@"Other Phone", nil) errorText:JMOLocalizedString(@"Please enter other phone", nil)];
      [self.OtherPhoneTextField setTextFieldNumberType];
  [self.OtherPhoneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter other phone", nil)];
  self.OtherPhoneTextField.textField.delegate              = self;
  self.waringLabel.text                                    = JMOLocalizedString(@"The option to hit (*) must be filled in.", nil);
  self.waringLabel.textColor                               = [[Common shared] getCommonRedColor];
  [self.nextBtn setTitle:JMOLocalizedString(@"Save", nil) forState:UIControlStateNormal animation:NO];
  [self.nextBtn setUpSmallCornerBorderButton];
  self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
  self.nextBtn.backgroundColor = [[Common shared] getCommonRedColor];

  [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

      [self.firstNameTextField checkValid];
      [self.titleTextField checkValid];
      [self.address1TextField checkValid];
      [self.address2TextField checkValid];
      [self.address3TextField checkValid];
      [self.addressDetailTextField checkValid] ;
      [self.phoneTextField checkValid];
      [self.OtherPhoneTextField checkValid];
      
    if ([self.firstNameTextField checkValid] && [self.titleTextField checkValid] && [self.address1TextField checkValid] && [self.address2TextField checkValid] && [self.address3TextField checkValid] && [self.addressDetailTextField checkValid] && [self.phoneTextField checkValid]) {

      if (self.deliveryAddressType == Edit) {

        [[ServerAPI shared] updateCurrentUserAddressFromAddressId:(int)self.deliveryAddress.id
                                              consignee_title:[[Common shared] getConsignee_titleIdFromConsignee_title:self.titleTextField.textField.text]
                                                     consignee:self.firstNameTextField.textField.text
                                                       mobile:self.phoneTextField.textField.text
                                                   contact_no:self.OtherPhoneTextField.textField.text
                                                      country:JMOLocalizedString(@"Hong Kong", nil)
                                                       region:self.address1TextField.textField.text
                                                     district:self.address2TextField.textField.text
                                                         area:self.address3TextField.textField.text
                                                      address:self.addressDetailTextField.textField.text
                                                   is_no_lift:self.checkView.on
                                              CompletionBlock:^(id response, NSError *error) {

                                                if ([[Common shared] checkNotError:error controller:self response:response]) {

                                                    
                                                     [[ServerAPI shared] listCurrentUserAddressCompletionBlock:^(id response, NSError *error) {
                                                        
                                                        if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                            
                                                            [self backBtnPressed:self];
                                                        }
                                                        
                                                    }];

                                                }

                                              }];
      }

      if (self.deliveryAddressType == Add) {

        __weak __typeof__(self) weakSelf = self;
          
          [self showLoadingHUD];
          
        [[ServerAPI shared] addCurrentUserAddressFromConsignee_title:[[Common shared] getConsignee_titleIdFromConsignee_title:self.titleTextField.textField.text]
                                                        consignee:self.firstNameTextField.textField.text
                                                          mobile:self.phoneTextField.textField.text
                                                      contact_no:self.OtherPhoneTextField.textField.text
                                                         country:JMOLocalizedString(@"Hong Kong", nil)
                                                          region:self.address1TextField.textField.text
                                                        district:self.address2TextField.textField.text
                                                            area:self.address3TextField.textField.text
                                                         address:self.addressDetailTextField.textField.text
                                                      is_no_lift:self.checkView.on
                                                 CompletionBlock:^(id response, NSError *error) {
                    
                                                     if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                         
                                                         CardID *cardID = (CardID *)response;
                                                         
                                                         [[ServerAPI shared] setCurrentUserAddressDefaultFromAddressId:[cardID.data intValue] completionBlock:^(id  response, NSError *error) {
                                                             
                                                             if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                                 
                                                                 [[ServerAPI shared] listCurrentUserAddressCompletionBlock:^(id response, NSError *error) {
                                                                     
                                                                     [self hideLoadingHUD];
                                                                     
                                                                     if ([[Common shared] checkNotError:error controller:self response:response]) {
                                                                         
                                                                         
                                                                         [weakSelf backBtnPressed:weakSelf];
                                                                     }
                                                                     
                                                                 }];

                                                                 
                                                             } else {
                                                                 
                                                                 [self hideLoadingHUD];
                                                                 
                                                             }
                                                             
                                                         }];

                                                         
                                                     } else {
                                                         
                                                         [self hideLoadingHUD];
                                                         
                                                     }

                                                 }];
      }
    }

  }];

  if (self.deliveryAddressType == Edit) {

    [self setUpDeliveryAddress:self.deliveryAddress];
    
  }
}

- (void)setUpNavigationBar {

  self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
  [self.ztoreNavigationBar rightMenuBarType];
   
  if (self.deliveryAddressType == Add) {
        
  self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Add New Delivery Address", nil);
        
  } else {
        
  self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Edit Delivery Address", nil);
        
  }

  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = NO;
  [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Delete Address", nil) forState:UIControlStateNormal];
  [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(usePromoBtnPress) forControlEvents:UIControlEventTouchDown];
    
}

- (void)usePromoBtnPress {
    
    [[ServerAPI shared] deleteCurrentUserAddressFromAddressId:(int)self.deliveryAddress.id completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self backBtnPressed:self];
        }
        
    }];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
   
    if (textField == self.phoneTextField.textField || textField == self.OtherPhoneTextField.textField) {
        
    if (range.length + range.location > textField.text.length) {
        
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
    return newLength <= 8;
        
    } else {
        
        return YES;
    }
}

- (void)setUpTick {

  [[Common shared] setUpTickFromCheckBox:self.checkView];
  self.checkLabel.font    = [[Common shared] getNormalTitleFont];
  self.checkLabel.text    = JMOLocalizedString(@"Elevator unavailable", nil);
  self.liftWarning.text   = JMOLocalizedString(@"There is no lift service delivery address, shipping address is only 8 floors above the ground floor.", nil);
  self.liftWarning.font   = [[Common shared] getNormalFontWithSize:14];
  self.liftWarning.hidden = YES;
  self.checkView.hidden = NO;
  self.nextBtn.hidden = NO;
    
}

- (void)checkAnimationSelected {

  self.checkView.tintColor = [UIColor lightGrayColor];
  [self.checkView setOn:!self.checkView.on animated:YES];
  self.liftWarning.hidden = !self.checkView.on;
 
}

- (void)setupGetAddressDetail2Notification {

  [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationGetAddressDetail2 object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {

    [[ServerAPI shared] listDistrictFromRegion_id:(int)self.address1TextField.addressDetail.id
                              completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                                if ([[Common shared] checkNotError:error controller:self response:response]) {

                                  [self.address2TextField resetDropDownMenu];
                                  [self.address3TextField resetDropDownMenu];
                                  [self createDropDownMenuFromCustomTextFieldView:self.address2TextField addressData:(AddressData *)response dropDownMenutype:AddressDetail2];
                                }

                              }];

  }];
}

- (void)setupGetAddressDetail3Notification {

  [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationGetAddressDetail3 object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {

    [[ServerAPI shared] listAreaFromDistrict_id:(int)self.address2TextField.addressDetail.id
                            completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                              if ([[Common shared] checkNotError:error controller:self response:response]) {

                                [self.address3TextField resetDropDownMenu];
                                [self createDropDownMenuFromCustomTextFieldView:self.address3TextField addressData:(AddressData *)response dropDownMenutype:AddressDetail3];
                              }

                            }];

  }];
}

- (NSArray *)getStringArrayFromAddressData:(id)addressData {

  AddressData *address        = addressData;
  NSMutableArray *stringArray = [[NSMutableArray alloc] init];

  for (AddressDetail *addressDetail in address.data) {

    [stringArray addObject:addressDetail.name];
  }

  return [stringArray copy];
}

- (void)setUpDeliveryAddress:(DeliveryAddress *)deliveryAddress {

  self.deliveryAddress                       = deliveryAddress;
  self.phoneTextField.textField.text         = deliveryAddress.mobile;
  self.OtherPhoneTextField.textField.text    = deliveryAddress.contact_no;
  self.addressDetailTextField.textField.text = deliveryAddress.address;
  self.firstNameTextField.textField.text     = deliveryAddress.consignee;
  self.checkView.on                          = deliveryAddress.is_no_lift;
  self.titleTextField.textField.text         = [[Common shared] getConsigneeTitleFromTitle:deliveryAddress.consignee_title];
  self.titleTextField.textField.textColor    = [UIColor blackColor];
  self.liftWarning.hidden = !self.checkView.on;
    
  [[ServerAPI shared] listRegionFromCountry_id:1
                           completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                             if ([[Common shared] checkNotError:error controller:self response:response]) {

                               [self createDropDownMenuFromCustomTextFieldView:self.address1TextField addressData:(AddressData *)response dropDownMenutype:AddressDetail1];
                               [self.address1TextField updateTextFieldString:[(AddressData *)response getNameFromAddressId:deliveryAddress.region_id]];
                             }

                           }];

  [[ServerAPI shared] listDistrictFromRegion_id:(int)deliveryAddress.region_id
                            completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                              if ([[Common shared] checkNotError:error controller:self response:response]) {

                                [self createDropDownMenuFromCustomTextFieldView:self.address2TextField addressData:(AddressData *)response dropDownMenutype:AddressDetail2];
                                [self.address2TextField updateTextFieldString:[(AddressData *)response getNameFromAddressId:deliveryAddress.district_id]];
                              }

                            }];

  [[ServerAPI shared] listAreaFromDistrict_id:(int)deliveryAddress.district_id
                          completionBlock:^(id _Nullable response, NSError *_Nullable error) {

                            if ([[Common shared] checkNotError:error controller:self response:response]) {

                              [self createDropDownMenuFromCustomTextFieldView:self.address3TextField addressData:(AddressData *)response dropDownMenutype:AddressDetail3];
                              [self.address3TextField updateTextFieldString:[(AddressData *)response getNameFromAddressId:deliveryAddress.area_id]];
                              [self.address3TextField checkAddressDetail3LimitedDelivery];
                                
                            }

                          }];
}

- (void)createDropDownMenuFromCustomTextFieldView:(CustomTextFieldView *)customTextFieldView addressData:(AddressData *)addressData dropDownMenutype:(Type)dropDownMenutype {

  if (dropDownMenutype == AddressDetail3) {

    NSMutableArray *array = [[NSMutableArray alloc] init];

    for (AddressDetail *addressDetail in addressData.data) {

      if (addressDetail.id == 131 || addressDetail.id == 132 || addressDetail.id == 134) {

      } else {

        [array addObject:addressDetail];
      }
    }

    addressData.data = [array copy];
  }

  customTextFieldView.addressData = addressData;
  [customTextFieldView createDropDownMenuFromStringArray:[self getStringArrayFromAddressData:addressData] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:dropDownMenutype];
  customTextFieldView.textField.delegate = self;
  [customTextFieldView.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
}

@end
