//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AddressData.h"
#import "ConfirmOrderViewController.h"
#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import "SpecialyRequestViewController.h"
#import "TimeslotViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ActionSheetCustomPicker.h"

@implementation TimeslotViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  [self setupView];
  [super setPageName:@"Checkout - Timeslot"];
    
}

- (void)setupView {

  self.automaticallyAdjustsScrollViewInsets = NO;
  self.view.backgroundColor                 = [[Common shared] getProductDetailDescGrayBackgroundColor];

  [[self.specialyRequestEditBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    SpecialyRequestViewController *specialyRequestViewController = [[SpecialyRequestViewController alloc] init];
    [self.navigationController presentViewController:specialyRequestViewController animated:YES completion:nil];
 
  }];
  self.titleLabel.text = JMOLocalizedString(@"Select Your Delivery Slot", nil);
  self.titleLabel.font = self.dateBtn.titleLabel.font = self.timeBtn.titleLabel.font = [[Common shared] getBiggerNormalFont];
  self.dateTitleLabel.font = [[Common shared] getSmallContentNormalFont];
    
  [self.nextBtn setUpSmallCornerBorderButton];
  [self.nextBtn setTitle:JMOLocalizedString(@"Next", nil) forState:UIControlStateNormal animation:NO];
  self.specialyRequestLabel.text = [Common shared].specialyRequest.specialyRequestString;

  [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    if ([Common shared].selectedTimeslot) {

      ConfirmOrderViewController *confirmOrderViewController = [[ConfirmOrderViewController alloc] init];
      confirmOrderViewController.orderType = Current;
      [self.navigationController pushViewController:confirmOrderViewController animated:YES enableUI:NO];

    } else {

        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                        style:0 handler:^(UIAlertAction *action) {
                                                            
                                                            
                                                        }];
        
        [self showErrorFromMessage:JMOLocalizedString(@"Please Select Delivery Time Slot", nil) controller:self okBtn:okBtn];

    }

  }];

  [[self.specialyRequestBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    SpecialyRequestViewController *specialyRequestViewController = [[SpecialyRequestViewController alloc] init];
    [self.navigationController presentViewController:specialyRequestViewController animated:YES completion:nil];

  }];

  self.dayLabel1.font = self.dayLabel2.font = self.dayLabel3.font = self.dayLabel4.font = self.dayLabel5.font = self.dayLabel6.font = self.dayLabel7.font = [[Common shared] getNormalFont];
  self.dayBtn1.titleLabel.font = self.dayBtn2.titleLabel.font = self.dayBtn3.titleLabel.font = self.dayBtn4.titleLabel.font = self.dayBtn5.titleLabel.font = self.dayBtn6.titleLabel.font = self.dayBtn7.titleLabel.font = [[Common shared] getSmallContentNormalFont];

  [self.dateBtn makeRightImageView];
  [self.dateBtn setTitle:JMOLocalizedString(@"Choose Date", nil) forState:UIControlStateNormal animation:NO];
  [self.dateBtn setTitleColor:([Common shared].selectedDayBtn) ? [UIColor blackColor]: [[Common shared] getCommonRedColor] forState:UIControlStateNormal ];
  
  [self.timeBtn makeRightImageView];
  [self setUpTimeBtn];
  self.line.backgroundColor = [[Common shared] getShoppingCartLineColor];
    
  if ([Common shared].selectedDayBtn == nil) {
        
        self.addSpecialyRequestView.hidden = YES;
        [self hideDateView:YES animation:NO];
  }
    
  [[self.dayBtn1 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn1];

  }];

  [[self.dayBtn2 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn2];

  }];

  [[self.dayBtn3 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn3];

  }];

  [[self.dayBtn4 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn4];

  }];

  [[self.dayBtn5 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn5];

  }];

  [[self.dayBtn6 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn6];

  }];

  [[self.dayBtn7 rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self selectedDayBtn:self.dayBtn7];

  }];
    
  [[self.dateBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
      [self hideDateView:!self.hideDateView animation:YES];
      
  }];
    
    
}

- (void)refreshSpecialyRequestView {

  self.specialyRequestLabel.font = [[Common shared] getNormalTitleFont];
  self.specialyRequestDetailLabel.font    = [[Common shared] getSmallContentNormalFont];
  self.specialyRequestUserInputLabel.font = [[Common shared] getSmallContentNormalFont];

  if ([Common shared].specialyRequest.specialyRequestUserInput.length > 0 || [Common shared].specialyRequest.isChoiceString1 || [Common shared].specialyRequest.isChoiceString2 || [Common shared].specialyRequest.isChoiceString3) {

    self.editSpecialyRequestView.hidden = NO;
    self.addSpecialyRequestView.hidden  = YES;
    [self.specialyRequestEditLabel setUpTitleLabelFromText:[Common shared].specialyRequest.specialyRequestString];
    self.specialyRequestDetailLabel.text    = [[Common shared].specialyRequest getChoiceString];
    self.specialyRequestUserInputLabel.text = [[Common shared].specialyRequest getUserInputString];

  } else {

    self.editSpecialyRequestView.hidden = YES;
    [self.specialyRequestLabel setUpTitleLabelFromText:[Common shared].specialyRequest.specialyRequestString];
  }
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
  [self.ztoreNavigationBar twoCheckOutBarType];
  [self.ztoreNavigationBar.oneBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {

  [super viewWillAppear:animated];
  [self backToViewController];
  
  [[ServerAPI shared] getTimeslotFromCode:[[Common shared].selectedDeliveryAddress.area_code intValue]
                      completionBlock:^(id response, NSError *error) {

                        if ([[Common shared] checkNotError:error controller:self response:response]) {

                          [Common shared].timeslotData = response;

                            //check if previous date not equal to currect date array.
                            
                            if ([Common shared].selectedTimeslot) {
                                
                                BOOL selectDateValidate = NO;
                                
                                for (Timeslot *timeSlot in [Common shared].timeslotData.data) {
                                    
                                    if (timeSlot.date == [Common shared].selectedTimeslot.date && timeSlot.time_start == [Common shared].selectedTimeslot.time_start && timeSlot.time_end == [Common shared].selectedTimeslot.time_end) {
                                        
                                        selectDateValidate = YES;
                                        
                                    }
                                    
                                }
                                
                                if (!selectDateValidate) {
                                    
                                    [Common shared].selectedDayBtn.layer.cornerRadius   = 0;
                                    [Common shared].selectedDayBtn.titleLabel.textColor = [UIColor blackColor];
                                    [Common shared].selectedDayBtn.backgroundColor      = [UIColor clearColor];
                                    [Common shared].selectedDayBtn = nil;
                                    [self setUpTimeBtn];
                                }
                                
                            }
                            
                            
                          int j = [[Common shared].timeslotData getTimeSlotCount7];

                          for (int i = 0; i < [Common shared].timeslotData.data.count; i++) {

                            Timeslot *timeslot = [[Common shared].timeslotData.data objectAtIndex:i];
                              
                            if (i == 0 * j) {

                              self.dayLabel1.text = [timeslot getWeekString];
                              [Common shared].timeslotArray1 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn1 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray1];
                                BOOL today = [[NSCalendar currentCalendar] isDateInToday:[NSDate dateWithTimeIntervalSince1970:timeslot.date]];
                                self.firstMonthTimeSlot = timeslot;

                                self.toDayLabel.hidden = YES;
                                
                                if (today) {
                                    
                                    [self.dayBtn1 setUpGreyCircleButton];
                                    self.toDayLabel.textColor = [UIColor lightGrayColor];
                                    self.toDayLabel.text = JMOLocalizedString(@"ToDay", nil);
                                    self.toDayLabel.hidden = NO;
                                    self.toDayLabel.font = [[Common shared] getSmallContentNormalFont];

                                }
                            }

                            if (i == 1 * j) {

                              self.dayLabel2.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray2 = [[NSMutableArray alloc] init];
                                [self setUpButton:self.dayBtn2 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray2];
                             [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if (i == 2 * j) {

                              self.dayLabel3.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray3 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn3 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray3];
                              [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if (i == 3 * j) {

                              self.dayLabel4.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray4 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn4 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray4];
                              [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if (i == 4 * j) {

                              self.dayLabel5.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray5 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn5 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray5];
                               [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if (i == 5 * j) {

                              self.dayLabel6.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray6 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn6 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray6];
                              [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if (i == 6 * j) {

                              self.dayLabel7.text = [timeslot getWeekString];
                                 [Common shared].timeslotArray7 = [[NSMutableArray alloc] init];
                              [self setUpButton:self.dayBtn7 startNumber:i count:j timeslot:timeslot timeslotArray:[Common shared].timeslotArray7];
                              [self checkHaveSecondMonthStringFromTimeSlot:timeslot];
                            }

                            if ([Common shared].selectedDayBtn) {

                              [[Common shared].selectedDayBtn setUpSelectedDayBtn];
                            }
                              
                          }
                            
                         self.dateTitleLabel.text = (self.secondMonthTimeSlot) ? [NSString stringWithFormat:@"%@ - %@", [self.firstMonthTimeSlot getYearAndMonth] , [self.secondMonthTimeSlot getYearAndMonth]] : [NSString stringWithFormat:@"%@", [self.firstMonthTimeSlot getYearAndMonth]] ;

                        }

                      }];


  [self refreshSpecialyRequestView];
}

- (void)checkHaveSecondMonthStringFromTimeSlot:(Timeslot *)timeSlot {
    
    if (![[self.firstMonthTimeSlot getYearAndMonth] isEqualToString:[timeSlot getYearAndMonth]]) {
        
        self.secondMonthTimeSlot = timeSlot;
    }
    
}

- (void)setUpButton:(UIButton *)button startNumber:(int)startNumber count:(int)count timeslot:(Timeslot *)timeslot timeslotArray:(NSMutableArray *)timeslotArray {

  [button setTitle:[timeslot getDayString] forState:UIControlStateNormal animation:NO];
  button.tag = startNumber;
  BOOL userInteractionEnabled = false;

  for (int y = 0; y < count; y++) {
      
    Timeslot *timeslot                  = [[Common shared].timeslotData.data objectAtIndex:startNumber + y];
    userInteractionEnabled = (timeslot.available == true) ? true : userInteractionEnabled;
    [timeslotArray addObject:timeslot];
      
  }
   
  button.userInteractionEnabled = userInteractionEnabled;
  [button setTitleColor:(button.userInteractionEnabled) ? [UIColor blackColor] : [UIColor lightGrayColor] forState:UIControlStateNormal];
 
}

- (void)selectedDayBtn:(UIButton *)dayBtn {

  Timeslot *timeslot  = [[Common shared].timeslotData.data objectAtIndex:dayBtn.tag];
    
  if ([Common shared].selectedDayBtn) {

    [Common shared].selectedDayBtn.layer.cornerRadius   = 0;
    [Common shared].selectedDayBtn.titleLabel.textColor = [UIColor blackColor];
    [Common shared].selectedDayBtn.backgroundColor      = [UIColor clearColor];
  }

  [Common shared].selectedDayBtn    = dayBtn;
  [[Common shared].selectedDayBtn setUpSelectedDayBtn];

    if (dayBtn == self.dayBtn1) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray1;
    }
    
    if (dayBtn == self.dayBtn2) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray2;
    }
    
    if (dayBtn == self.dayBtn3) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray3;
    }
    
    if (dayBtn == self.dayBtn4) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray4;
    }
    
    if (dayBtn == self.dayBtn5) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray5;
    }
    
    if (dayBtn == self.dayBtn6) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray6;
    }
    
    if (dayBtn == self.dayBtn7) {
        
        [Common shared].selectedTimeArray = [Common shared].timeslotArray7;
    }
    
    NSMutableArray *stringArray       = [[NSMutableArray alloc] init];
    
    for (Timeslot *timeslot in [Common shared].selectedTimeArray) {

        if (timeslot.available) {

            [stringArray addObject:[timeslot getTimeString]];
            
        }
      
    }

    [Common shared].selectedTimeslot = nil;
    [self.timeBtn setTitle:JMOLocalizedString(@"Choose Time", nil) forState:UIControlStateNormal animation:NO];
    [self setUpTimeBtn];
    [self createDropDownMenuFromStringArray:stringArray button:self.timeBtn];
    
    [self.dateBtn setTitle:[timeslot getFullDateString] forState:UIControlStateNormal animation:NO];
    [self.dateBtn setTitleColor:([Common shared].selectedDayBtn) ? [UIColor blackColor] : [[Common shared] getCommonRedColor] forState:UIControlStateNormal];
    [self hideDateView:YES animation:YES];
}

- (void)setUpTimeBtn {
    
    if ([Common shared].selectedDayBtn == nil) {
        
        [self.timeBtn setTitleColor:[[Common shared] getShoppingCartLineColor] forState:UIControlStateNormal];
        [self.timeBtn setImage:[UIImage imageNamed:@"ic_keyboard_arrow_down_grey"] forState:UIControlStateNormal];
        
    } else {
    
    [self.timeBtn setTitleColor:([self.timeBtn.titleLabel.text isEqualToString:JMOLocalizedString(@"Choose Time", nil)]) ? [[Common shared] getCommonRedColor] : [UIColor blackColor] forState:UIControlStateNormal];
    [self.timeBtn setImage:[UIImage imageNamed:@"ic_keyboard_arrow_down"] forState:UIControlStateNormal];
    
        
    }
        
    self.timeBtn.userInteractionEnabled = ([Common shared].selectedDayBtn) ? YES : NO;
    
}

- (void)createDropDownMenuFromStringArray:(NSArray *)stringArray button:(UIButton *)button {

  [Common shared].menuItems = [[NSMutableArray alloc] init];
  self.pickerDataArray = stringArray;

  [button removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [button addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
}

- (void)pressChooseTitle:(UIButton *)sender {

    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
    
    [ActionSheetCustomPicker showPickerWithTitle:JMOLocalizedString(@"Choose Time", nil) delegate:self showCancelButton:YES origin:sender];
    
    if (isValid(self.pickerDataArray)) {
     
        self.selectedPickerData = self.pickerDataArray.firstObject;
        
    }

}

- (void)hideDateView:(BOOL)hide animation:(BOOL)animation {
    
    self.dateViewTop.constant = (hide) ? -165 : 0 ;
    self.hideDateView = hide;
    
    [UIView animateWithDuration:(animation) ? 0.5 : 0
                     animations:^{
                         
                         self.dateView.alpha = (hide) ? 0 : 1 ;
                         [self.dateView.superview layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];

    
}

#pragma mark - ActionSheetCustomPickerDelegate

- (void)actionSheetPickerDidSucceed:(AbstractActionSheetPicker *)actionSheetPicker origin:(id)origin {
    
    [self.timeBtn setTitle:self.selectedPickerData forState:UIControlStateNormal animation:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
    
    for (Timeslot *timeslot in [Common shared].selectedTimeArray) {
        
        if ([[timeslot getTimeString] isEqualToString:self.selectedPickerData]) {
            
            [Common shared].selectedTimeslot = timeslot;
            [self.timeBtn setTitle:[[Common shared].selectedTimeslot getTimeString] forState:UIControlStateNormal animation:NO];
            self.addSpecialyRequestView.hidden = NO;
            [self setUpTimeBtn];
            
        }
        
    }

}

@end
