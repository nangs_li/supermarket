//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AddDeliveryAddressViewController.h"
#import "DeliveryAddressCell.h"
#import "DeliveryAddressFooterCell.h"
#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>

@implementation DeliveryAddressViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  self.automaticallyAdjustsScrollViewInsets = NO;
    [super setPageName:@"Manage Delivery Address"];
       [self setupView];
  
    [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x){
        
        
        if ([Common shared].selectedDeliveryAddress) {
            
            if (!isValid([AppDelegate getAppDelegate].timeslotViewController)) {
                
                [AppDelegate getAppDelegate].timeslotViewController = [[TimeslotViewController alloc] init];
                
            }
            
            [self.navigationController pushViewController:[AppDelegate getAppDelegate].timeslotViewController animated:YES enableUI:NO];
            
        } else {
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                            style:0 handler:^(UIAlertAction *action) {
                                                                
                                                                
                                                            }];
            
            [self showErrorFromMessage:JMOLocalizedString(@"Please Select Address", nil) controller:self okBtn:okBtn];
            
        }
        
        
    }];

    [[Common shared] getDeliveryAddressDataCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self setupView];
        }
        
    }];
    
   self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    (self.editType == EditInCheckOut) ? [self backToViewController] : nil;
    (self.ztoreNavigationBar == nil) ? [self setUpNavigationBar] : nil;
    self.titleLabel.hidden = (self.editType == EditInCheckOut) ? NO : YES;
     [self.tableView reloadData];
    
    if (self.editType == EditInMyAccount) {
        
        [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.view).with.offset(0);

        }];
    }
}

- (void)setUpNavigationBar {

    if (self.editType == EditInCheckOut) {
    
        self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

  [self.ztoreNavigationBar oneCheckOutBarType];
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = YES;
  self.nextBtn.hidden = NO;
        
    } else {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Edit Delivery Address", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"", nil) forState:UIControlStateNormal];
    self.nextBtn.hidden = YES;
        
    }
    
}

- (void)setupView {
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.delegate                   = self;
    self.tableView.dataSource                 = self;
    self.view.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    [self.nextBtn setTitle:JMOLocalizedString(@"Next", nil) forState:UIControlStateNormal animation:NO];
    [self.nextBtn setUpSmallCornerBorderButton];
    self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    self.nextBtn.backgroundColor = [[Common shared] getCommonRedColor];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [textFieldView textFieldEnd];
}

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  return ([Common shared].deliveryAddressData.data) ? [Common shared].deliveryAddressData.data.count + 1 : 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  static NSString *cellIdentifier = @"DeliveryAddressCell";
  DeliveryAddressCell *cell       = (DeliveryAddressCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

  if (cell == nil) {

    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
    cell         = [nib objectAtIndex:0];
  }

  if (!(indexPath.row == [Common shared].deliveryAddressData.data.count)) {

    [cell setUpDeliveryAddress:[[Common shared].deliveryAddressData.data objectAtIndex:indexPath.row] selected:([[Common shared].deliveryAddressData.data objectAtIndex:indexPath.row] == [Common shared].selectedDeliveryAddress)];
    cell.editBtn.tag = indexPath.row;
    [cell.editBtn addTarget:self action:@selector(editBtnPressed:) forControlEvents:UIControlEventTouchDown];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
      
    return cell;

  } else {

    static NSString *cellIdentifier  = @"DeliveryAddressFooterCell";
    DeliveryAddressFooterCell *cell2 = (DeliveryAddressFooterCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell2 == nil) {

      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
      cell2        = [nib objectAtIndex:0];
    }
      
   cell2.selectionStyle = UITableViewCellSelectionStyleNone;
   [cell2 setUpDeliveryAddress];
      
    return cell2;
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

  return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

  if (indexPath.row == [Common shared].deliveryAddressData.data.count) {

    AddDeliveryAddressViewController *addDeliveryAddress = [[AddDeliveryAddressViewController alloc] init];
      addDeliveryAddress.deliveryAddressType = Add;
    [self.navigationController pushViewController:addDeliveryAddress animated:YES enableUI:NO];

  } else {

     DeliveryAddress *deliveryAddress = [[Common shared].deliveryAddressData.data objectAtIndex:indexPath.row];
     [Common shared].selectedDeliveryAddress = deliveryAddress;
     [self.tableView reloadData];
     
      [AppDelegate getAppDelegate].timeslotViewController = nil;
      [Common shared].selectedTimeslot = nil;
      [Common shared].selectedDayBtn = nil;
      [[Common shared].specialyRequest initString];
      
      if (self.selectDeliveryAddressTimer) {
          
          [self.selectDeliveryAddressTimer invalidate];
      }
      
      self.selectDeliveryAddressTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setCurrentUserAddressDefault) userInfo:nil repeats:NO];  
      
  }
}

- (void)setCurrentUserAddressDefault {
    
    [[ServerAPI shared] setCurrentUserAddressDefaultFromAddressId:(int)[Common shared].selectedDeliveryAddress.id completionBlock:^(id  response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            
        } else {
            
            [Common shared].selectedDeliveryAddress = nil;
            [self.tableView reloadData];
        }
        
    }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  return (indexPath.row == [Common shared].deliveryAddressData.data.count) ? 80 : 163;
}

- (void)editBtnPressed:(id)sender {

  UIButton *button                                     = sender;
  AddDeliveryAddressViewController *addDeliveryAddress = [[AddDeliveryAddressViewController alloc] init];
  addDeliveryAddress.deliveryAddressType = Edit;
  addDeliveryAddress.deliveryAddress = [[Common shared].deliveryAddressData.data objectAtIndex:button.tag];
  [self.navigationController pushViewController:addDeliveryAddress animated:YES enableUI:NO];
    
}

@end
