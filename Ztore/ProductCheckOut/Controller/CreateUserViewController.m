//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "CreateUserViewController.h"
#import "LoginViewController.h"

@implementation CreateUserViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupView];
    self.view.backgroundColor = self.centerView.backgroundColor = [[Common shared] getCommonRedColor];
    [super setPageName:@"Register with Email"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar loginMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
    
}

- (void)setupView {
    
    [self.bottomBtn setTitle:JMOLocalizedString(@"Register", nil) forState:UIControlStateNormal animation:NO];
    
    [self.bottomBtn setUpWhiteBottomBtnFromTitleText:JMOLocalizedString(@"Register", nil)];
    self.titleLabel.text = JMOLocalizedString(@"New User", nil);
    [self.titleLabel setFont:[[Common shared] getNormalFontWithSize:33]];
    
    
    UIColor *textColor = [UIColor whiteColor];
    NSDictionary *termNormalAttributeDict =  @{ NSForegroundColorAttributeName : textColor };
    NSMutableAttributedString *termString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", JMOLocalizedString(@"I have READ and UNDERSTAND Ztore ", nil)] attributes:termNormalAttributeDict];
    NSMutableAttributedString *policyLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"General Terms & Conditions", nil) attributes:@{ @"General Terms & Conditions" : @(YES),NSForegroundColorAttributeName:  textColor,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:policyLinkString];
    
    NSMutableAttributedString *termString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", JMOLocalizedString(@" as well as ", nil)] attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString2];
    NSMutableAttributedString *serviceLinkString = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@"Privacy Policy", nil) attributes:@{ @"Privacy Policy" : @(YES),NSForegroundColorAttributeName:  textColor,NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
    [termString appendAttributedString:serviceLinkString];
    NSMutableAttributedString *termString3 = [[NSMutableAttributedString alloc] initWithString:JMOLocalizedString(@", and AGREE the use of my personal data for direct marketing purpose stated in the Privacy Policy.", nil) attributes:termNormalAttributeDict];
    [termString appendAttributedString:termString3];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = NSTextAlignmentCenter;
    [termString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [termString length])];
    self.termText = termString;
    [self.termTextView setAttributedText:self.termText];
    UITapGestureRecognizer *textViewTapGesureeRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTapped:)];
    [self.termTextView addGestureRecognizer:textViewTapGesureeRecognizer];
    [self.termTextView setViewHeight:([[Common shared] isEnLanguage]) ? 70 : 150 ];
    self.emailTextField = [self.emailView addCustomTextFieldView];
    self.emailView.backgroundColor = [UIColor clearColor];
    [self.emailTextField setTextFieldLoginEmailType];
    self.emailTextField.textField.delegate              = self;
    self.passwordTextField = [self.passwordView addCustomTextFieldView];
    self.passwordView.backgroundColor = [UIColor clearColor];
    [self.passwordTextField setTextFieldPasswordType];
    self.passwordTextField.textField.delegate              = self;
    
    [[self.bottomBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.emailTextField checkValid];
        [self.passwordTextField checkValid];
        
         if ( [self.emailTextField checkValid] &&  [self.passwordTextField checkValid]) {
             
        [[ServerAPI shared] userRegisterWithEmail:self.emailTextField.textField.text password:self.passwordTextField.textField.text
                           completionBlock:^(id _Nullable response, NSError *_Nullable error){
                               
                               if ([[Common shared] checkNotError:error controller:self response:response]) {
                                   
                                   [self backToMainPageGetCurrentUser:YES];
                                       
                               }
                               
                           }];
             
         }
        
    }];
    
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer {
    
    UITextView *textView = (UITextView *)recognizer.view;
    
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    if (characterIndex < textView.textStorage.length) {
        
        NSRange range;
        id didTapTermOfService = [self.termText attribute:@"General Terms & Conditions" atIndex:characterIndex effectiveRange:&range];
        id didTapPrivacyPolicy = [self.termText attribute:@"Privacy Policy" atIndex:characterIndex effectiveRange:&range];
        // Handle as required...
        if (didTapPrivacyPolicy) {
            [self showPrivacyPolicy];
            
        } else if (didTapTermOfService) {
            
            [self showTermsOfService];
            
        }
        
    }
}

- (void)pressUsePromoBtn:(id)sender {
    
     [self.navigationController pushViewController:[[LoginViewController alloc] init] animated:YES enableUI:NO];
}

@end
