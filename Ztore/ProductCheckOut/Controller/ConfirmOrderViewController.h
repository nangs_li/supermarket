//
//  RightMenuVC.h
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ServerAPI.h"
#import "ConfirmOrderHeaderCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PaymentViewController.h"

typedef NS_ENUM(NSInteger, OrderType) {Current, History};

@interface ConfirmOrderViewController : _UIViewController <MBProgressHUDDelegate, UITableViewDelegate, UITableViewDataSource> {
    int selectedIndex;
    int headerHeight;
    int footerHeight;
    int rowHeight;
    int commonFontSize;
}

@property(strong, nonatomic) IBOutlet UITableView *myTableView;
@property(strong, nonatomic) ConfirmOrderHeaderCell *slidingOrderFooterCell;
@property(strong, nonatomic) UIView *footerView;
@property(weak, nonatomic) IBOutlet UIButton *nextBtn;
@property(assign, nonatomic) OrderType orderType;
@property(assign, nonatomic) int orderId;
@property(strong, nonatomic) PaymentSuccess *paymentdata;

@end
