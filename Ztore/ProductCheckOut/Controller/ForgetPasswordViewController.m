//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "MainRegisterViewController.h"

@implementation ForgetPasswordViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupView];
    self.view.backgroundColor = self.centerView.backgroundColor = [[Common shared] getCommonRedColor];
    [super setPageName:@"Forget Password"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar loginMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
    
}

- (void)setupView {
    
    self.titleLabel.text = JMOLocalizedString(@"Forget Password", nil);
     [self.titleLabel setFont:[[Common shared] getNormalFontWithSize:33]];
    self.emailTextField = [self.emailView addCustomTextFieldView];
    self.emailView.backgroundColor = [UIColor clearColor];
    [self.emailTextField setTextFieldLoginEmailType];
    self.emailTextField.textField.delegate              = self;
    [self.emailTextField setFieldText:JMOLocalizedString(@"We will send you a password reset email.", nil) errorText:JMOLocalizedString(@"Please enter a valid email", nil)];
    [self.emailTextField setupWhiteColorTextField];
    
     [self.bottomBtn setUpWhiteBottomBtnFromTitleText:JMOLocalizedString(JMOLocalizedString(@"Email to me", nil), nil)];
    
    [[self.bottomBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        if ( [self.emailTextField checkValid]) {
            
            [[ServerAPI shared] forgetPasswordWithEmail:self.emailTextField.textField.text completionBlock:^(id response, NSError *error) {
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                 
                     [self showErrorFromMessage:JMOLocalizedString(@"email is sent successfully", nil) controller:self okBtn:nil];
                    
                }
                
            }];
            
        }
        
    }];

}

- (void)pressUsePromoBtn:(id)sender {
    
    [self popToViewController:[MainRegisterViewController class]];
    
}

@end
