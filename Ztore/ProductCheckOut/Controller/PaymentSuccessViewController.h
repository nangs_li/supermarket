
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"

@interface PaymentSuccessViewController : _UIViewController

@property(weak, nonatomic) IBOutlet UILabel *paymentSuccessLabel;
@property(weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property(weak, nonatomic) IBOutlet UITextView *linkTextView;
@property(weak, nonatomic) IBOutlet UILabel *commentLabel;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *linkTextViewLeading;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *linkTextViewTrailing;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *linkTextViewHeight;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) PaymentSuccess *paymentSuccess;

@end
