//
//  Address.m
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import "TimeslotData.h"

@implementation Timeslot

- (NSString *)getDayString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getStartTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.time_start];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getEndTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.time_end];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getYearAndMonth {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    ([[Common shared] isEnLanguage]) ? [dateFormatter setDateFormat:@"YYYY-MMM"] : [dateFormatter setDateFormat:@"YYYY年MM月"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getTimeString {
    
    return [NSString stringWithFormat:@"%@-%@", [self getStartTimeString], [self getEndTimeString]];
}

- (NSString *)getFullDateAndTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ (%@) %@ %@", formattedDateString, [self getWeekString], [self getTimeString], JMOLocalizedString(@"arrive", nil)];
}

- (NSString *)getWeekString {
    
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate dateWithTimeIntervalSince1970:self.date]];
    
    switch ([component weekday]) {
        case 1:
            
            return  ([[Common shared] isEnLanguage]) ? @"Sun" : @"日";
            break;
        case 2:
            
           return  ([[Common shared] isEnLanguage]) ? @"Mon" : @"一";
            break;
          
        case 3:
            
            return  ([[Common shared] isEnLanguage]) ? @"Tue" : @"二";
            break;
            
        case 4:
            
            return  ([[Common shared] isEnLanguage]) ? @"Wed" : @"三";
            break;
            
        case 5:
            
            return  ([[Common shared] isEnLanguage]) ? @"Thu" : @"四";
            break;
            
        case 6:
            
            return  ([[Common shared] isEnLanguage]) ? @"Fri" : @"五";
            break;
            
        case 7:
            
            return  ([[Common shared] isEnLanguage]) ? @"Sat" : @"六";
            break;
            
        default:
            break;
    }
    
    return @"";
}

- (NSString *)getFullDateString {
    
    NSString *weekString = ([[Common shared] isEnLanguage]) ? [self getWeekString] : [NSString stringWithFormat:@"星期%@", [self getWeekString]];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    ([[Common shared] isEnLanguage]) ? [dateFormatter setDateFormat:@"dd/MMM"] : [dateFormatter setDateFormat:@"MM月dd日"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@\n%@", weekString, formattedDateString];
}

@end

@implementation TimeslotData

- (int)getTimeSlotCount7 {
    
    return (int)self.data.count / 7;
}

@end
