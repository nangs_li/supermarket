//
//  Address.m
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import "PaymentSuccess.h"

@implementation PaymentSuccess

@end

@implementation PaymentSuccessData

- (NSString *)getYearString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getFullDateFormatString {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter localeDateFormatter];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getDayString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getWeekString {
    
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate dateWithTimeIntervalSince1970:self.date]];
    
    switch ([component weekday]) {
        case 1:
            
            return  ([[Common shared] isEnLanguage]) ? @"Sun" : @"日";
            break;
        case 2:
            
            return  ([[Common shared] isEnLanguage]) ? @"Mon" : @"一";
            break;
            
        case 3:
            
            return  ([[Common shared] isEnLanguage]) ? @"Tue" : @"二";
            break;
            
        case 4:
            
            return  ([[Common shared] isEnLanguage]) ? @"Wed" : @"三";
            break;
            
        case 5:
            
            return  ([[Common shared] isEnLanguage]) ? @"Thu" : @"四";
            break;
            
        case 6:
            
            return  ([[Common shared] isEnLanguage]) ? @"Fri" : @"五";
            break;
            
        case 7:
            
            return  ([[Common shared] isEnLanguage]) ? @"Sat" : @"六";
            break;
            
        default:
            break;
    }
    
    return @"";
}

- (NSString *)getFullDateString {
    
    NSString *weekString = ([[Common shared] isEnLanguage]) ? [self getWeekString] : [NSString stringWithFormat:@"星期%@", [self getWeekString]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    ([[Common shared] isEnLanguage]) ? [dateFormatter setDateFormat:@"dd/MM"] : [dateFormatter setDateFormat:@"MM月dd日"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@\n%@", weekString, formattedDateString];
}

- (NSString *)getStartTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.delivery_start];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getEndTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.delivery_end];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getTimeString {
    
    return [NSString stringWithFormat:@"%@-%@", [self getStartTimeString], [self getEndTimeString]];
}

- (NSString *)getFullDateAndTimeString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return [NSString stringWithFormat:@"%@ (%@) %@ %@", formattedDateString, [self getWeekString], [self getTimeString], JMOLocalizedString(@"arrive", nil)];
}

- (Discounts *)isZdollarDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"ZDOLLAR"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;
    
}

- (Discounts *)isPromoCodeDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"PROMO_CODE"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;
    
}

- (NSString *)getProductIdString {
    
    return [NSString stringWithFormat:@"%ld", (long)self.id];
}

@end

@implementation Payment_All_Language

@end

@implementation Order_Status_All_Language

@end

@implementation Address

@end

@implementation Shipping_All_Language

@end

@implementation Rating

@end

