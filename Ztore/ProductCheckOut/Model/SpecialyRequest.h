//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialyRequest : REObject

@property(nonatomic, strong) NSString *choiceString1;
@property(nonatomic, assign) BOOL isChoiceString1;
@property(nonatomic, strong) NSString *choiceString2;
@property(nonatomic, assign) BOOL isChoiceString2;
@property(nonatomic, strong) NSString *choiceString3;
@property(nonatomic, assign) BOOL isChoiceString3;
@property(nonatomic, strong) NSString *specialyRequestUserInput;
@property(nonatomic, strong) NSString *specialyRequestString;
@property(nonatomic, strong) NSString *specialyRequestPlaceHolder;

+ (instancetype)shared;
- (NSString *)getChoiceString;
- (NSString *)getUserInputString;
- (void)initString;

@end
