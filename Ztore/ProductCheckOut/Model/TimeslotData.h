//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Timeslot

@end

@interface Timeslot : REObject

@property(nonatomic, copy) NSString *end_at;

@property(nonatomic, copy) NSString *shipping_fee;

@property(nonatomic, assign) NSInteger time_start;

@property(nonatomic, assign) NSInteger time_end;

@property(nonatomic, assign) BOOL available;

@property(nonatomic, assign) NSInteger date;

@property(nonatomic, copy) NSString *start_at;

@property(nonatomic, copy) NSString *free_shipping_threshold;

- (NSString *)getDayString;

- (NSString *)getTimeString;

- (NSString *)getWeekString;

- (NSString *)getFullDateString;

- (NSString *)getFullDateAndTimeString;

- (NSString *)getYearAndMonth;

@end

@interface TimeslotData : REObject

@property(nonatomic, strong) NSArray<Timeslot> *data;

- (int)getTimeSlotCount7;

@end
