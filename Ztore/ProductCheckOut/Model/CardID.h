//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardID : REObject

@property(nonatomic, strong) NSString *data;

@end

@interface WrongCardID : REObject

@property(nonatomic, strong) NSArray<NSString *> *data;

@end
