//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductDetailModel.h"
#import <Foundation/Foundation.h>
#import "UserAccount.h"

@class Payment_All_Language, Order_Status_All_Language, Address, Shipping_All_Language, Rating;

@protocol PaymentSuccessData

@end

@interface PaymentSuccessData : REObject

@property(nonatomic, assign) NSInteger delivery_end;

@property(nonatomic, copy) NSString *shipping_code;

@property(nonatomic, copy) NSString *shipping;

@property(nonatomic, strong) Shipping_All_Language *shipping_all_language;

@property(nonatomic, copy) NSString *order_status;

@property(nonatomic, assign) NSInteger tx_gen;

@property(nonatomic, copy) NSString *delivery_fee;

@property(nonatomic, copy) NSString *promotion_pool_description;

@property(nonatomic, strong) Payment_All_Language *payment_all_language;

@property(nonatomic, copy) NSString *payment_code;

@property(nonatomic, strong) NSArray<Products> *products;

@property(nonatomic, strong) NSArray<Discounts> *discounts;

@property(nonatomic, copy) NSString *discounted_subtotal;

@property(nonatomic, copy) NSString *total_rebate_zdollar;

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *payment;

@property(nonatomic, assign) NSInteger delivery_date;

@property(nonatomic, assign) NSInteger delivery_start;

@property(nonatomic, strong) UserAccountData *user;

@property(nonatomic, copy) NSString *order_sn;

@property(nonatomic, strong) Order_Status_All_Language *order_status_all_language;

@property(nonatomic, copy) NSString *product_subtotal;

@property(nonatomic, copy) NSString *discounted_subtotal_before_zdollar;

@property(nonatomic, copy) NSString *total_price;

@property(nonatomic, copy) NSString *user_remark;

@property(nonatomic, assign) NSInteger date;

@property(nonatomic, copy) NSString *order_status_code;

@property(nonatomic, copy) NSString *remark;

@property(nonatomic, copy) NSString *promotion_description;

@property(nonatomic, strong) Address *address;

@property(nonatomic, strong) Rating *rating;

@property(nonatomic, assign) BOOL is_rated;

@property(nonatomic, assign) BOOL newYearFirstObject;

- (NSString *)getYearString;

- (NSString *)getFullDateFormatString;

- (NSString *)getDayString;

- (NSString *)getWeekString;

- (NSString *)getFullDateString;

- (NSString *)getFullDateAndTimeString;

- (Discounts *)isZdollarDiscount;

- (Discounts *)isPromoCodeDiscount;

- (NSString *)getProductIdString;

@end

@interface Payment_All_Language : REObject

@property(nonatomic, copy) NSString *tc;

@property(nonatomic, copy) NSString *en;

@end

@interface Order_Status_All_Language : REObject

@property(nonatomic, copy) NSString *tc;

@property(nonatomic, copy) NSString *en;

@end

@interface Address : REObject

@property(nonatomic, copy) NSString *consignee;

@property(nonatomic, copy) NSString *temperature;

@property(nonatomic, copy) NSString *contact_no;

@property(nonatomic, assign) BOOL is_agree_reused_box;

@property(nonatomic, copy) NSString *mobile;

@property(nonatomic, assign) BOOL is_pass_to_guard;

@property(nonatomic, copy) NSString *area;

@property(nonatomic, assign) BOOL is_collect_box_glass;

@property(nonatomic, copy) NSString *address;

@property(nonatomic, assign) BOOL is_no_lift;

@property(nonatomic, copy) NSString *region;

@property(nonatomic, copy) NSString *district;

@property(nonatomic, assign) NSInteger consignee_title;

@property(nonatomic, copy) NSString *country;

@property(nonatomic, copy) NSString *area_code;

@end

@interface Shipping_All_Language : REObject

@property(nonatomic, copy) NSString *tc;

@property(nonatomic, copy) NSString *en;

@end

@interface Rating : REObject

@property(nonatomic, assign) int rating_accuracy;

@property(nonatomic, assign) int rating_appearance;

@property(nonatomic, strong) NSString *rating_comment;

@property(nonatomic, assign) int rating_friendliness;

@property(nonatomic, assign) int rating_ontime;

@property(nonatomic, assign) int rating_situation;

@end

@interface PaymentSuccess : REObject

@property(nonatomic, strong) PaymentSuccessData *data;

@end
