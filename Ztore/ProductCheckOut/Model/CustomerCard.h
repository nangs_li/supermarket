//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CustomerCard

@end

@interface CustomerCard : REObject

@property(nonatomic, strong) NSString *cardType;
@property(nonatomic, strong) NSString *cardholderName;
@property(nonatomic, strong) NSString *expirationDate;
@property(nonatomic, strong) NSString *expirationMonth;
@property(nonatomic, strong) NSString *expirationYear;
@property(nonatomic, strong) NSString *id;
@property(nonatomic, strong) NSString *maskedNumber;
@property(nonatomic, strong) NSString *cvc;
@property(nonatomic, assign) BOOL oldCard;

@end
