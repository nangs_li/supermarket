//
//  Address.m
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import "SpecialyRequest.h"

@implementation SpecialyRequest

+ (instancetype)shared {
  static id instance_ = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance_ = [[self alloc] init];
  });

  return instance_;
}

- (id)init {

  self                            = [super init];
    
  return self;
}

- (void)initString {
    
    self.choiceString1              = JMOLocalizedString(@"Please leave my order at management desk if nobody at home when delivery arrives", nil);
    self.choiceString2              = JMOLocalizedString(@"Please use new paper box to deliver", nil);
    self.choiceString3              = JMOLocalizedString(@"Please collect paper carton or soft drink bottles from last purchase", nil);
    self.specialyRequestPlaceHolder      = JMOLocalizedString(@"If you have any requests, please fill in this column. We will try our best to help you.", nil);
    self.specialyRequestString = JMOLocalizedString(@"Special Requirement(s):", nil);
    self.isChoiceString2 = self.isChoiceString3 = self.isChoiceString1 = NO;
    self.specialyRequestUserInput = nil;

}

- (NSString *)getChoiceString {

    NSMutableString *choiceString = [NSMutableString string];
    (self.isChoiceString1) ?  [choiceString appendString:[NSString stringWithFormat:@"-%@", self.choiceString1]] : nil;
    (choiceString.length>0 && self.isChoiceString2) ?  [choiceString appendString:[NSString stringWithFormat:@"\n"]] : nil;
    (self.isChoiceString2) ?  [choiceString appendString:[NSString stringWithFormat:@"-%@", self.choiceString2]] : nil;
      (choiceString.length>0 && self.isChoiceString3) ?  [choiceString appendString:[NSString stringWithFormat:@"\n"]] : nil;
    (self.isChoiceString3) ?  [choiceString appendString:[NSString stringWithFormat:@"-%@", self.choiceString3]] : nil;
    
    return choiceString;
    
}

- (NSString *)getUserInputString {
    
    NSString *string = @"_";
    
    if (self.specialyRequestUserInput.length > 0) {
        
    string = [string stringByAppendingString:self.specialyRequestUserInput];
        
    }
    
    return  (self.specialyRequestUserInput.length > 0) ?  string : nil;
    
}

@end
