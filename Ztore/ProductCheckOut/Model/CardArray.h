//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerCard.h"

@interface CardArray : REObject

@property(nonatomic, strong) NSArray<CustomerCard> *data;

@end
