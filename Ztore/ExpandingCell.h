//
//  ExpandingCell.h
//  Ztore
//
//  Created by ken on 2/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductCategoryModel.h"
#import "SLExpandableTableView.h"
#import <UIKit/UIKit.h>

@class ProductCategoryData;

@interface ExpandingCell : UITableViewCell <UIExpandingTableViewCell>

@property(weak, nonatomic) IBOutlet UIButton *menuBtn;
@property(weak, nonatomic) IBOutlet UIImageView *expandImage;
@property(weak, nonatomic) IBOutlet UILabel *expandLabel;
@property(assign, nonatomic, getter=isLoading) BOOL loading;
@property(readonly, nonatomic) UIExpansionStyle expansionStyle;
@property(strong, nonatomic) NSIndexPath *indexPath;
@property(weak, nonatomic) IBOutlet UIView *whiteLine;
@property(weak, nonatomic) IBOutlet UIImageView *menuIconView;

- (void)setUpCellFromProductCategoryData:(ProductCategoryData *)productCategoryData indexPath:(NSIndexPath *)indexPath;
- (void)setUpSelectedCellFromProductCategoryData:(ProductCategoryData *)productCategoryData indexPath:(NSIndexPath *)indexPath;
- (void)setUpSubCellFromProductChildren:(Children *)children indexPath:(NSIndexPath *)indexPath;

@end
