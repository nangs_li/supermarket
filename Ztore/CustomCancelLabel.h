
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

typedef NS_ENUM(NSInteger, LabelType) { UsePromoCode ,DeliveryFee, UseZDollar, VIPZDollarReBate, UseZDollarCode, TotalPrice};

@protocol CustomCancelLabelDelegate <NSObject>

- (void)pressCancelBtnFromLabelType:(LabelType)labelType;
- (void)pressUseBtnFromLabelType:(LabelType)labelType;

@end

@interface CustomCancelLabel : UIView

@property(strong, nonatomic) IBOutlet UIView *contentView;
@property(strong, nonatomic) id<CustomCancelLabelDelegate> cancelBtnDelegate;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property(weak, nonatomic) IBOutlet UILabel *feeLabel;
@property(assign, nonatomic) LabelType labelType;

- (void)setUpViewFromTitleText:(NSString *)titleText feeText:(NSString *)feeText labelType:(LabelType)labelType haveCancelBtn:(BOOL)haveCancelBtn discount:(Discounts *)discount haveCheckOutBtn:(BOOL)haveCheckOutBtn;

@end
