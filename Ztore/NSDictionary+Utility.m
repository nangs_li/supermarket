//
//  NSDictionary+Utility.m
//  real-v2-ios
//
//  Created by Ken on 25/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "NSDictionary+Utility.h"

@implementation NSDictionary (Utility)

- (NSString *)jsonString {
  NSError *error;
  NSString *jsonString;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];

  if (error) {
    DDLogError(@"jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
    jsonString = @"{}";
  } else {
    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  }

  return jsonString;
}

+ (NSDictionary *)dictionaryFromJsonString:(NSString *)jsonString {
  NSError *error;
  NSData *data       = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary *dict = [[NSDictionary alloc] init];
  id jsonObject      = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

  if (error) {
    DDLogError(@"fromJsonString: error: %@", error.localizedDescription);
  } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
    dict = (NSDictionary *)jsonObject;
  } else {
    DDLogError(@"fromJsonString: error: not a dictionary");
  }

  return dict;
}

@end
