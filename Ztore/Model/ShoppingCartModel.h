//
//  ShppingCart.h
//  Ztore
//
//  Created by ken on 26/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductList.h"
#import <Foundation/Foundation.h>

@interface Name_All_Language : REObject

@property(nonatomic, copy) NSString *tc;

@property(nonatomic, copy) NSString *en;

@end

@protocol Discounts

@end

@interface Discounts : REObject

@property(nonatomic, copy) NSString *subtotal;

@property(nonatomic, copy) NSString *code;

@property(nonatomic, copy) NSString *unit_price;

@property(nonatomic, assign) NSInteger qty;

@property(nonatomic, copy) NSString *rebate_zdollar;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, strong) Name_All_Language *name_all_language;

@end

@protocol Results

@end

@interface Results : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, assign) BOOL status;

@property(nonatomic, assign) NSInteger stock_qty;

@property(nonatomic, assign) NSInteger purchase_quota;

@property(nonatomic, assign) BOOL affected_by_bundle;

@end

@interface ShoppingCartData : REObject

@property(nonatomic, assign) BOOL status;

@property(nonatomic, strong) NSString *error;

@property(nonatomic, assign) NSInteger error_code;

@property(nonatomic, strong) NSString *data;

@property(nonatomic, copy) NSString *product_subtotal;

@property(nonatomic, strong) NSArray<Products> *products;

@property(nonatomic, copy) NSString *delivery_fee;

@property(nonatomic, copy) NSString *discounted_subtotal_before_zdollar;

@property(nonatomic, strong) NSArray<Discounts> *discounts;

@property(nonatomic, copy) NSString *discounted_subtotal;

@property(nonatomic, copy) NSString *free_shipping_min_order_amount;

@property(nonatomic, copy) NSString *total_price;

@property(nonatomic, copy) NSString *total_rebate_zdollar;

- (Discounts *)isZdollarDiscount;

- (Discounts *)isPromoCodeDiscount;

- (Discounts *)isFRIENDDiscount;

- (Discounts *)isBASKETDiscount;

- (BOOL)checkIsNotEnoughStock;

@end

@interface ShoppingCartMainData : REObject

@property(nonatomic, strong) ShoppingCartData *cart;

@property(nonatomic, strong) NSArray<Results> *results;

@end

@interface AddProductReturnShoppingCartModel : REObject

@property(nonatomic, strong) ShoppingCartMainData *data;

@end
