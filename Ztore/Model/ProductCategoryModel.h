//
//  ProductCategoryModel.h
//  Ztore
//
//  Created by Ken on 23/8/2016.
//  Copyright © ken. All rights reserved.
//

#import "REObject.h"
#import "ProductDetailModel.h"

@class ProductCategoryData, Hover_Icons, Icons, Children;

@protocol Children

@end

@interface Children : REObject

@property(nonatomic, copy) NSString *link;

@property(nonatomic, copy) NSString *related_id;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, copy) NSString *type;

@end

@protocol ProductCategoryData

@end

@interface ProductCategoryData : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, strong) NSString *category_id;

@property(nonatomic, copy) NSString *link;

@property(nonatomic, assign) BOOL is_mutual;

@property(nonatomic, strong) Icons *hover_icons;

@property(nonatomic, assign) BOOL has_line;

@property(nonatomic, copy) NSString *icon;

@property(nonatomic, strong) NSArray<Children> *children;

@property(nonatomic, copy) NSString *hover_icon;

@property(nonatomic, copy) NSString *hover_mobile_icon;

@property(nonatomic, copy) NSString *mobile_icon;

@property(nonatomic, assign) BOOL is_red;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, strong) Icons *icons;

@end

@interface Hover_Icons : REObject

@property(nonatomic, copy) NSString *idpi;

@property(nonatomic, copy) NSString *xhdpi;

@property(nonatomic, copy) NSString *mdpi;

@property(nonatomic, copy) NSString *hdpi;

@property(nonatomic, copy) NSString *xxhdpi;

@property(nonatomic, copy) NSString *xxxhdpi;

- (Zoom *)changeToZoomClass;

@end

@interface Icons : REObject

@property(nonatomic, copy) NSString *idpi;

@property(nonatomic, copy) NSString *xhdpi;

@property(nonatomic, copy) NSString *mdpi;

@property(nonatomic, copy) NSString *hdpi;

@property(nonatomic, copy) NSString *xxhdpi;

@property(nonatomic, copy) NSString *xxxhdpi;

- (Zoom *)changeToZoomClass;

@end

@interface ProductCategoryModel : REObject

@property(nonatomic, strong) NSArray<ProductCategoryData> *data;

@end
