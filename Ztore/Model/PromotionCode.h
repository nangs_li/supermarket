//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PromotionCodeData

@end

@interface PromotionCodeData : REObject

@property(nonatomic, assign) BOOL is_not_allow_cancel;

@property(nonatomic, copy) NSString *code;

@end

@interface PromotionCode : REObject

@property(nonatomic, strong) PromotionCodeData *data;

@end
