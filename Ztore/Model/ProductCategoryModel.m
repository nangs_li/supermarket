//
//  ProductCategoryModel.m
//  Ztore
//
//  Created by Ken on 23/8/2016.
//  Copyright © ken. All rights reserved.
//

#import "ProductCategoryModel.h"

@implementation ProductCategoryModel

@end

@implementation ProductCategoryData

@end

@implementation Hover_Icons

- (Zoom *)changeToZoomClass {
    
    Zoom *zoom  = [[Zoom alloc] init];
    zoom.mdpi   = self.mdpi;
    zoom.xhdpi  = self.xhdpi;
    zoom.hdpi   = self.hdpi;
    zoom.xxhdpi = self.xxhdpi;
    
    return zoom;
}

@end

@implementation Icons

- (Zoom *)changeToZoomClass {
    
    Zoom *zoom  = [[Zoom alloc] init];
    zoom.mdpi   = self.mdpi;
    zoom.xhdpi  = self.xhdpi;
    zoom.hdpi   = self.hdpi;
    zoom.xxhdpi = self.xxhdpi;
    
    return zoom;
}

@end

@implementation Children

@end
