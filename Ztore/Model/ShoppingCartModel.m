//
//  ShppingCart.m
//  Ztore
//
//  Created by ken on 26/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ShoppingCartModel.h"

@implementation Name_All_Language

@end

@implementation Discounts

@end

@implementation Results

@end

@implementation AddProductReturnShoppingCartModel

@end

@implementation ShoppingCartMainData

@end

@implementation ShoppingCartData

- (Discounts *)isZdollarDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"ZDOLLAR"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;

}

- (Discounts *)isPromoCodeDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"PROMO_CODE"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;
    
}

- (Discounts *)isFRIENDDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"FRIEND"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;
    
}

- (Discounts *)isBASKETDiscount {
    
    Discounts *discount = nil;
    
    for (Discounts *discounts in self.discounts) {
        
        if ([discounts.code isEqualToString:@"BASKET"]) {
            
            discount = discounts;
            
        }
    }
    
    return discount;
    
}

- (BOOL)checkIsNotEnoughStock {
    
    BOOL checkIsNotEnoughStock = NO;
    
    for (Products *products in self.products) {
        
        Products *productObject = [products getGlobalProduct];
        
        checkIsNotEnoughStock = ([productObject isNotEnoughStock]) ? YES : checkIsNotEnoughStock ;
        
    }
    
    return checkIsNotEnoughStock;
    
}

@end


