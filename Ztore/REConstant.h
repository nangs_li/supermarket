//
//  REConstant.h
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, REAPIQueueStatus) { REAPIQueueStatusPending = 0, REAPIQueueStatusSuccess, REAPIQueueStatusFail };

typedef NS_ENUM(NSInteger, REHttpMethod) { REHttpMethodGet = 0, REHttpMethodPost };

typedef NS_ENUM(NSInteger, RESocialNetworkType) { RESocialNetworkTypeFacebook = 1, RESocialNetworkTypeTweeter = 2, RESocialNetworkTypeGoogle = 3, RESocialNetworkTypeWeibo = 4, RESocialNetworkTypeRealNetwork = 5 };

typedef NS_ENUM(NSInteger, RETabIndex) { RETabIndexSearch = 0, RETabIndexNewFeed, RETabIndexChat, RETabIndexMe, RETabIndexSetting };

@interface REConstant : NSObject

FOUNDATION_EXPORT NSTimeInterval const kTransitionDuration;
FOUNDATION_EXPORT NSTimeInterval const kAnimationDuration;
FOUNDATION_EXPORT NSTimeInterval const kSearchBarInputDelayDuration;

@end
