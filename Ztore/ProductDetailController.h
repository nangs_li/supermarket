//
//  ProductDetailController.h
//  Ztore
//
//  Created by ken on 4/8/16.
//  Copyright © ken. All rights reserved.
//

#import "_UIViewController.h"
#import "BlueTag.h"
#import "AddProductObject.h"

@interface ProductDetailController : _UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, BlueTagDelegate, AddProductObjectDelegate>

@property(strong, nonatomic) UIViewController *parentController;
@property(strong, nonatomic) UIPageViewController *pageViewController_images;
@property(retain, nonatomic) UIPageControl *pageControl_images;
@property(strong, nonatomic) IBOutlet UIScrollView *scrollView_parent;
@property(weak, nonatomic) IBOutlet UIButton *backBtn;
@property(retain, nonatomic) UIButton *btn_like;
@property(retain, nonatomic) UIButton *upBtn;
@property(retain, nonatomic) UIButton *downBtn;
@property(retain, nonatomic) BlueTag *view_soldOut;
@property(retain, nonatomic) UIView *view_cart;
@property(retain, nonatomic) UIButton *btn_addCartImage;
@property(retain, nonatomic) UILabel *label_cartQty;
@property(retain, nonatomic) IBOutlet UIView *view_images;
@property(retain, nonatomic) UIView *topViewContainer;
@property(retain, nonatomic) UILabel *label_brand;
@property(retain, nonatomic) UILabel *label_productName;
@property(retain, nonatomic) UILabel *label_productCountry;
@property(retain, nonatomic) UILabel *label_volume;
@property(retain, nonatomic) UIView *view_prices;
@property(retain, nonatomic) UIView *nib_firstView;
@property(retain, nonatomic) UILabel *label_orginalPrice;
@property(retain, nonatomic) UILabel *label_actualPrice;
@property(retain, nonatomic) UILabel *label_productDesc;
@property(retain, nonatomic) UIButton *btn_productDesc;
@property(retain, nonatomic) UIView *view_tagList;
@property(retain, nonatomic) UIView *view_expandDesc;
@property(retain, nonatomic) UIView *view_description;
@property(assign, nonatomic) int productId;
@property(strong, nonatomic) AddProductObject *addProductObject;
@property(strong, nonatomic) BlueTag *blueTag;
@property(strong, nonatomic) NSMutableArray *blueTagArray;
@property(strong, nonatomic) Products *orignalProduct;
@property(strong, nonatomic) ProductDetailModel *productDetailModel;
@property(strong, nonatomic) Products *product;

- (void)setLabelCartQtyText;

@end
