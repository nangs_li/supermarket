//
//  ProductCategoryController.h
//  Ztore
//
//  Created by ken on 19/7/16.
//  Copyright © ken. All rights reserved.
//

#import "CAPSPageMenu.h"
#import <UIKit/UIKit.h>
#import "EHHorizontalSelectionView.h"
#import "MainPageRowView.h"
#import "XLsn0wLoop.h"
#import "BigProductCollectionCell.h"

@interface ProductCategoryController : _UIViewController <UIViewControllerTransitioningDelegate, UIScrollViewDelegate, EHHorizontalSelectionViewProtocol, UICollectionViewDelegateFlowLayout, XLsn0wLoopDelegate, CAPSPageMenuDelegate, BlueTagDelegate, ProductCollectionCellDelegate>

@property(nonatomic) NSUInteger numberOfTabs;
@property(nonatomic) NSInteger parentCatId;
#pragma mark mainPage
@property(weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *mainScrollView;
@property(weak, nonatomic) IBOutlet UIView *centerView;
@property(weak, nonatomic) IBOutlet UIView *bannerView;
@property(weak, nonatomic) IBOutlet UIView *iconView;
@property(strong, nonatomic) ProductDetailArray *mainPageNewProductList;
@property(strong, nonatomic) FeatureProductListArray *mainPageProductList;
@property(strong, nonatomic) NSMutableArray *viewArray;
@property(strong, nonatomic) NSMutableArray *collectectViewArray;
@property(strong, nonatomic) MainPageRowView *perviousSwipeView;
@property(weak, nonatomic) IBOutlet UILabel *cashIconLabel;
@property(weak, nonatomic) IBOutlet UILabel *delveryIconLabel;
@property(weak, nonatomic) IBOutlet UILabel *returnMoneyIconLabel;
@property(weak, nonatomic) IBOutlet XLsn0wLoop *bannerLoopView;
@property(strong, nonatomic) CAPSPageMenu *pageMenu;
@property(strong, nonatomic) UIActivityIndicatorView *loadingView_BannerView;
@property(strong, nonatomic) UIActivityIndicatorView *loadingView_NewItem;
@property(assign, nonatomic) BOOL showMainPage;

- (void)getCategoryAPIFromLevel2Category_ID:(NSInteger)Level2Category_ID;
- (void)callMainPageAPI;
- (void)listProductGroupByCategoryFromLevel3Category_ID:(NSInteger)Level3Category_ID;
- (void)showMainPage:(BOOL)show;
- (void)reloadAllCollectionView;

@end
