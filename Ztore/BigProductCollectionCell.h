//
//  OnSaleCollectionCell.h
//  Ztore
//
//  Created by ken on 7/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductCollectionCell.h"

@interface BigProductCollectionCell : ProductCollectionCell

- (void)setUpProductCellDataFromProduct:(Products *)product mainPage:(BOOL)mainPage;

@end
