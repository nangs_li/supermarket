//
//  REAgentProfile.m
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentProfile.h"
#import "REDBAgentProfile.h"

@implementation REDBAgentProfile

+ (NSString *)primaryKey {
  return @"MemberID";
}

+ (NSArray *)indexedProperties {
  return @[ @"MemberID" ];
}

- (REAgentProfile *)toREObject {

  REAgentProfile *dbAgentProfile = [[REAgentProfile alloc] init];
  dbAgentProfile.FollowerCount   = self.FollowerCount;
  dbAgentProfile.Specialties     = (NSMutableArray<Specialties> *)[NSMutableArray arrayWithRLMArray:self.Specialties];
  dbAgentProfile.Version         = self.Version;
  dbAgentProfile.FollowingCount  = self.FollowingCount;
  dbAgentProfile.LicenseNumber   = self.LicenseNumber;
  dbAgentProfile.IsFollowing     = self.IsFollowing;
  dbAgentProfile.Languages       = (NSMutableArray<Languages2> *)[NSMutableArray arrayWithRLMArray:self.Languages];
  dbAgentProfile.QBID            = self.QBID;
  dbAgentProfile.MemberType      = self.MemberType;
  dbAgentProfile.PhotoURL        = self.PhotoURL;
  dbAgentProfile.MemberID        = self.MemberID;
  dbAgentProfile.MemberName      = self.MemberName;
  dbAgentProfile.Experiences     = (NSMutableArray<Experiences> *)[NSMutableArray arrayWithRLMArray:self.Experiences];
  dbAgentProfile.PastClosings    = (NSMutableArray<PastClosings> *)[NSMutableArray arrayWithRLMArray:self.PastClosings];

  return dbAgentProfile;
}

@end

@implementation DBLanguages

- (Languages2 *)toREObject {

  Languages2 *dbLanguages = [[Languages2 alloc] init];
  dbLanguages.Language    = self.Language;
  dbLanguages.LangIndex   = self.LangIndex;

  return dbLanguages;
}

@end

@implementation DBExperiences

- (Experiences *)toREObject {

  Experiences *dbExperiences = [[Experiences alloc] init];
  dbExperiences.IsCurrentJob = self.IsCurrentJob;
  dbExperiences.EndDate      = self.EndDate;
  dbExperiences.Title        = self.Title;
  dbExperiences.Company      = self.Company;
  dbExperiences.StartDate    = self.StartDate;

  return dbExperiences;
}

@end

@implementation DBSpecialties

- (Specialties *)toREObject {

  Specialties *specialties = [[Specialties alloc] init];
  specialties.Specialty    = self.Specialty;

  return specialties;
}

@end

@implementation DBPastClosings

- (PastClosings *)toREObject {

  PastClosings *dbPastClosings  = [[PastClosings alloc] init];
  dbPastClosings.PropertyType   = self.PropertyType;
  dbPastClosings.SoldDate       = self.SoldDate;
  dbPastClosings.State          = self.State;
  dbPastClosings.SoldDateString = self.SoldDateString;
  dbPastClosings.Address        = self.Address;
  dbPastClosings.Country        = self.Country;
  dbPastClosings.SoldDate       = self.SoldDate;
  dbPastClosings.Position       = self.Position;
  dbPastClosings.ID             = self.ID;
  dbPastClosings.SpaceType      = self.SpaceType;

  return dbPastClosings;
}

@end
