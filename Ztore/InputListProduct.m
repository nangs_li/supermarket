//
//  InputListProduct.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "InputListProduct.h"

@implementation InputListProduct

- (id)init {

  self              = [super init];
  self.category_ids = nil;
  self.tags         = nil;
  self.shop_ids     = nil;
  self.brand_ids    = nil;
  self.keyword      = nil;
  self.order_by     = nil;
  self.result_start      = 0;
  self.result_limit      = PRODUCT_LIST_LIMIT;

  return self;
}

- (BOOL)checkHaveSearchType {
    
    return (self.type == search_tags || self.type == search_brand_ids);
}

@end
