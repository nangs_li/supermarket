//
//  UIColor+REColor.m
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "UIColor+REColor.h"

@implementation UIColor (REColor)

+ (UIColor *)RETopBarBlueColor {

  return [UIColor colorWithRed:66 / 255.0f green:77 / 255.0f blue:93 / 255.0f alpha:1.0];
}

+ (UIColor *)REDimBackgroundColor {

  return [UIColor colorWithWhite:0 alpha:0.8];
}

@end
