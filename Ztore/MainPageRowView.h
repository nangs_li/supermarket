
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "MainPageRowView.h"

@interface MainPageRowView : UIView

@property(weak, nonatomic) IBOutlet EHHorizontalSelectionView *horizontalView;
@property(weak, nonatomic) IBOutlet UIButton *titleBtn;

@end
