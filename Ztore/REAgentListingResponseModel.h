//
//  REAgentListingResponseModel.h
//  real-v2-ios
//
//  Created by Ken on 15/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentListing.h"
#import "REServerResponseModel.h"

@interface REAgentListingResponseModel : REServerResponseModel

@property(nonatomic, strong) NSMutableArray<REAgentListing> *AgentListings;

@end
