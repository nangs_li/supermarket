//
//  NSString+Utility.m
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

- (NSString *)trim {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)isValidEmail {
    
        BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
        NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
        NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
        return [emailTest evaluateWithObject:self];
    
}

- (NSNumber *)toNSNmuer {
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    return [f numberFromString:self];
}

- (NSString *)removeHtmlCode {
    
    NSString *descString = @"";
    NSString *desc;
    
    if (isValid(self)) {
        
        desc = [self stringByReplacingOccurrencesOfString:@"<br>" withString:@"abcdefghijklionmop"];
        NSAttributedString *attr = [[NSAttributedString alloc] initWithData:[desc dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                              NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                         documentAttributes:nil
                                                                      error:nil];
        desc = [[attr string] stringByReplacingOccurrencesOfString:@"abcdefghijklionmop" withString:@"\n"];
        desc = [desc stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        descString = desc;
        
    }
    
    return [[NSAttributedString alloc] initWithString:descString].string;
    
}

@end
