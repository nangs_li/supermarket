//
//  Constants.m
//  Ztore
//
//  Created by ken on 18/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Constants.h"

@implementation Constants

const bool IS_DEBUG = true;

// NSString *BASE_URL = @"http://ztore-api.innoverz.com/api/call/ios";
// NSString *GETENCRYPTKEY_URL =
// @"http://ztore-api.innoverz.com/api/getPublicKey/ios";

//--------Production Server API-----------
// NSString *BASE_URL = @"https://api.ztore.com/api/call/ios";
// NSString *GETENCRYPTKEY_URL = @"https://api.ztore.com/api/getPublicKey/ios";

//--------Testing Server API-----------
// NSString *BASE_URL = @"https://staging-api.ztore.com/api/call/ios";
// NSString *GETENCRYPTKEY_URL =
// @"https://staging-api.ztore.com/api/getPublicKey/ios";


//NSString *BASE_URL          = @"http://api.ztore-test.opp.com.hk:8109/api/call/ios";
//NSString *GETENCRYPTKEY_URL = @"http://api.ztore-test.opp.com.hk:8109/api/getPublicKey/ios";


//--------Testing Server API-----------
NSString *BASE_URL          = @"https://staging-api.ztore.com/api/call/ios";
NSString *GETENCRYPTKEY_URL = @"https://staging-api.ztore.com/api/getPublicKey/ios";

const int ENCODESTR_LIMIT    = 25;

//-------------------------------------------

const float DEFAULT_FONT_SIZE        = 16.0f;

const float GL_COMMON_MARGIN = 18.0f;
const float GL_COMMON_HEIGHT = 42.0f;
const float GL_TAG_MARGIN    = 16.0f;
const float GL_TAG_HEIGHT    = 33.0f;
const float GL_BUTTON_HEIGHT = 26.0f;

const float INCREASE_PRODUCT_SPEED     = 0.15;
const float INCREASE_PRODUCT_API_SPEED = 0.8;

const int PRODUCT_LIST_LIMIT = 24;

const int MainPage_ScrollView_Left_Space      = 43;
const int MinSpaceBetweenCell                 = 18;
const int MainPage_TopScrollView_Left_Space   = 70;
const int MainPage_TopScrollViewCellHeight    = 128;
const int MainPage_ScrollViewCellHeight       = 158;

const int TopMessageBarDuration = 5;

@end
