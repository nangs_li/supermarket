//
//  REAgentListing.h
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  strongright © 2016 ken. All rights reserved.
//

#import "REDBObject.h"

@class Reasons, Address, Photos, REAgentListing;

RLM_ARRAY_TYPE(DBReasons)

@interface DBReasons : REDBObject

@property(nonatomic, strong) NSString *Reason3URL;

@property(nonatomic, strong) NSString *Reason3;

@property(nonatomic, strong) NSString *Reason2;

@property(nonatomic, strong) NSString *Reason1;

@property(nonatomic, assign) int SloganIndex;

@property(nonatomic, assign) int LanguageIndex;

@property(nonatomic, strong) NSString *Reason1URL;

@property(nonatomic, strong) NSString *Reason2URL;

- (Reasons *)toREObject;

@end

RLM_ARRAY_TYPE(DBAddress)

@interface DBAddress : REDBObject

@property(nonatomic, strong) NSString *FormattedAddress;

@property(nonatomic, strong) NSString *Lang;

@property(nonatomic, strong) NSString *ShortAddress;

@property(nonatomic, strong) NSString *PlaceID;

- (Address *)toREObject;

@end

RLM_ARRAY_TYPE(DBPhotos)

@interface DBPhotos : REDBObject

@property(nonatomic, strong) NSString *URL;

@property(nonatomic, assign) int PhotoID;

@property(nonatomic, assign) int Position;

@property(nonatomic, assign) int Height;

@property(nonatomic, assign) int Width;

- (Photos *)toREObject;

@end

RLM_ARRAY_TYPE(REDBAgentListing)

@interface REDBAgentListing : REDBObject

@property(nonatomic, strong) RLMArray<DBReasons *><DBReasons> *Reasons;

@property(nonatomic, assign) int BedroomCount;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForIndian;

@property(nonatomic, assign) int SloganIndex;

@property(nonatomic, assign) int KitchenCount;

@property(nonatomic, assign) int LivingRoomCount;

@property(nonatomic, assign) int PropertyType;

@property(nonatomic, assign) int PropertySize;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForRoman;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForChinese;

@property(nonatomic, strong) NSString *CurrencyUnit;

@property(nonatomic, strong) RLMArray<DBAddress *><DBAddress> *Address;

@property(nonatomic, strong) NSString *SizeUnit;

@property(nonatomic, assign) int BathroomCount;

@property(nonatomic, strong) NSString *CreationDate;

@property(nonatomic, assign) int PropertyPrice;

@property(nonatomic, assign) int ListingFollowerCount;

@property(nonatomic, assign) int BalconyCount;

@property(nonatomic, strong) RLMArray<DBPhotos *><DBPhotos> *Photos;

@property(nonatomic, assign) int AgentListingID;

@property(nonatomic, assign) int SizeUnitType;

@property(nonatomic, assign) int Version;

@property(nonatomic, assign) int SpaceType;

- (REAgentListing *)toREObject;

@end
