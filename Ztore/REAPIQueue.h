//
//  REAPIQueue.h
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REObject.h"

@class REDBAPIQueue;

@interface REAPIQueue : REObject

@property(strong, nonatomic) NSString *queueId;
@property(assign, nonatomic) REHttpMethod httpMethod;
@property(strong, nonatomic) NSMutableArray *dependingQueues;
@property(strong, nonatomic) NSMutableArray *pendingQueues;
@property(assign, nonatomic) NSInteger retryCount;
@property(strong, nonatomic) REServerResponseModel *response;
@property(strong, nonatomic) NSString *url;
@property(strong, nonatomic) NSDictionary *parameter;
@property(assign, nonatomic) REAPIQueueStatus status;

- (REDBAPIQueue *)toREDBObject;

+ (REAPIQueue *)queueWithUrl:(NSString *)url parameter:(NSDictionary *)parameter httpMethod:(REHttpMethod)httpMethod;

+ (REAPIQueue *)queueWithUrl:(NSString *)url parameter:(NSDictionary *)parameter httpMethod:(REHttpMethod)httpMethod dependencies:(NSArray<REAPIQueue *> *)dependencies;

@end
