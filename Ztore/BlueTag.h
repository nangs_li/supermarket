
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//
#import "ProductList.h"

@class BlueTag;

typedef NS_ENUM(NSInteger, TagType) { BlueTagType, SoldOutType};

@protocol BlueTagDelegate <NSObject>

- (void)pressBlueTagBtnFromPromotions:(Promotions *)promotions;
- (void)pressSoldOutBtnFromProduct:(Products *)product;

@end

@interface BlueTag : UIView

@property(weak, nonatomic) IBOutlet UIButton *titleBtn;
@property(strong, nonatomic) Promotions *promotions;
@property(strong, nonatomic) id<BlueTagDelegate> delegate;
@property(assign, nonatomic) TagType tagType;
@property(strong, nonatomic) Products *product;

- (void)setUpFromPromotions:(Promotions *)promotions;
- (void)setUpNotStockCase;
- (void)setUpSoldoutViewFromProduct:(Products *)product;

@end
