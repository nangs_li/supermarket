//
//  ServerAPI.h
//  Ztore
//
//  Created by ken on 17/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "Constants.h"
#import "DCArrayMapping.h"
#import "DCKeyValueObjectMapping.h"
#import "DCParserConfiguration.h"
#import "RSA.h"
#import "UIView+Toast.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AddressData.h"
#import "AutoCompleteProduct.h"
#import "GetCategory.h"
#import "InputListProduct.h"
#import "InputUpdateCurrentUser.h"
#import "ProductCategoryModel.h"
#import "ProductDetailModel.h"
#import "ProductList.h"
#import "ShoppingCartModel.h"
#import "UserAccount.h"
#import "PromotionCode.h"
#import "CustomerCard.h"
#import "CardID.h"
#import "ClientToken.h"
#import "CardArray.h"
#import "PaymentSuccess.h"
#import <AFNetworking/AFNetworking.h>
#import "BannerModel.h"

@class ServerAPI;

@interface ServerAPI : NSObject
+ (nullable instancetype)shared;

#pragma mark SessionKey
- (nullable NSString *)getSessionKey;
- (void)initEncryptKeyCompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark UserAddress
- (void)listCurrentUserAddressCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)addCurrentUserAddressFromConsignee_title:(int)consignee_title
                                        consignee:(nullable NSString *)consignee
                                          mobile:(nullable NSString *)mobile
                                      contact_no:(nullable NSString *)contact_no
                                         country:(nullable NSString *)country
                                          region:(nullable NSString *)region
                                        district:(nullable NSString *)district
                                            area:(nullable NSString *)area
                                         address:(nullable NSString *)address
                                      is_no_lift:(BOOL)is_no_lift
                                 CompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)updateCurrentUserAddressFromAddressId:(int)addressId
                                 consignee_title:(int)consignee_title
                                        consignee:(nullable NSString *)consignee
                                          mobile:(nullable NSString *)mobile
                                      contact_no:(nullable NSString *)contact_no
                                         country:(nullable NSString *)country
                                          region:(nullable NSString *)region
                                        district:(nullable NSString *)district
                                            area:(nullable NSString *)area
                                         address:(nullable NSString *)address
                                      is_no_lift:(BOOL)is_no_lift
                                 CompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)deleteCurrentUserAddressFromAddressId:(int)addressId completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)setCurrentUserAddressDefaultFromAddressId:(int)addressId completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Delievery Address
- (void)listRegionFromCountry_id:(int)country_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)listDistrictFromRegion_id:(int)region_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)listAreaFromDistrict_id:(int)district_id completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Timeslot
- (void)getTimeslotFromCode:(int)code completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Payment
- (void)getBraintreeClientTokenCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getCustomerCardCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)deleteCustomerCardFromCardId:(nullable NSString *)cardId CompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)verifyNewCreditCardFromPayment_method_nonce:(nullable NSString *)payment_method_nonce cardholder_name:(nullable NSString *)cardholder_name CompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)verifyExistCreditCardFromCard_id:(nullable NSString *)card_id cvv:(nullable NSString *)cvv CompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)createOrderWithPaymentByPaymentMethodCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)createOrderWithPaymentWithNewCustomerCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)createOrderWithCoDPaymentCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)createOrderWithPaymentMethodFromPaymentCode:(nullable NSString *)paymentCode paymentMethodNonce:(nullable NSString *)paymentMethodNonce completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark login 
- (void)isLoginCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)loginWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)userRegisterWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)connectFbAccountWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password accessToken:(nonnull NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)forgetPasswordWithEmail:(nonnull NSString *)email completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)changePasswordWithOldPassword:(nonnull NSString *)oldPassword newPassword:(nonnull NSString *)newPassword completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)connectFbAccountWithLoginedFromAccessToken:(nonnull NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)fbLoginWithAccessToken:(nonnull NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)logoutCompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark User Data
- (void)getCurrentUserCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)setPromotionCode:(nullable NSString *)code completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark CheckOut
- (void)getPromotionCodeCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)updateCurrentUserWithInput:(nonnull InputUpdateCurrentUser *)input completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Left Menu
- (void)getCategoryTreeCompletion:(nullable REAPICommonBlock)completionBlock;
- (void)getCategoryChildFromParent_id:(NSInteger)parent_id completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Main Page
- (void)getBannerListCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)listProductGroupByCategoryFromParent_category_id:(NSInteger)parent_category_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)listFeaturedProductFromLimit:(int)limit completionBlock:(nullable REAPICommonBlock)completionBlock ;
- (void)listFeaturedCategoryProductListFromLimit:(int)limit completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Product Detail
- (void)getProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getProductByUrlKey:(nullable NSString *)urlKey completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)userListAddProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)userListDeleteProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Product Filter
- (void)getProductCountFromInput:(nullable InputListProduct *)input completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Promotion Page
- (void)getPromotionInfoFromSn:(nullable NSString *)sn completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Product CollectionView
- (void)listProductWithAllBrandTagFromInput:(nonnull InputListProduct *)input completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark My Favourite Page
- (void)userListListProductCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)userListListRecentlyPurchasedProductCompletionBlock:(nullable REAPICommonBlock)completionBlock;

- (void)setUseZdollar:(BOOL)useZdollar completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Add Product And Right Menu get ShoppingCart
- (void)cartAddProductFromProductArraysReturnCart:(BOOL)returnCart onlyGetShoppingCart:(BOOL)onlyGetShoppingCart completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Search View Controller Auto Complete
- (void)getAutoCompleteWithKeyword:(nullable NSString *)keyword completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Order History Page
- (void)listCurrentUserPaidOrderCompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark ConfirmOrder Page
- (void)getCurrentUserPaidOrderFromOrder_id:(int)order_id completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Rating Page
- (void)ratingOrderServiceFromOrder_id:(int)order_id rating_friendliness:(nullable NSNumber *)rating_friendliness rating_appearance:(nullable NSNumber *)rating_appearance rating_ontime:(nullable NSNumber *)rating_ontime rating_accuracy:(nullable NSNumber *)rating_accuracy rating_situation:(nullable NSNumber *)rating_situation remark:(nullable NSString *)remark completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark ZDollar Page
- (void)getUserZDollarLogCompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Contact us Page
- (void)sendEmailToUsTitle:(nullable NSString *)title
                                    name:(nullable NSString *)name
                                       email:(nullable NSString *)email
                                   mobile:(nullable NSString *)mobile
                                      need:(nullable NSString *)need
                                       remark:(nullable NSString *)remark
                                CompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark Product Notice
- (void)addProductNoticeFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)cancelProductNoticeFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark get Free Shpping Min Order Amount
- (void)getFreeShppingMinOrderAmountCompletionBlock:(nullable REAPICommonBlock)completionBlock;

@end
