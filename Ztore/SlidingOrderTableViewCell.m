//
//  SlidingOrderTableViewCell.m
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FICDPhotoLoader.h"
#import "ServerAPI.h"
#import "SlidingOrderTableViewCell.h"

@implementation SlidingOrderTableViewCell

- (void)awakeFromNib {
  // Initialization code

  [super awakeFromNib];
  [self setUpAllBtn];
    
}

- (void)setUpAllBtn {
    
    self.addProductObject = [[AddProductObject alloc] init];
    self.addProductObject.delegate = self;
    self.addProductObject.upBtn = self.upBtn;
    self.addProductObject.downBtn = self.downBtn;
    self.addProductObject.btn_addToCart = self.btn_addToCart;
    [self.addProductObject setUpAllAction];

}

- (void)setUpOrderCellFromProduct:(Products *)product ProductCellType:(ProductCellType)productCellType {

  self.productCellType = productCellType;
  self.product          = product;
  self.label_brand.font = [[Common shared] getSmallContentNormalFont];
  [self.label_brand setTextColor:[[Common shared] getCommonRedColor]];
  [self.label_brand setText:product.brand];
  UILabel *label_productName = self.label_Name;
  label_productName.font     = [[Common shared] getSmallContentNormalFont];
  label_productName.text     = product.name;
  [label_productName setBackgroundColor:[UIColor clearColor]];
  [label_productName setNumberOfLines:3];
  [label_productName sizeToFit];
  UILabel *label_volume = self.label_Volume;
  label_volume.font     = [[Common shared] getSmallContentNormalFont];
  label_volume.text     = product.volume;
  [label_volume setBackgroundColor:[UIColor clearColor]];
  [label_volume setTextColor:[[Common shared] getCommonGrayColor]];
  [label_volume setNumberOfLines:1];
  [label_volume sizeToFit];

  self.label_price.font    = [[Common shared] getSmallContentNormalFont];
  self.label_price.text    = [[Common shared] getStringFromPriceString:product.price.promotion_price];
  self.label_subtotal.font = [[Common shared] getContentBoldFont];
  self.label_subtotal.text = [[Common shared] getStringFromPriceString:product.discounted_subtotal];

  [FICDPhotoLoader loaderImageForImageView:self.imageView_product withPhotoUrl:[FICDPhotoLoader getPhotoUrlInProduct:[product.image changeToZoomClass]]];

  NSString *freeString = JMOLocalizedString(@"Free", nil);
    
  [product setUpHideButton];
  
    if (self.productCellType == ConfirmOrderHistoryType) {
        
        if (product.isFreeProduct) {
            
            self.label_Quality.text = [NSString stringWithFormat:@"%li", (long)(product.cart_freebie_qty)];
            
        } else {
            
            self.label_Quality.text = [NSString stringWithFormat:@"%li", (long)(product.cart_qty - product.cart_freebie_qty)];
        }
        
    } else {
    
    product = [[Common shared] setQtyLabelTextFromProduct:product label:self.label_Quality];
        
    }

  UIColor *pressUpBtnColor = (product.pressUpbtn) ? [[Common shared] getPressIncreseButtonColor] : [UIColor clearColor];

  UIColor *pressDownBtnColor = (product.pressDownbtn) ? [[Common shared] getPressIncreseButtonColor] : [UIColor clearColor];

  [self.upBtn setBackgroundColor:pressUpBtnColor];

  [self.downBtn setBackgroundColor:pressDownBtnColor];

  self.upBtn.alpha = (product.hideupbtn == YES) ? 0.3 : 1;
  //    self.upBtn.userInteractionEnabled = (product.hideupbtn == YES) ? NO : YES;
  self.downBtn.alpha = (product.hidedownbtn == YES) ? 0.3 : 1;
  //    self.downBtn.userInteractionEnabled = (product.hidedownbtn == YES) ? NO : YES;
  self.label_standard.font = [[Common shared] getSmallContentNormalFont];
  self.label_standard.text = [[Common shared] getStringFromPriceString:product.subtotal];
  [[Common shared] setStrikethroughForLabel:self.label_standard withText:[[Common shared] getStringFromPriceString:product.subtotal]];
  self.label_promotion.font           = [[Common shared] getContentBoldFont];
  self.label_promotion.text           = [[Common shared] getStringFromPriceString:product.discounted_subtotal];
  self.label_promotion.textColor      = [[Common shared] getCommonRedColor];
  self.label_subtotal.text            = [[Common shared] getStringFromPriceString:product.subtotal];
  self.label_promationPrice.textColor = [[Common shared] getCommonRedColor];

  if (self.myFavouritePage) {
        
        [self.label_standard setHidden:TRUE];
        [self.label_promotion setHidden:TRUE];
        [self.label_subtotal setHidden:TRUE];
      
  } else {
        
  if (product.isCanAddFreeProduct) {

    int subtotalCount = [self.product.subtotal intValue];

    for (Promotions *promotion in self.product.promotions) {

      if ([promotion.type isEqualToString:@"freebie"]) {

        subtotalCount += [promotion.subtotal intValue];
      }
    }

    [[Common shared] setStrikethroughForLabel:self.label_standard withText:[[Common shared] getStringFromPriceString:[NSString stringWithFormat:@"%d", subtotalCount]]];

    if ([self.label_standard.text isEqualToString:product.discounted_subtotal]) {

      [self.label_standard setHidden:TRUE];
      [self.label_promotion setHidden:TRUE];
      [self.label_subtotal setHidden:FALSE];

    } else {

      [self.label_standard setHidden:FALSE];
      [self.label_promotion setHidden:FALSE];
      [self.label_subtotal setHidden:TRUE];
    }

  } else if (product.isFreeProduct) {

    [self.label_standard setHidden:TRUE];
    [self.label_promotion setHidden:TRUE];
    [self.label_subtotal setHidden:FALSE];
    self.label_subtotal.text      = freeString;
    self.label_subtotal.textColor = [[Common shared] getCommonRedColor];


  } else if ([product.subtotal isEqualToString:product.discounted_subtotal]) {

    [self.label_standard setHidden:TRUE];
    [self.label_promotion setHidden:TRUE];
    [self.label_subtotal setHidden:FALSE];
    self.label_subtotal.textColor = [UIColor blackColor];

  } else {

    [self.label_standard setHidden:FALSE];
    [self.label_promotion setHidden:FALSE];
    [self.label_subtotal setHidden:TRUE];
    self.label_subtotal.textColor = [UIColor blackColor];
  }

  }

  self.upBtn.hidden   = (product.isFreeProduct) ? YES : NO;
  self.downBtn.hidden = (product.isFreeProduct) ? YES : NO;
  self.label_price.font = self.label_promationPrice.font = [[Common shared] getSmallContentNormalFont];
    
  if ([product.price.standard_price isEqualToString:product.price.promotion_price]) {

    [self.label_promationPrice setHidden:FALSE];
    [self.label_price setHidden:TRUE];
    self.label_promationPrice.text = [[Common shared] getStringFromPriceString:product.price.promotion_price];
    [self.label_promationPrice setTextColor:[UIColor blackColor]];

  } else {

    [self.label_promationPrice setHidden:FALSE];
    [self.label_price setHidden:FALSE];
    self.label_price.text = [[Common shared] getStringFromPriceString:product.price.standard_price];
    [[Common shared] setStrikethroughForLabel:self.label_price withText:[[Common shared] getStringFromPriceString:product.price.standard_price]];
    self.label_promationPrice.text = [[Common shared] getStringFromPriceString:product.price.promotion_price];
    [self.label_price setTextColor:[[Common shared] getCommonGrayColor]];
    [self.label_promationPrice setTextColor:[[Common shared] getCommonRedColor]];
    self.label_promationPrice.font      = [[Common shared] getContentBoldFont];
      
  }
  
  if (product.isFreeProduct) {
        
     self.label_promationPrice.text = @"$0";
     self.label_promationPrice.textColor = [[Common shared] getCommonRedColor];
        
  }
    
    [self.btn_addToCart setHidden:YES];
    [self.shoppingCartDeteteBtn setHidden:YES];
    self.label_Quality.hidden = self.upBtn.hidden = self.downBtn.hidden = NO;
    self.topLine.hidden = self.downLine.hidden = self.leftLine.hidden = self.rightLine.hidden = YES;
    self.topLine.backgroundColor = self.downLine.backgroundColor = self.leftLine.backgroundColor = self.rightLine.backgroundColor = [[Common shared] getCommonRedColor];
    self.deleteBtn.hidden = YES;
    
    if (self.productCellType == FavouriteType) {
        
        self.deleteBtn.hidden = NO;
        [self.btn_addToCart setHidden:(product.cart_qty > 0) || [product isNotEnoughStock]];
        self.label_Quality.hidden = self.upBtn.hidden = self.downBtn.hidden = !self.btn_addToCart.hidden;
        [self.label_standard setHidden:TRUE];
        [self.label_promotion setHidden:TRUE];
        [self.label_subtotal setHidden:TRUE];

    } else if (self.productCellType == RecentPurchaseType) {
    
        self.deleteBtn.hidden = YES;
        [self.btn_addToCart setHidden:(product.cart_qty > 0) || [product isNotEnoughStock]];
         self.label_Quality.hidden = self.upBtn.hidden = self.downBtn.hidden = !self.btn_addToCart.hidden;
        [self.label_standard setHidden:TRUE];
        [self.label_promotion setHidden:TRUE];
        [self.label_subtotal setHidden:TRUE];
        
    } else if (self.productCellType == ShoppingCartType) {
      
        if ([product isNotEnoughStock])  {
            
            self.shoppingCartDeteteBtn.hidden = NO;
            self.upBtn.hidden = self.downBtn.hidden = self.label_Quality.hidden = !self.shoppingCartDeteteBtn.hidden;
            self.deleteBtn.hidden = YES;
            self.topLine.hidden = self.downLine.hidden = self.leftLine.hidden = self.rightLine.hidden = NO;

        }

  }

    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)invalidAllTimer {

  if (self.addProductObject.increaseProductTimer) {

    [self.addProductObject.increaseProductTimer invalidate];
  }

  if (self.addProductObject.decreaseProductTimer) {

    [self.addProductObject.decreaseProductTimer invalidate];
  }

  self.product.pressDownbtn = NO;
  self.product.pressUpbtn   = NO;

  [self setUpOrderCellFromProduct:self.product ProductCellType:self.productCellType];
    
}

- (void)addProductAPIWithAddCount:(int)addCount {

  [[Common shared] addProductQtyFromProduct:self.product addCount:addCount];

   [self setUpOrderCellFromProduct:self.product ProductCellType:self.productCellType];
}

- (void)cleanProductStock {
    
    [self addProductAPIWithAddCount:-(int)self.product.cart_qty];
    
}

- (IBAction)deleteBtnPressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(deleteBtnPressedFromCell:product:)]) {
        [self.delegate deleteBtnPressedFromCell:self product:self.product];
    }
    
}

@end
