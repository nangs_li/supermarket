//
//  UIColor+REColor.h
//  real-v2-ios
//
//  Created by Ken on 14/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (REColor)

+ (UIColor *)RETopBarBlueColor;

+ (UIColor *)REDimBackgroundColor;

@end
