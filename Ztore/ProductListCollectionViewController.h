//
//  ProductListCollectionViewController.h
//  Ztore
//
//  Created by ken on 22/7/16.
//  Copyright © ken. All rights reserved.
//

#import "_UIViewController.h"
#import "ProductAdanceSearchView.h"
#import "BlueTag.h"
#import "PromotionHeaderView.h"
#import "ProductCollectionCell.h"

@class PromotionModel;

@interface ProductListCollectionViewController : _UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, ProductAdanceSearchViewDelegate, UIViewControllerTransitioningDelegate, UICollectionViewDelegateFlowLayout, ProductCollectionCellDelegate>

@property(retain, nonatomic) NSString *labelString;
@property(retain, nonatomic) UIViewController *parentController;
@property(strong, nonatomic) ProductList *productList;
@property(retain, nonatomic) InputListProduct *inputListProduct;
@property(retain, nonatomic) NSMutableArray *products;
@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
@property(nonatomic, strong) ProductAdanceSearchView *productAdanceSearchView;
@property(assign, nonatomic) BOOL haveCallProductListAPI;
@property(strong, nonatomic) MJRefreshAutoNormalFooter *footer;
@property(assign, nonatomic) BOOL cleanProducts;
@property(strong, nonatomic) PromotionModel *promotionModel;
@property(strong, nonatomic) PromotionHeaderView *headCell;

+ (ProductListCollectionViewController *)initViewControllerWithInputListProduct:(InputListProduct *)inputListProduct;
- (void)getListProductWithtBrands:(NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(NSArray<NSString *> *)productTags selectedSort:(NSString *)sort;

- (void)initCollectionView:(UICollectionView *)myCollectionView collectionViewFlowLayout:(UICollectionViewFlowLayout *)myCollectionViewFlowLayout;
- (void)callgetListProduct;
- (void)listProductFromProductListData:(ProductListData *)productListData;

@end
