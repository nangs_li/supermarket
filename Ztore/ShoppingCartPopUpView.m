//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ShoppingCartPopUpView.h"

#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation ShoppingCartPopUpView

#pragma mark set Up ShoppingCart PopUpView

- (void)setupView {

  self.usePromoCodeTextField =   [self.usePromoCodeView addCustomTextFieldView];
  [self.usePromoCodeTextField setFieldText:JMOLocalizedString(@"Enter Code", nil) errorText:JMOLocalizedString(@"This promotion code is invalid", nil)];
  self.usePromoCodeTextField.textField.delegate = self;
  self.titleLabel.text                          = JMOLocalizedString(@"Use Promo Code", nil);
  [self.backBtn setTitle:JMOLocalizedString(@"Back", nil) forState:(UIControlState)UIControlStateNormal animation:NO];
  self.backBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
  [self.backBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
  [self.useBtn setTitle:JMOLocalizedString(@"Use", nil) forState:(UIControlState)UIControlStateNormal animation:NO];
  [self.useBtn setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
  self.useBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];


}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];

}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [textFieldView textFieldEnd];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
