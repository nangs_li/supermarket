//
//  SlidingOrderTableViewCell.h
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ShoppingCartModel.h"
#import <UIKit/UIKit.h>
#import "AddProductObject.h"

typedef NS_ENUM(NSInteger, ProductCellType) { ShoppingCartType, FavouriteType, RecentPurchaseType, ConfirmOrderHistoryType, ConfirmOrderType};


@class SlidingOrderTableViewCell;

@protocol MyFavouriteCellDelegate <NSObject>

- (void)deleteBtnPressedFromCell:(SlidingOrderTableViewCell *)cell product:(Products *)product;

@end

@interface SlidingOrderTableViewCell : UITableViewCell <AddProductObjectDelegate>

@property(strong, nonatomic) IBOutlet UILabel *label_brand;
@property(strong, nonatomic) IBOutlet UILabel *label_price;
@property(strong, nonatomic) IBOutlet UILabel *label_subtotal;
@property(strong, nonatomic) IBOutlet UIImageView *imageView_product;
@property(strong, nonatomic) IBOutlet UILabel *label_Name;
@property(strong, nonatomic) IBOutlet UILabel *label_Volume;
@property(strong, nonatomic) IBOutlet UILabel *label_Quality;
@property(strong, nonatomic) IBOutlet UIButton *upBtn;
@property(strong, nonatomic) IBOutlet UIButton *downBtn;
@property(strong, nonatomic) Products *product;
@property(strong, nonatomic) IBOutlet UILabel *label_standard;
@property(strong, nonatomic) IBOutlet UILabel *label_promotion;
@property(strong, nonatomic) IBOutlet UILabel *label_promationPrice;
@property(strong, nonatomic) AddProductObject *addProductObject;
@property(assign, nonatomic) BOOL historyPrdouct;
@property(assign, nonatomic) BOOL myFavouritePage;
@property(assign, nonatomic) ProductCellType productCellType;
@property(weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property(weak, nonatomic) IBOutlet UIButton *shoppingCartDeteteBtn;
@property(strong, nonatomic) id<MyFavouriteCellDelegate> delegate;
@property(weak, nonatomic) IBOutlet UIButton *btn_addToCart;
@property(weak, nonatomic) IBOutlet UIView *topLine;
@property(weak, nonatomic) IBOutlet UIView *downLine;
@property(weak, nonatomic) IBOutlet UIView *rightLine;
@property(weak, nonatomic) IBOutlet UIView *leftLine;

- (void)setUpAllBtn;
- (void)setUpOrderCellFromProduct:(Products *)product ProductCellType:(ProductCellType)productCellType;
- (void)cleanProductStock ;

@end
