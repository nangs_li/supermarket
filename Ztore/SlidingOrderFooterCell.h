//
//  SlidingOrderFooterCell.h
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ShoppingCartModel.h"
#import <UIKit/UIKit.h>
#import "CustomCancelLabel.h"
#import "RightMenuVC.h"

@interface SlidingOrderFooterCell : UIView

@property(weak, nonatomic) IBOutlet UILabel *label_subtotalText;
@property(weak, nonatomic) IBOutlet UIButton *btn_checkout;
@property(weak, nonatomic) IBOutlet UILabel *label_subtotalFee;
@property(weak, nonatomic) IBOutlet UIButton *expandButton;
@property(weak, nonatomic) IBOutlet UILabel *label_OrderDetail;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductSubtotal;
@property(weak, nonatomic) IBOutlet UILabel *label_ProductPrice;
@property(weak, nonatomic) IBOutlet UITextField *greyLine;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *checkoutBtnHeight;
@property(weak, nonatomic) IBOutlet UITextField *greyLine2;
@property(weak, nonatomic) IBOutlet UIButton *btn_zdollarTitle;
@property(weak, nonatomic) IBOutlet UIView *checkoutBtnBottomView;
@property(weak, nonatomic) IBOutlet CustomCancelLabel *deliveryFeeView;
@property(weak, nonatomic) IBOutlet CustomCancelLabel *useZdollarView;
@property(weak, nonatomic) IBOutlet UIView *totalPriceView;
@property(weak, nonatomic) IBOutlet UITextField *greyLine3;
@property(weak, nonatomic) IBOutlet UITextField *totalPriceGeyLine;
@property(weak, nonatomic) IBOutlet CustomCancelLabel *totalPriceTextView;
@property(assign, nonatomic) int viewHeight;
@property(assign, nonatomic) int zdollarViewCount;
@property(strong, nonatomic) NSMutableArray *viewArray;
@property(strong, nonatomic) NSMutableArray *zdollarViewArray;
@property(strong, nonatomic) UIView *lastView;

- (void)setupDataFromCart:(ShoppingCartData *)cart rightMenuVC:(RightMenuVC *)rightMenuVC userAccountData:(UserAccountData *)userAccountData haveCheckOutBtn:(BOOL)haveCheckOutBtn;

@end
