//
//  InputUpdateCurrentUser.h
//  Ztore
//
//  Created by ken on 24/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InputUpdateCurrentUser : NSObject

@property(nonatomic, strong) NSString *email;
@property(nonatomic, strong) NSString *first_name;
@property(nonatomic, strong) NSString *last_name;
@property(nonatomic, strong) NSString *company;
@property(nonatomic, strong) NSString *contact_no;
@property(nonatomic, strong) NSString *mobile;
@property(nonatomic, strong) NSString *title;

@end
