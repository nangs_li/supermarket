//
//  ProductCategory.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

//#import "GetCategory.h"
//
//@implementation GetProductCategory
//
//@end
//
//@implementation GetCategory
//
//@end
//
//
#import "ProductDetailModel.h"
#import <JSONModel/JSONModel.h>

@implementation ProductDetailModel

@end

@implementation ProductDetailArray

- (void)hideAllPressAction {

    for (Products *product in self.data) {
        
      product.pressDownbtn =  product.pressUpbtn = NO;
        
    }
}

- (NSArray *)removeAllNotAvailableProduct {
    
    NSMutableArray<Products> *dataArray = [[NSMutableArray<Products> alloc] init];
    
    for (Products  *product in self.data) {
        
        if (product.available) {
            
            [dataArray addObject:product];
        }
        
    }
    
    return [dataArray copy];
}

@end

@implementation ProductAddWishList

@end

@implementation ProductPrice

@end

@implementation ProductImages

@end

@implementation Zoom

@end

@implementation Normal

@end
