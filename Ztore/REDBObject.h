//
//  REDBObject.h
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import <Realm/Realm.h>

@class REObject;

RLM_ARRAY_TYPE(REDBObject)

@interface REDBObject : RLMObject

@property(nonatomic, assign) NSInteger updatedStamp;

- (nonnull REObject *)toREObject; // Abstract method. Must be implemented in subclasses

@end
