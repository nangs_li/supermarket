//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "PromotionHeaderView.h"
#import "PromotionModel.h"
#import "FICDPhotoLoader.h"

@implementation PromotionHeaderView

#pragma mark App LifeCycle

- (void)setupViewFromPromotion:(PromotionModel *)promotion urlLink:(NSString *)urlLink {
    
    self.urlLink = urlLink;
    self.titleLabel.text = [promotion.data.title removeHtmlCode];
    self.titleLabel.font = [[Common shared] getNormalTitleFont];
    self.titleLabel.textColor = [UIColor blackColor];
    self.dateLabel.text = [promotion.data.desc removeHtmlCode];
    self.dateLabel.font = [[Common shared] getSmallContentNormalFont];
    self.dateLabel.textColor = [UIColor lightGrayColor];
   
//    [self.promotionImageView loadImageURL:[NSURL URLWithString:[urlLink stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] withIndicator:YES];
    [FICDPhotoLoader loaderImageForImageView:self.promotionImageView withPhotoUrl:[self.urlLink stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    self.promotionImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.backgroundColor = [UIColor whiteColor];
    [self.titleLabel sizeToFit];
    [self.dateLabel sizeToFit];
    
    self.imageViewHeight.constant = (isValid(self.urlLink)) ? 78 : 0;
    
}

- (CGFloat)getTotalHeight {
  
   self.titleLabel.font = [[Common shared] getNormalTitleFont];
   self.dateLabel.font = [[Common shared] getSmallContentNormalFont];
   self.imageViewHeight.constant = (isValid(self.urlLink)) ? 78 : 0;
   CGFloat height = (isValid(self.urlLink)) ? 78 : 0;
   [self.titleLabel sizeToFit];
   [self.dateLabel sizeToFit];
    CGFloat titleHeight =  [Common heightOfTextForString:self.titleLabel.text andFont:self.titleLabel.font maxSize:CGSizeMake([RealUtility screenBounds].size.width - 24, MAXFLOAT)];
    
    [self.titleLabel updateViewHeight:height];
    
    return (height + 15 + titleHeight + 6 + self.dateLabel.frame.size.height + 15);
}

@end
