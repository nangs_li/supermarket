//
//  RadialGradientLayer.m
//  productionreal2
//
//  Created by Ken on 17/11/2015.
//  Copyright © 2015 Ken. All rights reserved.
//

#import "RadialGradientLayer.h"

@implementation RadialGradientLayer

- (instancetype)init {
  self = [super init];
    
  if (self) {
    self.locations = @[ @(0.0f), @(1.0f) ];
    self.scales = @[ @(1.5f), @(1.0f) ];
    [self setNeedsDisplay];
  }
    
  return self;
}

+ (BOOL)needsDisplayForKey:(NSString *)key {
    
  if ([@"locations" isEqualToString:key] ||
      [@"locations" isEqualToString:key]) {
      
    return YES;
  }
    
  return [super needsDisplayForKey:key];
}

- (id<CAAction>)actionForKey:(NSString *)key {
    
  if ([key isEqualToString:@"locations"]) {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];
    animation.timingFunction = [CAMediaTimingFunction
        functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fromValue = self.locations;
      
    return animation;
  } else if ([key isEqualToString:@"scales"]) {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];
    animation.timingFunction = [CAMediaTimingFunction
        functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fromValue = self.scales;
      
    return animation;
  }
    
  return [super actionForKey:key];
}

- (void)drawInContext:(CGContextRef)ctx {

  // Create gradient
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  CGFloat locations[] = {[self.locations[0] floatValue],
                         [self.locations[1] floatValue]};

  UIColor *centerColor = [UIColor colorWithWhite:0 alpha:0.3];
  UIColor *edgeColor = [UIColor colorWithWhite:0 alpha:0.0];

  NSArray *colors =
      [NSArray arrayWithObjects:(__bridge id)centerColor.CGColor,
                                (__bridge id)edgeColor.CGColor, nil];
  CGGradientRef gradient = CGGradientCreateWithColors(
      colorSpace, (__bridge CFArrayRef)colors, locations);

  // Scaling transformation and keeping track of the inverse
  CGAffineTransform scaleT = CGAffineTransformMakeScale(
      [self.scales[0] floatValue], [self.scales[1] floatValue]);
  CGAffineTransform invScaleT = CGAffineTransformInvert(scaleT);

  // Extract the Sx and Sy elements from the inverse matrix
  // (See the Quartz documentation for the math behind the matrices)
  CGPoint invS = CGPointMake(invScaleT.a, invScaleT.d);

  // Transform center and radius of gradient with the inverse
  CGPoint center = CGPointMake((self.bounds.size.width / 2) * invS.x,
                               ((self.bounds.size.height / 2)) * invS.y);
  CGFloat minWidth = MIN(self.bounds.size.width, self.bounds.size.height);
  CGFloat radius = (minWidth / 2) * invS.x;

  // Draw the gradient with the scale transform on the context
  CGContextScaleCTM(ctx, scaleT.a, scaleT.d);
  CGContextDrawRadialGradient(ctx, gradient, center, 0, center, radius,
                              kCGGradientDrawsBeforeStartLocation);

  // Reset the context
  CGContextScaleCTM(ctx, invS.x, invS.y);

  // Continue to draw whatever else ...

  // Clean up the memory used by Quartz
  CGGradientRelease(gradient);
  CGColorSpaceRelease(colorSpace);
    
}

@end
