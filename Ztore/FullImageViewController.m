//
//  FullImageViewController.m
//  Ztore
//
//  Created by ken on 24/9/15.
//  Copyright (c) ken. All rights reserved.
//

#import "FullImageViewController.h"
#import "PageViewImageController.h"

@interface FullImageViewController ( )

@end

@implementation FullImageViewController {
  int imagePageViewCurIdx;
  float previousScale;
  float IMAGE_HEIGHT;
  float INDICATOR_HEIGHT;
}

NSString *CANCEL_IMAGE_NAME = @"ic_clear_48pt_3x";

- (void)viewDidLoad {
  [super viewDidLoad];
  imagePageViewCurIdx = 0;
  previousScale       = 1.0f;
  IMAGE_HEIGHT        = self.view.frame.size.width;
  INDICATOR_HEIGHT    = 44;

  // Do any additional setup after loading the view.
  [self.view setBackgroundColor:[UIColor whiteColor]];

  self.pageViewController_images            = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
  self.pageViewController_images.dataSource = self;
  self.pageViewController_images.delegate   = self;
  [[self.pageViewController_images view] setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height + 40)];

  PageViewImageController *initImageViewController = [self imageViewControllerAtIndex:0];
  NSArray *viewControllers                         = [NSArray arrayWithObject:initImageViewController];
  [self.pageViewController_images setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
  [self addChildViewController:self.pageViewController_images];
  [self.view addSubview:[self.pageViewController_images view]];
  [self.pageViewController_images didMoveToParentViewController:self];

  NSArray *subviews          = self.pageViewController_images.view.subviews;
  UIPageControl *thisControl = nil;
  for (int i = 0; i < [subviews count]; i++) {
    if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
      thisControl = (UIPageControl *)[subviews objectAtIndex:i];
    }
  }
  // set pageControl position
  self.pageControl_images = [[UIPageControl alloc] initWithFrame:CGRectMake(thisControl.frame.origin.x, self.view.frame.size.height - INDICATOR_HEIGHT, thisControl.frame.size.width, thisControl.frame.size.height)];
  [self.pageControl_images setCenter:self.view.center];
  self.pageControl_images                               = [[UIPageControl alloc] initWithFrame:CGRectMake(self.pageControl_images.frame.origin.x, self.view.frame.size.height - INDICATOR_HEIGHT, thisControl.frame.size.width, thisControl.frame.size.height)];
  self.pageControl_images.numberOfPages                 = self.imageUrls.count;
  self.pageControl_images.currentPage                   = 0;
  self.pageControl_images.pageIndicatorTintColor        = [UIColor lightGrayColor];
  self.pageControl_images.currentPageIndicatorTintColor = [[Common shared] getCommonRedColor];
  [self.pageControl_images setNeedsLayout];
  [self.view addSubview:self.pageControl_images];
  [thisControl removeFromSuperview];

  UIImageView *imageView_cancel = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 44, 44)];
  imageView_cancel.contentMode  = UIViewContentModeScaleAspectFill;
  [imageView_cancel setImage:[UIImage imageNamed:CANCEL_IMAGE_NAME]];
  [[Common shared] addClickEventForView:imageView_cancel withTarget:self action:@"dismissFullImageView"];

  [self.view addSubview:imageView_cancel];

  [imageView_cancel mas_makeConstraints:^(MASConstraintMaker *make) {

    make.top.equalTo(imageView_cancel.superview).with.offset(50); // with is an optional semantic filler
    make.right.equalTo(imageView_cancel.superview).with.offset(-30);
    make.height.mas_equalTo(44);
    make.width.mas_equalTo(44);

  }];
}

- (void)dismissFullImageView {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (PageViewImageController *)imageViewControllerAtIndex:(int)index {
  PageViewImageController *childViewController = [[PageViewImageController alloc] init];
  childViewController.index                    = index;
  childViewController.isFullScreen             = true;
  childViewController.imageUrl                 = (NSString *)self.imageUrls[index];

  return childViewController;
}

#pragma mark - PageViewControllerDelegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
  int index = [(PageViewImageController *)viewController index];

  if (index == 0) {
    return nil;
  }
  index--;

  return [self imageViewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
  int index = [(PageViewImageController *)viewController index];
  index++;

  if (index == self.imageUrls.count) {
    return nil;
  }

  return [self imageViewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
  for (int i = 0; i < pendingViewControllers.count; i++) {
    imagePageViewCurIdx = [(PageViewImageController *)[pendingViewControllers objectAtIndex:i] index];
    NSLog(@"willTransitionToViewControllers: %i", imagePageViewCurIdx);
  }
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {

  if (completed) {
    NSLog(@"didFinishAnimating:(BOOL)finished with index: %i", imagePageViewCurIdx);
    [self.pageControl_images setCurrentPage:imagePageViewCurIdx];
  }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
  return self.imageUrls.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
  return 0;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
