//
//  RealUtility.m
//  real-v2-ios
//
//  Created by Li Ken on 5/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "RealUtility.h"

@implementation RealUtility

+ (CGRect)screenBounds {
  CGRect screenBounds = [UIScreen mainScreen].bounds;

  if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
    screenBounds.size = CGSizeMake(screenBounds.size.height, screenBounds.size.width);
  }

  return screenBounds;
}

+ (NSString *)getDecimalFormatByString:(NSString *)numberString withUnit:(NSString *)unit {

  NSDecimalNumber *rawDecimal   = [NSDecimalNumber decimalNumberWithString:numberString];
  NSString *decimalFormatString = [NSNumberFormatter localizedStringFromNumber:rawDecimal numberStyle:NSNumberFormatterDecimalStyle];

  if (isValid(unit)) {
    decimalFormatString = [decimalFormatString stringByAppendingString:unit];
  }

  return decimalFormatString;
}

+ (void)getImageBy:(PHAsset *)asset size:(CGSize)size handler:(PHAssetBlock)handler {
  PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
  requestOptions.resizeMode             = PHImageRequestOptionsResizeModeExact;
  requestOptions.deliveryMode           = PHImageRequestOptionsDeliveryModeHighQualityFormat;

  // this one is key
  requestOptions.synchronous = true;
  PHImageManager *manager    = [PHImageManager defaultManager];
  [manager requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeDefault options:requestOptions resultHandler:handler];
}

+ (NSString *)floatToString:(float)val {

  NSString *ret = [NSString stringWithFormat:@"%.1f", val];
  unichar c     = [ret characterAtIndex:[ret length] - 1];

  while (c == 48) { // 0
    ret = [ret substringToIndex:[ret length] - 1];
    c   = [ret characterAtIndex:[ret length] - 1];

    // After finding the "." we know that everything left is the decimal number,
    // so get a substring excluding the "."
    if (c == 46) {

      ret = [ret substringToIndex:[ret length] - 1];
    }
  }

  return ret;
}

+ (NSString *)getMonthNameByIndex:(int)index {

  NSDateFormatter *df = [NSDateFormatter localeDateFormatter];

  return [[df monthSymbols] objectAtIndex:index];
}

@end
