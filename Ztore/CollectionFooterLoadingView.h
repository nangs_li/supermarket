//
//  CollectionFooterLoadingView.h
//  Ztore
//
//  Created by ken on 27/8/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionFooterLoadingView : UICollectionReusableView

@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
