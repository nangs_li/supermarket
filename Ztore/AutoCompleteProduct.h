//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AutoCompleteBrands

@end

@interface AutoCompleteBrands : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *name;

@end

@protocol AutoCompleteProducts

@end

@interface AutoCompleteProducts : REObject

@property(nonatomic, copy) NSString *url_key;

@property(nonatomic, copy) NSString *name;

@end

@interface AutoCompleteProductData : REObject

@property(nonatomic, strong) NSArray<AutoCompleteBrands> *brands;

@property(nonatomic, strong) NSArray<NSString *> *tags;

@property(nonatomic, strong) NSArray<AutoCompleteProducts> *products;

- (id)getObjectFromIndexPath:(NSIndexPath *)indexPath;

- (id)getNameFromIndexPath:(NSIndexPath *)indexPath;

- (NSUInteger)getObjectCountFromSection:(NSInteger)section;

@end

@interface AutoCompleteProduct : REObject

@property(nonatomic, strong) AutoCompleteProductData *data;

@end
