//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "DeliveryFeePopUpView.h"

#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation DeliveryFeePopUpView

#pragma mark set Up Delivery Fee Pop Up View

- (void)setupDeliveryFreePopUpViewFromDeliveryFree:(NSString *)deliveryFree limitPrice:(NSString *)limitPrice {
    
    self.titleLabel.font = [[Common shared] getNormalFontWithSize:20];
    
    if ([[Common shared] isEnLanguage]) {
        
        self.titleLabel.text                          = [NSString stringWithFormat:JMOLocalizedString(@"You need to pay $%d delivery fee unless you purchase above $%d，You can...", nil), [deliveryFree intValue] , [limitPrice intValue]];
    } else {
        
        self.titleLabel.text                          = [NSString stringWithFormat:JMOLocalizedString(@"You need to pay $%d delivery fee unless you purchase above $%d，You can...", nil), [limitPrice intValue] , [deliveryFree intValue]];
        
    }
    
    [self.backBtn setTitle:JMOLocalizedString(@"Go Shopping Again", nil) forState:(UIControlState)UIControlStateNormal animation:NO];
    self.backBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    [self.backBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.useBtn setTitle:JMOLocalizedString(@"Proceed Payment", nil) forState:(UIControlState)UIControlStateNormal animation:NO];
    [self.useBtn setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
    self.useBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    
}

@end
