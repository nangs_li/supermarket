//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductList.h"

@protocol Banner

@end

@interface Banner : REObject

@property(nonatomic, copy) NSString *name;

@property(nonatomic, copy) NSString *banner_item_id;

@property(nonatomic, copy) Zoom *image;

@end

@interface PromotionData : REObject

@property(nonatomic, copy) NSString *desc;

@property(nonatomic, copy) NSString *end_at;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) Banner *banner;

@end

@interface PromotionModel : REObject

@property(nonatomic, strong) PromotionData *data;

@end
