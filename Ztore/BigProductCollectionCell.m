

//
//  OnSaleCollectionCell.m
//  Ztore
//
//  Created by ken on 7/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "FICDPhotoLoader.h"
#import "BigProductCollectionCell.h"

@implementation BigProductCollectionCell

- (void)awakeFromNib {
  // Initialization code

  [super awakeFromNib];
    
}

- (void)setUpProductCellDataFromProduct:(Products *)product mainPage:(BOOL)mainPage {

    [super setUpProductCellDataFromProduct:product mainPage:mainPage];
    self.addProductView.hidden = self.btn_addToCart.hidden = YES;
    
    self.label_ProductName.text = [NSString stringWithFormat:@"%@-%@", product.brand, self.label_ProductName.text];
    self.label_ProductName.textAlignment = self.label_ProductPrices.textAlignment = NSTextAlignmentLeft;
    
}

@end
