//
//  NSMutableArray+REDatabase.m
//  real-v2-ios
//
//  Created by Derek Cheung on 25/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "NSMutableArray+REDatabase.h"

// Models
#import "REDBObject.h"

@implementation NSMutableArray (REDatabase)

+ (NSMutableArray *)arrayWithRLMArray:(RLMArray *)array {

  NSMutableArray *output = [NSMutableArray new];

  for (REDBObject *object in array) {

    [output addObject:[object toREObject]];
  }

  return output;
}

@end
