//
//  REAPIQueue.m
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "LoginData.h"
// Models
#import "DBLoginData.h"

@implementation LoginData

- (DBLoginData *)toREDBObject {
  DBLoginData *dbLoginData   = [[DBLoginData alloc] init];
  dbLoginData.id         = self.id;
  dbLoginData.email = self.email;
  dbLoginData.password = self.password;
    
    
  return dbLoginData;
}

@end
