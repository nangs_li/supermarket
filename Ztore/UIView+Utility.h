//
//  UIView+Utility.h
//  productionreal2
//
//  Created by Ken on 26/8/15.
//  Copyright (c) 2015 ken. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "CustomTextFieldView.h"
#import "REAPIClient.h"

enum {
  RelativeDirectionLeft = 0,
  RelativeDirectionRight,
  RelativeDirectionTop,
  RelativeDirectionBottom,
};
typedef NSUInteger RelativeDirection;

enum {
  ViewAlignmentCenter = 0,
  ViewAlignmentLeft,
  ViewAlignmentRight,
  ViewAlignmentTop,
  ViewAlignmentBottom,
  ViewAlignmentHorizontalCenter,
  ViewAlignmentVerticalCenter,
};
typedef NSUInteger ViewAlignment;

@interface UIView (Utility)

- (void)moveViewTo:(nullable UIView *)targetView direction:(RelativeDirection)direction padding:(int)padding;
- (void)setViewAlignmentInSuperView:(ViewAlignment)aligment padding:(int)padding;
- (void)align:(nullable UIView *)targetView direction:(ViewAlignment)aligment padding:(int)padding;
- (void)addSlidingBlinkEffect:(CGFloat)gradientWidth transparency:(CGFloat)transparency duration:(CGFloat)duration repeatCount:(CGFloat)repeat;
- (void)addFlashingEffect:(nullable UIColor *)flashingColor duration:(CGFloat)duration;
- (void)addRadialGradient:(CGFloat)duration;
- (void)drawShadowOpacity;
- (void)drawGreyBorder;
- (void)drawTopShadow;
- (void)clearDrawShadowOpacity;
- (void)drawCricleShadowOpacity;
- (nullable UIView *)getCommonRedStatusBar;
- (void)matchSizeWithSuperViewFromAutoLayout;
- (void)matchSizeWithOtherView:(nullable UIView *)otherView;
- (void)matchSizeWithMessageBarFromAutoLayoutHeight:(int)height;
- (void)matchSizeWithSuperViewNavigationBarFromAutoLayoutTop:(int)top;
- (void)matchSizeWithSuperViewNavigationBarFromAutoLayoutTop:(int)top bottom:(int)bottom;
- (void)matchSizeWithSuperViewWithFullScreenAndAutoLayoutTop:(int)top;
- (void)matchSizeWithSuperViewBottomFromHeight:(int)height;
- (void)matchSizeWithNavigationBarAutoLayout;
- (void)setViewHeight:(int)height;
- (void)updateViewHeight:(int)height;
- (void)removeAllSubView;
- (void)hideAllSubView;
- (void)removeAllSubViewAction;
- (nullable CustomTextFieldView *)addCustomTextFieldView;
- (void)fadeInCompletion:(nullable REAPICommonBlock)completion;
- (void)fadeOutCompletion:(nullable REAPICommonBlock)completion;
- (CGFloat)getViewYAddHeight;
- (void)setWidthWithScreenWidth;
- (void)removeAllConstraints;

@end
