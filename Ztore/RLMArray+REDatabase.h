//
//  RLMArray+REDatabase.h
//  real-v2-ios
//
//  Created by Derek Cheung on 25/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Realm/Realm.h>

// Models
#import "REDBObject.h"

@interface RLMArray (REDatabase)

+ (RLMArray *)arrayWithNSMutableArray:(NSMutableArray *)array objectClass:(Class)objectClass;

@end
