//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GetCategory;

@protocol GetCategory

@end

@interface GetCategory : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *code;

@property(nonatomic, copy) NSString *url_key;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, assign) NSInteger depth;

@property(nonatomic, strong) NSArray<Products> *products;

@end

@interface GetProductCategory : REObject

@property(nonatomic, strong) GetCategory *data;

@end

@interface GetProductCategoryArray : REObject

@property(nonatomic, strong) NSArray<GetCategory> *data;

@end
