//
//  NSURL+Utility.m
//  productionreal2
//
//  Created by Ken on 10/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import "NSDateFormatter+Utility.h"

@implementation NSDateFormatter (Utility)

+ (NSDateFormatter *)localeDateFormatter {

  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
  NSLocale *Locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
  [dateFormatter setLocale:Locale];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
  return dateFormatter;
}

+ (NSDateFormatter *)enUsDateFormatter {

  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  NSTimeZone *timeZone           = [NSTimeZone timeZoneWithName:@"UTC"];
  [dateFormatter setTimeZone:timeZone];
  NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
  [dateFormatter setLocale:usLocale];

  return dateFormatter;
}

- (void)setupDateFormatter {

  [self setDateFormat:@"h:mm a"];
  NSString *languageCode = self.locale.localeIdentifier;

  if ([languageCode isEqualToString:@"fr"]) {

    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [self setLocale:usLocale];
  }

  if ([languageCode isEqualToString:@"zh-Hant"] || [languageCode isEqualToString:@"zh-Hans"]) {

    [self setDateFormat:@"a h:mm"];
  }
}

@end
