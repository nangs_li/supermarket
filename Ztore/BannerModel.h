//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BannerModelData, Item, Detail;

@protocol Detail

@end

@interface Detail : REObject

@property(nonatomic, assign) NSInteger position_id;

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, assign) BOOL is_external_link;

@property(nonatomic, copy) NSString *cat_url_key;

@property(nonatomic, copy) NSString *image_url;

@property(nonatomic, copy) NSString *link;

@property(nonatomic, copy) NSString *abgroup_id;

@property(nonatomic, copy) NSString *abgroup_type;

@property(nonatomic, assign) BOOL is_friend;

@property(nonatomic, copy) NSString *page_url;

@property(nonatomic, assign) NSInteger sort_order;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, assign) NSInteger shop_id;

@end

@protocol Item

@end

@interface Item : REObject

@property(nonatomic, copy) NSString *url;

@property(nonatomic, strong) NSArray<Detail > *detail;

@end

@protocol BannerModelData

@end

@interface BannerModelData : REObject

@property(nonatomic, assign) NSInteger position_id;

@property(nonatomic, assign) NSInteger abgroup;

@property(nonatomic, strong) NSArray<Item > *item;

@end

@interface BannerModel : REObject

@property(nonatomic, strong) NSArray<BannerModelData > *data;

@end

