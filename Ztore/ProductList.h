//
//  ProductList.h
//  Ztore
//
//  Created by ken on 17/9/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductDetailModel.h"
#import <Foundation/Foundation.h>

@protocol Image

@end

@interface Image : REObject

@property(nonatomic, copy) NSString *xhdpi;

@property(nonatomic, copy) NSString *mdpi;

@property(nonatomic, copy) NSString *hdpi;

@property(nonatomic, copy) NSString *xxhdpi;

- (Zoom *)changeToZoomClass;

@end

@protocol Price

@end

@interface Price : REObject

@property(nonatomic, copy) NSString *promotion_price;

@property(nonatomic, copy) NSString *standard_price;

@property(nonatomic, copy) NSString *rank_price;

@end

@protocol Promotions

@end

@interface Promotions : REObject

@property(nonatomic, copy) NSString *id;

@property(nonatomic, copy) NSString *sn;

@property(nonatomic, copy) NSString *type;

@property(nonatomic, copy) NSString *label;

@property(nonatomic, copy) NSString *unit_price;

@property(nonatomic, copy) NSString *qty;

@property(nonatomic, copy) NSString *subtotal;

@end

@protocol Products_Brand

@end

@interface Products_Brand : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, assign) BOOL selected;

@end

@protocol Products_Specialty_Tag

@end

@interface Products_Specialty_Tag : REObject

@property(nonatomic, copy) NSString *name;

@property(nonatomic, assign) BOOL is_active;

@property(nonatomic, assign) BOOL selected;

@end

@protocol Products

@end

@interface Products : REObject

@property(nonatomic, copy) NSString *meta_keyword;

@property(nonatomic, strong) NSArray<NSString *> *category_ids;

@property(nonatomic, copy) NSString *order9;

@property(nonatomic, assign) BOOL is_notice;

@property(nonatomic, copy) NSString *volume;

@property(nonatomic, copy) NSString *url_key;

@property(nonatomic, copy) NSString *country;

@property(nonatomic, strong) NSArray<NSString *> *default_category_path;

@property(nonatomic, assign) NSInteger stock_qty;

@property(nonatomic, strong) Image *image;

@property(nonatomic, copy) NSString *sn;

@property(nonatomic, copy) NSString *zdollar_percentage;

@property(nonatomic, strong) NSArray<NSString *> *tags;

@property(nonatomic, assign) BOOL is_new;

@property(nonatomic, copy) NSString *brand;

@property(nonatomic, strong) NSArray<ProductImages> *images;

@property(nonatomic, assign) NSInteger cart_qty;

@property(nonatomic, assign) NSInteger cart_freebie_qty;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, copy) NSString *meta_description;

@property(nonatomic, assign) NSInteger brand_id;

@property(nonatomic, assign) NSInteger default_category_id;

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, assign) BOOL cart_available;

@property(nonatomic, strong) NSArray<Promotions> *promotions;

@property(nonatomic, copy) NSString *shop;

@property(nonatomic, assign) BOOL is_exist_in_wish_list;

@property(nonatomic, strong) NSArray *category_paths;

@property(nonatomic, assign) NSInteger purchase_quota;

@property(nonatomic, strong) Price *price;

@property(nonatomic, assign) BOOL is_hot;

@property(nonatomic, strong) NSString *desc;

@property(nonatomic, assign) BOOL available;

@property(nonatomic, assign) NSInteger freebie_qty;

@property(nonatomic, copy) NSString *subtotal;

@property(nonatomic, copy) NSString *discounted_subtotal;

@property(nonatomic, assign) NSInteger qty;

@property(nonatomic, copy) NSString *barcode;

@property(nonatomic, assign) BOOL is_visible;

@property(nonatomic, assign) BOOL hideupbtn;

@property(nonatomic, assign) BOOL hidedownbtn;

@property(nonatomic, assign) BOOL pressUpbtn;

@property(nonatomic, assign) BOOL pressDownbtn;

@property(nonatomic, assign) BOOL isFreeProduct;

@property(nonatomic, assign) BOOL isCanAddFreeProduct;

- (void)setUpHideButton;

- (void)setUpProductType;

- (NSString *)getProductIdString;

- (BOOL)isNotEnoughStock;

- (NSAttributedString *)getDesc;

- (Products *)getGlobalProduct;

@end

@protocol ProductList

@end

@interface ProductList : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, assign) int sort_order;

@property(nonatomic, strong) NSString *url_key;

@property(nonatomic, strong) NSString *name;

@property(nonatomic, strong) NSString *image_url;

@property(nonatomic, strong) NSString *mobile_image_url;

@property(nonatomic, strong) NSArray<Products> *products;

@property(nonatomic, strong) NSArray<Products_Specialty_Tag> *products_specialty_tag;

@property(nonatomic, strong) NSArray<NSString *> *products_tag;

@property(nonatomic, strong) NSArray *product_active_brand;

@property(nonatomic, strong) NSArray<Products_Brand> *products_brand;

@property(nonatomic, strong) NSArray<NSString *> *selectedProducts_tag;

- (void)checkEmptyArray;

- (NSArray<Products_Specialty_Tag> *)getIsActivedSpecialtyTagArray;

- (ProductList *)getReSetSeletedProductList;

@end

@interface FeatureProductListArray : REObject

@property(nonatomic, strong) NSArray<ProductList> *data;

@end

@interface ProductListData : REObject

@property(nonatomic, strong) ProductList *data;

@end

@interface ProductListArray : REObject

@property(nonatomic, strong) NSArray<Products> *data;

@end
