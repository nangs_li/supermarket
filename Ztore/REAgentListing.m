//
//  REAgentListing.m
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentListing.h"
#import "REDBAgentListing.h"

@implementation REAgentListing

- (REDBAgentListing *)toREDBObject {

  REDBAgentListing *dbAgentListing                = [[REDBAgentListing alloc] init];
  dbAgentListing.Reasons                          = (RLMArray<DBReasons * ><DBReasons> *)[RLMArray arrayWithNSMutableArray:self.Reasons objectClass:[DBReasons class]];
  dbAgentListing.BedroomCount                     = self.BedroomCount;
  dbAgentListing.PropertyPriceFormattedForIndian  = self.PropertyPriceFormattedForIndian;
  dbAgentListing.SloganIndex                      = self.SloganIndex;
  dbAgentListing.KitchenCount                     = self.KitchenCount;
  dbAgentListing.LivingRoomCount                  = self.LivingRoomCount;
  dbAgentListing.PropertyType                     = self.PropertyType;
  dbAgentListing.PropertySize                     = self.PropertySize;
  dbAgentListing.PropertyPriceFormattedForRoman   = self.PropertyPriceFormattedForRoman;
  dbAgentListing.PropertyPriceFormattedForChinese = self.PropertyPriceFormattedForChinese;
  dbAgentListing.CurrencyUnit                     = self.CurrencyUnit;
  dbAgentListing.Address                          = (RLMArray<DBAddress * ><DBAddress> *)[RLMArray arrayWithNSMutableArray:self.Address objectClass:[DBAddress class]];
  dbAgentListing.SizeUnit                         = self.SizeUnit;
  dbAgentListing.BathroomCount                    = self.BathroomCount;
  dbAgentListing.CreationDate                     = self.CreationDate;
  dbAgentListing.PropertyPrice                    = self.PropertyPrice;
  dbAgentListing.ListingFollowerCount             = self.ListingFollowerCount;
  dbAgentListing.BalconyCount                     = self.BalconyCount;
  dbAgentListing.Photos                           = (RLMArray<DBPhotos * ><DBPhotos> *)[RLMArray arrayWithNSMutableArray:self.Photos objectClass:[DBPhotos class]];
  dbAgentListing.AgentListingID                   = self.AgentListingID;
  dbAgentListing.SizeUnitType                     = self.SizeUnitType;
  dbAgentListing.Version                          = self.Version;
  dbAgentListing.SpaceType                        = self.SpaceType;

  return dbAgentListing;
}

- (NSArray *)getReason {

  NSMutableArray *reasonArray = [[NSMutableArray alloc] init];
  Reasons *firstReason        = nil;

  //    if (!self.selectedLangIndex) {
  //        firstReason = [self.Reasons firstObject];
  //    }else{
  //        for (Reasons *reason in self.Reasons) {
  //            if (reason.LanguageIndex == [self.selectedLangIndex intValue]) {
  //                firstReason = reason;
  //                break;
  //            }
  //        }
  //    }
  NSString *reason1   = firstReason.Reason1;
  NSString *reason2   = firstReason.Reason2;
  NSString *reason3   = firstReason.Reason3;
  NSString *mediaURL1 = firstReason.Reason1URL;
  NSString *mediaURL2 = firstReason.Reason2URL;
  NSString *mediaURL3 = firstReason.Reason3URL;
  PHAsset *asset1     = firstReason.asset1;
  PHAsset *asset2     = firstReason.asset2;
  PHAsset *asset3     = firstReason.asset3;

  if (!mediaURL1) {
    mediaURL1 = @"";
  }

  if (!mediaURL2) {
    mediaURL2 = @"";
  }

  if (!mediaURL3) {
    mediaURL3 = @"";
  }

  if (!asset1) {
    asset1 = nil;
  }

  if (!asset2) {
    asset2 = nil;
  }

  if (!asset3) {
    asset3 = nil;
  }

  if (!reason1) {
    reason1 = @"";
  }

  if (!reason2) {
    reason2 = @"";
  }

  if (!reason3) {
    reason3 = @"";
  }

  for (int i = 0; i < 3; i++) {
    NSString *reason   = @"";
    NSString *mediaURL = @"";
    PHAsset *asset     = nil;

    if (i == 0) {
      reason   = reason1;
      mediaURL = mediaURL1;
      asset    = asset1;
    } else if (i == 1) {
      reason   = reason2;
      mediaURL = mediaURL2;
      asset    = asset2;
    } else if (i == 2) {
      reason   = reason3;
      mediaURL = mediaURL3;
      asset    = asset3;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:reason forKey:@"reason"];
    [dict setObject:mediaURL forKey:@"mediaURL"];

    if (asset) {
      [dict setObject:asset forKey:@"asset"];
    }
    [reasonArray addObject:dict];
  }

  return reasonArray;
}

@end

@implementation Reasons

- (DBReasons *)toREDBObject {

  DBReasons *dbReasons    = [[DBReasons alloc] init];
  dbReasons.Reason3URL    = self.Reason3URL;
  dbReasons.Reason3       = self.Reason3;
  dbReasons.Reason2       = self.Reason2;
  dbReasons.Reason1       = self.Reason1;
  dbReasons.SloganIndex   = self.SloganIndex;
  dbReasons.LanguageIndex = self.LanguageIndex;
  dbReasons.Reason1URL    = self.Reason1URL;
  dbReasons.Reason2URL    = self.Reason2URL;

  return dbReasons;
}

@end

@implementation Address2

- (DBAddress *)toREDBObject {

  DBAddress *dbAddress       = [[DBAddress alloc] init];
  dbAddress.FormattedAddress = self.FormattedAddress;
  dbAddress.Lang             = self.Lang;
  dbAddress.ShortAddress     = self.ShortAddress;
  dbAddress.PlaceID          = self.PlaceID;

  return dbAddress;
}

@end

@implementation Photos

- (DBPhotos *)toREDBObject {

  DBPhotos *dbPhotos = [[DBPhotos alloc] init];
  dbPhotos.URL       = self.URL;
  dbPhotos.PhotoID   = self.PhotoID;
  dbPhotos.Height    = self.Height;
  dbPhotos.Width     = self.Width;

  return dbPhotos;
}

@end
