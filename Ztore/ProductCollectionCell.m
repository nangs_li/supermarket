

//
//  OnSaleCollectionCell.m
//  Ztore
//
//  Created by ken on 7/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "FICDPhotoLoader.h"
#import "ProductCollectionCell.h"
#import "MainRegisterViewController.h"

@implementation ProductCollectionCell

- (void)awakeFromNib {
  // Initialization code

  [super awakeFromNib];
    [self setUpAllBtn];
    [self.soldOutBtn addTarget:self action:@selector(soldOutAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setUpAllBtn {
    
    self.addProductObject = [[AddProductObject alloc] init];
    self.addProductObject.delegate = self;
    self.addProductObject.upBtn = self.upBtn;
    self.addProductObject.downBtn = self.downBtn;
    self.addProductObject.btn_addToCart = self.btn_addToCart;
    [self.addProductObject setUpAllAction];
    self.blueTag = [[[NSBundle mainBundle] loadNibNamed:@"BlueTag" owner:self options:nil] objectAtIndex:0];
    [self addSubview:self.blueTag];
    [self.blueTag mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self.mas_centerX).with.offset(0);
        make.bottom.equalTo(self.imageView_product.mas_bottom).with.offset(0);
        make.width.mas_lessThanOrEqualTo(self.frame.size.width);
        
    }];
    
    self.blueTag.hidden = YES;
    
}

- (void)setBounds:(CGRect)bounds {
  [super setBounds:bounds];
  self.contentView.frame = bounds;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

  [super touchesBegan:touches withEvent:event];
  UITouch *touch       = [touches anyObject];
  self.clickedLocation = [touch locationInView:touch.view];
}

- (void)setUpProductCellDataFromProduct:(Products *)product mainPage:(BOOL)mainPage {

  self.isMainPageCell = mainPage;
  self.product = product;
  self.product = [[Common shared] setQtyLabelTextFromProduct:self.product label:self.labelQty];
  Price *price = product.price;
  // set product name label
  self.label_ProductName.font = [[Common shared] getContentNormalFont];
  self.label_ProductName.text = product.name;
  [self.label_ProductName setBackgroundColor:[UIColor clearColor]];

  // set other labels
  self.label_brand.font       = [[Common shared] getContentNormalFont];
  self.label_brand.text       = product.brand;
  self.label_volume.font      = [[Common shared] getSmallContentNormalFont];
  self.label_volume.text      = product.volume;
  self.label_volume.textColor = [[Common shared] getCommonGrayColor];

  // set price labels
  self.label_ProductActualPricePrices.font      = [[Common shared] getContentBoldFont];
  self.label_ProductActualPricePrices.text      = [[Common shared] getStringFromPriceString:product.price.promotion_price];
  self.label_ProductActualPricePrices.textColor = [[Common shared] getCommonRedColor];
  self.label_ProductOrginalPrices.font          = [[Common shared] getSmallContentNormalFont];
  self.label_ProductOrginalPrices.textColor     = [[Common shared] getCommonGrayColor];
  self.label_ProductPrices.font                 = [[Common shared] getContentBoldFont];
  self.label_ProductPrices.text                 = [[Common shared] getStringFromPriceString:product.price.promotion_price];
  [[Common shared] setStrikethroughForLabel:self.label_ProductOrginalPrices withText:[[Common shared] getStringFromPriceString:price.standard_price]];

  if (price.promotion_price == price.standard_price) {

    [self.label_ProductOrginalPrices setHidden:TRUE];
    [self.label_ProductActualPricePrices setHidden:TRUE];
    [self.label_ProductPrices setHidden:FALSE];

  } else {

    [self.label_ProductOrginalPrices setHidden:FALSE];
    [self.label_ProductActualPricePrices setHidden:FALSE];
    [self.label_ProductPrices setHidden:TRUE];
  }

  self.imageView_product.contentMode = UIViewContentModeScaleAspectFit;
  [FICDPhotoLoader loaderImageForImageView:self.imageView_product withPhotoUrl:[FICDPhotoLoader getPhotoUrlInProduct:[product.image changeToZoomClass]]];
    
    if (self.product.stock_qty == 0) {
        
        self.soldOutView.hidden = NO;
        self.soldOutView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
        [self.soldOutBtn setTitle:(self.product.is_notice) ? JMOLocalizedString(@"Cancel Notification", nil) : JMOLocalizedString(@"Notify Me", nil) forState:UIControlStateNormal animation:NO];
        [self.soldOutBtn setTitleColor:[[Common shared] getSoldOutButtonTitleColor] forState:UIControlStateNormal];
        [self.blueTag setUpNotStockCase];
        
    } else {
    
  self.soldOutView.hidden = YES;
        
  // add product to cart event
  [self.btn_addToCart.titleLabel setFont:[[Common shared] getContentBoldFont]];
  [self.btn_addToCart setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
  [self.lineView setBackgroundColor:[[Common shared] getLineColor]];
  [self.btn_addToCart setTitle:JMOLocalizedString(@"ADD TO CART", nil) forState:UIControlStateNormal animation:NO];
  [self.addProductView setBackgroundColor:[[Common shared] getCommonRedColor]];
  [self.addProductView setHidden:!(self.product.cart_qty > 0)];
  [self.btn_addToCart drawGreyBorder];
  [self.blueTag setUpFromPromotions:self.product.promotions.firstObject];
  
  }
  [self rebuildBlueTagLayout];
    
  self.increaseBtnImage.alpha = (product.hideupbtn == YES) ? 0.3 : 1;
  self.decreaseBtnImage.alpha = (product.hidedownbtn == YES) ? 0.3 : 1;
    
    if (!self.isMainPageCell) {
        
        [self drawShadowOpacity];
        
    }
    
    
    
}

- (void)rebuildBlueTagLayout {
    
    CGFloat height = [Common heightOfTextForString:self.blueTag.titleBtn.titleLabel.text andFont:self.blueTag.titleBtn.titleLabel.font maxSize:CGSizeMake(self.blueTag.titleBtn.frame.size.width, MAXFLOAT)];
    
    [self.blueTag mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(height + 6);
        make.centerX.equalTo(self.mas_centerX).with.offset(0);
        make.bottom.equalTo(self.imageView_product.mas_bottom).with.offset(0);
        make.width.mas_lessThanOrEqualTo(self.frame.size.width);
        
    }];
    
}

- (void)addProductAPIWithAddCount:(int)addCount {
    
    [[Common shared] addProductQtyFromProduct:self.product addCount:addCount];
    
    [self setUpProductCellDataFromProduct:self.product mainPage:self.isMainPageCell];
}

- (void)soldOutAction {
    
    Products *productObject = [self.product getGlobalProduct];
    
    if ([self.delegate respondsToSelector:@selector(pressSoldOutBtnFromProduct:)]) {
        
        [self.delegate pressSoldOutBtnFromProduct:productObject];
        
    }
    
}

- (void)invalidAllTimer {
    
}

@end
