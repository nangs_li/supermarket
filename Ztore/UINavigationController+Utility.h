//
//  NSString+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface UINavigationController (Utility)

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated enableUI:(BOOL)enableUI;

@end
