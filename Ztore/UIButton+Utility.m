//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "Common.h"
#import "UIButton+Utility.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIButton (Utility)

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing {
  CGFloat insetAmount    = spacing / 2.0;
  self.imageEdgeInsets   = UIEdgeInsetsMake(0, -insetAmount, 0, insetAmount);
  self.titleEdgeInsets   = UIEdgeInsetsMake(0, insetAmount, 0, -insetAmount);
  self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
}

- (void)setUpDefaultTitleFontSize {

  [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:18.0]];
}

- (void)setUpDefaultDetailTextFontSize {

  [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0]];
}

- (void)setUpAutoScaleButton {

  self.titleLabel.numberOfLines             = 1;
  self.titleLabel.adjustsFontSizeToFitWidth = YES;
  self.titleLabel.lineBreakMode             = NSLineBreakByClipping;
}

- (void)setUpRedBorderButton {
    
    self.clipsToBounds      = YES;
    self.layer.borderColor  = [[Common shared] getCommonRedColor].CGColor;
    self.layer.borderWidth  = 2.0f;
    [self setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];;
    
}

- (void)setUpWhiteBorderButton {
    
    self.clipsToBounds      = YES;
    self.layer.borderColor  = [UIColor whiteColor].CGColor;
    self.layer.borderWidth  = 2.0f;
}

- (void)setUpCircleButton {

  self.clipsToBounds      = YES;
  self.layer.cornerRadius = self.bounds.size.width / 2;
  self.layer.borderColor  = [UIColor whiteColor].CGColor;
  self.layer.borderWidth  = 2.0f;
}

- (void)setUpLeftMenuButton {

  [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  self.tintColor       = [UIColor blackColor];
  self.backgroundColor = [UIColor clearColor];
  self.titleLabel.font = [[Common shared] getNormalFont];
}

- (void)setUpLeftSelectedMenuButton {

  [self setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
  self.tintColor = [[Common shared] getCommonRedColor];
  [self setBackgroundColor:[UIColor whiteColor]];
  self.titleLabel.font = [[Common shared] getNormalBoldFont];
}

- (void)setUpCornerBorderButton {

  self.layer.cornerRadius  = 8.0f;
  self.layer.borderWidth   = 1.0f;
  self.layer.masksToBounds = YES;
  self.layer.borderColor   = [[Common shared] getCommonRedColor].CGColor;
  [self setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
  self.titleLabel.font = [[Common shared] getNormalTitleFont];
}

- (void)setUpSmallCornerBorderButton {

  self.layer.cornerRadius  = 4.0f;
  self.layer.borderWidth   = 1.0f;
  self.layer.masksToBounds = YES;
  self.layer.borderColor   = [[Common shared] getCommonRedColor].CGColor;
  self.titleLabel.font     = [[Common shared] getNormalBoldTitleFont];
  self.backgroundColor     = [[Common shared] getCommonRedColor];
  [self setViewHeight:60];
}

- (void)setUpForgetPasswordBtn {
    
    self.layer.cornerRadius  = 4.0f;
    self.layer.borderWidth   = 1.0f;
    self.layer.masksToBounds = YES;
    self.titleLabel.font     = [[Common shared] getSmallContentNormalFont];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.layer.borderColor   = [UIColor whiteColor].CGColor;
    self.backgroundColor     = [UIColor clearColor];
    
}

- (void)setCancelUseBtnFromTitleText:(NSString *)titleText {

  NSDictionary *attrDict           = @{NSFontAttributeName : [[Common shared] getContentNormalFont], NSForegroundColorAttributeName : [[Common shared] getCommonRedColor]};
  NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:titleText attributes:attrDict];
  [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleText length])];
  [self setAttributedTitle:title forState:UIControlStateNormal];
}

- (void)setAttributedBtnFromTitleText:(NSString *)titleText {

  NSDictionary *attrDict           = @{NSFontAttributeName : [[Common shared] getContentNormalFont], NSForegroundColorAttributeName : [UIColor blackColor]};
  NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:titleText attributes:attrDict];
  [self setAttributedTitle:title forState:UIControlStateNormal];
}

- (void)makeRightImageView {

  self.transform            = CGAffineTransformMakeScale(-1.0, 1.0);
  self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
  self.imageView.transform  = CGAffineTransformMakeScale(-1.0, 1.0);
  [self setTitle:JMOLocalizedString(@"Choose Time", nil) forState:UIControlStateNormal animation:NO] ;
}

- (void)setUpGreyCircleButton {

  self.layer.masksToBounds = true;
  self.layer.cornerRadius  = self.frame.size.width / 2;
  self.layer.borderWidth   = 1.0f;
  self.layer.borderColor   = [UIColor lightGrayColor].CGColor;
    
}

- (void)setUpWhiteBottomBtnFromTitleText:(NSString *)titleText {
    
    [self setTitle:titleText forState:UIControlStateNormal animation:NO];
    [self setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
    [self setUpSmallCornerBorderButton];
    self.layer.borderColor   = [UIColor whiteColor].CGColor;
    self.backgroundColor     = [UIColor whiteColor];
    
}

- (void)setUpCheckOutButtonFromTitleText:(NSString *)titleText {

  [self setTitle:titleText forState:UIControlStateNormal animation:NO];
  self.titleLabel.font = [[Common shared] getSmallContentNormalFont];
  [self setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
  self.layer.masksToBounds      = true;
  self.layer.borderColor        = [UIColor whiteColor].CGColor;
  self.layer.borderWidth        = 1.0f;
  self.layer.backgroundColor    = [UIColor whiteColor].CGColor;
  self.titleLabel.textAlignment = NSTextAlignmentCenter;
  self.clipsToBounds            = YES;

  [self mas_updateConstraints:^(MASConstraintMaker *make) {

    make.height.mas_equalTo(22);
    make.width.mas_equalTo(22);
    self.layer.cornerRadius = 22 / 2;

  }];
    
}

- (void)setUpCheckOutBiggerButtonFromTitleText:(NSString *)titleText {

  [self setUpCheckOutButtonFromTitleText:titleText];
  self.titleLabel.font      = [[Common shared] getNormalFont];
  [self setTitleColor:[[Common shared] getCommonRedColor]forState:UIControlStateNormal];
  [self mas_updateConstraints:^(MASConstraintMaker *make) {

    make.height.mas_equalTo(30);
    make.width.mas_equalTo(30);
    self.layer.cornerRadius = 30 / 2;

  }];
    
}

- (void)setUpSelectedDayBtn {
    
    self.clipsToBounds      = YES;
    self.layer.cornerRadius = self.bounds.size.width / 2;
    self.layer.borderColor  = [UIColor clearColor].CGColor;
    self.layer.borderWidth  = 2.0f;
    self.backgroundColor    = [[Common shared] getCommonRedColor];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
}

- (void)setTitle:(NSString *)title forState:(UIControlState)state animation:(BOOL)animation {
    
    if (!animation) {
        
        [UIView performWithoutAnimation:^{
            [self setTitle:title forState:UIControlStateNormal];
            [self layoutIfNeeded];
        }];
        
    } else {
        
         [self setTitle:title forState:UIControlStateNormal];
        
    }
    
}

- (void)setAttributedTitle:(NSAttributedString *)title forState:(UIControlState)state animation:(BOOL)animation {
    
    if (!animation) {
        
        [UIView performWithoutAnimation:^{
            [self setAttributedTitle:title forState:UIControlStateNormal];
            [self layoutIfNeeded];
        }];
        
    } else {
        
        [self setAttributedTitle:title forState:UIControlStateNormal];
        
    }
    
}

@end
