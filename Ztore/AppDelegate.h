//
//  AppDelegate.h
//  Ztore
//
//  Created by ken on 9/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductCategoryModel.h"
#import "ProductDetailController.h"
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "TimeslotViewController.h"
#import "ProductCategoryController.h"
#import "LeftMenuVC.h"
#import "PopUpView.h"

@class RightMenuVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate, TSMessageViewProtocol>

@property(strong, nonatomic) UIWindow *window;

#pragma mark api calling
@property(assign, nonatomic) BOOL haveAPIProcessing;

#pragma mark rigft and left menu open
@property(assign, nonatomic) BOOL leftMenuIsOpen;
@property(assign, nonatomic) BOOL rightMenuIsOpen;
@property(assign, nonatomic) BOOL didPressLeftMenu;

#pragma mark - Free Shipping Min Order Amount
@property(assign, nonatomic) double shipping_fee;

#pragma mark Retry API Pop Up View
@property(strong, nonatomic) PopUpView *popUpView;

#pragma mark Product List Data
// Product List Title name
@property(strong, nonatomic) NSString *searchSelectedString;
@property(strong, nonatomic) Children *Level2_childrenProduct;

#pragma mark only one main NavigationController
@property(strong, nonatomic) UINavigationController *mainPageNavigationController;

#pragma mark gobal page
@property(strong, nonatomic) ProductDetailController *productDetailController;
@property(strong, nonatomic) TimeslotViewController *timeslotViewController;
@property(strong, nonatomic) ProductCategoryController *productCategoryController;
@property(strong, nonatomic) RightMenuVC *rightMenuVC;
@property(strong, nonatomic) LeftMenuVC *leftMenuVC;

#pragma mark init
+ (AppDelegate *)getAppDelegate;

#pragma mark add Product Object
@property(strong, nonatomic) NSTimer *addShoppingCartFromExistingCartTimer;
@property(strong, nonatomic) UIViewController *topVisbleViewController;

#pragma mark messageBar
@property(assign, nonatomic) CGFloat messageBarVerticalOffset;
@property(assign, nonatomic) int messageBarHeight;
- (void)customizeMessageView:(TSMessageView *)messageView;
- (CGFloat)messageLocationOfMessageView:(TSMessageView *)messageView;
- (void)showMessageBarFromResponse:(id)response;

#pragma mark - PopUpView Method
- (void)showPopUpViewFromErrorMessage:(NSString *)errorMessage;
- (void)hidePopUpView;

@end
