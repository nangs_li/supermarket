//
//  Common.h
//  Ztore
//
//  Created by ken on 22/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Constants.h"
#import "MainVC.h"
#import "ProductList.h"
#import "UIColor+CreateMethods.h"
#import "UIFont+OpenSans.h"
#import "ZtoreNavigationBar.h"
#import <Foundation/Foundation.h>
#import "DeliveryAddress.h"
#import "TimeslotData.h"
#import "SpecialyRequest.h"
#import "BEMCheckBox.h"
#import "ShoppingCartModel.h"
#import "CustomerCard.h"
#import "CardID.h"
#import "CardArray.h"
#import "PaymentSuccess.h"
#import "ErrorMessageBar.h"
#import "TSMessageView.h"
#import "UserAccount.h"
#import "ZDollarModel.h"
#import "OrderHistory.h"
#import "AutoCompleteProduct.h"

@class InputListProduct, _UIViewController;

@interface Common : NSObject

#pragma mark only one Product Data
@property(strong, nonatomic) NSMutableDictionary *productDict;
@property(strong, nonatomic) NSMutableArray *productStockArray;
@property(strong, nonatomic) NSMutableArray *productChangeArray;
- (int)addProductQtyFromProduct:(Products *)product addCount:(int)addCount;

#pragma mark messageBar
@property(strong, nonatomic) NSString *addProductQtyString;
@property(strong, nonatomic) ErrorMessageBar *messageBar;
@property(strong, nonatomic) TSMessageView *messageView;

#pragma mark DeliveryAddressViewController
@property(strong, nonatomic) DeliveryAddressData *deliveryAddressData;
@property(strong, nonatomic) DeliveryAddress *selectedDeliveryAddress;

#pragma mark TimeslotViewController
@property(strong, nonatomic) NSMutableArray *selectedTimeArray;
@property(strong, nonatomic) NSMutableArray *timeslotArray1;
@property(strong, nonatomic) NSMutableArray *timeslotArray2;
@property(strong, nonatomic) NSMutableArray *timeslotArray3;
@property(strong, nonatomic) NSMutableArray *timeslotArray4;
@property(strong, nonatomic) NSMutableArray *timeslotArray5;
@property(strong, nonatomic) NSMutableArray *timeslotArray6;
@property(strong, nonatomic) NSMutableArray *timeslotArray7;
@property(strong, nonatomic) TimeslotData *timeslotData;
@property(strong, nonatomic) NSMutableArray *menuItems;
@property(strong, nonatomic) Timeslot *selectedTimeslot;
@property(strong, nonatomic) UIButton *selectedDayBtn;
@property(strong, nonatomic) SpecialyRequest *specialyRequest;

#pragma mark Check Out Object
@property(strong, nonatomic) Class backToViewController;
@property(strong, nonatomic) ShoppingCartData *shoppingCartData;
@property(strong, nonatomic) CardArray *customerCardArray;
@property(strong, nonatomic) CustomerCard *selectedCard;
@property(strong, nonatomic) CustomerCard *selectedCardnew;
@property(assign, nonatomic) BOOL is_store_card;
@property(strong, nonatomic) NSString *clientToken;
@property(strong, nonatomic) PaymentSuccess *paymentSuccess;

#pragma mark MyAccount
@property(strong, nonatomic) UserAccount *userAccont;
@property(strong, nonatomic) ZDollarModel *zDollarModel;
@property(strong, nonatomic) OrderHistory *orderHistory;
@property(strong, nonatomic) PaymentSuccessData *didSelectOrderHistory;
@property(strong, nonatomic) ProductDetailArray *favouriteList;
@property(strong, nonatomic) ProductDetailArray *recentPurchaseList;

#pragma mark Product Detail Page TagFilter
@property(strong, nonatomic) NSString *tagString;
@property(strong, nonatomic) NSString *order_by_String;

#pragma mark Product Detail Page (From Search)
@property(strong, nonatomic) AutoCompleteProducts *autoCompleteProducts;

#pragma mark init
+ (instancetype)shared;

#pragma mark handle api return
- (BOOL)checkNotError:(NSError *)error controller:(UIViewController *)controller response:(id)response;
- (BOOL)checkNotEnoughStockFromResponse:(id)response;

#pragma mark set only one Product

- (void)setProduct:(nullable Products *)product;
- (nullable Products *)setQtyLabelTextFromProduct:(nullable Products *)product label:(nullable UILabel *)label;
- (void)setProductQtyChangeFromProductId:(NSInteger)productId ProductQtyChange:(NSInteger)ProductQtyChange;
- (nullable Products *)getProductFromProductId:(NSInteger)productId;

#pragma mark freeBie Product
- (nullable NSArray<Products> *)getFreebieTypeProductFromProduct:(nullable NSArray<Products> *)product;

#pragma mark lanuage
- (nullable NSString *)getCurrentLanguage;
- (BOOL)isEnLanguage;

#pragma mark SetUpView
- (void)addClickEventForView:(nullable UIView *)view withTarget:(nullable UIViewController *)target action:(nullable NSString *)action;
- (void)setStrikethroughForLabel:(nullable UILabel *)label withText:(nullable NSString *)text;
- (void)setUpTickFromCheckBox:(nullable BEMCheckBox *)checkBox;

#pragma mark CheckOutData
- (void)cleanAllCheckOutData;

#pragma mark InputListProduct (input object for getting product api)
- (nullable InputListProduct *)getInputListProductWithtBrands:(nullable NSArray<Products_Brand *> *)productBrands productSpecialtyTags:(nullable NSArray<Products_Specialty_Tag *> *)productSpecialtyTags productTags:(nullable NSArray<NSString *> *)productTags selectedSort:(nullable NSString *)selectedSort inputListProduct:(nullable InputListProduct *)inputListProduct;

#pragma mark fontsize and color
- (nullable UIColor *)getCommonPinkRedColor;
- (nullable UIColor *)getCommonAlphaWhiteColor;
- (nullable UIColor *)getCommonRedColor;
- (nullable UIColor *)getCommonGrayColor;
- (nullable UIColor *)getCommonGrayBackgroundColor;
- (nullable UIColor *)getFilterAnimationColor;
- (nullable UIColor *)getTagColor;
- (nullable UIColor *)getLineColor;
- (nullable UIColor *)getShoppingCartLineColor;
- (nullable UIColor *)getPressIncreseButtonColor;
- (nullable UIColor *)getTextFieldPlaceHolderColor;
- (nullable UIColor *)getSoldOutButtonTitleColor;
- (nullable UIColor *)getProductDetailDescGrayBackgroundColor;
- (nullable UIColor *)getTableViewSeparatorGreyBackgroundColor;
- (nullable UIColor *)getColorWithRed:(int)red greed:(int)green blue:(int)blue;
- (nullable UIFont *)getBiggerNormalFont;
- (nullable UIFont *)getBiggerBoldTitleFont;
- (nullable UIFont *)getNormalTitleFont;
- (nullable UIFont *)getNormalBoldTitleFont;
- (nullable UIFont *)getNormalFont;
- (nullable UIFont *)getNormalBoldFont;
- (nullable UIFont *)getContentNormalFont;
- (nullable UIFont *)getContentBoldFont;
- (nullable UIFont *)getSmallContentBoldFont;
- (nullable UIFont *)getSmallContentNormalFont;
- (nullable UIFont *)fontWithName:(nullable NSString *)name size:(int)size;
- (nullable UIFont *)getNormalFontWithSize:(int)size;
- (nullable UIFont *)getBoldFontWithSize:(int)size;

#pragma mark get $ String
- (nullable NSString *)getStringFromPrice:(float)price;
- (nullable NSString *)getStringFromPriceString:(nullable NSString *)price;

#pragma mark make line ui
- (nullable CALayer *)getNormalBreakLineWithFrame:(CGRect)frame;

#pragma mark save and get local data
- (void)setValue:(nullable id)value forKey:(nullable NSString *)key;
- (nullable id)valueForKey:(nullable NSString *)key;

#pragma mark tag view
- (nullable UIView *)tagView:(nullable UIView *)view_tagList setTagsList:(nullable NSArray *)tags setSelectedTagsList:(nullable NSArray *)selectedTagsList addTarget:(nullable id)target action:(nullable SEL)selector;

#pragma mark title (Mrs or Mr ......)dropdown view data
- (int)getConsignee_titleIdFromConsignee_title:(nullable NSString *)consignee_title;
- (nullable NSString *)getConsigneeTitleFromTitle:(NSInteger )title;

#pragma mark NavigationBar
+ (nullable ZtoreNavigationBar *)getRightMenuBar;
+ (nullable ZtoreNavigationBar *)getMainMenuBar;


#pragma mark API
- (void)getZDollarModelCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getFavouriteListCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getRecentPurchaseListCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getDeliveryAddressDataCompletionBlock:(nullable REAPICommonBlock)completionBlock;
- (void)getOrderHistoryCompletionBlock:(nullable REAPICommonBlock)completionBlock;

#pragma mark push to other page
- (void)pushToProductDetailPageFromProduct:(nullable Products *)product;
- (void)pushToProductListPageFromInputListProduct:(nullable InputListProduct *)InputListProduct controller:(nullable _UIViewController *)controller;
- (void)notLoginToRegisterPage;
- (void)notLoginToLoginPage;

#pragma mark GA
- (void)sendGAForpageName:(nullable NSString *)pageName;
- (void)sendGAEcommerceForPaymentSuccess:(nullable PaymentSuccess *)paymentSuccess;

#pragma mark heightOfTextForString
+ (CGFloat)heightOfTextForString:(nullable NSString *)aString andFont:(nullable UIFont *)aFont maxSize:(CGSize)aSize;

#pragma mark isLogin
- (BOOL)isLogin;
- (void)setIsLogin:(BOOL)isLogin;

@end
