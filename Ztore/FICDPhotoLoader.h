//
//  FICDPhotoLoader.h
//  Ztore
//
//  Created by ken on 28/8/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductDetailModel.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FICDPhotoLoader : NSObject

+ (int)getScreenScale;
+ (NSString *)getPhotoUrlInProduct:(Zoom *)productImage;
+ (void)loaderImageForImageView:(UIImageView *)imageView withPhotoUrl:(NSString *)photoUrl;

@end
