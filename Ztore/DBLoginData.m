//
//  REDBAPIQueue.m
//  real-v2-ios
//
//  Created by Li Ken on 25/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "DBLoginData.h"

// Models
#import "LoginData.h"

@implementation DBLoginData

+ (NSString *)primaryKey {
  return @"id";
}

+ (NSArray *)indexedProperties {
  return @[ @"id" ];
}

- (LoginData *)toREObject {
    
  LoginData *loginData   = [[LoginData alloc] init];
  loginData.id         = self.id;
  loginData.email = self.email;
  loginData.password = self.password;
    
  return loginData;
}

@end
