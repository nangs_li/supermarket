//
//  RELog.h
//  real-v2-ios
//
//  Created by Derek Cheung on 17/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

// We want to use the following log levels:
//
// Error
// DB
// API
// Info
// Debug
//
// All we have to do is undefine the default values,
// and then simply define our own however we want.

// First undefine the default stuff we don't want to use.

#undef LOG_FLAG_ERROR
#undef LOG_FLAG_WARN
#undef LOG_FLAG_INFO
#undef LOG_FLAG_DEBUG
#undef LOG_FLAG_VERBOSE

#undef LOG_LEVEL_ERROR
#undef LOG_LEVEL_WARN
#undef LOG_LEVEL_INFO
#undef LOG_LEVEL_DEBUG
#undef LOG_LEVEL_VERBOSE

#undef LOG_ERROR
#undef LOG_WARN
#undef LOG_INFO
#undef LOG_DEBUG
#undef LOG_VERBOSE

#undef DDLogError
#undef DDLogWarn
#undef DDLogInfo
#undef DDLogDebug
#undef DDLogVerbose

#undef DDLogCError
#undef DDLogCWarn
#undef DDLogCInfo
#undef DDLogCDebug
#undef DDLogCVerbose

// Now define everything how we want it

#define LOG_FLAG_ERROR (1 << 0)
#define LOG_FLAG_DB (1 << 1)
#define LOG_FLAG_API (1 << 2)
#define LOG_FLAG_INFO (1 << 3)
#define LOG_FLAG_DEBUG (1 << 4)

#define LOG_LEVEL_ERROR (LOG_FLAG_ERROR)
#define LOG_LEVEL_DB (LOG_FLAG_DB | LOG_LEVEL_ERROR)
#define LOG_LEVEL_API (LOG_FLAG_API | LOG_LEVEL_DB)
#define LOG_LEVEL_INFO (LOG_FLAG_INFO | LOG_LEVEL_API)
#define LOG_LEVEL_DEBUG (LOG_FLAG_DEBUG | LOG_LEVEL_INFO)

#define LOG_ERROR (ddLogLevel & LOG_FLAG_ERROR)
#define LOG_DB (ddLogLevel & LOG_FLAG_DB)
#define LOG_API (ddLogLevel & LOG_FLAG_API)
#define LOG_INFO (ddLogLevel & LOG_FLAG_INFO)
#define LOG_DEBUG (ddLogLevel & LOG_FLAG_DEBUG)

#define LOG_ASYNC_ENABLED YES

#define DDLogError(frmt, ...) LOG_MAYBE(NO, LOG_LEVEL_DEF, LOG_FLAG_ERROR, 0, nil, __PRETTY_FUNCTION__, CustomLoggingFormat(frmt), ##__VA_ARGS__)
#define DDLogInfo(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, LOG_FLAG_INFO, 0, nil, __PRETTY_FUNCTION__, CustomLoggingFormat(frmt), ##__VA_ARGS__)
#define DDLogDB(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, LOG_FLAG_DB, 0, nil, __PRETTY_FUNCTION__, CustomLoggingFormat(frmt), ##__VA_ARGS__)
#define DDLogAPI(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, LOG_FLAG_API, 0, nil, __PRETTY_FUNCTION__, CustomLoggingFormat(frmt), ##__VA_ARGS__)
#define DDLogDebug(frmt, ...) LOG_MAYBE(LOG_ASYNC_ENABLED, LOG_LEVEL_DEF, LOG_FLAG_DEBUG, 0, nil, __PRETTY_FUNCTION__, CustomLoggingFormat(frmt), ##__VA_ARGS__)

#define CustomLoggingFormat(frmt) (@"%s[Line %d]\n" frmt "\n"), __PRETTY_FUNCTION__, __LINE__
