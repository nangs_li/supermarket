//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MesssageBar.h"

#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation MesssageBar

+ (instancetype)shared {
    
    static id instance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] initFromXib];
    });
    
    return instance_;
}

#pragma mark App LifeCycle

- (MesssageBar *)setupDeliveryViewFromShoppingCartModel:(AddProductReturnShoppingCartModel *)shoppingCartModel addProductQtyString:(NSString *)addProductQtyString {
   
    self.shoppingCartModel = shoppingCartModel;
    self.addProductQtyString = addProductQtyString;
    MesssageBar *messageBar             = self;
    [messageBar removeAllSubView];
    [messageBar addSubview:messageBar.deliveryView];
    [messageBar.deliveryView matchSizeWithSuperViewFromAutoLayout];
    messageBar.circleBtn.titleLabel.font = [[Common shared] getNormalFont];
    [messageBar.circleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    messageBar.circleBtn.layer.masksToBounds      = true;
    messageBar.circleBtn.layer.borderColor        = [UIColor whiteColor].CGColor;
    messageBar.circleBtn.layer.borderWidth        = 1.0f;
    messageBar.circleBtn.layer.backgroundColor    = [UIColor whiteColor].CGColor;
    messageBar.circleBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    messageBar.circleBtn.clipsToBounds            = YES;
    messageBar.circleBtn.layer.cornerRadius = 24 / 2;
    self.progressViewBar.progressDirection = M13ProgressViewBarProgressDirectionLeftToRight;
    self.progressViewBar.showPercentage = NO;
    self.progressViewBar.progressBarCornerRadius = 0.0;
    self.progressViewBar.secondaryColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    self.progressViewBar.primaryColor = [UIColor whiteColor];
    self.progressViewBar.animationDuration = 1;
    self.progressViewBar.progressBarThickness = 6;
    self.messsageBarType = AddProductType;
    
    return messageBar;
}

- (id)initFromXib {
    
    self                             = [[[NSBundle mainBundle] loadNibNamed:@"MesssageBar" owner:self options:nil] objectAtIndex:0];
    
    return self;
}

- (void)progressAnimation {
    
    if (self.messsageBarType == AddProductType) {
    
    [Common shared].messageView.duration = TopMessageBarDuration;
    
    BOOL positiveInt = ([self.addProductQtyString rangeOfString:@"-"].location == NSNotFound) ? YES : NO;
    NSString *addProductQty = [self.addProductQtyString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [self.circleBtn setTitle:addProductQty forState:UIControlStateNormal];
    self.titleLabel.text = (positiveInt) ? JMOLocalizedString(@"item(s) added to cart", nil) : JMOLocalizedString(@"item(s) removed from to cart", nil) ;
    
    double subtotal = [self.shoppingCartModel.data.cart.discounted_subtotal_before_zdollar doubleValue];
    [AppDelegate getAppDelegate].shipping_fee = (self.shoppingCartModel.data.cart.free_shipping_min_order_amount == nil) ? [AppDelegate getAppDelegate].shipping_fee : [self.shoppingCartModel.data.cart.free_shipping_min_order_amount doubleValue];
    
    if (subtotal  == 0)  {
        
        self.contentLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Only $%.2f more to FREE delivery!", nil) , [AppDelegate getAppDelegate].shipping_fee];
        [self.progressViewBar setProgress:0 animated:YES];
        self.progressViewBar.hidden = NO;
        
    } else if ([AppDelegate getAppDelegate].shipping_fee > subtotal) {
        
        self.progressViewBar.hidden = NO;
        double freeDelivery = [AppDelegate getAppDelegate].shipping_fee - subtotal;
        self.contentLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Only $%.2f more to FREE delivery!", nil) , freeDelivery];
        double percent = freeDelivery / [AppDelegate getAppDelegate].shipping_fee;
        [self.progressViewBar setProgress:(1-percent) animated:YES];
        
    } else {
        
        [self.progressViewBar setProgress:100 animated:YES];
        self.progressViewBar.hidden = YES;
        
        self.contentLabel.text = JMOLocalizedString(@"You can enjoy FREE Delivery Now!", nil);
        
    }
    
    }
    
}

@end
