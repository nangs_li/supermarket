//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ErrorMessageBar.h"

#import "RealUtility.h"
#import <QuartzCore/QuartzCore.h>

@implementation ErrorMessageBar

- (ErrorMessageBar *)setUpErrorMessageFromErrorString:(NSString *)errorString {
    
    ErrorMessageBar *messageBar             = self;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.text = errorString;
    self.titleLabel.font = [[Common shared] getContentNormalFont];
    [messageBar removeAllSubView];
    [messageBar addSubview:messageBar.deliveryView];
    [messageBar.deliveryView matchSizeWithSuperViewFromAutoLayout];
    self.messsageBarType = ErrorMessageType;
    
    return messageBar;
}

@end
