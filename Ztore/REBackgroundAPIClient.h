//
//  REBackgroundAPIClient.h
//  real-v2-ios
//
//  Created by Alex Hung on 25/5/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "REAPIClient.h"

@class REAPIQueue;

@interface REBackgroundAPIClient : REAPIClient

- (REAPIQueue *_Nonnull)post:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter;
- (REAPIQueue *_Nonnull)get:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter;
- (REAPIQueue *_Nonnull)post:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter dependencies:(NSArray<REAPIQueue *> *_Nullable)dependencies;
- (REAPIQueue *_Nonnull)get:(NSString *_Nonnull)url parameter:(NSDictionary *_Nullable)parameter dependencies:(NSArray<REAPIQueue *> *_Nullable)dependencies;

- (void)loopPendingQueue;

@end
