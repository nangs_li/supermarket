//
//  Constants.h
//  Ztore
//
//  Created by ken on 18/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Constants;

#pragma mark kNotificationRe
#define kNotificationReloadLeftMenu @"kNotificationReloadLeftMenu"
#define kNotificationOpenRightMenu @"kNotificationOpenRightMenu"
#define kNotificationCloseRightMenu @"kNotificationCloseRightMenu"
#define kNotificationCloseRightMenuHaveAnimation @"kNotificationCloseRightMenuHaveAnimation"
#define kNotificationOpenOrCloseLeftMenu @"kNotificationOpenOrCloseLeftMenu"
#define kNotificationCloseLeftMenu @"kNotificationCloseLeftMenu"
#define kNotificationCloseLeftMenuHaveAnimation @"kNotificationCloseLeftMenuHaveAnimation"
#define kNotificationCallAddProductAPIFromProductListChangeProductQty @"kNotificationCallAddProductAPIFromProductListChangeProductQty"
#define kNotificationGetMyFavouriteList @"kNotificationGetMyFavouriteList"
#define kNotificationSetUpTableViewDataFromMyTableView @"kNotificationSetUpTableViewDataFromMyTableView"
#define kNotificationGetAddressDetail2 @"kNotificationGetAddressDetail2"
#define kNotificationGetAddressDetail3 @"kNotificationGetAddressDetail3"
#define kNotificationReloadProductDetailUI @"kNotificationReloadProductDetailUI"
#define kNotificationUpdateProductListPageUI @"kNotificationUpdateProductListPageUI"
#define kNotificationEndTextField @"kNotificationEndTextField"
#define kNotificationShoppingCartNotData @"kNotificationShoppingCartNotData"

#pragma mark Server End Point
extern NSString *BASE_URL;
extern NSString *GETENCRYPTKEY_URL;

extern const bool IS_DEBUG;

#pragma mark ENcode string limit one time
extern const int ENCODESTR_LIMIT;

//-------------------------------------------

#pragma mark Default Font Size
extern const float DEFAULT_FONT_SIZE;

#pragma mark Common Margin
extern const float GL_COMMON_MARGIN;
extern const float GL_COMMON_HEIGHT;
extern const float GL_TAG_MARGIN;
extern const float GL_TAG_HEIGHT;
extern const float GL_BUTTON_HEIGHT;

#pragma mark Add Product Speed
extern const float INCREASE_PRODUCT_SPEED;
extern const float INCREASE_PRODUCT_API_SPEED;

extern const int PRODUCT_LIST_LIMIT;

#pragma mark MainPage

extern const int MainPage_ScrollView_Left_Space;

extern const int MinSpaceBetweenCell;

extern const int MainPage_TopScrollView_Left_Space;

extern const int MainPage_TopScrollViewCellHeight;

extern const int MainPage_ScrollViewCellHeight;

#pragma mark TopMessageBar

extern const int TopMessageBarDuration;

@interface Constants : NSObject

@end
