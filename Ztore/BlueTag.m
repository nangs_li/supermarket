//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "BlueTag.h"
#import <QuartzCore/QuartzCore.h>

@implementation BlueTag

- (void)setUpFromPromotions:(Promotions *)promotions {

    self.titleBtn.userInteractionEnabled = YES;
    self.tagType = BlueTagType;
    
    if (isValid(promotions)) {
        
        self.promotions = promotions;
        [self.titleBtn setTitle:self.promotions.label forState:UIControlStateNormal animation:NO];
        
        self.hidden = NO;
        
    } else {
        
        self.hidden = YES;
    }

    self.titleBtn.titleLabel.font = [[Common shared] fontWithName:@"OpenSans-Bold" size:DEFAULT_FONT_SIZE - 4 ];
    [self.titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor colorWithRed:77 / 255.0 green:188 / 255.0 blue:213 / 255.0 alpha:1];
    self.layer.cornerRadius = 4;
   
}

- (IBAction)pressBlueTagBtn:(id)sender {
    
    if (self.tagType == BlueTagType) {
        
        if ([self.delegate respondsToSelector:@selector(pressBlueTagBtnFromPromotions:)]) {
            [self.delegate pressBlueTagBtnFromPromotions:self.promotions];
        }
        
    } else {
        
        Products *productObject = [self.product getGlobalProduct];
        
        if ([self.delegate respondsToSelector:@selector(pressSoldOutBtnFromProduct:)]) {
            [self.delegate pressSoldOutBtnFromProduct:productObject];
            
        }

        
    }

    
}

- (void)setUpNotStockCase {
    
    self.hidden = NO;
    self.titleBtn.userInteractionEnabled = NO;
    self.titleBtn.titleLabel.font = [[Common shared] getSmallContentBoldFont];
    [self.titleBtn setTitle:JMOLocalizedString(@"SOLD OUT", nil) forState:UIControlStateNormal animation:NO];
    [self.titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.backgroundColor = [UIColor lightGrayColor];
    self.layer.cornerRadius = 4;
    
}

- (void)setUpSoldoutViewFromProduct:(Products *)product {
    
    self.product = product;
    self.tagType = SoldOutType;
    self.hidden = NO;
    self.titleBtn.userInteractionEnabled = YES;
    self.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    [self.titleBtn setTitle:(product.is_notice) ? JMOLocalizedString(@"Cancel Notification", nil) : JMOLocalizedString(@"Notify Me", nil) forState:UIControlStateNormal];
    [self.titleBtn setTitleColor:[[Common shared] getSoldOutButtonTitleColor] forState:UIControlStateNormal];
    self.layer.cornerRadius  = 66 / 2;
    self.layer.masksToBounds = YES;
    self.titleBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
    [self drawCricleShadowOpacity];
    
    [self.titleBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self.mas_centerY).with.offset(0);
        make.centerX.equalTo(self.mas_centerX).with.offset(0);
        
    }];

    
}

@end
