//
//  ProductSearchResultTableViewCell.m
//  Ztore
//
//  Created by ken on 19/7/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductSearchResultTableViewCell.h"

@implementation ProductSearchResultTableViewCell

- (void)setAttributedText:(NSString *)text searchBarText:(NSString *)searchBarText {
    
    NSRange range                             = [text rangeOfString:searchBarText options:NSCaseInsensitiveSearch];
    NSDictionary *attrs                       = @{NSFontAttributeName : [[Common shared] getNormalFont]};
    NSDictionary *subAttrs                    = @{NSFontAttributeName : [[Common shared] getNormalBoldFont]};
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    [attributedText setAttributes:subAttrs range:range];
    self.label_text.attributedText = attributedText;
    
}

@end
