//
//  RealServerResponseModel.h
//  productionreal2
//
//  Created by Ken on 19/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import "REObject.h"

@interface REServerResponseModel : REObject

@property(nonatomic, assign) NSInteger ErrorCode;
@property(nonatomic, strong) NSString *ApiName;
@property(nonatomic, strong) NSString *UniqueKey;
@property(nonatomic, strong) NSString *ErrorMsg;

@end
