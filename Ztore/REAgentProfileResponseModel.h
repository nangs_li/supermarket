//
//  REAgentProfileResponseModel.h
//  real-v2-ios
//
//  Created by Ken on 15/7/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentProfile.h"
#import "REServerResponseModel.h"

@interface REAgentProfileResponseModel : REServerResponseModel

@property(nonatomic, strong) NSMutableArray<REAgentProfile> *AgentProfiles;

@end
