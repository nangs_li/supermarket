//
//  SlidingOrderFooterCell.m
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "PromotionCode.h"
#import "SlidingOrderFooterCell.h"

@implementation SlidingOrderFooterCell

- (void)setupDataFromCart:(ShoppingCartData *)cart rightMenuVC:(RightMenuVC *)rightMenuVC userAccountData:(UserAccountData *)userAccountData haveCheckOutBtn:(BOOL)haveCheckOutBtn {

  self.alpha = 1;
  self.viewHeight = 165;
  self.label_OrderDetail.text     = JMOLocalizedString(@"Order Detail", nil);
  self.label_OrderDetail.font     = [[Common shared] getContentNormalFont];
  self.label_ProductSubtotal.text = JMOLocalizedString(@"Product Subtotal", nil);
  self.label_ProductSubtotal.font = [[Common shared] getContentNormalFont];
  self.label_subtotalText.text         = JMOLocalizedString(@"Subtotal", nil);
  self.label_subtotalText.font         = [[Common shared] getContentNormalFont];
  
    if (self.viewArray) {
        
        for (UIView *view in self.viewArray) {
            
            [view removeFromSuperview];
        }
        
    }
    
        self.viewArray = [[NSMutableArray alloc] init];

    CustomCancelLabel *perviousCustomCancelLabel;

    // if not promo code discount then add use promo code line
    
    if ([cart isPromoCodeDiscount] == nil) {
       
        NSMutableArray *array = [[NSMutableArray alloc] init];
        Discounts *discount = [[Discounts alloc] init];
        discount.rebate_zdollar = @"0";
        discount.subtotal = @"0";
        discount.code = @"PROMO_CODE";
        discount.name = JMOLocalizedString(@"Use Promo Code" , nil);
        
        [array addObject:discount];
        
          for (Discounts *discountObject in cart.discounts) {
            
              [array addObject:discountObject];
        }
        
        cart.discounts = [array copy];
        
    }
    
    //  add custom cancel label row on productSubtotal label bottom
    for (Discounts *discounts in cart.discounts) {
        
            if (![discounts.code isEqualToString:@"ZDOLLAR"]) {
        
                if ([discounts.subtotal intValue] < 0 || ([discounts.rebate_zdollar intValue] == 0  && [discounts.subtotal intValue] == 0)) {
                
                  CustomCancelLabel *customCancelLabel = [self createCustomCancelLabelFromRightMenuVC:rightMenuVC];
                    
                    if (self.viewArray.count == 1) {
                        
                        [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                            
                            make.top.equalTo(self.label_ProductSubtotal.mas_bottom).with.offset(8);
                            
                        }];
                        
                        
                        
                    } else {
                        
                        if (perviousCustomCancelLabel) {
                            
                            [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                                
                                make.top.equalTo(perviousCustomCancelLabel.mas_bottom).with.offset(0);
                                
                            }];
                            
                        }
                        
                    }
                    
                    BOOL haveCancelBtn = ([discounts.name isEqualToString:JMOLocalizedString(@"Use Promo Code" , nil)]) ? NO : YES;
                    
                     [customCancelLabel setUpViewFromTitleText:discounts.name feeText:[[Common shared] getStringFromPriceString:discounts.subtotal] labelType:UsePromoCode haveCancelBtn:haveCancelBtn discount:discounts haveCheckOutBtn:haveCheckOutBtn];
                    perviousCustomCancelLabel = customCancelLabel;
                    
                }
        
            }
        
    }
  
    //  handle if not perviousCustomCancelLabel case    add use promo code
    if (!isValid(perviousCustomCancelLabel)) {
        
        Discounts *discounts = cart.discounts.firstObject;
        
        CustomCancelLabel *customCancelLabel = [self createCustomCancelLabelFromRightMenuVC:rightMenuVC];
        
        if (self.viewArray.count == 1) {
            
            [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(self.label_ProductSubtotal.mas_bottom).with.offset(8);
                
            }];
            
            
            
        } else {
            
            if (perviousCustomCancelLabel) {
                
                [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    
                    make.top.equalTo(perviousCustomCancelLabel.mas_bottom).with.offset(0);
                    
                }];
                
            }
            
        }
        
        BOOL haveCancelBtn = ([discounts.name isEqualToString:JMOLocalizedString(@"Use Promo Code" , nil)]) ? NO : YES;
        
        [customCancelLabel setUpViewFromTitleText:discounts.name feeText:[[Common shared] getStringFromPriceString:discounts.subtotal] labelType:UsePromoCode haveCancelBtn:haveCancelBtn discount:discounts haveCheckOutBtn:haveCheckOutBtn];
        perviousCustomCancelLabel = customCancelLabel;
    
    // grey line top is customcancel label bottom
        [perviousCustomCancelLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.greyLine.mas_top).with.offset(0);
            
        }];

        
    } else {
   
  // grey line top is customcancel label bottom
    [perviousCustomCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.greyLine.mas_top).with.offset(0);
        
    }];
        
    }
    
    // subtotl label and delivery view and use zdollar view is fixed in xib.
    
    NSString *deliveryText;
    
    // handle haveCheckOutBtn and haveCheckOutBtn case change delivery text
        
    if (haveCheckOutBtn) {
        
        deliveryText = ([cart.free_shipping_min_order_amount intValue] == 0) ? JMOLocalizedString(@"Delivery Fee", nil) : [NSString stringWithFormat:JMOLocalizedString(@"Delivery(%@$ Free Delivery Fee)", nil), cart.free_shipping_min_order_amount];
        
    } else {
        
        deliveryText = JMOLocalizedString(@"Delivery Fee", nil);
    }
        [self.deliveryFeeView setUpViewFromTitleText:deliveryText feeText:[[Common shared] getStringFromPriceString:cart.delivery_fee] labelType:DeliveryFee haveCancelBtn:NO discount:nil haveCheckOutBtn:haveCheckOutBtn];
         [self.deliveryFeeView updateViewHeight:33];
        self.viewHeight = self.viewHeight + 33;
        self.deliveryFeeView.alpha = 1;

    self.useZdollarView.alpha = 1;
    
    Discounts *discounts2 = [cart isZdollarDiscount];
  // if account have use zdollar discount add after use Zdollar Row
    
    if (discounts2) {
        
        [self.useZdollarView updateViewHeight:33];
        self.viewHeight = self.viewHeight + 33;
        [self.useZdollarView setUpViewFromTitleText:discounts2.name feeText:[[Common shared] getStringFromPriceString:discounts2.subtotal] labelType:UseZDollar haveCancelBtn:YES discount:discounts2 haveCheckOutBtn:haveCheckOutBtn];
        
        
    } else {
        
        // if account have zdollar add before use Zdollar Row or set height == 0

        [self.useZdollarView setUpViewFromTitleText:JMOLocalizedString(@"Use Zdollar", nil) feeText:[[Common shared] getStringFromPriceString:@"0"] labelType:UseZDollar haveCancelBtn:NO discount:nil haveCheckOutBtn:haveCheckOutBtn];
     
        if ([userAccountData.zdollar intValue] > 0) {
            
            [self.useZdollarView updateViewHeight:33];
            self.viewHeight = self.viewHeight + 33;
            [self.useZdollarView setUpViewFromTitleText:JMOLocalizedString(@"Use Zdollar", nil) feeText:[[Common shared] getStringFromPriceString:@"0"] labelType:UseZDollar haveCancelBtn:NO discount:nil haveCheckOutBtn:haveCheckOutBtn];
            
        } else {
            
            [self.useZdollarView updateViewHeight:0];
            self.useZdollarView.alpha = 0;
        }
        
    }


    //have total_rebate_zdollar + 44
    if ([cart.total_rebate_zdollar intValue] > 0) {
        
        self.viewHeight = self.viewHeight + 44;
        self.greyLine2.hidden = NO;
        self.btn_zdollarTitle.hidden = NO;
        [self.btn_zdollarTitle.imageView setTintColor:[UIColor clearColor]];
        
    } else {
        
        self.greyLine2.hidden = YES;
        self.btn_zdollarTitle.hidden = YES;
        [self.btn_zdollarTitle setImage:[UIImage imageNamed:@"ic_keyboard_arrow_down_3x"] forState:UIControlStateNormal];
    }
    
    [self setZDollarRebateTitleBtnFromCart:cart];
    
    self.zdollarViewCount = 0;
    self.zdollarViewArray = [[NSMutableArray alloc] init];

    CustomCancelLabel *perviousCustomCancelLabel2;
 
//  add custom cancel label row bottom on self.btn_zdollarTitle
    
    for (Discounts *discounts in cart.discounts) {
        
        if (![discounts.code isEqualToString:@"ZDOLLAR"]) {
            
            if ([discounts.rebate_zdollar intValue] > 0) {
                
                CustomCancelLabel *customCancelLabel = [self createCustomCancelLabelFromRightMenuVC:rightMenuVC];
                [self.zdollarViewArray addObject:customCancelLabel];
                
                if (self.zdollarViewArray.count == 1) {
                    
                    [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                        
                        make.top.equalTo(self.btn_zdollarTitle.mas_bottom).with.offset(8);
                        
                    }];
                    
                    
                    
                } else {
                    
                    [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                        
                        make.top.equalTo(perviousCustomCancelLabel2.mas_bottom).with.offset(-6);
                        
                    }];
                    
                    
                }
                
                [customCancelLabel setUpViewFromTitleText:discounts.name feeText:[[Common shared] getStringFromPriceString:discounts.rebate_zdollar] labelType:UseZDollarCode haveCancelBtn:YES discount:discounts haveCheckOutBtn:haveCheckOutBtn];
                perviousCustomCancelLabel2 = customCancelLabel;
                self.zdollarViewCount++;
                
            }
            
        }
        
    }
    
  self.greyLine.userInteractionEnabled = NO;
  self.greyLine.backgroundColor = [[Common shared] getShoppingCartLineColor];
  self.label_ProductPrice.font         = [[Common shared] getContentBoldFont];
  self.label_ProductPrice.text         = [[Common shared] getStringFromPriceString:cart.product_subtotal];
  self.label_subtotalFee.font          = [[Common shared] getContentBoldFont];
  self.label_subtotalFee.text          = [[Common shared] getStringFromPriceString:cart.discounted_subtotal_before_zdollar];
  self.label_subtotalFee.textColor     = [[Common shared] getCommonRedColor];
  [self.label_subtotalFee sizeToFit];
  self.greyLine2.userInteractionEnabled = NO;
  self.greyLine2.backgroundColor = [[Common shared] getShoppingCartLineColor];
  self.greyLine3.backgroundColor = [[Common shared] getShoppingCartLineColor];
  [self setUpCheckOutBtnFromCart:cart];
    self.checkoutBtnBottomView.backgroundColor              = [[Common shared] getProductDetailDescGrayBackgroundColor];
    
    // handle haveCheckOutBtn and not haveCheckOutBtn case
    
    if (!haveCheckOutBtn) {

    self.checkoutBtnBottomView.alpha = self.btn_checkout.alpha = 0;
    self.viewHeight = self.viewHeight - 58 - 8;
    self.expandButton.hidden = self.label_OrderDetail.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
        
    [self.totalPriceView updateViewHeight:60];
    [self.totalPriceView setHidden:NO];
    self.viewHeight = self.viewHeight + 60;
    [self.totalPriceTextView setUpViewFromTitleText:JMOLocalizedString(@"Total", nil) feeText:[[Common shared] getStringFromPriceString:cart.total_price] labelType:TotalPrice haveCancelBtn:NO discount:nil haveCheckOutBtn:haveCheckOutBtn];
    self.totalPriceTextView.titleLabel.font           = [[Common shared] getContentBoldFont];
    [self.btn_zdollarTitle setImage:nil forState:UIControlStateNormal];
        
    self.lastView = self.totalPriceView;
        
    } else {
        
    [self.totalPriceView updateViewHeight:0];
    [self.totalPriceView setHidden:YES];
        
    if (perviousCustomCancelLabel2) {
            
            self.lastView = perviousCustomCancelLabel2;
            
    } else if (perviousCustomCancelLabel) {
            
             self.lastView = perviousCustomCancelLabel;
            
    } else {
            
            self.lastView = self.deliveryFeeView;
    }
        
    if (haveCheckOutBtn && isValid(perviousCustomCancelLabel2)) {
            
    self.viewHeight = self.viewHeight - 16;
            
    }
        
    }
    
    self.totalPriceView.backgroundColor = [UIColor clearColor];
    
    [self setNeedsLayout];
    
    [self bringSubviewToFront:self.btn_checkout];
    [self bringSubviewToFront:self.checkoutBtnBottomView];
    
}

- (void)setUpCheckOutBtnFromCart:(ShoppingCartData *)cart {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *font1 = [[Common shared] getNormalTitleFont];
    UIFont *font2 = [[Common shared] getNormalBoldTitleFont];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSParagraphStyleAttributeName:style}; // Added line
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font2,
                            NSParagraphStyleAttributeName:style}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:JMOLocalizedString(@"CHECKOUT NOW", nil)    attributes:dict1]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:JMOLocalizedString(@" - Total%@", nil), [[Common shared] getStringFromPriceString:cart.total_price]]      attributes:dict2]];
    [UIView setAnimationsEnabled:NO];
    [self.btn_checkout setAttributedTitle:attString forState:UIControlStateNormal animation:NO];
    [UIView setAnimationsEnabled:YES];
    [[self.btn_checkout titleLabel] setNumberOfLines:0];
    [[self.btn_checkout titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    self.btn_checkout.titleLabel.textColor = [UIColor whiteColor];
    [self.btn_checkout setUpSmallCornerBorderButton];
    self.backgroundColor              = [[Common shared] getProductDetailDescGrayBackgroundColor];
    self.btn_checkout.backgroundColor = (cart.products.count > 0) ? [[Common shared] getCommonRedColor] : [UIColor lightGrayColor] ;
    self.btn_checkout.layer.borderColor = self.btn_checkout.backgroundColor.CGColor;
    self.btn_checkout.userInteractionEnabled = (cart.products.count > 0) ? YES : NO ;
    
}

- (void)setZDollarRebateTitleBtnFromCart:(ShoppingCartData *)cart {
    
    [self.btn_zdollarTitle makeRightImageView];
    [self.btn_zdollarTitle setTitleColor:[[Common shared] getCommonPinkRedColor] forState:UIControlStateNormal];
    self.btn_zdollarTitle.titleLabel.textColor = [[Common shared] getCommonPinkRedColor];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    UIFont *font1 = [[Common shared] getContentNormalFont];
    UIFont *font2 = [[Common shared] getContentBoldFont];
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font1,
                            NSParagraphStyleAttributeName:style}; // Added line
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),
                            NSFontAttributeName:font2,
                            NSParagraphStyleAttributeName:style}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:JMOLocalizedString(@"Z-Dollar Rebate", nil)attributes:dict1]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", [[Common shared] getStringFromPriceString:cart.total_rebate_zdollar]]  attributes:dict2]];
    [UIView setAnimationsEnabled:NO];
    [self.btn_zdollarTitle setAttributedTitle:attString forState:UIControlStateNormal animation:NO];
    [UIView setAnimationsEnabled:YES];

}

- (CustomCancelLabel *)createCustomCancelLabelFromRightMenuVC:(RightMenuVC *)rightMenuVC {
    
    CustomCancelLabel *customCancelLabel = [[CustomCancelLabel alloc] init];
    customCancelLabel.cancelBtnDelegate = rightMenuVC;
    [self.viewArray addObject:customCancelLabel];
    self.viewHeight = self.viewHeight + 33;
    [self addSubview:customCancelLabel];
    
    [customCancelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(33);
        make.left.equalTo(self.btn_checkout.mas_left).with.offset(0);
        make.right.equalTo(self.btn_checkout.mas_right).with.offset(0);
        
    }];
    
    return customCancelLabel;
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.label_ProductSubtotal.preferredMaxLayoutWidth = CGRectGetWidth(self.label_ProductSubtotal.frame);
    self.label_ProductPrice.preferredMaxLayoutWidth = CGRectGetWidth(self.label_ProductPrice.frame);

}

@end
