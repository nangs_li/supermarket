//
//  ProductSearchBarController.h
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AutoCompleteProduct.h"
#import "ProductSearchHistoryTableViewHeader.h"
#import "ProductSearchResultTableViewCell.h"
#import "_UIViewController.h"

@interface ProductSearchBarController : _UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
  // UI
  IBOutlet UITableView *searchHistoryTableView;
  IBOutlet UITableView *searchResultTableView;
  IBOutlet UISearchDisplayController *searchBarController;
  ProductSearchHistoryTableViewHeader *searchHistoryTableHeaderView;

  // Control

  // Data
  NSArray *resultSectionTitleList;
  NSMutableArray *searchHistoryList;
  bool isAddedHistoryTableView;
}
@property(nonatomic, assign) id delegate;
@property(nonatomic, strong) ZtoreNavigationBar *navigationBar;
@property(nonatomic, strong) NSIndexPath *selectIndexPath;
@property(nonatomic, strong) AutoCompleteProductData *autoCompleteProductData;
@property(strong, nonatomic) NSTimer *autoCompleteAPIProductAPITimer;

@end
