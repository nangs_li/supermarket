
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

@class PromotionModel;

@interface PromotionHeaderView : UICollectionReusableView

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *dateLabel;
@property(weak, nonatomic) IBOutlet UIImageView *promotionImageView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;
@property(weak, nonatomic) NSString *urlLink;

- (void)setupViewFromPromotion:(PromotionModel *)promotion urlLink:(NSString *)urlLink;
- (CGFloat)getTotalHeight;

@end
