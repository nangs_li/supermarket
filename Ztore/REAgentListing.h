//
//  REAgentListing.h
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  strongright © 2016 ken. All rights reserved.
//

#import "REObject.h"

@class REDBAgentListing, DBReasons, DBAddress, DBPhotos;

@protocol Reasons

@end

@interface Reasons : REObject

@property(nonatomic, strong) NSString *Reason3URL;

@property(nonatomic, strong) NSString *Reason3;

@property(nonatomic, strong) NSString *Reason2;

@property(nonatomic, strong) NSString *Reason1;

@property(nonatomic, assign) int SloganIndex;

@property(nonatomic, assign) int LanguageIndex;

@property(nonatomic, strong) NSString *Reason1URL;

@property(nonatomic, strong) NSString *Reason2URL;

@property(nonatomic, strong) PHAsset *asset1;

@property(nonatomic, strong) PHAsset *asset2;

@property(nonatomic, strong) PHAsset *asset3;

- (DBReasons *)toREDBObject;

@end

@protocol Address2

@end

@interface Address2 : REObject

@property(nonatomic, strong) NSString *FormattedAddress;

@property(nonatomic, strong) NSString *Lang;

@property(nonatomic, strong) NSString *ShortAddress;

@property(nonatomic, strong) NSString *PlaceID;

- (DBAddress *)toREDBObject;

@end

@protocol Photos

@end

@interface Photos : REObject

@property(nonatomic, strong) NSString *URL;

@property(nonatomic, assign) int PhotoID;

@property(nonatomic, assign) int Position;

@property(nonatomic, assign) int Height;

@property(nonatomic, assign) int Width;

- (DBPhotos *)toREDBObject;

@end

@protocol REAgentListing

@end

@interface REAgentListing : REObject

@property(nonatomic, strong) NSMutableArray<Reasons> *Reasons;

@property(nonatomic, assign) int BedroomCount;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForIndian;

@property(nonatomic, assign) int SloganIndex;

@property(nonatomic, assign) int KitchenCount;

@property(nonatomic, assign) int LivingRoomCount;

@property(nonatomic, assign) int PropertyType;

@property(nonatomic, assign) int PropertySize;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForRoman;

@property(nonatomic, strong) NSString *PropertyPriceFormattedForChinese;

@property(nonatomic, strong) NSString *CurrencyUnit;

@property(nonatomic, strong) NSMutableArray<Address2> *Address;

@property(nonatomic, strong) NSString *SizeUnit;

@property(nonatomic, assign) int BathroomCount;

@property(nonatomic, strong) NSString *CreationDate;

@property(nonatomic, assign) int PropertyPrice;

@property(nonatomic, assign) int ListingFollowerCount;

@property(nonatomic, assign) int BalconyCount;

@property(nonatomic, strong) NSMutableArray<Photos> *Photos;

@property(nonatomic, assign) int AgentListingID;

@property(nonatomic, assign) int SizeUnitType;

@property(nonatomic, assign) int Version;

@property(nonatomic, assign) int SpaceType;

@property(nonatomic, assign) BOOL isPreview;

- (REDBAgentListing *)toREDBObject;

- (NSArray *)getReason;

@end
