
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface PopUpView : ShoppingCartPopUpView

- (void)setupErrorMessageView;

@end
