//
//  ExpandingCell.m
//  Ztore
//
//  Created by ken on 2/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "ExpandingCell.h"
#import "FICDPhotoLoader.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ExpandingCell

- (void)awakeFromNib {
  // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (NSString *)accessibilityLabel {
  return self.textLabel.text;
}

- (void)setLoading:(BOOL)loading {
  if (loading != _loading) {
    _loading = loading;
    [self _updateDetailTextLabel];
  }
}

- (void)setExpansionStyle:(UIExpansionStyle)expansionStyle animated:(BOOL)animated {
  if (expansionStyle != _expansionStyle) {
    _expansionStyle = expansionStyle;
    [self _updateDetailTextLabel];
  }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {

  if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    [self _updateDetailTextLabel];
    self.backgroundColor = [UIColor yellowColor];
  }

  return self;
}

- (void)_updateDetailTextLabel {
  if (self.isLoading) {
    self.detailTextLabel.text = @"Loading data";
  } else {
    switch (self.expansionStyle) {
    case UIExpansionStyleExpanded:
      self.detailTextLabel.text = @"Click to collapse";
      break;
    case UIExpansionStyleCollapsed:
      self.detailTextLabel.text = @"Click to expand";
      break;
    }
  }
}

- (void)setUpCellFromProductCategoryData:(ProductCategoryData *)productCategoryData indexPath:(NSIndexPath *)indexPath {
  
  self.backgroundColor = [UIColor clearColor];
  self.contentView.backgroundColor = [UIColor clearColor] ;
  ProductCategoryData *proCat = productCategoryData;
  UIButton *btn_supercat      = self.menuBtn;
//  [FICDPhotoLoader loaderImageForImageView:self.menuIconView withPhotoUrl:[FICDPhotoLoader getPhotoUrlInProduct:[proCat.icons changeToZoomClass]]];
//  [FICDPhotoLoader loaderImageForImageView:[[UIImageView alloc]init] withPhotoUrl:[FICDPhotoLoader getPhotoUrlInProduct:[proCat.hover_icons changeToZoomClass]]];
      [FICDPhotoLoader loaderImageForImageView:self.menuIconView withPhotoUrl:proCat.mobile_icon];
      [FICDPhotoLoader loaderImageForImageView:[[UIImageView alloc]init] withPhotoUrl:proCat.hover_mobile_icon];

  [btn_supercat setTitle:proCat.name forState:(UIControlState)UIControlStateNormal animation:NO];
  [btn_supercat setUpLeftMenuButton];
  self.selectionStyle    = UITableViewCellSelectionStyleNone;
  self.indexPath         = indexPath;
  self.expandImage.image = [UIImage imageNamed:@"ic_add_3x"];
  [self.whiteLine setHidden:YES];
}

- (void)setUpSelectedCellFromProductCategoryData:(ProductCategoryData *)productCategoryData indexPath:(NSIndexPath *)indexPath {
    
  ProductCategoryData *proCat = productCategoryData;
  [self setUpCellFromProductCategoryData:productCategoryData indexPath:indexPath];
//  [FICDPhotoLoader loaderImageForImageView:self.menuIconView withPhotoUrl:[FICDPhotoLoader getPhotoUrlInProduct:[proCat.hover_icons changeToZoomClass]]];
      [FICDPhotoLoader loaderImageForImageView:self.menuIconView withPhotoUrl:proCat.hover_mobile_icon];
  [self.menuBtn setUpLeftSelectedMenuButton];
  self.expandImage.image = [UIImage imageNamed:@"ic_remove_3x"];
  [self.whiteLine drawShadowOpacity];
  [self.whiteLine setHidden:NO];
  self.backgroundColor = [UIColor whiteColor] ;
  self.contentView.backgroundColor = [UIColor whiteColor] ;
}

- (void)setUpSubCellFromProductChildren:(Children *)children indexPath:(NSIndexPath *)indexPath {

  UIButton *btn_supercat = self.menuBtn;
  [btn_supercat setImage:nil forState:UIControlStateNormal];
  self.expandImage.hidden = YES;
  self.expandLabel.hidden = YES;
  [btn_supercat setTitle:children.name forState:(UIControlState)UIControlStateNormal animation:NO];
  [self setBackgroundColor:[[Common shared] getCommonGrayBackgroundColor]];
  self.indexPath = indexPath;
}

@end
