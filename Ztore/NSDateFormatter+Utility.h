//
//  NSURL+Utility.h
//  productionreal2
//
//  Created by Ken on 10/11/2015.
//  Copyright © 2015 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Utility)
//- (NSDateFormatter *)NSDateFormatter;
//- (void)setNSDateFormatterFromCurrentLanguage;
+ (NSDateFormatter *)localeDateFormatter;
+ (NSDateFormatter *)enUsDateFormatter;
- (void)setupDateFormatter;

@end
