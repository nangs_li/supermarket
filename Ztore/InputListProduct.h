//
//  InputListProduct.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InputListProduct : NSObject

typedef NS_ENUM(NSInteger, InputListProductType) { mainPage_category ,parent_category_id, press_blue_tag, search_brand_ids, search_tags };

@property(assign, nonatomic) int parent_category_id;
@property(nonatomic, strong) NSArray *category_ids;
@property(nonatomic, strong) NSArray *tags;
@property(nonatomic, strong) NSArray *shop_ids;
@property(nonatomic, strong) NSArray *brand_ids;
@property(nonatomic, strong) NSString *keyword;
@property(nonatomic, strong) NSArray *order_by;
@property(nonatomic, strong) NSString *sn;
@property(assign, nonatomic) int result_start;
@property(assign, nonatomic) int result_limit;
@property(assign, nonatomic) InputListProductType type;

- (BOOL)checkHaveSearchType;

@end
