
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ShoppingCartPopUpView : UIView <UIScrollViewDelegate, UITextFieldDelegate>

@property(strong, nonatomic) IBOutlet UIView *usePromoCodeView;
@property(strong, nonatomic) IBOutlet UILabel *titleLabel;
@property(strong, nonatomic) CustomTextFieldView *usePromoCodeTextField;
@property(strong, nonatomic) IBOutlet UIButton *backBtn;
@property(strong, nonatomic) IBOutlet UIButton *useBtn;
@property(strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIView *centerView;
@property(weak, nonatomic) IBOutlet UILabel *errorDetailLabel;
@property(weak, nonatomic) IBOutlet UIImageView *retryImageView;

- (void)setupView;

@end
