//
//  OnSaleViewController.m
//  Ztore
//
//  Created by ken on 30/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "KLCPopup.h"
#import "ProductCategoryController.h"
#import "ProductListCollectionViewController.h"
#import "ProductSearchBarController.h"
#import "EHHorizontalSelectionView+Utility.h"
#import "MainPageBottomView.h"
#import <QuartzCore/QuartzCore.h>
#import "WebViewViewController.h"

@interface ProductCategoryController ( ) {

  NSMutableArray *fragments;
  NSMutableArray *tagTitles;
  int curFragmentIndex;
  UIViewController *productAdanceSearchViewController;
    
}

@end

@implementation ProductCategoryController

#pragma mark ProductCategoryController

- (void)perpareCallCategoryAPI {
    
    [self loadingFromColor:nil];
    fragments = [[NSMutableArray alloc] init];
    
}

- (void)initPageMenu {
    
    if (isValid(self.pageMenu)) {
        
        [self.pageMenu.view removeFromSuperview];
    }
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:247.0 / 255.0 green:247.0 / 255.0 blue:247.0 / 255.0 alpha:0.75],
                                 CAPSPageMenuOptionViewBackgroundColor: [[Common shared] getProductDetailDescGrayBackgroundColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [[Common shared] getCommonRedColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuItemFont: [[Common shared] getContentBoldFont],
                                 CAPSPageMenuOptionMenuHeight: @(60.0),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[[Common shared] getCommonRedColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor:[[Common shared] getCommonGrayColor],
                                 CAPSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth:@(YES),
                                 CAPSPageMenuOptionMenuMargin:@(0.0),
                                 CAPSPageMenuOptionCenterMenuItems:@(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:fragments frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    _pageMenu.scrollAnimationDurationOnMenuItemTap = 100;
    [_pageMenu.menuScrollView drawShadowOpacity];
    _pageMenu.delegate = self;
    [self.view addSubview:_pageMenu.view];
    
}

- (void)getCategoryAPIFromLevel2Category_ID:(NSInteger)Level2Category_ID {
    
    [self perpareCallCategoryAPI];
    self.parentCatId = Level2Category_ID;
    
    [[ServerAPI shared] getCategoryChildFromParent_id:self.parentCatId
                                      completionBlock:^(id _Nullable response, NSError *_Nullable error) {
                                          
                                          [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = NO;
                                          
                                          if ([[Common shared] checkNotError:error controller:self response:response]) {
                                              
                                              [self getCategoryChildCallbackFromProductCategoryModel:response] ;
                                          }
                                          
                                      }];
    

}

- (void)getCategoryChildCallbackFromProductCategoryModel:(ProductCategoryModel *)productCategoryModel {

  NSArray<ProductCategoryData> *data = productCategoryModel.data;
  NSArray *statusCodes               = productCategoryModel.statusCodes;

  if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {

    self.numberOfTabs         = [data count];
    fragments                 = [[NSMutableArray alloc] initWithCapacity:[data count]];
    tagTitles                 = [[NSMutableArray alloc] initWithCapacity:[data count]];
    for (int i = 0; i < self.numberOfTabs; i++) {

      ProductCategoryData *category            = data[i];
      InputListProduct *inputListProduct                    = [[InputListProduct alloc] init];
      inputListProduct.type = parent_category_id;
      inputListProduct.parent_category_id                    = (int) category.id;
      ProductListCollectionViewController *cvc = [ProductListCollectionViewController initViewControllerWithInputListProduct:inputListProduct];
      cvc.parentController                     = self;
      cvc.title = [NSString stringWithFormat:@"  %@   ", category.name];
      [fragments addObject:cvc];
      [tagTitles addObject:category.name];
    }
    
      ProductListCollectionViewController *cvc = fragments.firstObject;
      [cvc callgetListProduct];
      cvc.haveCallProductListAPI = YES;
      
      [self initPageMenu];
      
 }
    
}

- (void)listProductGroupByCategoryFromLevel3Category_ID:(NSInteger)Level3Category_ID {
    
    [self perpareCallCategoryAPI];
    self.parentCatId = Level3Category_ID;
    
    [[ServerAPI shared] listProductGroupByCategoryFromParent_category_id:self.parentCatId completionBlock:^(id response, NSError *error) {
                                          
                                          [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = NO;
        
                                          if ([[Common shared] checkNotError:error controller:self response:response]) {
                                              
                                              [self listProductGroupByCategoryCallbackFromGetProductCategoryArray:response] ;
                                          }
                                          
                                      }];

}

- (void)listProductGroupByCategoryCallbackFromGetProductCategoryArray:(GetProductCategoryArray *)getProductCategoryArray {
    
    
    NSArray<GetCategory> *data = getProductCategoryArray.data;
    NSArray *statusCodes               = getProductCategoryArray.statusCodes;
    
    if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
        
        self.numberOfTabs         = [data count];
        fragments                 = [[NSMutableArray alloc] initWithCapacity:[data count]];
        tagTitles                 = [[NSMutableArray alloc] initWithCapacity:[data count]];
        for (int i = 0; i < self.numberOfTabs; i++) {
            
            GetCategory *getCategory            = data[i];
            InputListProduct *inputListProduct                    = [[InputListProduct alloc] init];
            inputListProduct.type = mainPage_category;
            inputListProduct.parent_category_id                    = (int) getCategory.id;
            ProductListCollectionViewController *cvc = [ProductListCollectionViewController initViewControllerWithInputListProduct:inputListProduct];
            cvc.parentController                     = self;
            cvc.title = [NSString stringWithFormat:@"  %@   ", getCategory.name];
            cvc.products = [getCategory.products mutableCopy];
            cvc.haveCallProductListAPI = YES;
            [cvc endCallAPILoading];
            [fragments addObject:cvc];
            [tagTitles addObject:getCategory.name];
            
        }
        
        [self initPageMenu];
        
    }

    
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
    [AppDelegate getAppDelegate].mainPageNavigationController = self.navigationController;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.mainScrollView.delegate = self;
    self.loadingView_BannerView = [self addLoadingInBannerView:self.bannerView];
    self.loadingView_NewItem = [self addLoadingInNewItem:self.view];
    [self setUpMainNavigationBar];
    [self showMainPage:YES];

}

- (void)setUpMainNavigationBar {
    
    self.ztoreNavigationBar = [Common getMainMenuBar];
    [self.navigationController.navigationBar addSubview:self.ztoreNavigationBar];
    [self.ztoreNavigationBar matchSizeWithSuperViewFromAutoLayout];

}

- (void)setUpNavigationBar {
    
    [self.ztoreNavigationBar mainMenuBarType];
    self.ztoreNavigationBar.rightMenuImage2.hidden = self.ztoreNavigationBar.rightBtn2.hidden = NO;
    self.ztoreNavigationBar.titleLabel.text = ([AppDelegate getAppDelegate].Level2_childrenProduct.name == nil) ? JMOLocalizedString(@"Menu", nil) : [AppDelegate getAppDelegate].Level2_childrenProduct.name;
    [self.ztoreNavigationBar.rightBtn2 addTarget:self action:@selector(rightBtn2Press) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.rightBtn addTarget:self action:@selector(rightBtnPress) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(leftBtnPress) forControlEvents:UIControlEventTouchDown];
    [self showMainPage:self.showMainPage];

}

- (void)leftBtnPress {
    
     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenOrCloseLeftMenu object:nil];
}

- (void)rightBtnPress {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenRightMenu object:nil];
}

- (void)rightBtn2Press {
    
    self.ztoreNavigationBar.rightBtn2.userInteractionEnabled = NO;
    ProductSearchBarController *targetController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductSearchBarController"];
    [self.navigationController pushViewController:targetController animated:YES enableUI:NO];
    self.ztoreNavigationBar.rightBtn2.userInteractionEnabled = YES;
    
}
     
- (void)viewWillAppear:(BOOL)animated {

  [super viewWillAppear:animated];
  self.navigationItem.hidesBackButton = YES;
  self.cashIconLabel.text = JMOLocalizedString(@"Cash On Delivery", nil);
  self.delveryIconLabel.text = JMOLocalizedString(@"Same-day delivery", nil);
  self.returnMoneyIconLabel.text = JMOLocalizedString(@"Refund/Return Guarantee", nil);
  self.cashIconLabel.font = self.delveryIconLabel.font = self.returnMoneyIconLabel.font = [[Common shared] getSmallContentNormalFont];
  [self enableSlidePanGestureForLeftMenu];
  [self enableSlidePanGestureForRightMenu];
  [self mainSlideMenu].leftMenu = [AppDelegate getAppDelegate].leftMenuVC;
  [self reloadAllCollectionView];
    
}

- (void)reloadAllCollectionView {
    
    if (isValid(self.collectectViewArray)) {
        
        for (UICollectionView *collectionView in self.collectectViewArray) {
            
            [collectionView reloadData];
        }
        
    }
    
    if (fragments.count > self.pageMenu.currentPageIndex) {
        
        ProductListCollectionViewController *productListViewController = fragments[self.pageMenu.currentPageIndex];
        [productListViewController.collectionView reloadData];
        
    }

}

- (void)viewWillDisappear:(BOOL)animated {

  [super viewWillDisappear:animated];
  [self disableSlidePanGestureForRightMenu];
  [self mainSlideMenu].leftMenu = nil;
    
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  [AppDelegate getAppDelegate].productCategoryController = self;
  
}

- (void)callMainPageAPI {
    
    [self callBannerList];
    [self callFeatureProductList];
    [[ServerAPI shared] isLoginCompletionBlock:^(id response, NSError *error) {
        
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadLeftMenu object:nil];    
    [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
        
    }];
    
    [[ServerAPI shared] getFreeShppingMinOrderAmountCompletionBlock:^(id response, NSError *error) {
        
    }];
    
    [[AppDelegate getAppDelegate].leftMenuVC getMenu];
    
}

- (void)showMainPage:(BOOL)show {
    
    self.pageMenu.view.hidden = self.showMainPage = show;
    self.mainScrollView.hidden = !show;
    [self.view bringSubviewToFront:self.mainScrollView];
    self.ztoreNavigationBar.titleLabel.hidden = !self.mainScrollView.hidden;
    self.ztoreNavigationBar.ztoreWhiteLogoImageView.hidden = self.mainScrollView.hidden;
    self.centerView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    show ? [super setPageName:@"Home"] : [super setPageName:@"Category"];
    [TSMessage dismissActiveNotification];
    
}

- (void)loadingFromColor:(UIColor *)color {
    
    [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = YES;
    [super loadingFromColor:color];
    
}

- (void)endCallAPILoading {
    
    [AppDelegate getAppDelegate].productCategoryController.pageMenu.view.hidden = NO;
    [super endCallAPILoading];
    
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
  
    ProductListCollectionViewController *viewController = (ProductListCollectionViewController *)controller;
    
    if (!viewController.haveCallProductListAPI) {
        viewController.haveCallProductListAPI = YES;
        
        // call when self.inputListProduct.type equal to mainPage_category ,parent_category_id
        [viewController callgetListProduct];
        
    } else {
        
        [viewController.collectionView reloadData];
        
    }
    
}

#pragma mark mainPage

- (void)callBannerList {
    
    [[ServerAPI shared] getBannerListCompletionBlock:^(id response, NSError *error) {
        
        [self.loadingView_BannerView stopAnimating];
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            NSMutableArray *imageArry = [[NSMutableArray alloc] init];
            BannerModel *bannerModel = response;
            
            BannerModelData *bannerModelData = bannerModel.data.firstObject;
            
            if (isValid(bannerModelData)) {
                
                Item *item = bannerModelData.item.firstObject;
                
                if (isValid(item)) {
                    
                    for (Detail *detail in item.detail) {
                        
                        [imageArry addObject:detail.image_url];
                    }
                    
                }
            }
            
            self.bannerLoopView.imageArray = [imageArry copy];
            self.bannerLoopView.xlsn0wDelegate = self;
            self.bannerLoopView.time = 4;
            [self.bannerLoopView setPageColor:[UIColor clearColor] andCurrentPageColor:[UIColor clearColor]];
        }
        
    }];
    
}

- (void)callFeatureProductList {
    
    [[ServerAPI shared] listFeaturedProductFromLimit:12 completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            self.mainPageNewProductList = response;
            
            [[ServerAPI shared] listFeaturedCategoryProductListFromLimit:0 completionBlock:^(id response, NSError *error) {
                
                [self.loadingView_NewItem stopAnimating];
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                    
                    self.mainPageProductList = response;
                    
                    [self setUpSwipeView];
                    
                }
                
            }];

        } else {
            
            [self.loadingView_NewItem stopAnimating];
            
        }
        
    }];
    
}

- (void)setUpSwipeView {
    
    self.viewArray = [[NSMutableArray alloc] init];
    self.collectectViewArray = [[NSMutableArray alloc] init];
    
    MainPageRowView *swipeView = [self createSwipeViewNewItem:YES];
    [swipeView.titleBtn makeRightImageView];
    [swipeView.titleBtn setTitle:JMOLocalizedString(@"New Products", nil) forState:UIControlStateNormal animation:NO];
    swipeView.titleBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    [swipeView.titleBtn setImage:nil forState:UIControlStateNormal];
    
    [swipeView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.iconView.mas_bottom).with.offset(12);
        
    }];
    
    self.perviousSwipeView = swipeView;
    
    for (ProductList *productList in self.mainPageProductList.data) {
        
        MainPageRowView *swipeView = [self createSwipeViewNewItem:NO];
        [swipeView.titleBtn makeRightImageView];
        [swipeView.titleBtn setTitle:productList.name forState:UIControlStateNormal animation:NO];
        swipeView.titleBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
        
        [swipeView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.perviousSwipeView.mas_bottom).with.offset(12);
            
        }];
        
        self.perviousSwipeView = swipeView;
    }
    
    MainPageBottomView *mainPageBottomView = [[[NSBundle mainBundle] loadNibNamed:@"MainPageBottomView" owner:self options:nil] objectAtIndex:0];
    [self.centerView addSubview:mainPageBottomView];
    
    mainPageBottomView.bottomBtn.clipsToBounds      = YES;
    mainPageBottomView.bottomBtn.layer.cornerRadius = mainPageBottomView.bottomBtn.bounds.size.width / 2;
    mainPageBottomView.bottomBtn.layer.borderColor  = [UIColor clearColor].CGColor;
    mainPageBottomView.bottomBtn.layer.borderWidth  = 0.1f;
    
    mainPageBottomView.bottomLabel.text = JMOLocalizedString(@"-- Click on the menu to see more items --", nil);
    mainPageBottomView.bottomLabel.font = [[Common shared] getSmallContentNormalFont];
    
    [mainPageBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.perviousSwipeView.mas_bottom).with.offset(0);
        make.left.equalTo(self.centerView.mas_left).with.offset(0);
        make.right.equalTo(self.centerView.mas_right).with.offset(0);
        make.bottom.equalTo(self.centerView.mas_bottom).with.offset(0);
        
    }];
    
    [[mainPageBottomView.bottomBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {

        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenOrCloseLeftMenu object:nil];
        
    }];
    
    [self.view setNeedsLayout];

}

- (MainPageRowView *)createSwipeViewNewItem:(BOOL)newItem {
    
    MainPageRowView *mainPageRowView = [[[NSBundle mainBundle] loadNibNamed:@"MainPageRowView" owner:self options:nil] objectAtIndex:0];
    EHHorizontalSelectionView *swipeView = mainPageRowView.horizontalView;
    [swipeView registerCellNib:[UINib nibWithNibName:@"ProductCollectionCell" bundle:nil] withClass:[ProductCollectionCell class]];
    [swipeView registerCellNib:[UINib nibWithNibName:@"BigProductCollectionCell" bundle:nil] withClass:[BigProductCollectionCell class]];
    swipeView.delegate = self;
    swipeView.collectionView.delegate = self;
    swipeView.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    [self.viewArray addObject:mainPageRowView];
    [self.collectectViewArray addObject:swipeView.collectionView];
    swipeView.tag = mainPageRowView.titleBtn.tag = swipeView.collectionView.tag = self.viewArray.count - 1;
    [mainPageRowView.titleBtn addTarget:self action:@selector(pressTitleBtn:) forControlEvents:UIControlEventTouchDown];
    [self.centerView addSubview:mainPageRowView];
    mainPageRowView.backgroundColor = [UIColor whiteColor];
    
    [mainPageRowView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(newItem ? 354 : 300);
        make.left.equalTo(mainPageRowView.superview.mas_left).with.offset(0);
        make.right.equalTo(mainPageRowView.superview.mas_right).with.offset(0);
        
    }];
    
    return mainPageRowView;
    
}

- (void)pressTitleBtn:(UIButton *)sender {
    
    if (sender.tag == 0) {
        
      
        
    } else {
        
//        ProductList *productList = self.mainPageProductList.data[sender.tag - 1];
//        self.mainNavigationBar.titleLabel.text = productList.url_key;
//        [self listProductGroupByCategoryFromLevel3Category_ID:productList.id];

    }

}

- (IBAction)pressCashBtn:(id)sender {
    
    WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
    zDollarViewController.titleString = JMOLocalizedString(@"FAQ", nil);
    zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_FAQ/mobile" : @"https://www.ztore.com/tc/article/article_FAQ/mobile";
    [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
    
}

- (IBAction)pressDeliveryBtn:(id)sender {
    
    WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
    zDollarViewController.titleString = JMOLocalizedString(@"About Delivery", nil);
    zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_delivery/mobile" : @"https://www.ztore.com/tc/article/article_delivery/mobile";
    [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
    
}

- (IBAction)pressReturnMoneyBtn:(id)sender {
    
    WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
    zDollarViewController.titleString = JMOLocalizedString(@"FAQ", nil);
    zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_FAQ/mobile" : @"https://www.ztore.com/tc/article/article_FAQ/mobile";
    [[AppDelegate getAppDelegate].mainPageNavigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
    
}

#pragma mark - EHHorizontalSelectionViewProtocol

- (NSUInteger)numberOfItemsInHorizontalSelection:(EHHorizontalSelectionView *)hSelView {
    
    NSInteger numberOfItems;
    
    if (hSelView.tag == 0) {
        
        numberOfItems = self.mainPageNewProductList.data.count;
        
    } else {
        
        ProductList *productList = self.mainPageProductList.data[hSelView.tag - 1];
        numberOfItems = productList.products.count;
        
    }
    
    return numberOfItems;
}

- (NSString *)titleForItemAtIndex:(NSUInteger)index forHorisontalSelection:(EHHorizontalSelectionView *)hSelView {
    
      return @"";
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Products *product;
    
    if (collectionView.tag == 0) {
        
        product = self.mainPageNewProductList.data[indexPath.row];
        
    } else {
        
        ProductList *productList = self.mainPageProductList.data[collectionView.tag - 1];
        product = productList.products[indexPath.row];
        
    }
    
   [[Common shared] pushToProductDetailPageFromProduct:product];

}

- (EHHorizontalViewCell *)selectionView:(EHHorizontalSelectionView *)selectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductCollectionCell *cell;
    
    Products *product;
    
    if (selectionView.tag == 0) {
        
        cell = (ProductCollectionCell *)[selectionView dequeueReusableCellWithReuseIdentifier:@"ProductCollectionCell" forIndexPath:indexPath];
    
        product = self.mainPageNewProductList.data[indexPath.row];
        
        cell.blueTag.delegate = self;
        cell.delegate = self;
        
        
    } else {
        
        cell = (BigProductCollectionCell *)[selectionView dequeueReusableCellWithReuseIdentifier:@"BigProductCollectionCell" forIndexPath:indexPath];

        ProductList *productList = self.mainPageProductList.data[selectionView.tag - 1];
        product = productList.products[indexPath.row];
        cell.blueTag.delegate = self;
        
    }
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    [cell setUpProductCellDataFromProduct:product mainPage:YES];
    
    return cell;
}

#pragma mark collectionView Layout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    int space = (collectionView.tag > 0) ? MainPage_ScrollView_Left_Space : MainPage_TopScrollView_Left_Space;
    
    return UIEdgeInsetsMake(0, space , 0, space);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
     if (collectionView.tag == 0) {
         
         return CGSizeMake(MainPage_TopScrollViewCellHeight, 275);
         
     } else {
         
         return CGSizeMake(MainPage_ScrollViewCellHeight, 220);
     }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {

    return MinSpaceBetweenCell;
}

#pragma mark main Page Custom Loading

- (UIActivityIndicatorView *)addLoadingInBannerView:(UIView *)view {
    
    if (!isValid(self.loadingView_BannerView) ) {
        
        // Create and add the Activity Indicator to splashView
        self.loadingView_BannerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingView_BannerView.alpha                    = 1;
        self.loadingView_BannerView.hidesWhenStopped         = YES;
        // add the components to the view
        [view addSubview:self.loadingView_BannerView];
        
        [self.loadingView_BannerView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(view.mas_centerX).with.offset(0);
            make.centerY.equalTo(view.mas_centerY).with.offset(0);
            
        }];
        
    }
    
    [self.loadingView_BannerView startAnimating];
    
    return self.loadingView_BannerView;
}

- (UIActivityIndicatorView *)addLoadingInNewItem:(UIView *)view {
    
    if (!isValid(self.loadingView_NewItem) ) {
        
        // Create and add the Activity Indicator to splashView
        self.loadingView_NewItem = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadingView_NewItem.alpha                    = 1;
        self.loadingView_NewItem.hidesWhenStopped         = YES;
        // add the components to the view
        [view addSubview:self.loadingView_NewItem];
        
        [self.loadingView_NewItem mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(view.mas_centerX).with.offset(0);
            make.centerY.equalTo(view.mas_centerY).with.offset(0);
            
        }];
        
    }
    
    [self.loadingView_NewItem startAnimating];
    
    return self.loadingView_NewItem;
}

#pragma mark XLsn0wLoopDelegate

- (void)loopView:(XLsn0wLoop *)loopView clickImageAtIndex:(NSInteger)index {
    
}

@end
