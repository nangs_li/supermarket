
#import "LeftMenuVC.h"
#import "NSError+RENetworking.h"
#import "ProductDetailController.h"
#import "REAPIClient.h"
#import "REAPIManager.h"
#import "ServerAPI.h"
#import <objc/message.h>
#import "DeliveryAddress.h"
#import "TimeslotData.h"
#import "FacebookLogin.h"
#import "FBSDKLoginManager.h"
#import "OrderHistory.h"
#import "ZDollarModel.h"
#import "RightMenuVC.h"
#import "PromotionModel.h"
#import <OneSignal/OneSignal.h>

@implementation ServerAPI

NSUserDefaults *userDefaults;
UIViewController *vController;
REAPIClient *reAPIClient;

//----------------------Private method-----------------------------------

- (void)showErrorMessage:(NSError *)error target:(id)target {

  if ([target isKindOfClass:[UIViewController class]]) {

    UIViewController *controller = (UIViewController *)target;
    [controller.view makeToast:NSLocalizedString(@"error_connect_fail", nil)];

  } else {

    NSLog(@"Network connect fail: %@", error);
  }
}

- (NSString *)encryptData:(NSString *)jsonStr {

  int strlen                    = (int)[jsonStr length];
  NSMutableArray *encryptedStrs = [[NSMutableArray alloc] init];
  int curInx                    = 0;
  BOOL haveError = NO;
    
  while (curInx *ENCODESTR_LIMIT < strlen) {
      
    int startindex      = ENCODESTR_LIMIT * curInx;
    NSRange needleRange = NSMakeRange(startindex, strlen - startindex < ENCODESTR_LIMIT ? strlen - startindex : ENCODESTR_LIMIT);
    NSString *substr    = [jsonStr substringWithRange:needleRange];
    NSString *encryptString = [RSA encryptString:substr publicKey:[self getEncryptKey]];
    [encryptedStrs addObject:(encryptString) ? encryptString : @" "];
    curInx++;
      
      if (encryptString == nil) {
          
          haveError = YES;
          [userDefaults removeObjectForKey:@"sessionkey"];
           DDLogDebug(@"encryptDatahaveError");
          
      }
      
  }
   
    return (haveError) ? @"" : [encryptedStrs componentsJoinedByString:@"_"] ;
}

- (NSString *)getRequestParamsWithclassName:(NSString *)className methodName:(NSString *)methodName {

  return [self getRequestParamsWithClassName:className methodName:methodName data:[[NSDictionary alloc] init]];
}

- (NSString *)getRequestParamsWithClassName:(NSString *)className methodName:(NSString *)methodName data:(NSDictionary *)data {

  NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:className, @"classname", methodName, @"methodname", [[Common shared] getCurrentLanguage], @"language_code", [self getSessionKey], @"session_key", data, @"args", nil];
  NSError *error     = nil;
  // convert object to data
  NSData *jsonData  = [NSJSONSerialization dataWithJSONObject:info options:NSJSONWritingPrettyPrinted error:&error];
  NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

  if (IS_DEBUG) {
    DDLogDebug(@"Request Params: %@", jsonStr);
  }

    return ([[self encryptData:jsonStr] isEqualToString:@""]) ? [self getRequestParamsWithClassName:className methodName:methodName data:data] : [self encryptData:jsonStr];
}

- (NSString *)getEncryptKey {

  return [userDefaults stringForKey:@"encryptkey"];
}

- (NSString *)getSessionKey {

  NSString *key = [userDefaults stringForKey:@"sessionkey"];

  return key == nil ? @"" : key;
}

- (void)setSessionKey:(NSString *)key {

  [userDefaults setObject:key forKey:@"sessionkey"];
  [userDefaults synchronize];
}

- (void)checkResponse:(NSDictionary *)response error:(NSError *)error block:(nullable REAPICommonBlock)block class:(Class)class specialyCase:(BOOL)specialyCase  checkResponseBlock:(nullable REAPICommonBlock)checkResponseBlock {
    
    NSArray *statusCodesArray;
    
    if (IS_DEBUG) {
        
        DDLogAPI(@"server return string is \n%@", [response jsonString]);
    }
    
    if (block == nil) {
        
        statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_DONOTHING], nil];
        
    } else if (error) {
        
        block(nil, error);
        
        statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_RETRY], nil];
        
    } else {
        
        if (response == nil) {
            
            statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_ERROR], [NSNumber numberWithInteger:ACCESS_DENIED], nil];
            
        } else {
            
            NSDictionary *dictionary = (NSDictionary *)response;
            
            if ([(NSNumber *)[dictionary objectForKey:@"status"] boolValue] == TRUE) {
                
                statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_OK], dictionary, nil];
                
            } else {
                
                NSArray *errorArray;
                
                if ([dictionary objectForKey:@"error_code"] != nil) {
                    
                    errorArray = [[NSArray alloc] initWithObjects:dictionary, nil];
                    
                } else {
                    
                    errorArray = [[dictionary objectForKey:@"field_errors"] allObjects];
                }
                
                int errorCode = [(NSNumber *)[[errorArray objectAtIndex:0] objectForKey:@"error_code"] intValue];
                
                if (errorCode == UPDATE_SESSION_KEY) {  // should testing
                    
                    [self setSessionKey:[[errorArray objectAtIndex:0] objectForKey:@"data"]];
                    
                    statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_RETRY], nil];
                    
                } else {
                    
                    statusCodesArray = [NSArray arrayWithObjects:[NSNumber numberWithInt:STATUS_ERROR], [NSNumber numberWithInt:errorCode], nil];
                }
            }
        }
        
    }
    
    if ([(NSNumber *)statusCodesArray[0] intValue] == STATUS_RETRY) {
        
        if (error && error.code != kStatusCodeTimeOutError) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
            
            checkResponseBlock(statusCodesArray , nil );
            
        });
            
        } else {
            
             checkResponseBlock(statusCodesArray , nil );
            
        }
        
    } else if ([(NSNumber *)statusCodesArray[0] intValue] != STATUS_DONOTHING) {

        if (specialyCase) {
            
            checkResponseBlock(statusCodesArray , nil );
            
        } else {
            
            NSError *errorCode;
            REObject *deliveryAddress = [[class alloc] initWithDictionary:response error:&errorCode];
            deliveryAddress = [self addStatusCodes:statusCodesArray object:deliveryAddress];
            deliveryAddress.jsonDict = response;
            block(deliveryAddress, nil);
        
    }
        
    }
    
}

- (NSMutableDictionary *)emptyNSDictionary {

  return [[NSMutableDictionary alloc] init];
}

- (NSDictionary *)convertDataToDictionary:(NSData *)data {
  NSError *error = nil;
  id jsonObject  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];

  if (jsonObject == nil && error != nil) {

    return nil;
  }

  return (NSDictionary *)jsonObject;
}

- (NSString *)getStringData:(NSData *)response {
  NSDictionary *dictinary = [self convertDataToDictionary:response];

  return (NSString *)[dictinary objectForKey:@"data"];
}

- (NSNumber *)getNumberData:(NSData *)response {
  NSDictionary *dictinary = [self convertDataToDictionary:response];

  return (NSNumber *)[dictinary objectForKey:@"data"];
}

- (NSDictionary *)getDictionaryData:(NSData *)response {
  NSDictionary *dictinary = [self convertDataToDictionary:response];

  return (NSDictionary *)[dictinary objectForKey:@"data"];
}

- (NSArray *)getArrayData:(NSData *)response {

  NSDictionary *dictinary = [self convertDataToDictionary:response];

  return [[dictinary objectForKey:@"data"] allObjects];
}
//----------------------End private method-----------------------------------
//----------------------Public method-----------------------------------

+ (instancetype)shared {

  static id instance_ = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance_ = [[self alloc] init];

  });

  userDefaults = [NSUserDefaults standardUserDefaults];
  reAPIClient  = [REAPIClient sharedClient];

  return instance_;
}

- (void)initEncryptKeyCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json",nil];
    [manager GET:GETENCRYPTKEY_URL parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSString *encryptkey = [[NSString alloc] initWithData:responseObject
                                                 encoding:NSUTF8StringEncoding];
        
        [userDefaults setObject:encryptkey forKey:@"encryptkey"];
        [userDefaults synchronize];
        
         completionBlock(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        completionBlock(nil, error);
        
          dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
              
              [self initEncryptKeyCompletionBlock:completionBlock];
              
          });
        
    }];
    
}

- (void)addCurrentUserAddressFromConsignee_title:(int)consignee_title  consignee:(NSString *)consignee mobile:(NSString *)mobile contact_no:(NSString *)contact_no country:(NSString *)country region:(NSString *)region district:(NSString *)district area:(NSString *)area address:(NSString *)address is_no_lift:(BOOL)is_no_lift CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt:consignee_title], @"consignee_title",
                          consignee, @"consignee",
                          mobile, @"mobile",
                          contact_no, @"contact_no",
                          country, @"country", region, @"region", district,
                          @"district", area, @"area", address,
                          @"address", (is_no_lift) ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO], @"is_no_lift", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserAddress" methodName:@"addCurrentUserAddress" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {
                   
                   __weak typeof(self) weakSelf = self;
                   
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf addCurrentUserAddressFromConsignee_title:consignee_title consignee:consignee mobile:mobile contact_no:contact_no country:country region:region district:district area:area address:address is_no_lift:is_no_lift CompletionBlock:completionBlock];
                       
                  }];
               
           }
             progress:nil];
}

- (void)updateCurrentUserAddressFromAddressId:(int)addressId consignee_title:(int)consignee_title consignee:(NSString *)consignee mobile:(NSString *)mobile contact_no:(NSString *)contact_no country:(NSString *)country region:(NSString *)region district:(NSString *)district area:(NSString *)area address:(NSString *)address is_no_lift:(BOOL)is_no_lift CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    
    NSDictionary *json = [NSDictionary
                                dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:addressId], @"address_id",
                                                             [NSNumber numberWithInt:consignee_title], @"consignee_title",
                                                             consignee, @"consignee",
                                                             mobile, @"mobile",
                                                             contact_no, @"contact_no",
                                                             country, @"country", region, @"region", district,
                                                             @"district", area, @"area", address,
                                                             @"address", (is_no_lift) ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO], @"is_no_lift", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserAddress" methodName:@"updateCurrentUserAddress" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
                  [weakSelf checkResponse:response error:error block:completionBlock class:[DeliveryAddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {

                       [weakSelf updateCurrentUserAddressFromAddressId:addressId consignee_title:consignee_title consignee:consignee mobile:mobile contact_no:contact_no country:country region:region district:district area:area address:address is_no_lift:is_no_lift CompletionBlock:completionBlock];
                       
                  }];

           }
             progress:nil];
}

- (void)deleteCurrentUserAddressFromAddressId:(int)addressId completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:addressId], @"address_id", nil];
                          
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserAddress" methodName:@"deleteCurrentUserAddress" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {
               
                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[DeliveryAddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf deleteCurrentUserAddressFromAddressId:addressId completionBlock:completionBlock];
                       
               }];
               
               }
     
             progress:nil];
}

- (void)setCurrentUserAddressDefaultFromAddressId:(int)addressId completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:addressId], @"address_id", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserAddress" methodName:@"setCurrentUserAddressDefault" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[DeliveryAddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf setCurrentUserAddressDefaultFromAddressId:addressId completionBlock:completionBlock];
                       
               }];
               
           }
             progress:nil];
}

- (void)getTimeslotFromCode:(int)code completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:code], @"code", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Checkout" methodName:@"getTimeslot" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[TimeslotData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getTimeslotFromCode:code completionBlock:completionBlock];
                       
               }];
    
           }
             progress:nil];
}

- (void)getBraintreeClientTokenCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName]  methodName:@"getClientToken" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ClientToken class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getBraintreeClientTokenCompletionBlock:completionBlock];
                       
               }];
               
           }
             progress:nil];
}

- (void)getCustomerCardCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName]  methodName:@"getCustomerCard" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getCustomerCardCompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)deleteCustomerCardFromCardId:(NSString *)cardId CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:cardId, @"card_id", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName] methodName:@"deleteCustomerCard" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[DeliveryAddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf deleteCustomerCardFromCardId:cardId CompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)verifyNewCreditCardFromPayment_method_nonce:(NSString *)payment_method_nonce cardholder_name:(NSString *)cardholder_name CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:payment_method_nonce, @"payment_method_nonce" ,cardholder_name, @"cardholder_name", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName]  methodName:@"verifyNewCreditCard" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id statusCodes, NSError *error) {

                       [weakSelf verifyNewCreditCardFromPayment_method_nonce:payment_method_nonce cardholder_name:cardholder_name CompletionBlock:completionBlock];
                   
               }];


           }
             progress:nil];
}

- (void)verifyExistCreditCardFromCard_id:(NSString *)card_id cvv:(NSString *)cvv CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:card_id, @"card_id" ,cvv, @"cvv", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName]  methodName:@"verifyExistCreditCard" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf verifyExistCreditCardFromCard_id:card_id cvv:cvv CompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)createOrderWithPaymentMethodFromPaymentCode:(NSString *)paymentCode paymentMethodNonce:(NSString *)paymentMethodNonce completionBlock:(nullable REAPICommonBlock)completionBlock {

    NSMutableDictionary *firstjson = [self getCashOrderJson];    
    [firstjson removeObjectForKey:@"payment_code"];
    [firstjson setObject:paymentCode forKey:@"payment_code"];
    [firstjson setObject:paymentMethodNonce forKey:@"payment_method_nonce"];
    NSDictionary *json = [NSDictionary dictionaryWithDictionary:firstjson];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Payment\\BraintreeIntegration" methodName:@"createOrderWithPaymentMethod" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PaymentSuccess class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf createOrderWithPaymentMethodFromPaymentCode:paymentCode paymentMethodNonce:paymentMethodNonce completionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       [self paymentSuccessCompletionBlock:completionBlock response:response statusCodes:statusCodes];
                       
                   }
                   
               }];

               
           }
             progress:nil];
}

- (void)createOrderWithPaymentByPaymentMethodCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithDictionary:[self getOldCardOrderJson]];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName] methodName:@"createOrderWithPaymentByPaymentMethod" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PaymentSuccess class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf createOrderWithPaymentByPaymentMethodCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       [self paymentSuccessCompletionBlock:completionBlock response:response statusCodes:statusCodes];
                       
                   }
                   
               }];


           }
             progress:nil];
}

- (void)createOrderWithPaymentWithNewCustomerCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithDictionary:[self getNewCardOrderJson]];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:[self getBrainTreeMethodName] methodName:@"createOrderWithPaymentWithNewCustomer" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PaymentSuccess class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf createOrderWithPaymentWithNewCustomerCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       [self paymentSuccessCompletionBlock:completionBlock response:response statusCodes:statusCodes];
                   }
                   
               }];

           }
             progress:nil];
}

- (void)createOrderWithCoDPaymentCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
   NSDictionary *json = [NSDictionary dictionaryWithDictionary:[self getCashOrderJson]];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Payment\\CoDPayment" methodName:@"start" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PaymentSuccess class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf createOrderWithCoDPaymentCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       [self paymentSuccessCompletionBlock:completionBlock response:response statusCodes:statusCodes];
                   
                   }
                   
               }];

           }
             progress:nil];
}

- (void)paymentSuccessCompletionBlock:(nullable REAPICommonBlock)completionBlock response:(id)response statusCodes:(id)statusCodes {
    
    NSError *error;
    PaymentSuccess *deliveryAddress = [[PaymentSuccess alloc] initWithDictionary:response error:&error];
    deliveryAddress = (PaymentSuccess *)[self addStatusCodes:statusCodes object:deliveryAddress];
    [[Common shared] sendGAEcommerceForPaymentSuccess:deliveryAddress];
    deliveryAddress.jsonDict = response;
    completionBlock(deliveryAddress, nil);
    
}

- (NSMutableDictionary *)getCashOrderJson {
    
    
    NSNumber *number = [[Common shared] valueForKey:@"use_zdollar"];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    [dict setObject:[NSNumber numberWithInteger:[Common shared].selectedDeliveryAddress.id] forKey:@"user_address_id"];
    [dict setObject:[NSNumber numberWithInteger:[Common shared].selectedTimeslot.date] forKey:@"delivery_date"];
    [dict setObject:[NSNumber numberWithInteger:[Common shared].selectedTimeslot.time_start] forKey:@"delivery_time_start"];
    [dict setObject:[NSNumber numberWithInteger:[Common shared].selectedTimeslot.time_end] forKey:@"delivery_time_end"];
    [dict setObject:@"DELIVERY" forKey:@"shipping_code"];
    [dict setObject:@"TNG" forKey:@"payment_code"];
    [dict setObject:[NSNumber numberWithBool:![Common shared].specialyRequest.isChoiceString3] forKey:@"is_agree_reused_box"];
    [dict setObject:[NSNumber numberWithBool:[Common shared].specialyRequest.isChoiceString1] forKey:@"is_pass_to_guard"];
    [dict setObject:[NSNumber numberWithBool:[Common shared].specialyRequest.isChoiceString2] forKey:@"is_collect_box_glass"];
    [dict setObject:[NSNumber numberWithBool:[number boolValue]] forKey:@"use_zdollar"];
    [dict setObject:[NSNumber numberWithBool:FALSE] forKey:@"is_remove_user_promo_code"];
    NSString *specialyRequestUserInput = [Common shared].specialyRequest.specialyRequestUserInput;
    [dict setObject:isValid(specialyRequestUserInput) ? specialyRequestUserInput : @""  forKey:@"remark"];
  
    
  return dict;
}

- (NSMutableDictionary *)getOldCardOrderJson {
    
    NSMutableDictionary *json = [self getCashOrderJson];
    
    [json setObject:[Common shared].selectedCard.cardholderName forKey:@"cardholder_name"];
    [Common shared].selectedCard.id =  ([Common shared].selectedCard.id == nil) ? @"": [Common shared].selectedCard.id;
    [json setObject:[Common shared].selectedCard.id forKey:@"card_id"];
    [json setObject:[NSNumber numberWithBool:[Common shared].is_store_card] forKey:@"is_store_card"];
    [json setObject:[Common shared].selectedCard.cvc forKey:@"cvv"];

    return [json mutableCopy];
}

- (NSMutableDictionary *)getNewCardOrderJson {
    
        NSMutableDictionary *json = [self getCashOrderJson];
    
        [json setObject:[Common shared].selectedCardnew.cardholderName forKey:@"cardholder_name"];
    [Common shared].selectedCardnew.id =  ([Common shared].selectedCardnew.id == nil) ? @"": [Common shared].selectedCardnew.id;
        [json setObject:[Common shared].selectedCardnew.id forKey:@"card_id"];
        [json setObject:[NSNumber numberWithBool:[Common shared].is_store_card] forKey:@"is_store_card"];
        
        return [json mutableCopy];
}

- (NSString *)getBrainTreeMethodName {
    
    return [NSString stringWithFormat:@"Payment\\Braintree"];
}

- (void)listCurrentUserAddressCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserAddress" methodName:@"listCurrentUserAddress" data:nil]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf listCurrentUserAddressCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       DeliveryAddressData *deliveryAddress = [[DeliveryAddressData alloc] initWithDictionary:response error:&error];
                       deliveryAddress = (DeliveryAddressData *)[self addStatusCodes:statusCodes object:deliveryAddress];
                       
                          if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                              
                              [Common shared].deliveryAddressData = deliveryAddress;
                              
                              for (DeliveryAddress *deliveryAddress in [Common shared].deliveryAddressData.data) {
                                  
                                  if (deliveryAddress.is_default) {
                                      
                                      [Common shared].selectedDeliveryAddress = deliveryAddress;
                                  }
                                  
                              }
                          }
                       
                       if ([deliveryAddress getErrorCode] == NO_DATA) {
                           
                           [Common shared].deliveryAddressData = [[DeliveryAddressData alloc] init];
                           
                       }
      
                       completionBlock(deliveryAddress, nil);
                   }
                   
               }];

           }
             progress:nil];
}

- (void)listRegionFromCountry_id:(int)country_id completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:country_id], @"country_id", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Address" methodName:@"listRegion" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[AddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listRegionFromCountry_id:country_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)listDistrictFromRegion_id:(int)region_id completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:region_id], @"region_id", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Address" methodName:@"listDistrict" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[AddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listDistrictFromRegion_id:region_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)listAreaFromDistrict_id:(int)district_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:district_id], @"district_id", nil];

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"Address" methodName:@"listArea" data:json]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[AddressData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf listAreaFromDistrict_id:district_id completionBlock:completionBlock];

             }];

         }
           progress:nil];
}
// End Address API
//--------------------------------------------
// User API

- (void)isLoginCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"isLogin" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf isLoginCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       CardID *cardID = [[CardID alloc] initWithDictionary:response error:&error];
                       cardID = (CardID *)[self addStatusCodes:statusCodes object:cardID];
                       
                        if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                       
                       [Common shared].isLogin = [cardID.data isEqualToString:@"1"];
                           
                       }
                       
                       [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadLeftMenu object:nil];
                       
                       completionBlock(cardID, nil);
                       
                   }
                   
               }];

           }
             progress:nil];
}

- (void)loginWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password completionBlock:(nullable REAPICommonBlock)completionBlock {

    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:password, @"password" ,email, @"email", nil];
    
  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"login" data:json]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                 
             if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

               [weakSelf loginWithEmail:email password:password completionBlock:completionBlock];

             } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                 
                 if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                
                     [Common shared].isLogin = YES;
                     
                 }
                 
               UserAccount *userAccount = [[UserAccount alloc] initWithDictionary:response error:&error];
               userAccount = (UserAccount *)[self addStatusCodes:statusCodes object:userAccount];
               ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) ? [self saveEmailIntoDBWithEmail:email password:password] : nil;
                 
               completionBlock(userAccount, nil);
                 
             }
                 
             }];

         }
           progress:nil];
}

- (void)userRegisterWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password completionBlock:(nullable REAPICommonBlock)completionBlock {

    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:password, @"password" ,email, @"email", nil];
    
  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"register" data:json]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[UserAccount class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                 
             if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

               [weakSelf userRegisterWithEmail:email password:password completionBlock:completionBlock];
                 
             } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {

                 if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                     
                     [Common shared].isLogin = YES;
                     
                 }
                 
               UserAccount *userAccount = [[UserAccount alloc] initWithDictionary:response error:&error];
               userAccount = (UserAccount *)[self addStatusCodes:statusCodes object:userAccount];
               ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) ? [self saveEmailIntoDBWithEmail:email password:password] : nil;
                 
               completionBlock(userAccount, nil);
             }
                 
             }];
             
         }
           progress:nil];
}

- (void)connectFbAccountWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password accessToken:(nonnull NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:password, @"password" ,email, @"email", accessToken, @"accessToken", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"connectFbAccount" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf connectFbAccountWithEmail:email password:password  accessToken:accessToken completionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           [Common shared].isLogin = YES;
                           
                       }
                       
                       UserAccount *userAccount = [[UserAccount alloc] initWithDictionary:response error:&error];
                       userAccount = (UserAccount *)[self addStatusCodes:statusCodes object:userAccount];
                       ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) ? [self saveEmailIntoDBWithEmail:email password:password] : nil;
                       
                       completionBlock(userAccount, nil);
                   }
                   
               }];

           }
             progress:nil];
}

- (void)saveEmailIntoDBWithEmail:(nonnull NSString *)email password:(nonnull NSString *)password {
    
    LoginData *loginData = [[LoginData alloc] init];
    loginData.id          = @"123";
    loginData.email       = email;
    loginData.password    = password;
    
    if (![AppDelegate getAppDelegate].rightMenuVC.hideFooterView) {
        
        [[AppDelegate getAppDelegate].rightMenuVC pressExpandBtn];
        
    }
    
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        
        [self createUserDeviceFromPlayerId:userId completionBlock:^(id response, NSError *error) {
            
        }];
        
    }];
    
    [[REDatabase sharedInstance]  createOrUpdateLoginData:loginData];
    
    [self listCurrentUserAddressCompletionBlock:^(id response, NSError *error) {
        
    }];
    
}

- (void)changePasswordWithOldPassword:(nonnull NSString *)oldPassword newPassword:(nonnull NSString *)newPassword completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:oldPassword, @"old_password", newPassword, @"new_password", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"updateCurrentUserPassword" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[UserAccount class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf changePasswordWithOldPassword:oldPassword newPassword:newPassword completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)forgetPasswordWithEmail:(nonnull NSString *)email completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:email, @"email", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"forgetPassword" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[UserAccount class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf forgetPasswordWithEmail:email completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)connectFbAccountWithLoginedFromAccessToken:(NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:accessToken, @"accessToken", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"connectFbAccountWithLogined" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[FacebookLogin class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf connectFbAccountWithLoginedFromAccessToken:accessToken completionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           [Common shared].isLogin = YES;
                           
                       }
                       
                       FacebookLogin *userAccount = [[FacebookLogin alloc] initWithDictionary:response error:&error];
                       userAccount = (FacebookLogin *)[self addStatusCodes:statusCodes object:userAccount];
                       [[Common shared] setValue:accessToken forKey:@"accessToken"];
                       
                       completionBlock(userAccount, nil);
                       
                   }
                   
               }];
               
           }
             progress:nil];
}

- (void)fbLoginWithAccessToken:(NSString *)accessToken completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:accessToken, @"accessToken", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"fbLogin" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf fbLoginWithAccessToken:accessToken completionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           [Common shared].isLogin = YES;
                           
                       }
                       
                       FacebookLogin *userAccount = [[FacebookLogin alloc] initWithDictionary:response error:&error];
                       userAccount = (FacebookLogin *)[self addStatusCodes:statusCodes object:userAccount];
                       [[Common shared] setValue:accessToken forKey:@"accessToken"];
                       
                       completionBlock(userAccount, nil);
                       
                   }
                   
               }];
               
           }
             progress:nil];
}

- (void)logoutCompletionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"logout" data:[self emptyNSDictionary]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                 
             if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

               [weakSelf logoutCompletionBlock:completionBlock];

             } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {

               UserAccount *userAccount = [[UserAccount alloc] initWithDictionary:response error:&error];
               userAccount = (UserAccount *)[self addStatusCodes:statusCodes object:userAccount];
                 
                 if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                     
                     [Common shared].isLogin = NO;
                     [[FBSDKLoginManager new] logOut];
                     
                 }
                 
               completionBlock(userAccount, nil);
             }
                 
             }];

         }
           progress:nil];
}

- (void)getCurrentUserCompletionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"getCurrentUser" data:[self emptyNSDictionary]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[UserAccount class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                 
             if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

               [weakSelf getCurrentUserCompletionBlock:completionBlock];

             } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {

               UserAccount *userAccount = [[UserAccount alloc] initWithDictionary:response error:&error];
               userAccount = (UserAccount *)[self addStatusCodes:statusCodes object:userAccount];
                 
                 if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                     
                     [Common shared].userAccont = userAccount;
                     [[ServerAPI shared] saveEmailIntoDBWithEmail:[Common shared].userAccont.data.email password:@""];
                     
                 }
                 
               completionBlock(userAccount, nil);
             }
                 
             }];

         }
           progress:nil];
}

- (void)updateCurrentUserWithInput:(InputUpdateCurrentUser *)input completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"User" methodName:@"updateCurrentUser" data:[self getJsonFromInput:input]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[UserAccount class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf updateCurrentUserWithInput:input completionBlock:completionBlock];
                 
             }];

         }
           progress:nil];
}

- (void)getCategoryTreeCompletion:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"Menu" methodName:@"getMenu" data:[self emptyNSDictionary]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductCategoryModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf getCategoryTreeCompletion:completionBlock];

             }];

         }
           progress:nil];
}

- (void)getCategoryChildFromParent_id:(NSInteger)parent_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:parent_id], @"parent_id", nil];
  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"ProductCategory" methodName:@"listCategoryChild" data:json]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductCategoryModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf getCategoryChildFromParent_id:parent_id completionBlock:completionBlock];

             }];

         }
           progress:nil];
}

- (void)getCategoryFromCategory_Id:(int)category_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:category_id], @"category_id", nil];

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"ProductCategory" methodName:@"getCategory" data:json]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[GetProductCategory class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf getCategoryFromCategory_Id:category_id completionBlock:completionBlock];

             }];

         }
           progress:nil];
}

- (void)getBannerListCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Banner" methodName:@"getBannerList" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[BannerModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getBannerListCompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)listProductGroupByCategoryFromParent_category_id:(NSInteger)parent_category_id completionBlock:(nullable REAPICommonBlock)completionBlock {

    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:parent_category_id], @"parent_category_id", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"listProductGroupByCategory" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[GetProductCategoryArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listProductGroupByCategoryFromParent_category_id:parent_category_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)getProductByUrlKey:(NSString *)urlKey completionBlock:(nullable REAPICommonBlock)completionBlock {
    
      NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:urlKey, @"url_key", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"getProductByUrlKey" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getProductByUrlKey:urlKey completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)getProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"getProductById" data:[self getJsonFromProductID:product_id]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf getProductFromProduct_Id:product_id completionBlock:completionBlock];

             }];

         }
           progress:nil];
}

- (void)listFeaturedProductFromLimit:(int)limit completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"listFeaturedProduct" data:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:limit], @"limit", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listFeaturedProductFromLimit:limit completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)listFeaturedCategoryProductListFromLimit:(int)limit completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"listFeaturedCategoryProductList" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[FeatureProductListArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listFeaturedCategoryProductListFromLimit:limit completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)listProduct:(InputListProduct *)input completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"listProduct" data:[self getJsonFromInput:input]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductListArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf listProduct:input completionBlock:completionBlock];

             }];

         }
           progress:nil];
}

- (void)listProductWithAllBrandTagFromInput:(InputListProduct *)input completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"listProductWithAllBrandTag" data:[self getJsonFromInput:input]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductListData class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf listProductWithAllBrandTagFromInput:input completionBlock:completionBlock];
                       
               }];
               
           }
             progress:nil];
}

- (void)getPromotionInfoFromSn:(NSString *)sn completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"Promotion" methodName:@"getPromotionInfo" data:[NSDictionary dictionaryWithObjectsAndKeys:sn, @"sn", nil]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[PromotionModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf getPromotionInfoFromSn:sn completionBlock:completionBlock];
                 
             }];

         }
           progress:nil];
}

- (void)getProductCountFromInput:(InputListProduct *)input completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Product" methodName:@"getProductCount" data:[self getJsonFromInput:input]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getProductCountFromInput:input completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)setPromotionCode:(nullable NSString *)code completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:code, @"code", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Checkout" methodName:@"setPromotionCode" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf setPromotionCode:code completionBlock:completionBlock];
                       
               }];
               
           }
             progress:nil];
    
}

- (void)getPromotionCodeCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Checkout" methodName:@"getPromotionCode" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
                   
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PromotionCode class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getPromotionCodeCompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
    
}

- (REObject *)addStatusCodes:(NSArray *)statusCodes object:(REObject *)object {
    
    object = (object == nil) ? [[[ProductDetailArray class] alloc] init] : object;
    object.statusCodes = statusCodes;
    
    return object;
}

- (NSMutableDictionary *)getJsonFromInput:(NSObject *)inputObject {

  NSMutableDictionary *json = [NSMutableDictionary dictionary];

  if (inputObject == nil) {

    json = [self emptyNSDictionary];

  } else if ([inputObject isKindOfClass:[InputListProduct class]]) {

    InputListProduct *input = (InputListProduct *)inputObject;
      
      if (input.type == press_blue_tag) {
          
           (input.sn) ? [json setValue:input.sn forKey:@"promotion_sn"] : nil;
           (input.tags) ? [json setValue:input.tags forKey:@"tags"] : nil;
           (input.brand_ids) ? [json setValue:input.brand_ids forKey:@"brand_ids"] : nil;
           (input.order_by) ? [json setValue:input.order_by forKey:@"order_by"] : nil;
          
      } else if (input.type == mainPage_category || input.type == parent_category_id || input.type == search_brand_ids || input.type == search_tags) {
          
          (input.tags) ? [json setValue:input.tags forKey:@"tags"] : nil;
          (input.brand_ids) ? [json setValue:input.brand_ids forKey:@"brand_ids"] : nil;
          (input.order_by) ? [json setValue:input.order_by forKey:@"order_by"] : nil;
          
          [json setValue:[NSNumber numberWithInt:input.parent_category_id] forKey:@"parent_category_id"];
          [json setValue:input.category_ids forKey:@"category_ids"];
          
      }
      
      [json setValue:[NSNumber numberWithInt:input.result_start] forKey:@"result_start"];
      [json setValue:[NSNumber numberWithInt:input.result_limit] forKey:@"result_limit"];
      
  } else if ([inputObject isKindOfClass:[InputUpdateCurrentUser class]]) {

    InputUpdateCurrentUser *input = (InputUpdateCurrentUser *)inputObject;
   
    [json setValue:input.email forKey:@"email"];
    [json setValue:input.first_name forKey:@"first_name"];
    [json setValue:input.last_name forKey:@"last_name"];
    [json setValue:input.company forKey:@"company"];
    [json setValue:input.contact_no forKey:@"contact_no"];
    [json setValue:input.mobile forKey:@"mobile"];
    [json setValue:input.title forKey:@"title"];

  }

  return json;
}

- (NSDictionary *)getJsonFromProductID:(int)product_id {

  return [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:product_id], @"product_id", nil];
    
}
// End Product API
//--------------------------------------------------
// UserList API

- (void)userListListProductCompletionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"listProduct" data:[self emptyNSDictionary]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                 
             if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

               [weakSelf userListListProductCompletionBlock:completionBlock];

             } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {

               ProductDetailArray *productDetailArray = [[ProductDetailArray alloc] initWithDictionary:response error:&error];
               productDetailArray = (ProductDetailArray *)[self addStatusCodes:statusCodes object:productDetailArray];
                 
                 if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                     
                     for (Products *product in productDetailArray.data) {
                         
                         product.qty         = product.cart_qty;
                         product.freebie_qty = product.cart_freebie_qty;
                         [product setUpProductType];
                         [[Common shared] setProduct:product];
                         
                     }
                    
                     [productDetailArray hideAllPressAction];
                     [Common shared].favouriteList = productDetailArray;
                 }
                 
                 if ([productDetailArray getErrorCode] == NO_DATA) {
                     
                     [Common shared].favouriteList = productDetailArray;
                     
                 }
                 
                 
               completionBlock(productDetailArray, nil);
             }
                 
             }];

         }
           progress:nil];
}

- (void)userListListRecentlyPurchasedProductCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"listRecentlyPurchasedProduct" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf userListListRecentlyPurchasedProductCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       ProductDetailArray *productDetailArray = [[ProductDetailArray alloc] initWithDictionary:response error:&error];
                       productDetailArray = (ProductDetailArray *)[self addStatusCodes:statusCodes object:productDetailArray];
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           for (Products *product in productDetailArray.data) {
                               
                               product.qty         = product.cart_qty;
                               product.freebie_qty = product.cart_freebie_qty;
                               [product setUpProductType];
                               [[Common shared] setProduct:product];
                               [productDetailArray hideAllPressAction];
                               [Common shared].recentPurchaseList = productDetailArray;
                           }
                       }
                       
                       if ([productDetailArray getErrorCode] == NO_DATA) {
                           
                           [Common shared].recentPurchaseList = productDetailArray;
                           
                       }
                       
                       completionBlock(productDetailArray, nil);
                   }
                   
               }];

           }
             progress:nil];
}

- (void)userListAddProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"addProduct" data:[self getJsonFromProductID:product_id]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductAddWishList class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf userListAddProductFromProduct_Id:product_id completionBlock:completionBlock];
                 
             }];

         }
           progress:nil];
}

- (void)userListDeleteProductFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock {

  [reAPIClient post:BASE_URL
          parameter:@{
            @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"deleteProduct" data:[self getJsonFromProductID:product_id]]
          }
          requestID:@"abc"
         completion:^(id response, NSError *error) {

             __weak typeof(self) weakSelf = self;
             
             [weakSelf checkResponse:response error:error block:completionBlock class:[ProductAddWishList class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                 
               [weakSelf userListDeleteProductFromProduct_Id:product_id completionBlock:completionBlock];
                 
             }];

         }
           progress:nil];
}

- (void)cartAddProductFromProductArraysReturnCart:(BOOL)returnCart onlyGetShoppingCart:(BOOL)onlyGetShoppingCart completionBlock:(nullable REAPICommonBlock)completionBlock {

  NSMutableArray *productIdArray  = [[NSMutableArray alloc] init];
  NSMutableArray *productQtyArray = [[NSMutableArray alloc] init];

  DDLogDebug(@"cartAddProductFromProductArrays");

  for (NSDictionary *dict in [Common shared].productChangeArray) {

    [dict enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {

      if ([key isEqualToString:@"productChangeQty"]) {

        [productQtyArray addObject:object];

      } else {

        [productIdArray addObject:object];
      }

    }];
  }

  if (productIdArray.count > 0 || onlyGetShoppingCart) {
      
      int addProductQty = 0;
      
      for (NSString *productQty in productQtyArray) {
          
          addProductQty += [productQty intValue];
      }
      
  [Common shared].addProductQtyString = [NSString stringWithFormat:@"%d", addProductQty];
    NSDictionary *json                               = [NSDictionary dictionaryWithObjectsAndKeys:productIdArray, @"product_id", productQtyArray, @"qty", [NSNumber numberWithBool:YES], @"is_adjust", [NSNumber numberWithBool:returnCart], @"is_return_whole_cart", nil];
 
    [reAPIClient post:BASE_URL
            parameter:@{
              @"data" : [self getRequestParamsWithClassName:@"Cart" methodName:@"addProduct" data:json]
            }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

               __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
               if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {

                   [weakSelf cartAddProductFromProductArraysReturnCart:returnCart onlyGetShoppingCart:onlyGetShoppingCart completionBlock:completionBlock];

               } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {

                   [Common shared].productChangeArray = [[NSMutableArray alloc] init];
                   
                   if (returnCart) {
                       
                       AddProductReturnShoppingCartModel *shoppingCartModel = [[AddProductReturnShoppingCartModel alloc] initWithDictionary:response error:&error];
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           for (Products *product in shoppingCartModel.data.cart.products) {
                               
                               product.cart_qty         = product.qty;
                               product.cart_freebie_qty = product.freebie_qty;
                               [product setUpProductType];
                               [[Common shared] setProduct:product];
                               
                           }
                           
                       }
                       
                       shoppingCartModel = (AddProductReturnShoppingCartModel *)[self addStatusCodes:statusCodes object:shoppingCartModel];
                       shoppingCartModel.data.cart.statusCodes = statusCodes;
                       [Common shared].shoppingCartData = shoppingCartModel.data.cart;
                       
                       completionBlock(shoppingCartModel, nil);
                       
                   }
                   
               }
                   
               }];

           }
             progress:nil];
  } else {
      
      completionBlock(nil, nil);
  }
}

- (void)getAutoCompleteWithKeyword:(NSString *)keyword completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json = [NSDictionary dictionaryWithObjectsAndKeys:keyword, @"keyword", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Search" methodName:@"getAutoComplete" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
                   [weakSelf checkResponse:response error:error block:completionBlock class:[AutoCompleteProduct class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                       
                         [weakSelf getAutoCompleteWithKeyword:keyword completionBlock:completionBlock];
                       
                   }];
                   
               
           }
             progress:nil];
}

- (void)listCurrentUserPaidOrderCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Order" methodName:@"listCurrentUserPaidOrder" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[OrderHistory class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf listCurrentUserPaidOrderCompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       OrderHistory *productDetailArray = [[OrderHistory alloc] initWithDictionary:response error:&error];
                       productDetailArray = (OrderHistory *)[self addStatusCodes:statusCodes object:productDetailArray];
                       
                       if ([(NSNumber *)statusCodes[0] intValue] == STATUS_OK) {
                           
                           [Common shared].orderHistory = productDetailArray;
                            [[Common shared].orderHistory setUpNewYearFirstObject];
                       }

                       if ([productDetailArray getErrorCode] == NO_DATA) {
                           
                           [Common shared].orderHistory = [[OrderHistory alloc] init];
                           
                       }
                       
                       completionBlock(productDetailArray, nil);
                   }
                   
               }];

           }
             progress:nil];
}

- (void)getCurrentUserPaidOrderFromOrder_id:(int)order_id completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Order" methodName:@"getCurrentUserPaidOrder" data:[NSDictionary
                                       dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:order_id], @"order_id", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[PaymentSuccess class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getCurrentUserPaidOrderFromOrder_id:order_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)ratingOrderServiceFromOrder_id:(int)order_id rating_friendliness:(NSNumber *)rating_friendliness rating_appearance:(NSNumber *)rating_appearance rating_ontime:(NSNumber *)rating_ontime rating_accuracy:(NSNumber *)rating_accuracy rating_situation:(NSNumber *)rating_situation remark:(NSString *)remark completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Order" methodName:@"ratingOrderService" data:[NSDictionary
                                                                                                                           dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:order_id], @"order_id",rating_friendliness, @"rating_friendliness", rating_appearance, @"rating_appearance",rating_ontime, @"rating_ontime" , rating_accuracy, @"rating_accuracy", rating_situation, @"rating_situation", remark, @"rating_comment", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailArray class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf ratingOrderServiceFromOrder_id:order_id rating_friendliness:rating_friendliness rating_appearance:rating_appearance rating_ontime:rating_ontime rating_accuracy:rating_accuracy rating_situation:rating_situation remark:remark completionBlock:completionBlock];
                       
               }];
               
           }
             progress:nil];
}

- (void)getUserZDollarLogCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"ZDollar" methodName:@"getUserZDollarLog" data:[self emptyNSDictionary]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
                   
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ZDollarModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf getUserZDollarLogCompletionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)setUseZdollar:(BOOL)useZdollar completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    NSDictionary *json  = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:useZdollar], @"is_use_zdollar", nil];
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Checkout" methodName:@"setUseZdollar" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf setUseZdollar:useZdollar completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)sendEmailToUsTitle:(nullable NSString *)title
                      name:(nullable NSString *)name
                     email:(nullable NSString *)email
                    mobile:(nullable NSString *)mobile
                      need:(nullable NSString *)need
                    remark:(nullable NSString *)remark
           CompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    
    NSDictionary *obj = [NSDictionary
                          dictionaryWithObjectsAndKeys:title, @"title",
                          name, @"name",
                          email, @"email",
                          mobile, @"mobile",
                          need, @"need",
                          remark, @"remark", nil];
    NSDictionary *json = [NSDictionary
                          dictionaryWithObjectsAndKeys:
                          @"contactus", @"template",
                          @"聯絡我們", @"title",
                          obj, @"obj", nil];

    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"Email" methodName:@"sendEmailToUs" data:json]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {
 
                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
                   if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                       
                       [weakSelf sendEmailToUsTitle:title name:name email:email mobile:mobile need:need remark:remark CompletionBlock:completionBlock];
                       
                   } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                       
                       ProductDetailArray *deliveryAddress = [[ProductDetailArray alloc] initWithDictionary:response error:&error];
                       deliveryAddress = (ProductDetailArray *)[self addStatusCodes:statusCodes object:deliveryAddress];
                       completionBlock(deliveryAddress, nil);
                   }
                   
               }];

           }
             progress:nil];
}

- (void)createUserDeviceFromPlayerId:(NSString *)playerId completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"PushNotification" methodName:@"createUserDevice" data:[NSDictionary dictionaryWithObjectsAndKeys:playerId, @"player_id", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardID class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf createUserDeviceFromPlayerId:playerId completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)addProductNoticeFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"addProduct" data:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:product_id], @"product_id", [NSNumber numberWithBool:YES], @"is_notice", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf addProductNoticeFromProduct_Id:product_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)cancelProductNoticeFromProduct_Id:(int)product_id completionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"UserList" methodName:@"cancelProductNotice" data:[self getJsonFromProductID:product_id]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {

                   __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[ProductDetailModel class] specialyCase:NO checkResponseBlock:^(id  response, NSError *error) {
                   
                       [weakSelf cancelProductNoticeFromProduct_Id:product_id completionBlock:completionBlock];
                       
               }];

           }
             progress:nil];
}

- (void)getFreeShppingMinOrderAmountCompletionBlock:(nullable REAPICommonBlock)completionBlock {
    
    [reAPIClient post:BASE_URL
            parameter:@{
                        @"data" : [self getRequestParamsWithClassName:@"System" methodName:@"getSetting" data:[NSDictionary dictionaryWithObjectsAndKeys:@"FREE_SHIPPING_MIN_ORDER_AMOUNT", @"code", nil]]
                        }
            requestID:@"abc"
           completion:^(id response, NSError *error) {
               
               __weak typeof(self) weakSelf = self;
               
               [weakSelf checkResponse:response error:error block:completionBlock class:[CardArray class] specialyCase:YES checkResponseBlock:^(id statusCodes, NSError *error) {
                   
               if ([(NSNumber *)statusCodes[0] intValue] == STATUS_RETRY) {
                   
                   [weakSelf getFreeShppingMinOrderAmountCompletionBlock:completionBlock];
                   
               } else if ([(NSNumber *)statusCodes[0] intValue] != STATUS_DONOTHING) {
                   
                   CardID *cardId = [[CardID alloc] initWithDictionary:response error:&error];
                   cardId = (CardID *)[self addStatusCodes:statusCodes object:cardId];
                   [AppDelegate getAppDelegate].shipping_fee = [cardId.data doubleValue];
                   
                   completionBlock(cardId, nil);
                   
               }
                   
               }];
               
           }
             progress:nil];
}

@end
