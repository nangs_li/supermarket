//
//  REEnvConstant.h
//  real-v2-ios
//
//  Created by Derek Cheung on 17/5/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface REEnvConstant : NSObject

FOUNDATION_EXPORT BOOL const kDebugMode;

FOUNDATION_EXPORT NSString *const kServerAddress;

FOUNDATION_EXPORT DDLogLevel const ddLogLevel;

FOUNDATION_EXPORT NSString *const kGoogleAPIKey;

@end
