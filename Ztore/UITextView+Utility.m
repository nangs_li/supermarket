//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "Common.h"
#import "UITextView+Utility.h"

@implementation UITextView (Utility)

- (CGRect)textSizeRectFromTextView {
    
  CGRect rect = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, 0)
                            options:NSStringDrawingUsesLineFragmentOrigin
                         attributes:@{NSFontAttributeName:self.font}
                                        context:nil];
  rect.size.height = rect.size.height + 6;
    
  return rect;

}

@end
