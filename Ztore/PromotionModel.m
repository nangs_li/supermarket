//
//  ProductCategory.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "PromotionModel.h"

@implementation Banner

@end

@implementation PromotionData

+ (JSONKeyMapper *)keyMapper {
    
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"desc" : @"description" }];
}

@end

@implementation PromotionModel

@end
