//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "LeftMenuHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation LeftMenuHeaderView

#pragma mark App LifeCycle

- (void)setupView {
    
   self.loginLabel.text = ([Common shared].isLogin) ? JMOLocalizedString(@"My Account", nil) : JMOLocalizedString(@"Login/Register", nil);
   self.loginLabel.font = [[Common shared] getSmallContentNormalFont];
   self.accountView.layer.cornerRadius = self.accountView.bounds.size.width / 2;
   self.accountView.layer.masksToBounds = YES;
   self.myFavouriteLabel.text = JMOLocalizedString(@"My Favourite", nil);
   self.myFavouriteLabel.font = [[Common shared] getSmallContentNormalFont];
   self.favouriteView.layer.cornerRadius = self.favouriteView.bounds.size.width / 2;
   self.favouriteView.layer.masksToBounds = YES;
   self.homeLabel.text = JMOLocalizedString(@"Home Page", nil);
   self.homeLabel.font = [[Common shared] getSmallContentNormalFont];
   self.homeView.layer.cornerRadius = self.homeView.bounds.size.width / 2;
   self.homeView.layer.masksToBounds = YES;
    
}

@end
