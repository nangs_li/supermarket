//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "OrderCommentCell.h" 
#import <QuartzCore/QuartzCore.h>

@implementation OrderCommentCell

- (void)setTitleText:(NSString *)titleText paymentSuccess:(PaymentSuccess *)paymentSuccess {
    
    
    self.titleLabel.text = titleText;
    self.titleLabel.font = [[Common shared] getNormalTitleFont];
    self.titleLabel.textColor = [UIColor blackColor];
    
    self.textView.text = paymentSuccess.data.rating.rating_comment;
    self.textView.font = [[Common shared] getContentNormalFont];
    self.textView.textColor = [UIColor blackColor];
    self.textView.userInteractionEnabled = ![Common shared].didSelectOrderHistory.is_rated;
  
    [[self.textView layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.textView layer] setBorderWidth:1];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
