//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUMSlider.h"

@interface OrderRatingCell : UITableViewCell
@property(weak, nonatomic) IBOutlet HUMSlider *sliderFromNibSideColors;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property(strong, nonatomic) NSString *ratingString;

- (void)setTitleText:(NSString *)titleText value:(NSNumber *)value enableSlider:(BOOL)enableSlider;

@end
