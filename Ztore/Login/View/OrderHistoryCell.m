//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "OrderHistoryCell.h"

@implementation OrderHistoryCell

- (void)setTextFromPaymentData:(PaymentSuccessData *)paymentData {
    
    self.topViewHeight.constant = (paymentData.newYearFirstObject) ? 39 : 0;
    self.topViewYearLabel.textColor = [[Common shared] getCommonRedColor];
    self.topViewYearLabel.text = [paymentData getYearString];
    self.topViewYearLabel.font = [[Common shared] getSmallContentNormalFont];
    self.topViewYearLabel.hidden = (paymentData.newYearFirstObject) ? NO : YES;
    self.topView.backgroundColor = [[Common shared] getProductDetailDescGrayBackgroundColor];
    
    self.orderDateLabel.text = [paymentData getFullDateFormatString];
    self.orderDateLabel.textColor = [UIColor lightGrayColor];
    self.orderDateLabel.font = [[Common shared] getSmallContentNormalFont];
    
    self.orderLabel.text = JMOLocalizedString(@"Order Number", nil);
    self.orderLabel.textColor = [UIColor blackColor];
    self.orderLabel.font = [[Common shared] getSmallContentNormalFont];
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"#%@", paymentData.order_sn];
    self.orderNoLabel.textColor = [UIColor blackColor];
    self.orderNoLabel.font = [[Common shared] getNormalFont];
    
    self.rateLabel.text = JMOLocalizedString(@"No Rating", nil);
    self.rateLabel.textColor = [[Common shared] getCommonRedColor];
    self.rateLabel.font = [[Common shared] getContentNormalFont];
    self.rateLabel.hidden = paymentData.is_rated;
    

}

@end
