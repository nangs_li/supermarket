//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "MyAccountCell.h"

@implementation MyAccountCell

- (void)setTitleText:(NSString *)titleText subTitleText:(NSString *)subTitleText hideSubTitle:(BOOL)hideSubTitle {
    
    self.titleLabel.text = titleText;
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [[Common shared] getNormalTitleFont];
    self.subTitleLabel.text = subTitleText;
    self.subTitleLabel.textColor = [[Common shared] getLineColor];
    self.subTitleLabel.font = [[Common shared] getNormalFont];
    self.subTitleLabel.hidden = hideSubTitle;
    self.tickImageView.hidden = self.imageView.hidden = NO;
    self.selectionStyle = UITableViewCellSelectionStyleNone;

}

- (void)setTitleText:(NSString *)titleText accountDetailText:(NSString *)accountDetailText hideDetailText:(BOOL)hideDetailText {
    
    [self setTitleText:titleText subTitleText:@"" hideSubTitle:YES];
    self.tickImageView.hidden = self.subTitleLabel.hidden = self.imageView.hidden = YES;
    self.accountDetailLabel.hidden = NO;
    self.accountDetailLabel.text = (hideDetailText) ? JMOLocalizedString(@"Not set", nil): accountDetailText;
    self.accountDetailLabel.textColor = (hideDetailText) ? [[Common shared] getLineColor]: [UIColor blackColor];
    self.accountDetailLabel.font = [[Common shared] getNormalFont];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

@end
