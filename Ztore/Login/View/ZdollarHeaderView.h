
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

@interface ZdollarHeaderView : UIView

@property(weak, nonatomic) IBOutlet UILabel *zDollarDescLabel;
@property(weak, nonatomic) IBOutlet UIView *lineView;

- (void)setupView;

@end
