//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZDollarModel.h"

@interface ZDollarCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *dateLabel;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *zdollarLabel;


- (void)setDataFromZDollarData:(ZDollarModelData *)zdollardata;

@end
