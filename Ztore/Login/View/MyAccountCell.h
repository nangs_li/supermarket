//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *accountDetailLabel;

- (void)setTitleText:(NSString *)titleText subTitleText:(NSString *)subTitleText hideSubTitle:(BOOL)hideSubTitle;
- (void)setTitleText:(NSString *)titleText accountDetailText:(NSString *)accountDetailText hideDetailText:(BOOL)hideDetailText;

@end
