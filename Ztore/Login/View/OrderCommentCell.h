//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCommentCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UITextView *textView;
@property(strong, nonatomic) PaymentSuccessData *didSelectOrderHistory;

- (void)setTitleText:(NSString *)titleText paymentSuccess:(PaymentSuccess *)paymentSuccess;

@end
