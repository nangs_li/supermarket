//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ZDollarCell.h"

@implementation ZDollarCell

- (void)setDataFromZDollarData:(ZDollarModelData *)zdollardata {
    
    self.dateLabel.text = [zdollardata getFullDateFormatString];
    self.dateLabel.textColor = [UIColor lightGrayColor];
    self.dateLabel.font = [[Common shared] getSmallContentNormalFont];
    
    self.titleLabel.text = [zdollardata getTypeString];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [[Common shared] getContentBoldFont];
    
    self.zdollarLabel.text = [zdollardata getZdollarChangeString];
    self.zdollarLabel.textColor = (zdollardata.zdollar_change > 0) ?  [UIColor colorWithRed:23.0f / 255.0f green:153.0f / 255.0f blue:84.0f / 255.0f alpha:1.0] : [[Common shared] getCommonRedColor];
    self.zdollarLabel.font = [[Common shared] getBoldFontWithSize:14];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

@end
