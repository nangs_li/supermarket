//
//  SlidingMenuStaticCell.m
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "OrderRatingCell.h"

@implementation OrderRatingCell

- (void)setTitleText:(NSString *)titleText value:(NSNumber *)value enableSlider:(BOOL)enableSlider {
    
    
    self.titleLabel.text = titleText;
    self.titleLabel.font = [[Common shared] getNormalTitleFont];
    self.titleLabel.textColor = [UIColor blackColor];
    
    self.ratingLabel.text = self.ratingString;
    self.ratingLabel.font = [[Common shared] getSmallContentNormalFont];
    self.ratingLabel.textColor = [[Common shared] getCommonRedColor];
    
    // This uses custom colors on the images for each side.
    self.sliderFromNibSideColors.minimumValueImage = [UIImage imageNamed:@"MoodSad"];
    self.sliderFromNibSideColors.maximumValueImage = [UIImage imageNamed:@"MoodHappy"];
    
    [self.sliderFromNibSideColors setSaturatedColor:[UIColor redColor]
                                            forSide:HUMSliderSideLeft];
    [self.sliderFromNibSideColors setSaturatedColor:[UIColor redColor]
                                            forSide:HUMSliderSideRight];
    
    [self.sliderFromNibSideColors setDesaturatedColor:[UIColor lightGrayColor]
                                              forSide:HUMSliderSideLeft];
    [self.sliderFromNibSideColors setDesaturatedColor:[UIColor lightGrayColor]
                                              forSide:HUMSliderSideRight];
    self.sliderFromNibSideColors.tintColor = [UIColor redColor];
    self.sliderFromNibSideColors.value = [value doubleValue] / 5.0;
    self.sliderFromNibSideColors.sectionCount = 5;
    self.sliderFromNibSideColors.tickColor = [UIColor lightGrayColor];
    
    self.sliderFromNibSideColors.userInteractionEnabled = (enableSlider) ? YES : NO;
    self.sliderFromNibSideColors.hidden = (enableSlider) ? NO : YES;
    
    [self sliderChange:self];

}

- (IBAction)sliderChange:(id)sender {
    
    if (self.sliderFromNibSideColors.value <= 0.21) {
        
    self.ratingString = JMOLocalizedString(@"Very Dissatisfied", nil);
        
    } else if (self.sliderFromNibSideColors.value <= 0.41) {
        
    self.ratingString = JMOLocalizedString(@"Dissatisfied", nil);

        
    } else if (self.sliderFromNibSideColors.value <= 0.61) {
        
    self.ratingString = JMOLocalizedString(@"Accepted", nil);

        
    } else if (self.sliderFromNibSideColors.value  <= 0.81) {
        
    self.ratingString = JMOLocalizedString(@"Satisfied", nil);

        
    } else if (self.sliderFromNibSideColors.value <= 1.0) {
    
    self.ratingString = JMOLocalizedString(@"Very Satisfied", nil);
  
    }
    
    self.ratingLabel.text = self.ratingString;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
