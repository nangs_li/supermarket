//
//  TableViewCell.m
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "CustomTextFieldView.h"

@implementation CustomTextFieldView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationEndTextField object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self textFieldEnd];
        
        
    }];

}

- (void)setFieldText:(NSString *)text errorText:(NSString *)errorText {

  self.fieldLabel.text         = text;
  self.fieldLabel.font         = [[Common shared] getNormalFontWithSize:14];
  self.fieldLabel.textColor    = [UIColor grayColor];
  self.errorLabel.text         = errorText;
  self.errorText = errorText;
  self.errorLabel.font         = [[Common shared] getNormalFontWithSize:14];
  self.errorLabel.textColor    = [[Common shared] getCommonRedColor];
  self.textField.text          = @"";
  self.textField.font          = [[Common shared] getNormalTitleFont];
  self.redLine.backgroundColor = [[Common shared] getCommonRedColor];
  self.redLine.hidden          = YES;
  self.line.hidden             = NO;
}

- (void)setupWhiteColorTextField {
    
    self.backgroundColor = [UIColor clearColor];
    self.textField.textColor = self.errorLabel.textColor = self.redLine.backgroundColor = self.line.backgroundColor = self.fieldLabel.textColor = [UIColor whiteColor];
    
}

- (void)setupNormalColorTextField {
    
    self.backgroundColor = [UIColor clearColor];
    self.errorLabel.textColor = self.redLine.backgroundColor = [[Common shared] getCommonRedColor];
    self.textField.textColor = self.line.backgroundColor = [UIColor blackColor];
    self.fieldLabel.textColor = [UIColor grayColor];
    
}

- (void)textFieldBegin {
    
  self.redLine.hidden = NO;
  self.line.hidden    = YES;
    
}

- (void)textFieldEnd {

  self.redLine.hidden = YES;
  self.line.hidden    = NO;
  self.redLine.hidden = !self.errorStatus;

}

- (BOOL)checkValid {
    
    if (self.notCheckTextField) {
        
        return YES;
        
    } else {
    
    self.errorStatus = (self.textField.text.length > 0) ? NO : YES;
    
    if (self.dropDownMenutype == PhoneNumberField) {
        
    [self setUpErrorLabelFromErrorString:JMOLocalizedString(@"please input a 8-digit number", nil) condition:(self.textField.text.length < 8)];
        
    }
  
    if (self.dropDownMenutype == CardNumber) {
        
    [self setUpErrorLabelFromErrorString:JMOLocalizedString(@"Credit card number must be 15 or 16 digits", nil) condition:(self.textField.text.length != 15 && self.textField.text.length != 16)];
        
    }

    if (self.dropDownMenutype == CVCNumber) {
        
    [self setUpErrorLabelFromErrorString:JMOLocalizedString(@"CVV must be 3 or 4 digits", nil) condition:(self.textField.text.length != 3 && self.textField.text.length != 4)];
        
    }
    
    if (self.dropDownMenutype == Password) {
        
    [self setUpErrorLabelFromErrorString:JMOLocalizedString(@"Password must between 6 - 20 characters or more", nil) condition:(self.textField.text.length <6 || self.textField.text.length > 20)];
        
    }
    
    if (self.dropDownMenutype == Email) {
        
        [self setUpErrorLabelFromErrorString:JMOLocalizedString(@"Please enter a valid email", nil) condition:![self.textField.text isValidEmail]];
        
    }
    
    self.errorLabel.hidden = !self.errorStatus;
    self.redLine.hidden    = !self.errorStatus;
    self.line.hidden = !self.redLine.hidden;
    
    return !self.errorStatus;
        
    }
}

- (void)setErrorStatus {
    
    self.errorStatus = YES;
    self.errorLabel.hidden = !self.errorStatus;
    self.redLine.hidden    = !self.errorStatus;
    self.line.hidden = !self.redLine.hidden;
    
}

- (void)setUpErrorLabelFromErrorString:(NSString *)errorString condition:(BOOL)condition {
    
    self.errorLabel.text = condition ? errorString : self.errorText;
    
    if (!self.errorStatus) {
        
        self.errorStatus = condition ? YES : NO;
    }
    
}
    
- (void)createDropDownMenuFromStringArray:(NSArray *)stringArray placeholder:(NSString *)placeholder dropDownMenutype:(Type)dropDownMenutype {

  self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName : [[Common shared] getTextFieldPlaceHolderColor]}];
  self.textField.textColor             = [UIColor blackColor];
  self.dropDownBtn.hidden              = NO;
  
  self.stringArray = stringArray;
    
  NSMutableArray *menuItems = [[NSMutableArray alloc] init];

  self.menuArray        = [menuItems copy];
  self.dropDownMenutype = dropDownMenutype;

}

- (void)emptyMethod:(id)sender {
    
}

- (void)afterDoneFromSelectedValue:(NSString *)selectedValue {

  [self textFieldEnd];

  for (AddressDetail *addressDetail in self.addressData.data) {

    if ([addressDetail.name isEqualToString:selectedValue]) {

      self.addressDetail = addressDetail;
    }
  }

  (self.dropDownMenutype == AddressDetail1) ? [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGetAddressDetail2 object:nil] : nil;
  (self.dropDownMenutype == AddressDetail2) ? [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGetAddressDetail3 object:nil] : nil;
    
  [self checkAddressDetail3LimitedDelivery];
}

- (void)checkAddressDetail3LimitedDelivery {
    
    if (self.dropDownMenutype == AddressDetail3 ) {
        
        if ([self.textField.text isEqualToString:@"愉景灣"] || [self.textField.text isEqualToString:@"東涌"] || [self.textField.text isEqualToString:@"馬灣"] || [self.textField.text isEqualToString:@"Ma Wan"] || [self.textField.text isEqualToString:@"Tung Chung"] || [self.textField.text isEqualToString:@"Discovery Bay"] ) {
            
            self.errorLabel.text = JMOLocalizedString(@"Only limited delivery time applies on your district.", nil);
            self.errorLabel.hidden = NO;
            
        } else {
            
            self.errorLabel.text = JMOLocalizedString(@"Please choose area", nil);
            self.errorLabel.hidden = YES;
            
        }
        
    }

}

- (void)updateTextFieldString:(NSString *)textFieldString {

  self.textField.text      = textFieldString;
  self.textField.textColor = self.textField.textColor;
    
}

- (void)resetDropDownMenu {

  self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"Please choose", nil) attributes:@{NSForegroundColorAttributeName : [[Common shared] getTextFieldPlaceHolderColor]}];
  self.textField.text                  = nil;
  [self.dropDownBtn removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
}

- (void)setTextFieldNumberType {

  self.dropDownMenutype = PhoneNumberField;
  [self.textField setKeyboardType:UIKeyboardTypeNumberPad];
  [self setDoneBtnFromTextField:self.textField];
    
}

- (void)setTextFieldCardNumberType {
    
    self.dropDownMenutype = CardNumber;
    [self.textField setKeyboardType:UIKeyboardTypeNumberPad];
    [self setDoneBtnFromTextField:self.textField];

}

- (void)setTextFieldCVCNumberType {
 
    self.dropDownMenutype = CVCNumber;
    [self.textField setKeyboardType:UIKeyboardTypeNumberPad];
    [self setDoneBtnFromTextField:self.textField];
}

- (void)setDoneBtnFromTextField:(UITextField *)textField {
    
    UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:JMOLocalizedString(@"Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;
    
}

- (void)doneWithNumberPad {
    
    [self.textField resignFirstResponder];
    
}

- (void)setTextFieldLoginEmailType {
    
    [self setFieldText:JMOLocalizedString(@"Email*", nil) errorText:JMOLocalizedString(@"Please enter a valid email", nil)];
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"Please enter a email", nil) attributes:@{NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.3f]}];
    [self setupWhiteColorTextField];
    self.dropDownMenutype = Email;
}
    
- (void)setTextFieldPasswordType {
    
    [self setFieldText:JMOLocalizedString(@"Password*", nil) errorText:JMOLocalizedString(@"Please enter a valid password", nil)];
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:JMOLocalizedString(@"Please enter a password", nil) attributes:@{NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.3f]}];
    [self setupWhiteColorTextField];
    self.dropDownMenutype = Password;
    self.textField.secureTextEntry = YES;
    
}

- (void)setPlaceHolderFromText:(NSString *)text {
    
      self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName : [[Common shared] getTextFieldPlaceHolderColor]}];
    
}

- (void)setWhitePlaceHolderFromText:(NSString *)text {
    
     self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName : [UIColor colorWithWhite:1 alpha:0.3]}];
    
}

@end
