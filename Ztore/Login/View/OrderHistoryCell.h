//
//  SlidingMenuStaticCell.h
//  Ztore
//
//  Created by ken on 8/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentSuccess.h"

@interface OrderHistoryCell : UITableViewCell

@property(weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property(weak, nonatomic) IBOutlet UIView *topView;
@property(weak, nonatomic) IBOutlet UILabel *topViewYearLabel;
@property(weak, nonatomic) IBOutlet UILabel *orderDateLabel;
@property(weak, nonatomic) IBOutlet UILabel *orderLabel;
@property(weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property(weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property(weak, nonatomic) IBOutlet UILabel *rateLabel;

- (void)setTextFromPaymentData:(PaymentSuccessData *)paymentData;

@end
