//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "AccountHeaderView.h"

@implementation AccountHeaderView

#pragma mark App LifeCycle

- (void)setupViewFromUserAccount:(UserAccount *)userAccount {
    
    self.zDollarDescLabel.text = JMOLocalizedString(@"You can use your Z-Dollar balance in your NEXT PURCHASE. You can redeem up to a MAXIMUM of 50% of cart total per transaction.", nil);
    self.zDollarDescLabel.textColor = [UIColor whiteColor];
    self.zDollarDescLabel.font = [[Common shared] getNormalFontWithSize:14];
    [self setBackgroundColor:[[Common shared] getCommonRedColor]];
    
    NSString *firstName = ([Common shared].userAccont.data.first_name) ? [Common shared].userAccont.data.first_name : @"";
    NSString *lastName = ([Common shared].userAccont.data.last_name) ? [Common shared].userAccont.data.last_name : @"";
    NSString *name = [NSString stringWithFormat:@"%@ %@", lastName, firstName];
    self.zDollarDescLabel.text = [NSString stringWithFormat:JMOLocalizedString(@"Hello，%@", nil), name];
    [self.zDollarDescLabel setFont:[[Common shared] getNormalFontWithSize:33]];

}

@end
