//
//  TableViewCell.h
//  S18
//
//  Created by ios Developer 5 on 4/8/14.
//  Copyright (c) 2014 ios Developer 5. All rights reserved.
//

#import "AddressData.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Type) { Title, AddressDetail1, AddressDetail2, AddressDetail3, PhoneNumberField, ExpirationYear, ExpirationMonth, CardNumber, CVCNumber, Password ,Email};

@interface CustomTextFieldView : UIView

@property(weak, nonatomic) IBOutlet UILabel *fieldLabel;
@property(weak, nonatomic) IBOutlet UITextField *textField;
@property(weak, nonatomic) IBOutlet UIView *line;
@property(weak, nonatomic) IBOutlet UILabel *errorLabel;
@property(weak, nonatomic) IBOutlet UIView *redLine;
@property(weak, nonatomic) IBOutlet UIButton *dropDownBtn;
@property(assign, nonatomic) BOOL errorStatus;
@property(strong, nonatomic) NSArray *menuArray;
@property(strong, nonatomic) NSArray *stringArray;
@property(assign, nonatomic) Type dropDownMenutype;
@property(strong, nonatomic) AddressData *addressData;
@property(strong, nonatomic) AddressDetail *addressDetail;
@property(strong, nonatomic) NSString *errorText;
@property(assign, nonatomic) BOOL notCheckTextField;

#pragma textFieldBegin
- (void)textFieldBegin;

#pragma textFieldEnd
- (void)textFieldEnd;
- (BOOL)checkValid;
- (void)afterDoneFromSelectedValue:(NSString *)selectedValue;
- (void)checkAddressDetail3LimitedDelivery;

#pragma set up downMenu
- (void)createDropDownMenuFromStringArray:(NSArray *)stringArray placeholder:(NSString *)placeholder dropDownMenutype:(Type)dropDownMenutype;

#pragma mark Set Text
- (void)updateTextFieldString:(NSString *)textFieldString;
- (void)setFieldText:(NSString *)text errorText:(NSString *)errorText;
- (void)setPlaceHolderFromText:(NSString *)text;
- (void)setWhitePlaceHolderFromText:(NSString *)text;

#pragma mark Set Type
- (void)setupWhiteColorTextField;
- (void)setupNormalColorTextField;
- (void)resetDropDownMenu;
- (void)setErrorStatus;
- (void)setTextFieldNumberType;
- (void)setTextFieldCardNumberType;
- (void)setTextFieldCVCNumberType;
- (void)setTextFieldLoginEmailType;
- (void)setTextFieldPasswordType;

@end
