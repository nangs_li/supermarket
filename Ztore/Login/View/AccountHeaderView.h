
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//
#import "UserAccount.h"

@interface AccountHeaderView : UIView

@property(weak, nonatomic) IBOutlet UILabel *zDollarDescLabel;

- (void)setupViewFromUserAccount:(UserAccount *)userAccount;

@end
