//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ZdollarHeaderView.h"

@implementation ZdollarHeaderView

#pragma mark App LifeCycle

- (void)setupView {
    
    self.zDollarDescLabel.text = JMOLocalizedString(@"You can use your Z-Dollar balance in your NEXT PURCHASE. You can redeem up to a MAXIMUM of 50% of cart total per transaction.", nil);
    self.zDollarDescLabel.textColor = [UIColor whiteColor];
    self.zDollarDescLabel.font = [[Common shared] getNormalFontWithSize:14];
    [self setBackgroundColor:[[Common shared] getCommonPinkRedColor]];
}

@end
