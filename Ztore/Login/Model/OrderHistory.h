

//
//  ProductCategory.h
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductList.h"
#import <Foundation/Foundation.h>
#import "PaymentSuccess.h"

@interface OrderHistory : REObject

@property(nonatomic, strong) NSArray<PaymentSuccessData> *data;

- (void)setUpNewYearFirstObject;
- (int)getCountFromNoRatingOrderHistory;

@end


