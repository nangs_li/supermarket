//
//  User.h
//  Ztore
//
//  Created by ken on 24/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacebookLoginData : REObject

@property(nonatomic, strong) NSString *confirm_password;

@end

@interface FacebookLogin : REObject

@property(nonatomic, strong) FacebookLoginData *data;

@end
