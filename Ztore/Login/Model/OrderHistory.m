//
//  ProductCategory.m
//  Ztore
//
//  Created by ken on 25/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "OrderHistory.h"
#import <JSONModel/JSONModel.h>

@implementation OrderHistory

- (void)setUpNewYearFirstObject {
    
    NSString *yearString;
    
    for (PaymentSuccessData *paymentData in self.data) {
        
        if (yearString == nil) {
            
            yearString = [paymentData getYearString];
            
            paymentData.newYearFirstObject = YES;
            
        } else if ([yearString isEqualToString:[paymentData getYearString]]) {
            
            paymentData.newYearFirstObject = NO;
            
        } else if (![yearString isEqualToString:[paymentData getYearString]]) {
            
            paymentData.newYearFirstObject = YES;
            yearString = [paymentData getYearString];
            
        }
        
    }
}

- (int)getCountFromNoRatingOrderHistory {
    
    int count = 0;
    
    for (PaymentSuccessData *paymentData in self.data) {
        
        count = (paymentData.is_rated) ? count : count + 1 ;
            
    }
    
    return count;
}

@end
