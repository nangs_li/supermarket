//
//  ProductCategoryModel.h
//  Ztore
//
//  Created by Ken on 23/8/2016.
//  Copyright © ken. All rights reserved.
//

#import "REObject.h"

@class ZDollarModelData;

@protocol ZDollarModelData

@end

@interface ZDollarModelData : REObject

@property(nonatomic, assign) NSInteger log_date;

@property(nonatomic, assign) double zdollar_remain;

@property(nonatomic, assign) double zdollar_change;

@property(nonatomic, copy) NSString *type;

@property(nonatomic, copy) NSString *remark;

@property(nonatomic, assign) BOOL is_show_remark;

- (NSString *)getFullDateFormatString;

- (NSString *)getTypeString;

- (NSString *)getZdollarChangeString;

@end

@interface ZDollarModel : REObject

@property(nonatomic, strong) NSArray<ZDollarModelData> *data;

@end
