//
//  ProductCategoryModel.m
//  Ztore
//
//  Created by Ken on 23/8/2016.
//  Copyright © ken. All rights reserved.
//

#import "ZDollarModel.h"

@implementation ZDollarModelData

- (NSString *)getFullDateFormatString {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter localeDateFormatter];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.log_date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}

- (NSString *)getTypeString {
    
    NSString *typeString = JMOLocalizedString(@"other", nil);
    
    if ([self.type isEqualToString:@"REFERRAL"]) {
        
        typeString = JMOLocalizedString(@"referal programme", nil);
    }
    
    if ([self.type isEqualToString:@"ADMIN"]) {
        
        typeString = JMOLocalizedString(@"other", nil);
    }
    
    if ([self.type isEqualToString:@"ORDER_USED"]) {
        
        typeString = JMOLocalizedString(@"used by ordering", nil);
    }
    
//    if ([self.type isEqualToString:@"CREDIT_CARD_PROMO"]) {
//        
//        typeString = JMOLocalizedString(@"Extra 10% Z-dollar rebate with Fubon Bank credit card", nil);
//    }
    
    if ([self.type isEqualToString:@"ORDER_REBATE"]) {
        
        typeString = JMOLocalizedString(@"Rebate", nil);
    }
    
    if ([self.type isEqualToString:@"ORDER_CANCEL"]) {
        
        typeString = JMOLocalizedString(@"order cancelled", nil);
    }
    
    if (self.is_show_remark) {
        
        if (isValid(self.remark)) {
            
            typeString = typeString;
            
        }
        
    }
    
    return typeString;
  
}

- (NSString *)getZdollarChangeString {
    
   return (self.zdollar_change > 0) ? [[Common shared] getStringFromPriceString:[NSString stringWithFormat:@"%f", self.zdollar_change]] : [NSString stringWithFormat:@"-$%.2f", -self.zdollar_change];
    
}

@end

@implementation ZDollarModel

@end
