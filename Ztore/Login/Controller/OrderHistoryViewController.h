
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import "OrderHistory.h"

@interface OrderHistoryViewController : _UIViewController <UITableViewDelegate, UITableViewDataSource>

typedef NS_ENUM(NSInteger, OrderHistoryType) { PaymentSuccessType, MyAccountType};

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) NSArray *myAccountFristArray;
@property(strong, nonatomic) NSArray *myAccountFristSubTitleArray;
@property(strong, nonatomic) NSArray *myAccountSecondArray;
@property(strong, nonatomic) NSArray *myAccountThreeArray;
@property(assign, nonatomic) OrderHistoryType orderHistoryType;

@end
