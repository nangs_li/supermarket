//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MyAccountDetailViewController.h"
#import "MyAccountCell.h"
#import "FormViewController.h"

@implementation MyAccountDetailViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
    self.myAccountFristArray = @[JMOLocalizedString(@"Email", nil), JMOLocalizedString(@"Name And Title*", nil), JMOLocalizedString(@"Phone*", nil), JMOLocalizedString(@"Other Phone", nil), JMOLocalizedString(@"Company Name", nil), JMOLocalizedString(@"Password", nil)];
      self.myAccountFristSubTitleArray = [[NSMutableArray alloc] init];
    self.warningLabel.text                                    = JMOLocalizedString(@"The option to hit (*) must be filled in.", nil);
    self.warningLabel.textColor                               = [[Common shared] getCommonRedColor];
    [self.cneterView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
   
            
            [self getCurrentUserCallback];
    
    [super setPageName:@"My Account - User Information"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [self.tableView reloadData];
     [self getCurrentUserCallback];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Account Information", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"", nil) forState:UIControlStateNormal];
  

}

- (void)setupView {
    
    self.titleLabel.text = JMOLocalizedString(@"Account Information", nil);
    [self.titleLabel setFont:[[Common shared] getNormalFontWithSize:23]];
    [self.tableView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)getCurrentUserCallback {
    
    if ([(NSNumber *)[Common shared].userAccont.statusCodes[0] intValue] == STATUS_OK) {
        
        self.myAccountFristSubTitleArray = [[NSMutableArray alloc] init];
        
        NSString *firstName = ([Common shared].userAccont.data.first_name) ? [Common shared].userAccont.data.first_name : @"";
         NSString *lastName = ([Common shared].userAccont.data.last_name) ? [Common shared].userAccont.data.last_name : @"";
         NSString *title = [[Common shared] getConsigneeTitleFromTitle:[Common shared].userAccont.data.title];
        NSString *name = ([firstName isEqualToString:@""]) ? @"" : [NSString stringWithFormat:@"%@ %@ %@", lastName, firstName, title];
                
        [self.myAccountFristSubTitleArray addObject:([Common shared].userAccont.data.email) ? [Common shared].userAccont.data.email : @""];
         [self.myAccountFristSubTitleArray addObject:name];
         [self.myAccountFristSubTitleArray addObject:([Common shared].userAccont.data.mobile) ? [Common shared].userAccont.data.mobile : @""];
         [self.myAccountFristSubTitleArray addObject:([Common shared].userAccont.data.contact_no) ? [Common shared].userAccont.data.contact_no : @""];
         [self.myAccountFristSubTitleArray addObject:([Common shared].userAccont.data.company) ? [Common shared].userAccont.data.company : @""];
        [self.myAccountFristSubTitleArray addObject:@"*******"];
        
        if ([Common shared].userAccont.data.fb_user_id != nil) {
            
            self.myAccountFristArray = @[JMOLocalizedString(@"Email", nil), JMOLocalizedString(@"Name And Title*", nil), JMOLocalizedString(@"Phone*", nil), JMOLocalizedString(@"Other Phone", nil), JMOLocalizedString(@"Company Name", nil)];
        }
        
        [self.tableView reloadData];
        
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.myAccountFristArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     static NSString *cellIdentifier = @"MyAccountCell";
    MyAccountCell *cell = (MyAccountCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    NSString *accountDetailText = (self.myAccountFristSubTitleArray.count > indexPath.row) ? self.myAccountFristSubTitleArray[indexPath.row] : @"";
    [cell setTitleText:self.myAccountFristArray[indexPath.row] accountDetailText:accountDetailText hideDetailText:[accountDetailText isEqualToString:@""]];

    return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 72;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FormViewController *formViewController = [[FormViewController alloc] init];
    FormType formType;
    
    switch (indexPath.row) {
            
        case 0:
            
            formType = ChangeEmail;
            break;
            
        case 1:
            
            formType = ChangeName;
            break;
            
        case 2:
            
            formType = ChangePhone;
            break;
            
        case 3:
            
            formType = ChangeOtherPhone;
            break;
            
        case 4:
            
            formType = ChangeCompanyName;
            break;
            
        case 5:
            
            formType = ChangePassword;
            break;
            
        default:
            break;
    }

      [formViewController  setupFormType:formType];
      [self.navigationController pushViewController:formViewController animated:YES enableUI:NO];
    
}

@end
