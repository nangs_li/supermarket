//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "MyAccountViewController.h"
#import "MyAccountCell.h"
#import "MyAccountDetailViewController.h"
#import "DeliveryAddressViewController.h"
#import "MyFavouriteViewController.h"
#import "OrderHistoryViewController.h"
#import "ZDollarViewController.h"
#import "WebViewViewController.h"
#import "ContactUsViewController.h"

@implementation MyAccountViewController {
    
    int headerHeight;
}

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
    
  headerHeight = 110;
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
    self.myAccountFristArray = @[JMOLocalizedString(@"Account Information", nil), JMOLocalizedString(@"Delivery Address", nil), JMOLocalizedString(@"My Favourite", nil), JMOLocalizedString(@"Order History", nil), JMOLocalizedString(@"Z-Dollar", nil)];
     self.myAccountFristSubTitleArray = @[JMOLocalizedString(@"Edit Account Information", nil), JMOLocalizedString(@"Edit Delivery Address", nil), JMOLocalizedString(@"Edit My favorite list and Order history", nil), [NSString stringWithFormat:JMOLocalizedString(@"You have got (%@) order(s) not yet rated", nil), @"1"], @"$45.55"];
     self.myAccountSecondArray = @[JMOLocalizedString(@"About Delivery", nil), JMOLocalizedString(@"FAQ", nil), JMOLocalizedString(@"Terms & Conditions", nil), JMOLocalizedString(@"Privacy Policy", nil)];
    self.myAccountThreeArray = @[JMOLocalizedString(@"Get To Know Ztore!", nil), JMOLocalizedString(@"Our Belief", nil), JMOLocalizedString(@"Prospective Partners", nil), JMOLocalizedString(@"Contact Us", nil)];

    [self getCurrentUserCallback];
    [self.centerView setBackgroundColor:[[Common shared] getCommonRedColor]];
    [self refreshDataArray];
    [super setPageName:@"My Account"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [self refreshDataArray];
    
}

- (void)callAPI {
    
    [[Common shared] getZDollarModelCompletionBlock:^(id response, NSError *error) {
        
    }];
    [[Common shared] getFavouriteListCompletionBlock:^(id response, NSError *error) {
        
    }];
    [[Common shared] getRecentPurchaseListCompletionBlock:^(id response, NSError *error) {
        
    }];
    [[Common shared] getDeliveryAddressDataCompletionBlock:^(id response, NSError *error) {
        
    }];
    [[Common shared] getOrderHistoryCompletionBlock:^(id response, NSError *error) {
        
          if ([[Common shared] checkNotError:error controller:self response:response]) {
              
        [self refreshDataArray];
              
          }
        
    }];

}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar loginMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Logout", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
   

}

- (void)setupView {
    
    [self.tableView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)pressUsePromoBtn:(id)sender {
    
    [[ServerAPI shared] logoutCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadLeftMenu object:nil];
            
            [self backToMainPageGetCurrentUser:NO];
            
            [[Common shared] cleanAllCheckOutData];

        }

    }];
    
}

- (void)backBtnPressed:(id)sender {
    
    [self backToMainPageGetCurrentUser:NO];
    
}

- (void)getCurrentUserCallback {
    
    if ([(NSNumber *)[Common shared].userAccont.statusCodes[0] intValue] == STATUS_OK) {
 
        self.myAccountFristSubTitleArray = @[JMOLocalizedString(@"Edit Account Information", nil), JMOLocalizedString(@"Edit Delivery Address", nil), JMOLocalizedString(@"Edit My favorite list and Order history", nil), [NSString stringWithFormat:JMOLocalizedString(@"You have got (%@) order(s) not yet rated", nil), @"1"], [[Common shared] getStringFromPriceString:[Common shared].userAccont.data.zdollar]];
            [self.tableView reloadData];
        
    }
}

- (void)refreshDataArray {
    
    self.myAccountFristSubTitleArray = @[JMOLocalizedString(@"Edit Account Information", nil), JMOLocalizedString(@"Edit Delivery Address", nil), JMOLocalizedString(@"Edit My favorite list and Order history", nil), [NSString stringWithFormat:JMOLocalizedString(@"You have got (%d) order(s) not yet rated", nil), [[Common shared].orderHistory getCountFromNoRatingOrderHistory]], [[Common shared] getStringFromPriceString:[Common shared].userAccont.data.zdollar]];
    [self.tableView reloadData];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSUInteger count = 0;
    
    if (section == 0) {
        
        count = self.myAccountFristArray.count;
        
    } else if (section == 1) {
    
        count = self.myAccountSecondArray.count;
    
    } else {
     
        count = self.myAccountThreeArray.count;
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     static NSString *cellIdentifier = @"MyAccountCell";
    MyAccountCell *cell = (MyAccountCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    
    if (indexPath.section == 0) {
        
        [cell setTitleText:self.myAccountFristArray[indexPath.row] subTitleText:self.myAccountFristSubTitleArray[indexPath.row] hideSubTitle:NO];
        
    } else if (indexPath.section == 1) {
        
        [cell setTitleText:self.myAccountSecondArray[indexPath.row] subTitleText:@"" hideSubTitle:YES];
        
    } else if (indexPath.section == 2) {
        
        [cell setTitleText:self.myAccountThreeArray[indexPath.row] subTitleText:@"" hideSubTitle:YES];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.subTitleLabel.textColor = (indexPath.section == 0 && indexPath.row == 4) ? [[Common shared] getCommonRedColor] : [[Common shared] getLineColor];
    
        return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 72;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row == 0 ) {
        
        [self.navigationController pushViewController:[[MyAccountDetailViewController alloc] init] animated:YES enableUI:NO];
    }
    
    if (indexPath.section == 0 && indexPath.row == 1 ) {
        
        DeliveryAddressViewController *deliveryAddressViewController = [[DeliveryAddressViewController alloc] initWithNibName:@"DeliveryAddressViewController" bundle:nil];
        deliveryAddressViewController.editType = EditInMyAccount;
         [self.navigationController pushViewController:deliveryAddressViewController animated:YES enableUI:NO];
    }
    
    if (indexPath.section == 0 && indexPath.row == 2 ) {
        
        MyFavouriteViewController *myFavouriteViewController = [[MyFavouriteViewController alloc] initWithNibName:@"MyFavouriteViewController" bundle:nil];
        [self.navigationController pushViewController:myFavouriteViewController animated:YES enableUI:NO];
    }
    
    if (indexPath.section == 0 && indexPath.row == 3) {
        
        OrderHistoryViewController *orderHistoryViewController = [[OrderHistoryViewController alloc] initWithNibName:@"OrderHistoryViewController" bundle:nil];
        orderHistoryViewController.orderHistoryType = MyAccountType;
        [self.navigationController pushViewController:orderHistoryViewController animated:YES enableUI:NO];
     
    }
    
    if (indexPath.section == 0 && indexPath.row == 4) {
        
        ZDollarViewController *zDollarViewController = [[ZDollarViewController alloc] initWithNibName:@"ZDollarViewController" bundle:nil];
        [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
        
    }

    if (indexPath.section == 1 && indexPath.row == 0) {
        
        WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
        zDollarViewController.titleString = self.myAccountSecondArray[indexPath.row];
        zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_delivery/mobile" : @"https://www.ztore.com/tc/article/article_delivery/mobile";
        [zDollarViewController setPageName:@"My Account - About Delivery"];
        [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];

        
    }
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        
        WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
        zDollarViewController.titleString = self.myAccountSecondArray[indexPath.row];
        zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/article/article_FAQ/mobile" : @"https://www.ztore.com/tc/article/article_FAQ/mobile";
        [zDollarViewController setPageName:@"My Account - FAQ"];
        [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];

        
    }
    
    if (indexPath.section == 1 && indexPath.row == 2) {
        
        [self showTermsOfService];
        
    }
    
    if (indexPath.section == 1 && indexPath.row == 3) {
        
        [self showPrivacyPolicy];
        
    }
    
    if (indexPath.section == 2 && indexPath.row == 0) {
        
        WebViewViewController *zDollarViewController = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" bundle:nil];
        zDollarViewController.titleString = self.myAccountThreeArray[indexPath.row];
        zDollarViewController.urlString = ([[Common shared] isEnLanguage]) ? @"https://www.ztore.com/en/aboutZtore/mobile" : @"https://www.ztore.com/tc/aboutZtore/mobile";
        [zDollarViewController setPageName:@"My Account - About Ztore"];
        [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
        
    }

    if (indexPath.section == 2 && indexPath.row == 3) {
        
        ContactUsViewController *zDollarViewController = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
        [self.navigationController pushViewController:zDollarViewController animated:YES enableUI:NO];
        
    }


}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return headerHeight;
    }
    
    return 20.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"AccountHeaderView" owner:self options:nil] objectAtIndex:0];
        [self.headerView setupViewFromUserAccount:[Common shared].userAccont];
        
        return self.headerView;
    }
    
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    self.headerView.zDollarDescLabel.alpha = (headerHeight - scrollView.contentOffset.y) / headerHeight;
}

@end
