
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

typedef NS_ENUM(NSInteger, FormType) { ChangeEmail, ChangeName, ChangePhone, ChangeOtherPhone, ChangeCompanyName, ChangePassword};

@interface FormViewController : _UIViewController <UIScrollViewDelegate, FBSDKLoginButtonDelegate>

@property(weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(weak, nonatomic) IBOutlet UIView *oneView;
@property(weak, nonatomic) IBOutlet UIView *twoView;
@property(weak, nonatomic) IBOutlet UIView *threeView;
@property(weak, nonatomic) IBOutlet UIView *fourView;
@property(weak, nonatomic) IBOutlet UIView *fiveView;
@property(strong, nonatomic) CustomTextFieldView *oneTextField;
@property(strong, nonatomic) CustomTextFieldView *twoTextField;
@property(strong, nonatomic) CustomTextFieldView *threeTextField;
@property(strong, nonatomic) CustomTextFieldView *fourTextField;
@property(strong, nonatomic) CustomTextFieldView *fiveTextField;
@property(weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property(assign, nonatomic) FormType formType;

- (void)setupFormType:(FormType)formType;

@end
