
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface RegisterViewController : _UIViewController <UIScrollViewDelegate>

@property(weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(weak, nonatomic) IBOutlet UILabel *topWaringLabel;
@property(strong, nonatomic) IBOutlet UIView *emailView;
@property(strong, nonatomic) IBOutlet UIView *lastNameView;
@property(strong, nonatomic) IBOutlet UIView *firstNameView;
@property(strong, nonatomic) IBOutlet UIView *titleView;
@property(strong, nonatomic) IBOutlet UIView *companyNameView;
@property(strong, nonatomic) IBOutlet UIView *phoneView;
@property(strong, nonatomic) IBOutlet UIView *OtherPhoneView;
@property(strong, nonatomic) CustomTextFieldView *emailTextField;
@property(strong, nonatomic) CustomTextFieldView *lastNameTextField;
@property(strong, nonatomic) CustomTextFieldView *firstNameTextField;
@property(strong, nonatomic) CustomTextFieldView *titleTextField;
@property(strong, nonatomic) CustomTextFieldView *companyNameTextField;
@property(strong, nonatomic) CustomTextFieldView *phoneTextField;
@property(strong, nonatomic) CustomTextFieldView *OtherPhoneTextField;
@property(strong, nonatomic) IBOutlet UILabel *waringLabel;
@property(strong, nonatomic) IBOutlet UIButton *nextBtn;

@end
