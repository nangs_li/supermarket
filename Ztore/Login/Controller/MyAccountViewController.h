
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import "OrderHistory.h"
#import "AccountHeaderView.h"

@interface MyAccountViewController : _UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) NSArray *myAccountFristArray;
@property(strong, nonatomic) NSArray *myAccountFristSubTitleArray;
@property(strong, nonatomic) NSArray *myAccountSecondArray;
@property(strong, nonatomic) NSArray *myAccountThreeArray;
@property(weak, nonatomic) IBOutlet UIView *centerView;
@property(strong, nonatomic) AccountHeaderView *headerView;

@end
