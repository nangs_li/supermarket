//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ContactUsViewController.h"
#import "AddressData.h"
#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>

@implementation ContactUsViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    [self setupView];
    
}

- (void)setupView {
    
    self.titleLabel.text = JMOLocalizedString(@"Leave us a comment....", nil);
    self.titleLabel.font = [[Common shared] getNormalFontWithSize:30];
    self.titleDetailLabel.text = JMOLocalizedString(@"Ztore cares about your comment which would help us to improve the service everyday.", nil);
    self.titleDetailLabel.font = [[Common shared] getSmallContentNormalFont];
    self.view.backgroundColor = self.scrollView.backgroundColor = self.centerView.backgroundColor = [[Common shared] getCommonRedColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.titleTextField                       =  [self.titleView addCustomTextFieldView];
    [self.titleTextField setFieldText:JMOLocalizedString(@"Title*", nil) errorText:JMOLocalizedString(@"Please choose title", nil)];
    [self.titleTextField createDropDownMenuFromStringArray:@[JMOLocalizedString(@"Mr.", nil), JMOLocalizedString(@"Mrs.", nil), JMOLocalizedString(@"Miss", nil), JMOLocalizedString(@"Ms.", nil)] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:Title];
    [self.titleTextField setWhitePlaceHolderFromText:JMOLocalizedString(@"Please choose", nil)];
    [self.titleTextField.dropDownBtn setImage:[UIImage imageNamed:@"ic_keyboard_arrow_down_white_3x"] forState:UIControlStateNormal];
    [self.titleTextField.textField setViewHeight:32];
    self.titleTextField.textField.delegate = self;
    [self.titleTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
    [self.titleTextField setupWhiteColorTextField];
    self.firstNameTextField =   [self.firstNameView addCustomTextFieldView];;
    [self.firstNameTextField setFieldText:JMOLocalizedString(@"First Name*", nil) errorText:JMOLocalizedString(@"Please enter first name", nil)];
    [self.firstNameTextField setWhitePlaceHolderFromText:JMOLocalizedString(@"Please enter first name", nil)];
    self.firstNameTextField.textField.delegate              = self;
    [self.firstNameTextField setupWhiteColorTextField];
    self.emailTextField = [self.emailView addCustomTextFieldView];
    [self.emailTextField setTextFieldLoginEmailType];
    self.emailTextField.textField.delegate              = self;
    [self.emailTextField setFieldText:JMOLocalizedString(@"Email", nil) errorText:JMOLocalizedString(@"Please enter a valid email", nil)];
    [self.emailTextField setupWhiteColorTextField];
    [self.emailTextField setWhitePlaceHolderFromText:JMOLocalizedString(@"Please enter a valid email", nil)];
    [self.checkBtn addTarget:self action:@selector(checkAnimationSelected) forControlEvents:UIControlEventTouchDown];
    [self.checkBtn2 addTarget:self action:@selector(checkAnimationSelected2) forControlEvents:UIControlEventTouchDown];
    self.phoneTextField =  [self.phoneView addCustomTextFieldView];
    [self.phoneTextField setFieldText:JMOLocalizedString(@"Phone", nil) errorText:JMOLocalizedString(@"Please enter phone", nil)];
    [self.phoneTextField setTextFieldNumberType];
    [self.phoneTextField setWhitePlaceHolderFromText:JMOLocalizedString(@"Please enter phone", nil)];
    self.phoneTextField.textField.delegate              = self;
    [self.phoneTextField setupWhiteColorTextField];
    [self setUpTick];
    self.remarkField =  [self.remarkView addCustomTextFieldView];
    [self.remarkField setFieldText:JMOLocalizedString(@"Detail*", nil) errorText:JMOLocalizedString(@"Information is not correct", nil)];
    [self.remarkField setWhitePlaceHolderFromText:JMOLocalizedString(@"Information is not correct", nil)];
    self.remarkField.textField.delegate              = self;
    [self.remarkField setupWhiteColorTextField];
    self.waringLabel.text                                    = JMOLocalizedString(@"The option to hit (*) must be filled in.", nil);
    self.waringLabel.textColor                               = [UIColor whiteColor];
    [self.nextBtn setTitle:JMOLocalizedString(@"Send", nil) forState:UIControlStateNormal animation:NO];
    [self.nextBtn setUpSmallCornerBorderButton];
    self.nextBtn.titleLabel.font = [[Common shared] getNormalBoldTitleFont];
    self.nextBtn.backgroundColor = [UIColor whiteColor];
    [self.nextBtn setTitleColor:[[Common shared] getCommonRedColor] forState:UIControlStateNormal];
    
    [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
        
        [self.firstNameTextField checkValid];
        [self.titleTextField checkValid];
        [self.remarkField checkValid];
        
        
        if ([self.firstNameTextField checkValid] && [self.titleTextField checkValid] && [self.remarkField checkValid]) {
            
            
            if (self.emailTextField.textField.text.length > 0) {
                
                [self.emailTextField checkValid];
                
                
                if ([self.emailTextField checkValid]) {
                    
                     [self callSendUsEmailAPI];
                    
                }
                
            } else {
                
                 [self callSendUsEmailAPI];
                
            }

        }
        
    }];
    
}

- (void)callSendUsEmailAPI {
    
    if (self.checkView.on || self.checkView2.on) {
        
        NSString *needString;
        
        if (self.checkView.on) {
            
            needString = self.checkLabel.text;
            
        }
        
        if (self.checkView2.on) {
            
            needString = isValid(needString) ? [NSString stringWithFormat:@"%@,%@", needString , self.checkLabel2.text] : self.checkLabel2.text;
            
        }
        
        [self showLoadingHUD];
        [[ServerAPI shared] sendEmailToUsTitle:self.titleTextField.textField.text name:self.firstNameTextField.textField.text email:self.emailTextField.textField.text mobile:self.phoneTextField.textField.text need:needString remark:self.remarkField.textField.text CompletionBlock:^(id response, NSError *error) {
            
            [self hideLoadingHUD];
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                self.remarkField.textField.text = @"";
                
                [self showErrorFromMessage:JMOLocalizedString(@"email is sent successfully", nil) controller:self okBtn:nil];
                
            }
            
        }];
        
    } else {
        
        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                        style:0 handler:^(UIAlertAction *action) {
                                                            
                                                            
                                                        }];
        
        [self showErrorFromMessage:JMOLocalizedString(@"Please select an option", nil) controller:self okBtn:okBtn];
        
    }

}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    self.ztoreNavigationBar.usePromoBtn.hidden = YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
    [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
    [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.phoneTextField.textField) {
        
        if (range.length + range.location > textField.text.length) {
            
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return newLength <= 8;
        
    } else {
        
        return YES;
    }
}

- (void)setUpTick {
    
    [[Common shared] setUpTickFromCheckBox:self.checkView];
    self.checkView.tintColor        = [UIColor whiteColor];
    self.checkView.onTintColor      = [UIColor clearColor];
    self.checkView.onFillColor      = [UIColor whiteColor];
    self.checkView.onCheckColor     = [[Common shared] getCommonRedColor];
    [self.checkView reload];
    self.checkLabel.font    = [[Common shared] getNormalTitleFont];
    self.checkLabel.text    = JMOLocalizedString(@"Request a product", nil);
    self.checkLabel.textColor = [UIColor whiteColor];
    [[Common shared] setUpTickFromCheckBox:self.checkView2];
    self.checkView2.tintColor        = [UIColor whiteColor];
    self.checkView2.onTintColor      = [UIColor clearColor];
    self.checkView2.onFillColor      = [UIColor whiteColor];
    self.checkView2.onCheckColor     = [[Common shared] getCommonRedColor];
    [self.checkView2 reload];
    self.checkLabel2.font    = [[Common shared] getNormalTitleFont];
    self.checkLabel2.text    = JMOLocalizedString(@"Give other comment", nil);
    self.checkLabel2.textColor = [UIColor whiteColor];
}

- (void)checkAnimationSelected {
    
    self.checkView.tintColor = [UIColor whiteColor];
    [self.checkView setOn:!self.checkView.on animated:YES];
    
}

- (void)checkAnimationSelected2 {
    
    self.checkView2.tintColor = [UIColor whiteColor];
    [self.checkView2 setOn:!self.checkView2.on animated:YES];
    
}

@end
