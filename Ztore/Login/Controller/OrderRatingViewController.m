//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "OrderRatingViewController.h"
#import "OrderRatingCell.h"
#import "OrderCommentCell.h"
#import "ProductSearchHistoryTableViewHeader.h"

@implementation OrderRatingViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
    self.myAccountFristArray = @[JMOLocalizedString(@"Friendliness of Delivery man", nil), JMOLocalizedString(@"Appearance of Delivery man", nil), JMOLocalizedString(@"On-time deliveries", nil), JMOLocalizedString(@"Accuracy of goods", nil), JMOLocalizedString(@"Order Packaging", nil), JMOLocalizedString(@"Other comments", nil)];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    self.ratingArray = [[NSMutableArray alloc] init];
    
    if (![Common shared].didSelectOrderHistory.is_rated) {
        
        [self.ratingArray addObject:[NSNumber numberWithInt:2]];
        [self.ratingArray addObject:[NSNumber numberWithInt:2]];
        [self.ratingArray addObject:[NSNumber numberWithInt:2]];
        [self.ratingArray addObject:[NSNumber numberWithInt:2]];
        [self.ratingArray addObject:[NSNumber numberWithInt:2]];
        
    } else {
     
        [self.ratingArray addObject:[NSNumber numberWithInt:self.paymentdata.data.rating.rating_friendliness]];
        [self.ratingArray addObject:[NSNumber numberWithInt:self.paymentdata.data.rating.rating_appearance]];
        [self.ratingArray addObject:[NSNumber numberWithInt:self.paymentdata.data.rating.rating_ontime]];
        [self.ratingArray addObject:[NSNumber numberWithInt:self.paymentdata.data.rating.rating_accuracy]];
        [self.ratingArray addObject:[NSNumber numberWithInt:self.paymentdata.data.rating.rating_situation]];

    }

}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Rating", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
    self.ztoreNavigationBar.usePromoBtn.hidden = (self.paymentdata.data.rating.rating_accuracy == 0) ? NO : YES;
    [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(usePromoBtnPress) forControlEvents:UIControlEventTouchDown];

}

- (void)usePromoBtnPress {
    
    NSMutableArray *numberArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < self.myAccountFristArray.count; i++) {
        
        OrderRatingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        if (i < self.myAccountFristArray.count - 1) {
            
            [numberArray addObject:[self getIntFromUISlider:cell.sliderFromNibSideColors]];
            
        }
        
    }
    
    [[ServerAPI shared] ratingOrderServiceFromOrder_id:(int)self.paymentdata.data.id rating_friendliness:[numberArray objectAtIndex:0] rating_appearance:[numberArray objectAtIndex:1] rating_ontime:[numberArray objectAtIndex:2] rating_accuracy:[numberArray objectAtIndex:3] rating_situation:[numberArray objectAtIndex:4] remark:self.textView.text completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [Common shared].didSelectOrderHistory.is_rated = YES;
            self.paymentdata.data.rating.rating_comment = [Common shared].didSelectOrderHistory.rating.rating_comment = self.textView.text;
            self.paymentdata.data.rating.rating_friendliness = [[numberArray objectAtIndex:0] intValue];
            self.paymentdata.data.rating.rating_appearance = [[numberArray objectAtIndex:1] intValue];
            self.paymentdata.data.rating.rating_ontime = [[numberArray objectAtIndex:2] intValue];;
            self.paymentdata.data.rating.rating_accuracy = [[numberArray objectAtIndex:3] intValue];
            self.paymentdata.data.rating.rating_situation = [[numberArray objectAtIndex:4] intValue];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        
    }];

}

- (NSNumber *)getIntFromUISlider:(UISlider *)slider {
    
    if (slider.value <= 0.2) {
        
        return [NSNumber numberWithInt:1];
        
    } else if (slider.value <= 0.4) {
        
         return [NSNumber numberWithInt:2];
        
        
    } else if (slider.value <= 0.6) {
        
        return [NSNumber numberWithInt:3];
        
        
    } else if (slider.value <= 0.8) {
        
        return [NSNumber numberWithInt:4];
        
        
    } else if (slider.value <= 1.0) {
        
        return [NSNumber numberWithInt:5];
        
    }
    
    return [NSNumber numberWithInt:5];

}

- (void)setupView {
  
    [self.tableView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.myAccountFristArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.myAccountFristArray.count - 1) {
        
        static NSString *cellIdentifier = @"OrderCommentCell";
        OrderCommentCell *cell = (OrderCommentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell         = [nib objectAtIndex:0];
            
        }
 
        [cell setTitleText:self.myAccountFristArray[indexPath.row] paymentSuccess:self.paymentdata];
        self.textView = cell.textView;
        
        return cell;
    } else {
    
     static NSString *cellIdentifier = @"OrderRatingCell";
    OrderRatingCell *cell = (OrderRatingCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    
    [cell setTitleText:self.myAccountFristArray[indexPath.row] value:self.ratingArray[indexPath.row] enableSlider:(![Common shared].didSelectOrderHistory.is_rated)];

    return cell;
        
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     if (indexPath.row == self.myAccountFristArray.count - 1) {
      
         return 252;
     }
    
    return (![Common shared].didSelectOrderHistory.is_rated) ? 117 : 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 36.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  
    ProductSearchHistoryTableViewHeader *view = [ProductSearchHistoryTableViewHeader initHeader];
    [view setLabelText:JMOLocalizedString(@"Delivery Rating", nil)];
    
    return view;
}

@end
