//
//  RightMenuVC.m
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AppDelegate.h"
#import "FICDPhotoLoader.h"
#import "NSError+RENetworking.h"
#import "RealUtility.h"
#import "MyTableViewController.h"
#import "ShoppingCartModel.h"

@interface MyTableViewController ( )

@end

@implementation MyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    headerHeight             = 44;
    footerHeight             = 0;
    rowHeight                = 120;
    commonFontSize           = 10;
    [self setUpTableView];
    self.myTableView.emptyDataSetSource = self;
    self.myTableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.myTableView.tableFooterView = [UIView new];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self backToViewController];
    [self.myTableView reloadData];
    
}

- (void)backToViewController {
    
    if ([Common shared].backToViewController != nil) {
        
        if ([Common shared].backToViewController == [self class]) {
            
            [Common shared].backToViewController = nil;
            
        } else {
            
            [self.navigationController popViewControllerAnimated:NO];
        }
        
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.myTableView.userInteractionEnabled = YES;
    
}

- (void)setUpTableView {
    
    self.myTableView          = [[UITableView alloc] init];
    self.view                 = [[UIView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = self.myTableView.backgroundColor;
    [self.view addSubview:self.myTableView];
    self.myTableView.delegate                             = self;
    self.myTableView.dataSource                           = self;
    self.myTableView.allowsMultipleSelectionDuringEditing = NO;
    [self.myTableView matchSizeWithSuperViewNavigationBarFromAutoLayoutTop:0 bottom:-footerHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.tableViewType == MyFavouriteList) {
        
        return [Common shared].favouriteList == nil ? 0 : [Common shared].favouriteList.data.count;
        
    } else {
        
         return [Common shared].recentPurchaseList == nil ? 0 : [Common shared].recentPurchaseList.data.count;
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
      Products *product = (self.tableViewType == MyFavouriteList) ? [Common shared].favouriteList.data[indexPath.row] : [Common shared].recentPurchaseList.data[indexPath.row];
    
    static NSString *cellIdentifier = @"SlidingOrderTableViewCell";
    SlidingOrderTableViewCell *cell = (SlidingOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    [cell setUpOrderCellFromProduct:product ProductCellType: (self.tableViewType == MyFavouriteList) ? FavouriteType : RecentPurchaseType];
    cell.upBtn.tag   = indexPath.row;
    cell.downBtn.tag = indexPath.row;
    cell.tag         = indexPath.row;
    cell.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return rowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Products *product = (self.tableViewType == MyFavouriteList) ? [Common shared].favouriteList.data[indexPath.row] : [Common shared].recentPurchaseList.data[indexPath.row] ;

    self.myTableView.userInteractionEnabled = NO;
    [[Common shared] pushToProductDetailPageFromProduct:product];
    self.myTableView.userInteractionEnabled = YES;
}

#pragma mark MyFavouriteCellDelegate

- (void)deleteBtnPressedFromCell:(SlidingOrderTableViewCell *)cell product:(Products *)product {
    
   [self showLoadingHUD];
    
   [[ServerAPI shared] userListDeleteProductFromProduct_Id:(int)product.id completionBlock:^(id response, NSError *error) {
       
       [self hideLoadingHUD];
       
       if ([[Common shared] checkNotError:error controller:self response:response]) {

           if (self.tableViewType == MyFavouriteList) {
             
               [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationGetMyFavouriteList object:nil];
               
           }
           
       }
       
    }];
}

@end
