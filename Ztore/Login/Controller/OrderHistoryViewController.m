//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "OrderHistoryViewController.h"
#import "OrderHistoryCell.h"
#import "ConfirmOrderViewController.h"
#import "OrderRatingViewController.h"

@implementation OrderHistoryViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
  [self refreshDataArray];

    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    [super setPageName:@"My Account - Order History"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [self.tableView reloadData];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Order History ", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    self.ztoreNavigationBar.usePromoBtn.hidden = YES;


}

- (void)setupView {
    
    [self.tableView setBackgroundColor:[[Common shared] getProductDetailDescGrayBackgroundColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)callAPI {
    
    [[Common shared] getOrderHistoryCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self.tableView reloadData];
        }
        
    }];
    
}

- (void)backBtnPressed:(id)sender {
    
    if (self.orderHistoryType == MyAccountType) {
        
            [super backBtnPressed:sender];
        
    } else {
        
           [self backToMainPageGetCurrentUser:NO];
        
    }
    
}

- (void)refreshDataArray {
    
    NSString *zdollarString = (isValid([Common shared].userAccont.data.zdollar)) ? [Common shared].userAccont.data.zdollar : @"0" ;
    
    self.myAccountFristSubTitleArray = @[JMOLocalizedString(@"Edit Account Information", nil), JMOLocalizedString(@"Edit Delivery Address", nil), JMOLocalizedString(@"Edit My favorite list and Order history", nil), [NSString stringWithFormat:JMOLocalizedString(@"You have got (%d) order(s) not yet rated", nil), [[Common shared].orderHistory getCountFromNoRatingOrderHistory]], zdollarString];
    
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [Common shared].orderHistory.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     static NSString *cellIdentifier = @"OrderHistoryCell";
    OrderHistoryCell *cell = (OrderHistoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    

        
   [cell setTextFromPaymentData:[[Common shared].orderHistory.data objectAtIndex:indexPath.row]];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PaymentSuccessData *paymentData = [[Common shared].orderHistory.data objectAtIndex:indexPath.row];
    
    return (paymentData.newYearFirstObject) ? 143 : 104;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PaymentSuccessData *paymentData = [[Common shared].orderHistory.data objectAtIndex:indexPath.row];
    ConfirmOrderViewController *confirmOrderViewController = [[ConfirmOrderViewController alloc] init];
    confirmOrderViewController.orderType = History;
    confirmOrderViewController.orderId = (int)paymentData.id;
    [Common shared].didSelectOrderHistory = paymentData;
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.navigationController pushViewController:confirmOrderViewController animated:YES enableUI:NO];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

@end
