
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"

@interface MyAccountDetailViewController : _UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) NSArray *myAccountFristArray;
@property(strong, nonatomic) NSMutableArray *myAccountFristSubTitleArray;
@property(weak, nonatomic) IBOutlet UILabel *warningLabel;
@property(weak, nonatomic) IBOutlet UIView *cneterView;

@end
