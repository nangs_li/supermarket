//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "WebViewViewController.h"

@implementation WebViewViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    [self loadingFromColor:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)callAPI {
    
    [self setupView];
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.ztoreNavigationBar.usePromoBtn setHidden:YES];
    
}

- (void)setupView {
  
    self.ztoreNavigationBar.titleLabel.text = self.titleString;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    [self.webView loadRequest:request];
    [self.webView.scrollView setBounces:NO];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self endCallAPILoading];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self endCallAPILoading];
    
}

@end
