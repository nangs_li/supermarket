//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "FormViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "ActionSheetStringPicker.h"

@implementation FormViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    [self setupView];
    
}

- (void)setupFormType:(FormType)formType {
    self.formType = formType;
    [self setUpNavigationBar];
    
}

- (void)setupView {

  self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.facebookBtn.hidden = YES;
    self.oneView.hidden = NO;
    
    if (self.formType == ChangeEmail) {
        
        self.oneTextField                              = [self.oneView addCustomTextFieldView];
        [self.oneTextField setFieldText:JMOLocalizedString(@"Email*", nil) errorText:@""];
        self.oneTextField.textField.text                   = [Common shared].userAccont.data.email;
        self.oneTextField.textField.delegate               = self;
        self.oneTextField.textField.userInteractionEnabled = NO;
        self.twoView.hidden = self.threeView.hidden = self.fourView.hidden = self.fiveView.hidden = YES;
        self.facebookBtn.hidden = ([Common shared].userAccont.data.fb_user_id != nil) ? YES : NO;
        [self.facebookBtn setTitle:JMOLocalizedString(@"Link with Facebook", nil) forState:UIControlStateNormal animation:NO];
        self.facebookBtn.titleLabel.font = [[Common shared] getNormalTitleFont];
        
        FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
        loginButton.readPermissions = @[@"email"];
        loginButton.delegate = self;
        
        [[self.facebookBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {
            
            [loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        }];

        [super setPageName:@"My Account - User Information - Edit Email"];
    }

    if (self.formType == ChangeName) {
        
        self.twoTextField                              = [self.twoView addCustomTextFieldView];
        [self.twoTextField setFieldText:JMOLocalizedString(@"First Name*", nil) errorText:JMOLocalizedString(@"Please enter first name", nil)];
        [self.twoTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter first name", nil)];
        self.twoTextField.textField.delegate              = self;
        self.twoTextField.textField.text                   = ([Common shared].userAccont.data.first_name) ? [Common shared].userAccont.data.first_name : nil;
        self.fiveTextField                                  =  [self.fiveView addCustomTextFieldView];
        [self.fiveTextField setFieldText:JMOLocalizedString(@"Last Name*", nil) errorText:JMOLocalizedString(@"Please enter last name", nil)];
        [self.fiveTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter last name", nil)];
        self.fiveTextField.textField.delegate              = self;
        self.fiveTextField.textField.text                   = ([Common shared].userAccont.data.last_name) ? [Common shared].userAccont.data.last_name : nil;
        self.fourTextField                                    = [self.fourView addCustomTextFieldView];
        [self.fourTextField setFieldText:JMOLocalizedString(@"Title*", nil) errorText:JMOLocalizedString(@"Please choose area", nil)];
        
        
        
        
        [self.fourTextField createDropDownMenuFromStringArray:@[JMOLocalizedString(@"Mr.", nil), JMOLocalizedString(@"Mrs.", nil), JMOLocalizedString(@"Miss", nil), JMOLocalizedString(@"Ms.", nil)] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:Title];
        self.fourTextField.textField.delegate = self;
        [self.fourTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
        self.fourTextField.textField.text                   = ([[Common shared] getConsigneeTitleFromTitle:[Common shared].userAccont.data.title]) ? [[Common shared] getConsigneeTitleFromTitle:[Common shared].userAccont.data.title] : nil;
        self.fourTextField.textField.textColor = [UIColor blackColor];
        self.oneView.hidden = self.threeView.hidden = YES;
        self.twoView.hidden = self.fourView.hidden = self.fiveView.hidden = NO;
        [super setPageName:@"My Account - User Information - Edit Name and Title"];
    }
    
    if (self.formType == ChangePhone) {
        
        self.oneTextField                              = [self.oneView addCustomTextFieldView];
        [self.oneTextField setTextFieldNumberType];
        [self.oneTextField setFieldText:JMOLocalizedString(@"Phone*", nil) errorText:JMOLocalizedString(@"Please enter phone", nil)];
        [self.oneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter phone", nil)];
        self.oneTextField.textField.delegate              = self;
        self.oneTextField.textField.text                   = ([Common shared].userAccont.data.mobile) ? [Common shared].userAccont.data.mobile : nil;
       self.twoView.hidden = self.threeView.hidden = self.fourView.hidden = self.fiveView.hidden = YES;
       [super setPageName:@"My Account - User Information - Edit Mobile Phone"];
    }

    if (self.formType == ChangeOtherPhone) {
        
        self.oneTextField                              = [self.oneView addCustomTextFieldView];
        [self.oneTextField setTextFieldNumberType];
        [self.oneTextField setFieldText:JMOLocalizedString(@"Other Phone*", nil) errorText:JMOLocalizedString(@"Please enter other phone", nil)];
        [self.oneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter other phone", nil)];
        self.oneTextField.textField.delegate              = self;
         self.oneTextField.textField.text                   = ([Common shared].userAccont.data.contact_no) ? [Common shared].userAccont.data.contact_no : nil;
        self.oneTextField.notCheckTextField = YES;
        self.twoView.hidden = self.threeView.hidden = self.fourView.hidden = self.fiveView.hidden = YES;
        [super setPageName:@"My Account - User Information - Edit Other Number"];
    }

    
    if (self.formType == ChangeCompanyName) {
        
        self.oneTextField                              = [self.oneView addCustomTextFieldView];
        [self.oneTextField setFieldText:JMOLocalizedString(@"Company Name*", nil) errorText:JMOLocalizedString(@"Please enter company name", nil)];
        [self.oneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter company name", nil)];
        self.oneTextField.textField.delegate              = self;
        self.oneTextField.textField.text                   = ([Common shared].userAccont.data.company) ? [Common shared].userAccont.data.company : nil;
        self.oneTextField.notCheckTextField = YES;
        self.twoView.hidden = self.threeView.hidden = self.fourView.hidden = self.fiveView.hidden = YES;
        [super setPageName:@"My Account - User Information - Edit Company Name"];
        
    }
    
    if (self.formType == ChangePassword) {
        
        self.oneTextField                              = [self.oneView addCustomTextFieldView];
        [self.oneTextField setTextFieldPasswordType];
        [self.oneTextField setupNormalColorTextField];
        [self.oneTextField setFieldText:JMOLocalizedString(@"Old Password*", nil) errorText:JMOLocalizedString(@"Please enter old Password", nil)];
        [self.oneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter old Password", nil)];
        self.oneTextField.textField.delegate              = self;
        self.twoTextField                              = [self.twoView addCustomTextFieldView];
         [self.twoTextField setTextFieldPasswordType];
        [self.twoTextField setupNormalColorTextField];
        [self.twoTextField setFieldText:JMOLocalizedString(@"New Password*", nil) errorText:JMOLocalizedString(@"Please enter new Password", nil)];
        [self.twoTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter new Password", nil)];
        self.twoTextField.textField.delegate              = self;
        self.threeTextField                              = [self.threeView addCustomTextFieldView];
         [self.threeTextField setTextFieldPasswordType];
        [self.threeTextField setupNormalColorTextField];
        [self.threeTextField setFieldText:JMOLocalizedString(@"Confirm Password*", nil) errorText:JMOLocalizedString(@"Please enter confirm Password", nil)];
        [self.threeTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter confirm Password", nil)];
        self.threeTextField.textField.delegate              = self;
        self.twoView.hidden = self.threeView.hidden = NO;
        self.fourView.hidden = self.fiveView.hidden = YES;
        [super setPageName:@"My Account - User Information - Edit Password"];
        
    }
    
 }

- (void)setUpNavigationBar {

    
    NSString *titleLabel;
    
    if (self.formType == ChangeEmail) {
        
       titleLabel = [self getTitleLabelTextFromText:JMOLocalizedString(@"Email", nil)];
    }
    
    if (self.formType == ChangeName) {
        
       titleLabel = [self getTitleLabelTextFromText:JMOLocalizedString(@"Name And Title", nil)];
    }
    
    if (self.formType == ChangePhone) {
        
       titleLabel = [self getTitleLabelTextFromText:JMOLocalizedString(@"Phone", nil)];
        
    }
    
    if (self.formType == ChangeOtherPhone) {
        
       titleLabel = [self getTitleLabelTextFromText:JMOLocalizedString(@"Other Phone", nil)];
    }
    
    
    if (self.formType == ChangeCompanyName) {
        
        titleLabel = [self getTitleLabelTextFromText:JMOLocalizedString(@"Company Name", nil)];
        
    }
    
    if (self.formType == ChangePassword) {
        
        titleLabel = JMOLocalizedString(@"Change Password", nil);
    }
    
  self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
  [self.ztoreNavigationBar rightMenuBarType];
  self.ztoreNavigationBar.titleLabel.text = titleLabel;
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = NO;
  [self.ztoreNavigationBar.usePromoBtn setTitle:JMOLocalizedString(@"Save", nil) forState:UIControlStateNormal];
  [self.ztoreNavigationBar.usePromoBtn addTarget:self action:@selector(pressUsePromoBtn:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.titleLabel.font      = [[Common shared] getNormalTitleFont];
    
}

- (NSString *)getTitleLabelTextFromText:(NSString *)text {
    
    return ([[Common shared] isEnLanguage]) ? [NSString stringWithFormat:@"%@ %@", JMOLocalizedString(@"Set", nil), text] : [NSString stringWithFormat:@"%@%@", JMOLocalizedString(@"Set", nil), text];
}

- (void)pressUsePromoBtn:(id)sender {
    
        [self.oneTextField checkValid];
        [self.twoTextField checkValid];
        [self.threeTextField checkValid];
        [self.fourTextField checkValid];
        [self.fiveTextField checkValid];
    
    BOOL formValid = NO;
    
    if (self.formType == ChangeEmail) {
        
        formValid = [self.oneTextField checkValid];
        
    }
    
    if (self.formType == ChangeName) {
        
        formValid = [self.twoTextField checkValid] && [self.fourTextField checkValid] && [self.fiveTextField checkValid];
         [Common shared].userAccont.data.first_name = self.twoTextField.textField.text;
         [Common shared].userAccont.data.last_name = self.fiveTextField.textField.text;
         [Common shared].userAccont.data.title = [[Common shared] getConsignee_titleIdFromConsignee_title:self.fourTextField.textField.text];
    }
    
    if (self.formType == ChangePhone) {
        
        formValid = [self.oneTextField checkValid];
        [Common shared].userAccont.data.mobile = self.oneTextField.textField.text;
        
    }
    
    if (self.formType == ChangeOtherPhone) {
        
        formValid = [self.oneTextField checkValid];
        [Common shared].userAccont.data.contact_no = self.oneTextField.textField.text;
    }
    
    
    if (self.formType == ChangeCompanyName) {
        
        formValid = [self.oneTextField checkValid];
        [Common shared].userAccont.data.company = self.oneTextField.textField.text;
        
    }
    
    if (self.formType == ChangePassword) {
        
        formValid = [self.oneTextField checkValid] && [self.twoTextField checkValid] && [self.threeTextField checkValid];
        
    }

    
        if (formValid) {
            
            if (self.formType == ChangePassword) {
                
                if ([self.twoTextField.textField.text isEqualToString:self.threeTextField.textField.text]) {
                    
                    [[ServerAPI shared] changePasswordWithOldPassword:self.oneTextField.textField.text newPassword:self.twoTextField.textField.text completionBlock:^(id response, NSError *error) {
                        
                        if ([[Common shared] checkNotError:error controller:self response:response]) {
                            
                            
                            [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
                                
                                if ([[Common shared] checkNotError:error controller:self response:response]) {
                                    
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                                
                            }];
                            
                        }
                        
                    }];
                    
                } else {
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                                    style:0 handler:^(UIAlertAction *action) {
                                                                        
                                                                        
                                                                    }];

                   [self showErrorFromMessage:JMOLocalizedString(@"Password and Confirm Password are not same", nil) controller:self okBtn:okBtn];
                }
                
            } else {
            
            InputUpdateCurrentUser *input = [[InputUpdateCurrentUser alloc] init];
            input.title = [NSString stringWithFormat:@"%ld", (long)[Common shared].userAccont.data.title];
            input.first_name = [Common shared].userAccont.data.first_name;
            input.last_name = [Common shared].userAccont.data.last_name;
            input.company = [Common shared].userAccont.data.company;
            input.mobile = [Common shared].userAccont.data.mobile;
            input.contact_no = [Common shared].userAccont.data.contact_no;
            input.email = [Common shared].userAccont.data.email;

            [[ServerAPI shared] updateCurrentUserWithInput:input completionBlock:^(id response, NSError *error) {
                
                if ([[Common shared] checkNotError:error controller:self response:response]) {
                    
                   
                    [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
                        
                        if ([[Common shared] checkNotError:error controller:self response:response]) {
                            
                            [self.navigationController popViewControllerAnimated:YES];

                        }
                        
                    }];
  
                }
                
            }];
            }
            
        }
   
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

  if (self.formType == ChangeOtherPhone || self.formType == ChangePhone) {

    if (range.length + range.location > textField.text.length) {

      return NO;
    }

    NSUInteger newLength = [textField.text length] + [string length] - range.length;

    return newLength <= 8;

  } else {

    return YES;
  }
}

#pragma mark FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    
    if (result.token.tokenString) {
        
        [self showLoadingHUD];
        
        [[ServerAPI shared] connectFbAccountWithLoginedFromAccessToken:result.token.tokenString completionBlock:^(id response, NSError *error) {
            
            if ([[Common shared] checkNotError:error controller:self response:response]) {
                
                [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
                    
                    if ([[Common shared] checkNotError:error controller:self response:response]) {
                        
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    
                }];
                
            } else {
                
                [self handleErrorFromErrorCode:[response getErrorCode] methodName:@"connectFbAccount"];
            }
            
        }];

        
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

- (void)handleErrorFromErrorCode:(NSInteger)errorCode methodName:(NSString *)methodName {
    
    NSString *message;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                    style:0 handler:^(UIAlertAction *action) {
                                                        
                                                        
                                                    }];
    
    if (errorCode == PHP_ERROR) {
        message = JMOLocalizedString(@"Your Facebook account have already been linked with another Ztore account. Please send your info to inquiry@ztore.com and we will get back to you soon.", nil);
        
        okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                         style:0 handler:^(UIAlertAction *action) {
                                             
                                         
                                             
                                             
                                         }];
        
    }
    
    [self showErrorFromMessage:message controller:self okBtn:okBtn];
}

@end
