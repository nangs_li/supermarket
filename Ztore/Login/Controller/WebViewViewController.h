
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

@interface WebViewViewController : _UIViewController <UIWebViewDelegate>

@property(strong, nonatomic) NSString *titleString;
@property(strong, nonatomic) NSString *urlString;
@property(strong, nonatomic) IBOutlet UIWebView *webView;

- (void)setupView;

@end
