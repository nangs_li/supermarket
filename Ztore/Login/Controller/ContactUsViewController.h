
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import "CustomTextFieldView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "DeliveryAddress.h"

@interface ContactUsViewController : _UIViewController <UIScrollViewDelegate>

@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *titleDetailLabel;
@property(weak, nonatomic) IBOutlet UIView *centerView;
@property(strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UIView *titleView;
@property(strong, nonatomic) IBOutlet UIView *firstNameView;
@property(strong, nonatomic) IBOutlet UIView *emailView;
@property(strong, nonatomic) IBOutlet UIView *phoneView;
@property(strong, nonatomic) IBOutlet UIView *remarkView;
@property(strong, nonatomic) IBOutlet BEMCheckBox *checkView;
@property(strong, nonatomic) IBOutlet UIButton *checkBtn;
@property(strong, nonatomic) IBOutlet UILabel *checkLabel;
@property(strong, nonatomic) IBOutlet BEMCheckBox *checkView2;
@property(strong, nonatomic) IBOutlet UIButton *checkBtn2;
@property(strong, nonatomic) IBOutlet UILabel *checkLabel2;
@property(strong, nonatomic) CustomTextFieldView *titleTextField;
@property(strong, nonatomic) CustomTextFieldView *firstNameTextField;
@property(strong, nonatomic) CustomTextFieldView *emailTextField;
@property(strong, nonatomic) CustomTextFieldView *phoneTextField;
@property(strong, nonatomic) CustomTextFieldView *remarkField;
@property(strong, nonatomic) IBOutlet UILabel *waringLabel;
@property(strong, nonatomic) IBOutlet UIButton *nextBtn;

@end
