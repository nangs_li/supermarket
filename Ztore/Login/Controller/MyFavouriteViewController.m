//
//  OnSaleViewController.m
//  Ztore
//
//  Created by ken on 30/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "KLCPopup.h"
#import "MyFavouriteViewController.h"
#import "MyTableViewController.h"

@interface MyFavouriteViewController ( ) {

  NSMutableArray *fragments;
    
}

@end

@implementation MyFavouriteViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [super setPageName:@"My Account - My Favourite "];
    [self loadingFromColor:nil];
}

- (void)viewWillAppear:(BOOL)animated {

  [super viewWillAppear:animated];
  [self setTabeScrollViewTop];
  [TSMessage setDelegate:self];
  [TSMessage setDefaultViewController:self];
  [self enableSlidePanGestureForRightMenu];

}

- (void)setTabeScrollViewTop {
    
    if (self.tabScrollView == nil) {
        
        for (UIView *view in self.view.subviews) {
            
            if ([view isKindOfClass:[UIScrollView class]]) {
                
                self.tabScrollView = (UIScrollView *)view;
                
            }
            
        }
        
        [self.tabScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.tabScrollView.superview).with.offset(0);
            make.left.equalTo(self.tabScrollView.superview).with.offset(0);
            make.right.equalTo(self.tabScrollView.superview).with.offset(0);
            make.height.mas_equalTo(60);
            
        }];
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {

  [super viewWillDisappear:animated];
    
}

- (void)callAPI {
    
    fragments = [[NSMutableArray alloc] init];
    self.myTableViewController = [[MyTableViewController alloc] initWithNibName:@"MyTableViewController" bundle:nil];
    self.myTableViewController.tableViewType = MyFavouriteList;
    [fragments addObject:self.myTableViewController];
    self.myTableViewController2 = [[MyTableViewController alloc] initWithNibName:@"MyTableViewController" bundle:nil];
    self.myTableViewController2.tableViewType = OrderHistoryType;
    [fragments addObject:self.myTableViewController2];
    [self reloadTabViewDataNotification];
    
    [self setUpNavigationBar];
    [self setUpGetMyFavouriteListFromMyTableViewNotification];
    [self setUpTableViewDataFromMyTableViewNotification];
    [self endCallAPILoading];
    
    [[Common shared] getFavouriteListCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self.myTableViewController.myTableView reloadData];
            [self endCallAPILoading];
            [self reloadTabViewDataNotification];
            [self refreshPageMenu];
        }
        
    }];
    
    
    [[Common shared] getRecentPurchaseListCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self.myTableViewController2.myTableView reloadData];
            [self endCallAPILoading];
            [self reloadTabViewDataNotification];
            [self refreshPageMenu];
        }
        
    }];
    
}

- (void)refreshPageMenu {
    
    if (isValid(self.pageMenu)) {
        
        [self.pageMenu.view removeFromSuperview];
    }
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:247.0 / 255.0 green:247.0 / 255.0 blue:247.0 / 255.0 alpha:0.75],
                                 CAPSPageMenuOptionViewBackgroundColor: self.myTableViewController.view.backgroundColor,
                                 CAPSPageMenuOptionSelectionIndicatorColor: [[Common shared] getCommonRedColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor clearColor],
                                 CAPSPageMenuOptionMenuItemFont: [[Common shared] getContentBoldFont],
                                 CAPSPageMenuOptionMenuHeight: @(60.0),
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor:[[Common shared] getCommonRedColor],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor:[[Common shared] getCommonGrayColor],
                                 CAPSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth:@(YES),
                                 CAPSPageMenuOptionMenuMargin:@(0.0),
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl:@(YES),
                                 CAPSPageMenuOptionCenterMenuItems:@(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:fragments frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    _pageMenu.scrollAnimationDurationOnMenuItemTap = 100;
     [_pageMenu.menuScrollView drawShadowOpacity];
    _pageMenu.delegate = self;
    [self.view addSubview:_pageMenu.view];
    
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)setUpNavigationBar {
    
    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.ztoreNavigationBar productDetailBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"My Favourite", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.rightMenuImage setImage:[UIImage imageNamed:@"ic_shopping_cart_white_24pt"]];
    [self.ztoreNavigationBar.rightBtn addTarget:self action:@selector(rightBtnPress) forControlEvents:UIControlEventTouchDown];

}

- (void)rightBtnPress {
    
     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOpenRightMenu object:nil];
    
}

- (void)reloadTabViewDataNotification {
    
        NSString *tagTitle1 = [NSString stringWithFormat:JMOLocalizedString(@"My favorite list(%d)", nil), [Common shared].favouriteList.data.count];
        self.myTableViewController.title = [NSString stringWithFormat:@"  %@   ", tagTitle1];
        NSString *tagTitle2 = [NSString stringWithFormat:JMOLocalizedString(@"Order history(%d)", nil), [Common shared].recentPurchaseList.data.count];
        self.myTableViewController2.title = [NSString stringWithFormat:@"  %@   ", tagTitle2];
        [self hideLoadingHUD];
        [self refreshPageMenu];
    
}

- (void)backBtnPressed:(id)sender {
    
    [self rac_willDeallocSignal];
    [self rac_deallocDisposable];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
}

- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index {
    
}

- (void)setUpTableViewDataFromMyTableViewNotification {
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationSetUpTableViewDataFromMyTableView object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        [self.myTableViewController.myTableView reloadData];
        [self.myTableViewController2.myTableView reloadData];
        [self reloadTabViewDataNotification];
        
}];
    
}

- (void)setUpGetMyFavouriteListFromMyTableViewNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMyFavouriteList) name:kNotificationGetMyFavouriteList object:nil];
    
}

- (void)getMyFavouriteList {
    
    [[ServerAPI shared] userListListProductCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            if ([response getErrorCode] == STATUS_OK) {
                
                [self hideLoadingHUD];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpTableViewDataFromMyTableView object:nil userInfo:nil];
            }
            
            if ([response getErrorCode] == NO_DATA) {
                
                [Common shared].favouriteList = [[ProductDetailArray alloc] init];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpTableViewDataFromMyTableView object:nil];
                
            }

            
        }
        
    }];
}

@end
