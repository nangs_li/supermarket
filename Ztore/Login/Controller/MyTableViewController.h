//
//  RightMenuVC.h
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ServerAPI.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SlidingOrderTableViewCell.h"

typedef NS_ENUM(NSInteger, TableViewType) { MyFavouriteList, OrderHistoryType};

@interface MyTableViewController : _UIViewController <MBProgressHUDDelegate, UITableViewDelegate, UITableViewDataSource, MyFavouriteCellDelegate> {
  int selectedIndex;
  int headerHeight;
  int footerHeight;
  int rowHeight;
  int commonFontSize;
}

@property(assign, nonatomic) TableViewType tableViewType;
@property(strong, nonatomic) UITableView *myTableView;

@end
