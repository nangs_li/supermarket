
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "ZDollarModel.h"
#import "ZdollarHeaderView.h"

@interface ZDollarViewController : _UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(weak, nonatomic) IBOutlet UITableView *tableView;;
@property(weak, nonatomic) IBOutlet UILabel *zDollarsLabel;
@property(weak, nonatomic) IBOutlet UILabel *zDollarsRemainLabel;
@property(strong, nonatomic) ZdollarHeaderView *headerView;

@end
