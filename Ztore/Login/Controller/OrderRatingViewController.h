
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "CustomTextFieldView.h"
#import "DeliveryAddress.h"
#import "OrderHistory.h"

@interface OrderRatingViewController : _UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(strong, nonatomic) PaymentSuccess *paymentdata;
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong, nonatomic) NSArray *myAccountFristArray;
@property(strong, nonatomic) NSMutableArray *ratingArray;
@property(strong, nonatomic) UITextView *textView;

@end
