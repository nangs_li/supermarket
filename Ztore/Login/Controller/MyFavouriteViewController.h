//
//  ProductCategoryController.h
//  Ztore
//
//  Created by ken on 19/7/16.
//  Copyright © ken. All rights reserved.
//

#import "CAPSPageMenu.h"
#import <UIKit/UIKit.h>
@class MyTableViewController;

@interface MyFavouriteViewController : _UIViewController <UIViewControllerTransitioningDelegate, TSMessageViewProtocol, MBProgressHUDDelegate, CAPSPageMenuDelegate>

@property(nonatomic) NSInteger parentCatId;
@property(nonatomic, strong) UIScrollView *tabScrollView;
@property(strong, nonatomic) MyTableViewController *myTableViewController;
@property(strong, nonatomic) MyTableViewController *myTableViewController2;
@property(strong, nonatomic) CAPSPageMenu *pageMenu;

@end
