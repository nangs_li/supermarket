//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "RegisterViewController.h"

#import "DeliveryAddressViewController.h"
#import "KxMenu.h"
#import <QuartzCore/QuartzCore.h>

@implementation RegisterViewController

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  [self setupView];
  [super setPageName:@"Checkout - Edit User"];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
}

- (void)setupView {

  self.automaticallyAdjustsScrollViewInsets = NO;
  self.topWaringLabel.text                  = JMOLocalizedString(@"Since you haven't input personal data after registration, please finish this part below to proceed.", nil);
  self.topWaringLabel.font                  = [[Common shared] getNormalTitleFont];
  self.emailTextField                       = [self.emailView addCustomTextFieldView];
  [self.emailTextField setFieldText:JMOLocalizedString(@"Email*", nil) errorText:@""];
  LoginData *loginData                                 = [[REDatabase sharedInstance] getLoginDataFromID:@"123"];
  self.emailTextField.textField.text                   = loginData.email;
  self.emailTextField.textField.delegate               = self;
  self.emailTextField.textField.userInteractionEnabled = NO;
  self.firstNameTextField                              = [self.firstNameView addCustomTextFieldView];
  [self.firstNameTextField setFieldText:JMOLocalizedString(@"First Name*", nil) errorText:JMOLocalizedString(@"Please enter first name", nil)];
  [self.firstNameTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter first name", nil)];
  self.firstNameTextField.textField.delegate              = self;
  self.lastNameTextField                                  =  [self.lastNameView addCustomTextFieldView];
  [self.lastNameTextField setFieldText:JMOLocalizedString(@"Last Name*", nil) errorText:JMOLocalizedString(@"Please enter last name", nil)];
  [self.lastNameTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter last name", nil)];
  self.lastNameTextField.textField.delegate              = self;
  self.titleTextField                                    = [self.titleView addCustomTextFieldView];
  [self.titleTextField setFieldText:JMOLocalizedString(@"Title*", nil) errorText:JMOLocalizedString(@"Please choose area", nil)];
  [self.titleTextField createDropDownMenuFromStringArray:@[JMOLocalizedString(@"Mr.", nil), JMOLocalizedString(@"Mrs.", nil), JMOLocalizedString(@"Miss", nil), JMOLocalizedString(@"Ms.", nil)] placeholder:JMOLocalizedString(@"Please choose", nil) dropDownMenutype:Title];
  self.titleTextField.textField.delegate = self;
  [self.titleTextField.dropDownBtn addTarget:self action:@selector(pressChooseTitle:) forControlEvents:UIControlEventTouchDown];
  self.companyNameTextField = [self.companyNameView addCustomTextFieldView];
  [self.companyNameTextField setFieldText:JMOLocalizedString(@"Company Name", nil) errorText:JMOLocalizedString(@"Please enter company name", nil)];
  [self.companyNameTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter company name", nil)];
  self.companyNameTextField.textField.delegate              = self;
  self.phoneTextField                                       = [self.phoneView addCustomTextFieldView];
  [self.phoneTextField setFieldText:JMOLocalizedString(@"Phone*", nil) errorText:JMOLocalizedString(@"Please enter phone", nil)];
  [self.phoneTextField setTextFieldNumberType];
  [self.phoneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter phone", nil)];
  self.phoneTextField.textField.delegate              = self;
  self.OtherPhoneTextField                            = [self.OtherPhoneView addCustomTextFieldView];
  [self.OtherPhoneTextField setFieldText:JMOLocalizedString(@"Other Phone", nil) errorText:JMOLocalizedString(@"Please enter other phone", nil)];
  [self.OtherPhoneTextField setTextFieldNumberType];
  [self.OtherPhoneTextField setPlaceHolderFromText:JMOLocalizedString(@"Please enter other phone", nil)];
  self.OtherPhoneTextField.textField.delegate              = self;
  self.waringLabel.text                                    = JMOLocalizedString(@"The option to hit (*) must be filled in.", nil);
  self.waringLabel.textColor                               = [[Common shared] getCommonRedColor];
  [self.nextBtn setTitle:JMOLocalizedString(@"Next", nil) forState:UIControlStateNormal animation:NO];
  [self.nextBtn setUpSmallCornerBorderButton];

  [[self.nextBtn rac_signalForControlEvents:UIControlEventTouchDown] subscribeNext:^(id x) {

    [self.emailTextField checkValid];
    [self.firstNameTextField checkValid];
    [self.lastNameTextField checkValid];
    [self.titleTextField checkValid];
    [self.companyNameTextField checkValid];
    [self.phoneTextField checkValid];
    [self.OtherPhoneTextField checkValid];
      
      if ([self.emailTextField checkValid] && [self.firstNameTextField checkValid] && [self.lastNameTextField checkValid] && [self.titleTextField checkValid] && [self.phoneTextField checkValid]) {
      
      InputUpdateCurrentUser *input = [[InputUpdateCurrentUser alloc] init];
      input.title = [NSString stringWithFormat:@"%d", [[Common shared] getConsignee_titleIdFromConsignee_title:self.titleTextField.textField.text]];
      input.first_name = self.firstNameTextField.textField.text;
      input.last_name = self.lastNameTextField.textField.text;
      input.company = self.companyNameTextField.textField.text;
      input.mobile = self.phoneTextField.textField.text;
      input.contact_no = self.OtherPhoneTextField.textField.text;
      input.email = self.emailTextField.textField.text;
      
      [[ServerAPI shared] updateCurrentUserWithInput:input completionBlock:^(id response, NSError *error) {

          if ([[Common shared] checkNotError:error controller:self response:response]) {
              
              [[ServerAPI shared] getCurrentUserCompletionBlock:^(id response, NSError *error) {
                  
                  if ([[Common shared] checkNotError:error controller:self response:response]) {
                      
              DeliveryAddressViewController *deliveryAddressViewController = [[DeliveryAddressViewController alloc] initWithNibName:@"DeliveryAddressViewController" bundle:nil];
              deliveryAddressViewController.editType = EditInCheckOut;
              [self.navigationController pushViewController:deliveryAddressViewController animated:YES enableUI:NO];

                  }
                  
                  }];
          }
          
      }];
      
      }
  }];
}

- (void)setUpNavigationBar {

  self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
  [self.ztoreNavigationBar rightMenuBarType];
  self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"Account Information", nil);
  [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
  self.ztoreNavigationBar.usePromoBtn.hidden = YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldBegin];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

  CustomTextFieldView *textFieldView = (CustomTextFieldView *)textField.superview;
  [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEndTextField object:nil];
  [textFieldView textFieldEnd];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

  if (textField == self.phoneTextField.textField || textField == self.OtherPhoneTextField.textField) {

    if (range.length + range.location > textField.text.length) {

      return NO;
    }

    NSUInteger newLength = [textField.text length] + [string length] - range.length;

    return newLength <= 8;

  } else {

    return YES;
  }
}

@end
