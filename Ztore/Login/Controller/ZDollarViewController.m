//
//  PopUpCircularProgressController.m
//  productionreal2
//
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Real. All rights reserved.
//

#import "ZDollarViewController.h"
#import "ZDollarCell.h"

@implementation ZDollarViewController {
    
     int headerHeight;
}

#pragma mark App LifeCycle

- (void)viewDidLoad {

  [super viewDidLoad];
  [self setUpNavigationBar];
  self.automaticallyAdjustsScrollViewInsets = NO;
  [self setupView];
  headerHeight = 77;
    
    [[Common shared] getZDollarModelCompletionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:self response:response]) {
            
            [self.tableView reloadData];
        }
        
    }];

    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    [self.tableView reloadData];
    [self.view setBackgroundColor:[[Common shared] getCommonPinkRedColor]];
    [super setPageName:@"My Account - ZDollar"];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self backToViewController];
    self.zDollarsLabel.text = [[Common shared] getStringFromPriceString:[NSString stringWithFormat:@"%@", [Common shared].userAccont.data.zdollar]];

    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.ztoreNavigationBar.contentView setBackgroundColor:[[Common shared] getCommonRedColor]];
    
}

- (void)setUpNavigationBar {

    self.ztoreNavigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;

    [self.ztoreNavigationBar rightMenuBarType];
    self.ztoreNavigationBar.titleLabel.text = JMOLocalizedString(@"My Z-Dollar", nil);
    [self.ztoreNavigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchDown];
    [self.ztoreNavigationBar.usePromoBtn setHidden:YES];
    [self.ztoreNavigationBar.contentView setBackgroundColor:[[Common shared] getCommonPinkRedColor]];

}

- (void)setupView {
    
    self.zDollarsRemainLabel.text = JMOLocalizedString(@"Credit Balance", nil);
    self.zDollarsRemainLabel.textColor = [UIColor whiteColor];
    self.zDollarsRemainLabel.font = [[Common shared] getSmallContentNormalFont];
    
    self.zDollarsLabel.text = @"";
    self.zDollarsLabel.textColor = [UIColor whiteColor];
    [self.zDollarsLabel setFont:[[Common shared] getNormalFontWithSize:33]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  ([Common shared].zDollarModel.data.count == 0) ? 0 : [Common shared].zDollarModel.data.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     static NSString *cellIdentifier = @"ZDollarCell";
    ZDollarCell *cell = (ZDollarCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell         = [nib objectAtIndex:0];
        
    }
    
    [cell setDataFromZDollarData:[Common shared].zDollarModel.data[indexPath.row]];

    return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 72;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   
    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"ZdollarHeaderView" owner:self options:nil] objectAtIndex:0];
    [self.headerView setupView];
    
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
   
    return headerHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 1;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    self.headerView.lineView.alpha = self.headerView.zDollarDescLabel.alpha = (headerHeight - scrollView.contentOffset.y) / headerHeight;
}

@end
