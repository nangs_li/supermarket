//
//  NSString+Utility.m
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import "Common.h"
#import "UILabel+Utility.h"

@implementation UILabel (Utility)

- (void)setUpTitleLabelFromText:(NSString *)text {
    
    self.font            = [[Common shared] getNormalTitleFont];
    self.textColor       = [[Common shared] getCommonRedColor];
    self.text = text;
    
}

- (CGSize)sizeOfMultiLineLabel {
    
    //Label text
    NSString *aLabelTextString = [self text];
    
    //Label font
    UIFont *aLabelFont = [self font];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.frame.size.width;
    
    
           return [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aLabelFont
                                                        }
                                              context:nil].size;
    
    
    return [self bounds].size;
    
}

@end
