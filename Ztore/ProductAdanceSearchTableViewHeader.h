//
//  ProductAdanceSearchTableViewHeader.h
//  Ztore
//
//  Created by ken on 27/7/16.
//  Copyright © ken. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLExpandableTableView.h"

@interface ProductAdanceSearchTableViewHeader : UITableViewCell <UIExpandingTableViewCell>

@property(weak, nonatomic) IBOutlet UILabel *label_title;
@property(weak, nonatomic) IBOutlet UIImageView *imageView_right;
@property(weak, nonatomic) IBOutlet UILabel *expandLabel;
@property(assign, nonatomic, getter=isLoading) BOOL loading;
@property(readonly, nonatomic) UIExpansionStyle expansionStyle;

- (void)setExpandImageHidden:(BOOL)hidden;
- (void)runSpinAnimationOnViewDuration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;

@end
