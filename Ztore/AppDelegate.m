//
//  AppDelegate.m
//  Ztore
//
//  Created by ken on 9/6/15.
//  Copyright (c) ken. All rights reserved.
//

#import "AppDelegate.h"
#import "Common.h"
#import "FullImageViewController.h"
#import "LeftMenuVC.h"
#import "ProductDetailController.h"
#import "REDatabase.h"
#import <AFNetworking/AFNetworking.h>
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import <Google/Analytics.h>
#import "BraintreeCore.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MyFavouriteViewController.h"
#import "ProductListCollectionViewController.h"
#import "RightMenuVC.h"
#import <OneSignal/OneSignal.h>

@interface AppDelegate ( )

@end

NSString *BraintreeDemoAppDelegatePaymentsURLScheme = @"com.innoverz.ztorehk.payments";

@implementation AppDelegate

+ (AppDelegate *)getAppDelegate {

  return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

  [OneSignal initWithLaunchOptions:launchOptions appId:@"dc85e8b6-a392-4648-ae6b-b8ce6937207d"];
    
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  NSError *configureError;
  [[GGLContext sharedInstance] configureWithError:&configureError];
  NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

  // Optional: configure GAI options.
  GAI *gai                    = [GAI sharedInstance];
  gai.trackUncaughtExceptions = YES; // report uncaught exceptions
  [self setupLoggings];
  [[AFNetworkReachabilityManager sharedManager] startMonitoring];
  // crash report
  [Fabric with:@[ [Crashlytics class] ]];
  // set PayPal payment setting
  [BTAppSwitch setReturnURLScheme:BraintreeDemoAppDelegatePaymentsURLScheme];
  [self setUploadChangeProductQtyNotification];
    UIStoryboard *storyboard               = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MainVCNavigation"];
    self.window                            = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor            = [[Common shared] getProductDetailDescGrayBackgroundColor];
    self.window.rootViewController         = viewController;
    [self.window makeKeyAndVisible];
    
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void){
     
  [[ServerAPI shared] initEncryptKeyCompletionBlock:^(id _Nullable response, NSError *_Nullable error) {

      if ([[Common shared] checkNotError:error controller:self.window.rootViewController response:response]) {
          
          [self.productCategoryController callMainPageAPI];
          [self.rightMenuVC getShoppingCart];
      
      //       account:felix.hui.ios@innoverz.com
      //      password:Inno_2828
      
      // set default use_zdollar
      
      //       account:mobileiostester@ztore.com
      //      password:iostester
      
//      [[ServerAPI shared] loginWithEmail:@"felix.hui.ios@innoverz.com" password:@"Inno_2828" completionBlock:^(id response, NSError *error) {
//          
//      }];
//      
    }
      
    [[Common shared].specialyRequest initString];
    UIView *commonRedBar = [[AppDelegate getAppDelegate].window getCommonRedStatusBar];
    [commonRedBar mas_makeConstraints:^(MASConstraintMaker *make) {

      make.top.equalTo(commonRedBar.superview).with.offset(0); // with is an optional semantic filler
      make.height.mas_equalTo(20);
      make.width.mas_equalTo([RealUtility screenBounds].size.width);

    }];
    
  }];

      });
#pragma mark - Set All TableView Separator

  [[UITableView appearance] setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
  [[UITableView appearance] setSeparatorColor:[[Common shared] getLineColor]];
  [[UITableView appearance] setSeparatorInset:UIEdgeInsetsZero];
  [[UITableViewCell appearance] setSeparatorInset:UIEdgeInsetsZero];

  if ([UITableView instancesRespondToSelector:@selector(setLayoutMargins:)]) {

    [[UITableView appearance] setLayoutMargins:UIEdgeInsetsZero];
    [[UITableViewCell appearance] setLayoutMargins:UIEdgeInsetsZero];
    [[UITableViewCell appearance] setPreservesSuperviewLayoutMargins:NO];
  }
    
#pragma mark - Set All UINavigationBar
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
  return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 90000

- (BOOL)application:(__unused UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options {
    
    if ([[url.scheme lowercaseString] isEqualToString:[BraintreeDemoAppDelegatePaymentsURLScheme lowercaseString]]) {
        
        return [BTAppSwitch handleOpenURL:url options:options];
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

#endif

// Deprecated in iOS 9, but necessary to support < versions
- (BOOL)application:(__unused UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(__unused id)annotation {
    
    if ([[url.scheme lowercaseString] isEqualToString:[BraintreeDemoAppDelegatePaymentsURLScheme lowercaseString]]) {
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - Loggings Helpers

- (void)setupLoggings {

  setenv("XcodeColors", "YES", 0);
  [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:ddLogLevel];
  [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
  [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:242 / 255.0 green:58 / 255.0 blue:67 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_ERROR];
  [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:58 / 255.0 green:169 / 255.0 blue:242 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_DB];
  [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:242 / 255.0 green:113 / 255.0 blue:58 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_API];
  [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:172 / 255.0 green:242 / 255.0 blue:58 / 255.0 alpha:1.0] backgroundColor:nil forFlag:LOG_FLAG_INFO];
  [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor lightGrayColor] backgroundColor:nil forFlag:LOG_FLAG_DEBUG];

  DDLogDebug(@"Debug mode = %@", kDebugMode ? @"YES" : @"NO");
}

- (void)setUploadChangeProductQtyNotification {
    
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNotificationCallAddProductAPIFromProductListChangeProductQty object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        
        if (self.addShoppingCartFromExistingCartTimer) {
            
            [self.addShoppingCartFromExistingCartTimer invalidate];
        }
        
        self.addShoppingCartFromExistingCartTimer = [NSTimer scheduledTimerWithTimeInterval:INCREASE_PRODUCT_API_SPEED target:self selector:@selector(reloadShoppingCartFromExistingProducts) userInfo:nil repeats:NO];
        
    }];
    
}

- (void)reloadShoppingCartFromExistingProducts {
    
    [self cartAddProductFromProductArrays];
}

- (void)cartAddProductFromProductArrays {
    

    if ([[self getRootViewController] isKindOfClass:[MyFavouriteViewController class]]) {
        
        MyFavouriteViewController *myTableViewController = (MyFavouriteViewController *)[self getRootViewController];
        [myTableViewController showLoadingHUD];
        
    }
    
    if (self.rightMenuIsOpen) {
        
        [[AppDelegate getAppDelegate].rightMenuVC showLoadingHUD];
        
    }
    
    [[ServerAPI shared] cartAddProductFromProductArraysReturnCart:YES onlyGetShoppingCart:NO completionBlock:^(id response, NSError *error) {
        
        if ([[Common shared] checkNotError:error controller:[self getRootViewController] response:response]) {
            
             AddProductReturnShoppingCartModel *shoppingCartModel = (AddProductReturnShoppingCartModel *)response;
            
            Results *result =  shoppingCartModel.data.results.firstObject;
            
            if ([shoppingCartModel getErrorCode] == STATUS_OK && !result.status) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUpdateProductListPageUI object:nil];
                [[AppDelegate getAppDelegate].productCategoryController reloadAllCollectionView];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadProductDetailUI object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSetUpTableViewDataFromMyTableView object:nil];
                [self.rightMenuVC.myTableView reloadData];

                if ([[self getRootViewController] isKindOfClass:[_UIViewController class]]) {
                   
                    
                    _UIViewController *controller = (_UIViewController *) [self getRootViewController];
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                                    style:0 handler:^(UIAlertAction *action) {
                                                                        
                                                                        
                                                                    }];
                    
                    [controller showErrorFromMessage:JMOLocalizedString(@"Your desired quantity is not available for this product.", nil) controller:controller okBtn:okBtn];
                    
                    
                }
                
                
            } else {
            
            [TSMessage setDelegate:self];
            [TSMessage setDefaultViewController:[self getRootViewController]];
            
            if (self.rightMenuIsOpen) {
                
             [[AppDelegate getAppDelegate].rightMenuVC hideLoadingHUD];
                
            }
                
            [[AppDelegate getAppDelegate].rightMenuVC addProductAPiCallbackFromAddProductReturnShoppingCartModel:response];
    
            [self showMessageBarFromResponse:response];
                
            }

        }
        
    }];
}

- (UIViewController *)getRootViewController {
    
    return self.topVisbleViewController;
}

#pragma mark - MessageBar

- (void)showMessageBarFromResponse:(id)response {
    
    [TSMessage setDelegate:self];
    [TSMessage setDefaultViewController:[self getRootViewController]];
    
    if ([[self getRootViewController] isKindOfClass:[_UIViewController class]]) {
        
        _UIViewController *controller = (_UIViewController *)[self getRootViewController];
        [controller hideLoadingHUD];
        
    }
    
    BOOL isCanShowAfterAddProductMessageBar = NO;
    
        CGFloat verticalOffset = 0.1;
        
        if ([[self getRootViewController] isKindOfClass:[MyFavouriteViewController class]]) {
            
            verticalOffset = 0.1 + 60;
            isCanShowAfterAddProductMessageBar = YES;
            
        }
        
        if ([[self getRootViewController] isKindOfClass:[ProductListCollectionViewController class]]) {
           
            verticalOffset =  0.1;
            isCanShowAfterAddProductMessageBar = YES;
            
        }
        
        if ([[self getRootViewController] isKindOfClass:[ProductCategoryController class]]) {
            
            
                verticalOffset = ([AppDelegate getAppDelegate].productCategoryController.mainScrollView.hidden) ?  0.1 + 60 : 0.1 ;
                 isCanShowAfterAddProductMessageBar = YES;
                
        }
        
   
        if ([[self getRootViewController] isKindOfClass:[ProductDetailController class]]) {
            
            verticalOffset = 0.1;
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReloadProductDetailUI object:nil];
            isCanShowAfterAddProductMessageBar = YES;
            
        }
    
        for (UIView *view in [self getRootViewController].view.subviews) {
        
            if ([view isKindOfClass:[ZtoreNavigationBar class]]) {
                
                  verticalOffset = 64;
                
            }

        }
        
        self.messageBarVerticalOffset = verticalOffset;
        
        // setup top messageBar UI Type
        
        if ([response isKindOfClass:[AddProductReturnShoppingCartModel class]]) {
            
             [Common shared].messageBar = [[MesssageBar shared] setupDeliveryViewFromShoppingCartModel:response addProductQtyString:[Common shared].addProductQtyString];
            self.messageBarHeight = 91;
            
        }
    
       // setup top error messageBar UI Type
    
        if ([response isKindOfClass:[NSString class]]) {
            
            [Common shared].messageBar = [[[NSBundle mainBundle] loadNibNamed:@"ErrorMessageBar" owner:self options:nil] objectAtIndex:0];
            [Common shared].messageBar = [[Common shared].messageBar setUpErrorMessageFromErrorString:response];
            self.messageBarHeight = 64;
            
            if (self.rightMenuIsOpen) {
                
                self.messageBarVerticalOffset = verticalOffset = 20;
                [TSMessage setDefaultViewController:self.rightMenuVC];
                
            }
            
        }
        
        // some page not show top message Bar After call Add Product API
        
        if (!isCanShowAfterAddProductMessageBar && [response isKindOfClass:[AddProductReturnShoppingCartModel class]]) {
        
        } else {
            
        if (![TSMessage isNotificationActive]) {
            
            [TSMessage showNotificationInViewController:[TSMessage defaultViewController]
                                                  title:NSLocalizedString(@"3141241241241144124121422", nil)
                                               subtitle:NSLocalizedString(@"131221312312312412412", nil)
                                                  image:nil
                                                   type:TSMessageNotificationTypeMessage
                                               duration:TopMessageBarDuration
                                               callback:nil
                                            buttonTitle:nil
                                         buttonCallback:nil
                                             atPosition:TSMessageNotificationPositionNavBarOverlay
                                   canBeDismissedByUser:YES];
            
        } else {
            
            [TSMessage dismissActiveNotificationWithCompletion:^{
               
                [TSMessage showNotificationInViewController:[TSMessage defaultViewController]
                                                      title:NSLocalizedString(@"3141241241241144124121422", nil)
                                                   subtitle:NSLocalizedString(@"131221312312312412412", nil)
                                                      image:nil
                                                       type:TSMessageNotificationTypeMessage
                                                   duration:TopMessageBarDuration
                                                   callback:nil
                                                buttonTitle:nil
                                             buttonCallback:nil
                                                 atPosition:TSMessageNotificationPositionNavBarOverlay
                                       canBeDismissedByUser:YES];
                
            }];

            
        }
        
        }
        
    
}

- (CGFloat)messageLocationOfMessageView:(TSMessageView *)messageView {
    
    return self.messageBarVerticalOffset;
}

- (void)customizeMessageView:(TSMessageView *)messageView {
    
    BOOL nothaveMessageBar = YES;
    [Common shared].messageView = messageView;
    
    for (UIView *view in messageView.subviews) {
        
        if (view == [Common shared].messageBar) {
            
            nothaveMessageBar = NO;
            
        }
    }
    
    if (nothaveMessageBar) {
        
        [[Common shared].messageBar removeFromSuperview];
        [messageView removeAllSubView];
        [messageView addSubview:[Common shared].messageBar];
        [[Common shared].messageBar matchSizeWithMessageBarFromAutoLayoutHeight:self.messageBarHeight];
        
    }
    
    [[Common shared].messageBar progressAnimation];
    
}

#pragma mark Retry API Pop Up View

- (void)showPopUpViewFromErrorMessage:(NSString *)errorMessage {
    
    if (!self.popUpView) {
        
        self.popUpView = [[[NSBundle mainBundle] loadNibNamed:@"PopUpView" owner:self options:nil] objectAtIndex:0];
        [self.popUpView setupErrorMessageView];
        [self.window addSubview:self.popUpView];
        [self.popUpView fadeInCompletion:^(id response, NSError *error) {
            
        }];
        [self.popUpView matchSizeWithSuperViewFromAutoLayout];
        
    }
    
    self.popUpView.titleLabel.text = errorMessage;
    
}

- (void)hidePopUpView {
    
    if (self.popUpView) {
        
        [self.popUpView fadeOutCompletion:^(id response, NSError *error) {
            
            [self.popUpView removeFromSuperview];
            self.popUpView = nil;
            
        }];
        
        
    }
    
}

@end
