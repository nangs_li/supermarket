
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "M13ProgressViewBar.h"

typedef NS_ENUM(NSInteger, MesssageBarType) { AddProductType, ErrorMessageType};

@interface MesssageBar : UIView <UIScrollViewDelegate>

@property(strong, nonatomic) IBOutlet MesssageBar *deliveryView;
@property(strong, nonatomic) IBOutletCollection(UIView) NSArray *navBars;
@property(weak, nonatomic) IBOutlet UIButton *circleBtn;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *contentLabel;
@property(weak, nonatomic) IBOutlet M13ProgressViewBar *progressViewBar;
@property(strong, nonatomic) AddProductReturnShoppingCartModel *shoppingCartModel;
@property(strong, nonatomic) NSString *addProductQtyString;
@property(assign, nonatomic) MesssageBarType messsageBarType;

+ (instancetype)shared;
- (MesssageBar *)setupDeliveryViewFromShoppingCartModel:(AddProductReturnShoppingCartModel *)shoppingCartModel addProductQtyString:(NSString *)addProductQtyString;
- (void)progressAnimation;

@end
