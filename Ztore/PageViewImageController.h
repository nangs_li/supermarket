//
//  PageViewImageController.h
//  Ztore
//
//  Created by ken on 15/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewImageController : UIViewController <UIScrollViewDelegate>

@property(nonatomic, nonatomic) int index;
@property(nonatomic, nonatomic) int pageHeight;
@property(strong, nonatomic) NSString *imageUrl;
@property(nonatomic) bool isFullScreen;
@property(strong, nonatomic) UIScrollView *scrollView;
@property(strong, nonatomic) UIImageView *imageView;

@end
