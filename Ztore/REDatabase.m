//
//  REDatabase.m
//  real-v2-ios
//
//  Created by Li Ken on 19/5/2016.
//  Copyright © 2016年 ken. All rights reserved.
//

#import "REDatabase.h"

// Models
#import "REAPIQueue.h"
#import "REDBAPIQueue.h"

#import "REAgentListing.h"
#import "REAgentProfile.h"
#import "REDBAgentListing.h"
#import "REDBAgentProfile.h"

// Constants
//static NSInteger const kSchemaVersion = 1;
//static NSString *const kEncryptionKey = @"C85DCB777962817BCADBFC322D7F69F20F8B5CDB5FFDE30CB611E0F4D140F72A";
@interface REDatabase ( )

@property(nonatomic, strong) RLMRealm *realm;

@end

@implementation REDatabase

static REDatabase *sharedREDatabase;

#pragma mark - General

+ (REDatabase *)sharedInstance {
  @synchronized(self) {
    if (!sharedREDatabase) {
      sharedREDatabase       = [[REDatabase alloc] init];
      sharedREDatabase.realm = [sharedREDatabase getDatabase];
    }

    return sharedREDatabase;
  }
}

- (RLMRealm *)getDatabase {
//  RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
//  config.schemaVersion          = kSchemaVersion;
//
//  // database encryption
//  NSString *key   = kEncryptionKey;
//  NSData *keyData = [key dataUsingEncoding:NSASCIIStringEncoding];
//
//  config.encryptionKey = keyData;
//  [RLMRealmConfiguration setDefaultConfiguration:config];
  NSError *error  = nil;
  RLMRealm *realm = [RLMRealm defaultRealm];

  if (!realm) {
    // If the encryption key is wrong, `error` will say that it's an invalid
    // database
    DDLogDB(@"Error opening realm: %@", error);
  }

  return realm;
}

- (void)deleteObject:(RLMObject *)object {
  RLMRealm *realm = self.realm;
  [realm beginWriteTransaction];
  [realm deleteObject:object];
  [realm commitWriteTransaction];
}

- (void)deleteObjects:(RLMResults *)objects {
  RLMRealm *realm = self.realm;
  [realm beginWriteTransaction];
  [realm deleteObjects:objects];
  [realm commitWriteTransaction];
}

- (void)deleteAllDBData {
  RLMRealm *realm = self.realm;
  [realm beginWriteTransaction];
  [realm deleteAllObjects];
  [realm commitWriteTransaction];
}

- (void)createOrUpdateDBObject:(REDBObject *)dbObject {
  [self.realm beginWriteTransaction];
  dbObject.updatedStamp = [[NSDate date] timeIntervalSince1970] * 1000;
  [self.realm addOrUpdateObject:dbObject];
  [self.realm commitWriteTransaction];
}

#pragma mark - Address Search
//- (void)createOrUpdateGooglePlaceObject:(REGooglePlaceObject *)placeObject {
//    REDBObject *placeDBObject = [placeObject toREDBObject];
//    [self createOrUpdateDBObject:placeDBObject];
//}
//
//- (NSArray *)getLatestPlaceObject {
//    RLMResults<REDBGooglePlaceObject *> *result = [[REDBGooglePlaceObject
//    allObjects] sortedResultsUsingProperty:@"updatedStamp" ascending:NO];
//
//    NSMutableArray *places = [[NSMutableArray alloc]init];
//    NSInteger limit = result.count > 5 ? 5 : result.count;
//
//    for (NSInteger i = 0 ; i < limit; i++) {
//        REDBGooglePlaceObject *dbPlace = result[i];
//        [places addObject:[dbPlace toREObject]];
//    }
//
//    return places;
//}
#pragma mark - APIQueue

- (void)createOrUpdateAPIQueue:(REAPIQueue *)apiQueue {
  REDBObject *apiDBQueue = [apiQueue toREDBObject];
  [self createOrUpdateDBObject:apiDBQueue];
}

- (NSMutableArray *)getPendingAPIQueues {
  RLMResults<REDBAPIQueue *> *apiDBQueueArray = [REDBAPIQueue objectsWhere:@"ANY pendingQueues.@count = 0 AND status != %d", REAPIQueueStatusSuccess];
  NSMutableArray *apiQueueArray               = [[NSMutableArray alloc] init];

  for (REDBAPIQueue *dbApiQueue in apiDBQueueArray) {
    REAPIQueue *apiQueue = [dbApiQueue toREObject];
    [apiQueueArray addObject:apiQueue];
  }

  return apiQueueArray;
}

- (void)updatePendingQueuesWithAPIQueue:(REAPIQueue *)apiQueue {
  REDBAPIQueue *removeDBAPIQueue              = [apiQueue toREDBObject];
  RLMResults<REDBAPIQueue *> *apiDBQueueArray = [REDBAPIQueue objectsWithPredicate:[NSPredicate predicateWithFormat:@"ANY pendingQueues.queueId CONTAINS %@", apiQueue.queueId]];

  for (REDBAPIQueue *dbAPIQueue in apiDBQueueArray) {
    NSUInteger removeIndx = [dbAPIQueue.pendingQueues indexOfObjectWhere:@"queueId == %@", removeDBAPIQueue.queueId];
    [self.realm beginWriteTransaction];
    [dbAPIQueue.pendingQueues removeObjectAtIndex:removeIndx];
    [self.realm commitWriteTransaction];
  }
}

#pragma mark - AgentProfile

- (void)createOrUpdateAgentProfile:(REAgentProfile *)agentProfile {
  REDBObject *DBObject = [agentProfile toREDBObject];
  [self createOrUpdateDBObject:DBObject];
}

- (NSMutableArray<REAgentProfile *> *)getAgentProfileFromMIDArray:(NSMutableArray<NSString *> *)mIDArray {

  RLMResults<REDBAgentProfile *> *agentProfiles     = [REDBAgentProfile objectsWhere:@"MemberID IN %@", [NSArray arrayWithArray:mIDArray]];
  NSMutableArray<REAgentProfile> *agentProfileArray = [[NSMutableArray<REAgentProfile> alloc] init];

  for (REDBAgentProfile *agentProfile in agentProfiles) {
    REAgentProfile *agentProfileObject = [agentProfile toREObject];
    [agentProfileArray addObject:agentProfileObject];
  }

  return agentProfileArray;
}

#pragma mark - AgentListing

- (void)createOrUpdateAgentListing:(REAgentListing *)agentListing {

  REDBObject *DBObject = [agentListing toREDBObject];
  [self createOrUpdateDBObject:DBObject];
}

- (NSMutableArray<REAgentListing *> *)getAgentListingFromLIDArray:(NSMutableArray<NSString *> *)lIDArray {

  RLMResults<REDBAgentListing *> *agentListings     = [REDBAgentListing objectsWhere:@"AgentListingID IN %@", lIDArray];
  NSMutableArray<REAgentListing> *agentListingArray = [[NSMutableArray<REAgentListing> alloc] init];

  for (REDBAgentListing *agentListing in agentListings) {

    REAgentListing *agentListingObject = [agentListing toREObject];
    [agentListingArray addObject:agentListingObject];
  }

  return agentListingArray;
}

#pragma mark - LoginData

- (void)createOrUpdateLoginData:(LoginData *)loginData {
    
    REDBObject *DBObject = [loginData toREDBObject];
    [self createOrUpdateDBObject:DBObject];
}

- (LoginData *)getLoginDataFromID:(NSString *)Id {
    
    RLMResults<DBLoginData *> *dbLoginDataArray     = [DBLoginData objectsWhere:@"id == %@", Id];
    
    return (dbLoginDataArray.count > 0) ? [dbLoginDataArray.firstObject toREObject] : nil;
}

@end
