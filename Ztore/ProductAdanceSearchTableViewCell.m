//
//  ProductAdanceSearchTableViewCell.m
//  Ztore
//
//  Created by ken on 27/7/16.
//  Copyright © ken. All rights reserved.
//

#import "ProductAdanceSearchTableViewCell.h"

@implementation ProductAdanceSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
}

- (void)setUpTick {

    [[Common shared] setUpTickFromCheckBox:self.checkBoxView];
}

- (void)checkAnimationSelected:(BOOL)selected animated:(BOOL)animated {

  self.checkBoxView.tintColor = (selected == YES) ? [UIColor clearColor] : [UIColor lightGrayColor];
  [self.checkBoxView setOn:selected animated:animated];
}

@end
