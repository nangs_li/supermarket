
//  Created by Li Ken on 25/7/2016.
//  Copyright © 2016 Ken. All rights reserved.
//

#import "M13ProgressViewBar.h"
#import "MesssageBar.h"

@interface ErrorMessageBar : MesssageBar

- (ErrorMessageBar *)setUpErrorMessageFromErrorString:(NSString *)errorString;

@end
