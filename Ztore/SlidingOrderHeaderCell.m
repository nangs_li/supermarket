//
//  SlidingOrderHeaderCell.m
//  Ztore
//
//  Created by ken on 14/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "Common.h"
#import "SlidingOrderHeaderCell.h"

@implementation SlidingOrderHeaderCell

- (void)awakeFromNib {

  self.backgroundColor = [[Common shared] getCommonRedColor];
   [super awakeFromNib];
    
}

@end
