//
//  NSString+Utility.h
//  productionreal2
//
//  Created by Alex Hung on 22/10/15.
//  Copyright © 2015 Real. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface UILabel (Utility)

- (void)setUpTitleLabelFromText:(NSString *)text;
- (CGSize)sizeOfMultiLineLabel;

@end
