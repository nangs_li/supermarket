//
//  REAgentListing.m
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  Copyright © 2016 ken. All rights reserved.
//

#import "REAgentListing.h"
#import "REDBAgentListing.h"

@implementation REDBAgentListing

+ (NSString *)primaryKey {
  return @"AgentListingID";
}

+ (NSArray *)indexedProperties {
  return @[ @"AgentListingID" ];
}

- (REAgentListing *)toREObject {

  REAgentListing *dbAgentListing                  = [[REAgentListing alloc] init];
  dbAgentListing.Reasons                          = (NSMutableArray<Reasons> *)[NSMutableArray arrayWithRLMArray:self.Reasons];
  dbAgentListing.BedroomCount                     = self.BedroomCount;
  dbAgentListing.PropertyPriceFormattedForIndian  = self.PropertyPriceFormattedForIndian;
  dbAgentListing.SloganIndex                      = self.SloganIndex;
  dbAgentListing.KitchenCount                     = self.KitchenCount;
  dbAgentListing.LivingRoomCount                  = self.LivingRoomCount;
  dbAgentListing.PropertyType                     = self.PropertyType;
  dbAgentListing.PropertySize                     = self.PropertySize;
  dbAgentListing.PropertyPriceFormattedForRoman   = self.PropertyPriceFormattedForRoman;
  dbAgentListing.PropertyPriceFormattedForChinese = self.PropertyPriceFormattedForChinese;
  dbAgentListing.CurrencyUnit                     = self.CurrencyUnit;
  dbAgentListing.Address                          = (NSMutableArray<Address2> *)[NSMutableArray arrayWithRLMArray:self.Address];
  dbAgentListing.SizeUnit                         = self.SizeUnit;
  dbAgentListing.BathroomCount                    = self.BathroomCount;
  dbAgentListing.CreationDate                     = self.CreationDate;
  dbAgentListing.PropertyPrice                    = self.PropertyPrice;
  dbAgentListing.ListingFollowerCount             = self.ListingFollowerCount;
  dbAgentListing.BalconyCount                     = self.BalconyCount;
  dbAgentListing.Photos                           = (NSMutableArray<Photos> *)[NSMutableArray arrayWithRLMArray:self.Photos];
  dbAgentListing.AgentListingID                   = self.AgentListingID;
  dbAgentListing.SizeUnitType                     = self.SizeUnitType;
  dbAgentListing.Version                          = self.Version;
  dbAgentListing.SpaceType                        = self.SpaceType;

  return dbAgentListing;
}

@end

@implementation DBReasons

- (Reasons *)toREObject {

  Reasons *dbReasons      = [[Reasons alloc] init];
  dbReasons.Reason3URL    = self.Reason3URL;
  dbReasons.Reason3       = self.Reason3;
  dbReasons.Reason2       = self.Reason2;
  dbReasons.Reason1       = self.Reason1;
  dbReasons.SloganIndex   = self.SloganIndex;
  dbReasons.LanguageIndex = self.LanguageIndex;
  dbReasons.Reason1URL    = self.Reason1URL;
  dbReasons.Reason2URL    = self.Reason2URL;

  return dbReasons;
}

@end

@implementation DBAddress



- (Address2 *)toREObject {

  Address2 *dbAddress        = [[Address2 alloc] init];
  dbAddress.FormattedAddress = self.FormattedAddress;
  dbAddress.Lang             = self.Lang;
  dbAddress.ShortAddress     = self.ShortAddress;
  dbAddress.PlaceID          = self.PlaceID;

  return dbAddress;
}

@end

@implementation DBPhotos

- (Photos *)toREObject {

  Photos *dbPhotos = [[Photos alloc] init];
  dbPhotos.URL     = self.URL;
  dbPhotos.PhotoID = self.PhotoID;
  dbPhotos.Height  = self.Height;
  dbPhotos.Width   = self.Width;

  return dbPhotos;
}

@end
