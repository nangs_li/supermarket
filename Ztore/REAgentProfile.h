//
//  REAgentProfile.h
//  real-v2-ios
//
//  Created by Li Ken on 4/8/2016.
//  strongright © 2016 ken. All rights reserved.
//

#import "REObject.h"
@class REDBAgentProfile, DBLanguages, DBExperiences, DBSpecialties, DBPastClosings;

@protocol Languages2

@end

@interface Languages2 : REObject

@property(nonatomic, strong) NSString *Language;

@property(nonatomic, assign) int LangIndex;

- (DBLanguages *)toREDBObject;

@end

@protocol Experiences

@end

@interface Experiences : REObject

@property(nonatomic, assign) int IsCurrentJob;

@property(nonatomic, strong) NSString *EndDate;

@property(nonatomic, strong) NSString *Title;

@property(nonatomic, strong) NSString *Company;

@property(nonatomic, strong) NSString *StartDate;

- (DBExperiences *)toREDBObject;

- (NSString *)getPeriodString;

@end

@protocol Specialties

@end

@interface Specialties : REObject

@property(nonatomic, strong) NSString *Specialty;

- (DBSpecialties *)toREDBObject;

@end

@protocol PastClosings

@end

@interface PastClosings : REObject

@property(nonatomic, assign) int PropertyType;

@property(nonatomic, strong) NSString *SoldPrice;

@property(nonatomic, strong) NSString *State;

@property(nonatomic, strong) NSString *SoldDateString;

@property(nonatomic, strong) NSString *Address;

@property(nonatomic, strong) NSString *Country;

@property(nonatomic, strong) NSString *SoldDate;

@property(nonatomic, assign) int Position;

@property(nonatomic, assign) int ID;

@property(nonatomic, assign) int SpaceType;

- (DBPastClosings *)toREDBObject;

@end

@protocol REAgentProfile

@end

@interface REAgentProfile : REObject

@property(nonatomic, assign) int FollowerCount;

@property(nonatomic, strong) NSMutableArray<Specialties> *Specialties;

@property(nonatomic, assign) int Version;

@property(nonatomic, assign) int FollowingCount;

@property(nonatomic, strong) NSString *LicenseNumber;

@property(nonatomic, assign) int IsFollowing;

@property(nonatomic, strong) NSMutableArray<Languages2> *Languages;

@property(nonatomic, strong) NSString *QBID;

@property(nonatomic, assign) int MemberType;

@property(nonatomic, strong) NSString *PhotoURL;

@property(nonatomic, assign) int MemberID;

@property(nonatomic, strong) NSString *MemberName;

@property(nonatomic, strong) NSMutableArray<Experiences> *Experiences;

@property(nonatomic, strong) NSMutableArray<PastClosings> *PastClosings;

- (REDBAgentProfile *)toREDBObject;

- (NSString *)languageTagString;

- (NSString *)specialtyTagString;

@end
