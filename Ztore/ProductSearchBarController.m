//
//  ProductSearchBarController.m
//  Ztore
//
//  Created by ken on 13/7/15.
//  Copyright (c) ken. All rights reserved.
//

#import "ProductSearchBarController.h"
#import <AVFoundation/AVFoundation.h>

//#import "AudioController.h"
//#import "SpeechRecognitionService.h"
#import "ProductListCollectionViewController.h"
#import <AVFoundation/AVFoundation.h>

#define API_KEY @"AIzaSyDaOTNsCd19oH7k4ywgkFajE-mFpRzeuO8"

#define SAMPLE_RATE 16000

@interface ProductSearchBarController ( ) <AVAudioRecorderDelegate, AVAudioPlayerDelegate>

@property(strong, nonatomic) AVAudioRecorder *audioRecorder;
@property(strong, nonatomic) AVAudioPlayer *audioPlayer;

@end

@implementation ProductSearchBarController

@synthesize delegate;

static NSString *CODE_SEARCH_KEYWORD_LIST = @"searchkeywordlist";
static int TABLE_HH                       = 66;
static int RESULT_TABLE_HH                = 36;
static int VOICE_LISTENING_TIME           = 3;

- (void)viewDidLoad {
  [super viewDidLoad];
  self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    searchResultTableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
  resultSectionTitleList = [[NSArray alloc] initWithObjects:JMOLocalizedString(@"brands", nil), JMOLocalizedString(@"products", nil), JMOLocalizedString(@"tags", nil), nil];

  searchHistoryList   = [[[Common shared] valueForKey:CODE_SEARCH_KEYWORD_LIST] mutableCopy];

  if (searchHistoryList == nil) {
    searchHistoryList = [[NSMutableArray alloc] init];
  }
    
#pragma mark audioRecorder
   
    [self initVoiceMethod];
    [self checkGrantPermission];
    searchResultTableView.emptyDataSetSource = self;
    searchHistoryTableView.emptyDataSetDelegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated {

  [super viewWillAppear:animated];
  [self setUpVoiceBtn];

}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    searchResultTableView.userInteractionEnabled =  YES;
    [searchResultTableView reloadData];
    [searchHistoryTableView reloadData];
}

- (void)setUpNavigationBar {

    self.navigationBar = [AppDelegate getAppDelegate].productCategoryController.ztoreNavigationBar;
    [self.navigationBar productSearchBarType];
    [self.searchBar performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
    self.searchBar          = self.navigationBar.searchBar;
    self.searchBar.delegate = self;
    self.searchBar.barTintColor = [[Common shared] getCommonRedColor];
    self.searchBar.translucent  = NO;
    self.searchBar.placeholder  = JMOLocalizedString(@"Search or Voice Search", nil);
    [super setPageName:@"Quick Search"];
    [self prepareSearchHistoryTableView];
    [self.searchBar setImage:[UIImage imageNamed:@"ic_clear_3x"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    [self.searchBar setImage:[UIImage imageNamed:@"ic_clear_3x"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateHighlighted];
    [self.navigationBar.leftBtn addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBar.voiceBtn addTarget:self action:@selector(voiceBtnPress) forControlEvents:UIControlEventTouchUpInside];
}

- (void)voiceBtnPress {
    
    self.navigationBar.voiceBtn.userInteractionEnabled = NO;
    [self recordAudio:self];
    [self showLoadingHUDWithProgress:JMOLocalizedString(@"Listening...", nil)];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, VOICE_LISTENING_TIME * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue( ), ^(void) {
        
        [self showLoadingHUDWithProgress:JMOLocalizedString(@"Analysis...", nil)];
        [self processAudio:self];
        
    });
    
}

- (void)backBtnPressed:(id)sender {
    
    [super backBtnPressed:sender];
    [self.view endEditing:YES];
    [self.searchBar resignFirstResponder];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  if (tableView == searchResultTableView) {

    return 3;

  } else {

    return 1;
  }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  if (tableView == searchResultTableView) {

    return [self.autoCompleteProductData getObjectCountFromSection:section];

  } else if (tableView == searchHistoryTableView) {

    return [searchHistoryList count];

  } else {

    return 0;
  }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    if (tableView == searchHistoryTableView) {
        
     return (searchHistoryList.count == 0) ? 0 : RESULT_TABLE_HH;
        
    } else if (tableView == searchResultTableView) {
        
        return ([self.autoCompleteProductData getObjectCountFromSection:section] > 0) ? RESULT_TABLE_HH : 0;
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

  return 1;
}

- (void)clearSearchHistories:(id)target {
  [[Common shared] setValue:[[NSMutableArray alloc] init] forKey:CODE_SEARCH_KEYWORD_LIST];
  searchHistoryList = [[[Common shared] valueForKey:CODE_SEARCH_KEYWORD_LIST] mutableCopy];
  [searchHistoryTableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

  if (tableView == searchHistoryTableView) {
      
    if (searchHistoryTableHeaderView == nil) {
      searchHistoryTableHeaderView = [ProductSearchHistoryTableViewHeader initHeader];
      [[Common shared] addClickEventForView:searchHistoryTableHeaderView withTarget:self action:@"clearSearchHistories:"];
        
      [searchHistoryTableHeaderView setLabelHidden:(searchHistoryList.count == 0) ? TRUE : FALSE];
    
    }

    return searchHistoryTableHeaderView;
  } else if (tableView == searchResultTableView) {
      
    ProductSearchHistoryTableViewHeader *view = [ProductSearchHistoryTableViewHeader initHeader];
      
      [view setLabelText:(section < resultSectionTitleList.count) ? resultSectionTitleList[section] : @""];
      
      view.hidden = ([self.autoCompleteProductData getObjectCountFromSection:section] > 0) ? NO : YES;
   
    return view;
  } else {

    return nil;
  }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {

  return [[UIView alloc] initWithFrame:CGRectZero];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  static NSString *CellIdentifier = @"Cell";

  // Configure the cell...
  if (tableView == searchResultTableView) {
    CellIdentifier                         = @"ProductSearchResultTableViewCell";
    ProductSearchResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
      [tableView registerNib:[UINib nibWithNibName:@"ProductSearchResultTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
      cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }

      [cell setAttributedText:[self.autoCompleteProductData getNameFromIndexPath:indexPath] searchBarText:self.searchBar.text];

    return cell;

  } else if (tableView == searchHistoryTableView) {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [searchHistoryList objectAtIndex:indexPath.row];
    cell.textLabel.font = [[Common shared] getNormalFont];

    return cell;
  } else {

    return nil;
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

  return TABLE_HH;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

  self.selectIndexPath = indexPath;
  [self.searchBar resignFirstResponder];
    
  if (tableView == searchResultTableView) {
      
    [self searchProductStartAt:0 byKeyword:[self.autoCompleteProductData getNameFromIndexPath:indexPath] isAddToHistory:TRUE tableView:searchResultTableView];

  } else if (tableView == searchHistoryTableView) {

    NSString *keyword = [searchHistoryList objectAtIndex:indexPath.row];
    [self searchProductStartAt:0 byKeyword:keyword isAddToHistory:FALSE tableView:searchHistoryTableView];
      
  }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

  [self.searchBar resignFirstResponder];
}

#pragma mark - Search Function Responsible For Searching

- (void)getAutoComplete {

  [[ServerAPI shared] getAutoCompleteWithKeyword:self.searchBar.text
                           completionBlock:^(id response, NSError *error) {

                             if ([[Common shared] checkNotError:error controller:self response:response]) {

                               [self getAutoCompleteCallbackFromAutoCompleteProduct:response];
                             }

                           }];
}

- (void)getAutoCompleteCallbackFromAutoCompleteProduct:(AutoCompleteProduct *)autoCompleteProduct {

  AutoCompleteProductData *output = autoCompleteProduct.data;
  self.autoCompleteProductData = output;
    
  if ([(NSNumber *)autoCompleteProduct.statusCodes[0] intValue] == STATUS_OK || [autoCompleteProduct getErrorCode] == NO_DATA) {

    [self.view bringSubviewToFront:searchResultTableView];
    [searchResultTableView setHidden:FALSE];
    [searchResultTableView reloadData];

  } else {

    [searchResultTableView setHidden:TRUE];

  }
    
}

- (void)searchProductStartAt:(int)start byKeyword:(NSString *)keyword isAddToHistory:(BOOL)isAddToHistory tableView:(UITableView *)tableView {

    [self setUpVoiceBtn];
  // add keywords to search history
  if (isAddToHistory) {
      
    [self addSearchKeywordToHistory:keyword];
      
  }
    
    if (tableView == searchResultTableView) {
        
        if (self.selectIndexPath.section == 0) {
            
            AutoCompleteBrands *brands = [self.autoCompleteProductData.brands objectAtIndex:self.selectIndexPath.row];
            InputListProduct *inputListProduct                    = [[InputListProduct alloc] init];
            inputListProduct.type = search_brand_ids;
            inputListProduct.brand_ids                    = @[[NSString stringWithFormat: @"%ld", (long)brands.id]];
            [AppDelegate getAppDelegate].searchSelectedString = brands.name;
            [[Common shared] pushToProductListPageFromInputListProduct:inputListProduct controller:self];
            [self.searchBar resignFirstResponder];
            
        } else  if (self.selectIndexPath.section == 1) {
            
            [Common shared].autoCompleteProducts = [self.autoCompleteProductData.products objectAtIndex:self.selectIndexPath.row];
        
            Products *products = [[Products alloc] init];
            products.id = 0;
            searchResultTableView.userInteractionEnabled =  NO;
            [[Common shared] pushToProductDetailPageFromProduct:products];
            [self.searchBar resignFirstResponder];
    
            
        } else  if (self.selectIndexPath.section == 2) {
            
            [Common shared].tagString = [self.autoCompleteProductData.tags objectAtIndex:self.selectIndexPath.row];
            InputListProduct *inputListProduct                    = [[InputListProduct alloc] init];
            inputListProduct.type = search_tags;
            inputListProduct.tags                    = @[[Common shared].tagString];
            [AppDelegate getAppDelegate].searchSelectedString = [Common shared].tagString;
            [[Common shared] pushToProductListPageFromInputListProduct:inputListProduct controller:self];
            [self.searchBar resignFirstResponder];
            
        }
        
        self.searchBar.text = nil;
        [searchResultTableView setHidden:true];
        [searchResultTableView reloadData];
        [searchHistoryTableView setHidden:false];
        
    } else {
   
        [self searchBar:self.searchBar textDidChange:keyword];
 
    }
}

- (void)searchProductCallbackFromProductListArray:(ProductListArray *)productListArray {

  if ([(NSNumber *)productListArray.statusCodes[0] intValue] == STATUS_OK) {
      
      
      
  }
}

- (void)addSearchKeywordToHistory:(NSString *)keyword {

    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSString *string in searchHistoryList) {
        
        if (![string isEqualToString:keyword]) {
            
            [array addObject:string];
            
        }
        
    }
    
  searchHistoryList = array;
    
  [searchHistoryList insertObject:keyword atIndex:0];
    
  [[Common shared] setValue:searchHistoryList forKey:CODE_SEARCH_KEYWORD_LIST];
}

#pragma mark - Search Bar Implementation

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
  // Remove all objects first.
    searchBar.text = searchText;
    
    [self loadingFromColor:nil];
    
  if ([searchText length] != 0) {
      
      if (self.autoCompleteAPIProductAPITimer) {
          
          [self.autoCompleteAPIProductAPITimer invalidate];
      }
      
      self.autoCompleteAPIProductAPITimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(getAutoComplete) userInfo:nil repeats:NO];


    if (isAddedHistoryTableView) {

      isAddedHistoryTableView = FALSE;
    }

  } else {

    [self prepareSearchHistoryTableView];
  }

    [self setUpVoiceBtn];

}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
  [searchBarController setActive:NO];
  [[self presentingViewController] dismissViewControllerAnimated:false completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
  NSLog(@"Search Clicked");
  [self searchProductStartAt:0 byKeyword:searchBar.text isAddToHistory:TRUE tableView:searchResultTableView];
}

- (void)prepareSearchHistoryTableView {

  if (searchHistoryTableView != nil && !isAddedHistoryTableView) {

    searchResultTableView.hidden  = YES;
    searchHistoryTableView.hidden = NO;

    [searchHistoryTableView reloadData];
    [self.view bringSubviewToFront:searchHistoryTableView];
    isAddedHistoryTableView = TRUE;
  }
    
}

#pragma voice method

- (void)checkGrantPermission {
    
    AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];
    
    switch (permissionStatus) {
        case AVAudioSessionRecordPermissionUndetermined:{
            [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                // CALL YOUR METHOD HERE - as this assumes being called only once from user interacting with permission alert!
                if (granted) {
                    
                    self.navigationBar.voiceBtn.alpha = 1;
                    
                } else {
                    
                    self.navigationBar.voiceBtn.alpha = 0;
                }
            }];
            break;
        }
        case AVAudioSessionRecordPermissionDenied:
            self.navigationBar.voiceBtn.alpha = 0;
            break;
        case AVAudioSessionRecordPermissionGranted:
            self.navigationBar.voiceBtn.alpha = 1;
            break;
        default:
            // this should not happen.. maybe throw an exception.
            break;
    }

    
}

- (void)initVoiceMethod {
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:[self soundFilePath]];
    NSDictionary *recordSettings = @{AVEncoderAudioQualityKey:@(AVAudioQualityMax),
                                     AVEncoderBitRateKey: @16,
                                     AVNumberOfChannelsKey: @1,
                                     AVSampleRateKey: @(SAMPLE_RATE)};
    NSError *error;
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }

    
}

- (NSString *)soundFilePath {
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    
    return [docsDir stringByAppendingPathComponent:@"sound.caf"];
}

- (IBAction)recordAudio:(id)sender {
    
    [self stopAudio:sender];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [_audioRecorder record];
    
}

- (IBAction)playAudio:(id)sender {
    
    [self stopAudio:sender];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc]
                    initWithContentsOfURL:_audioRecorder.url
                    error:&error];
    _audioPlayer.delegate = self;
    _audioPlayer.volume = 1.0;
    
    if (error) {
        NSLog(@"Error: %@",
              error.localizedDescription);
    } else {
        [_audioPlayer play];
        
    }
}

- (IBAction)stopAudio:(id)sender {
    
    if (_audioRecorder.recording) {
        
        [_audioRecorder stop];
        
    } else if (_audioPlayer.playing) {
        
        [_audioPlayer stop];
    }
    
}

- (void)processAudio:(id)sender {
    
    [self stopAudio:sender];
    
    NSString *service = @"https:/speech.googleapis.com/v1beta1/speech:syncrecognize";
    service = [service stringByAppendingString:@"?key="];
    service = [service stringByAppendingString:API_KEY];
    NSData *audioData = [NSData dataWithContentsOfFile:[self soundFilePath]];
    NSDictionary *configRequest = @{@"encoding":@"LINEAR16",
                                    @"sampleRate":@(SAMPLE_RATE),
                                    @"languageCode":([[Common shared] isEnLanguage] ) ? @"en" : @"zh-HK",
                                    @"maxAlternatives":@3};
    NSDictionary *audioRequest = @{@"content":[audioData base64EncodedStringWithOptions:0]};
    NSDictionary *requestDictionary = @{@"config":configRequest,
                                        @"audio":audioRequest};
    NSError *error;
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestDictionary
                                                          options:0
                                                            error:&error];
    
    NSString *path = service;
    NSURL *URL = [NSURL URLWithString:path];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    // if your API key has a bundle ID restriction, specify the bundle ID like this:
    [request addValue:[[NSBundle mainBundle] bundleIdentifier] forHTTPHeaderField:@"X-Ios-Bundle-Identifier"];
    NSString *contentType = @"application/json";
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     dataTaskWithRequest:request
     completionHandler:
     ^(NSData *data, NSURLResponse *response, NSError *error) {
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            [self hideLoadingHUD];
                            self.navigationBar.voiceBtn.userInteractionEnabled = YES;
                            [self endCallAPILoading];
                            
                            if (isValid(data)) {
                                
                            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                            
                            if (isValid(json)) {
                                
                                NSArray *array = [json objectForKey:@"results"];
                                NSDictionary *dict = array.firstObject;
                                NSArray *result =  [dict objectForKey:@"alternatives"];
                                NSDictionary *dict2 = result.firstObject;
                                self.searchBar.text = [dict2 objectForKey:@"transcript"];
                                [self searchBar:self.searchBar textDidChange:self.searchBar.text];
                                NSLog(@"RESULT: %@", array);
                                
                            } else {
                                
                                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                                                style:0 handler:^(UIAlertAction *action) {
                                                                                    
                                                                                    
                                                                                }];
                                
                                [self showErrorFromMessage:JMOLocalizedString(@"No Record", nil) controller:self okBtn:okBtn];
                                
                            }
                            
                            } else {
                                
                                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:JMOLocalizedString(@"OK", nil)
                                                                                style:0 handler:^(UIAlertAction *action) {
                                                                                    
                                                                                    
                                                                                }];
                                
                                [self showErrorFromMessage:JMOLocalizedString(kStatusMessageNetworkFailError, nil) controller:self okBtn:okBtn];
                                
                            }
                            
                        });
     }];
    [task resume];
}

- (void)setUpVoiceBtn {
    
    self.navigationBar.voiceBtn.hidden = ([self.searchBar.text length] != 0) ? YES : NO;
    [self.searchBar setImage:self.navigationBar.voiceBtn.hidden ? [UIImage imageNamed:@"ic_clear_3x"] : nil forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];
    [self.searchBar setImage:self.navigationBar.voiceBtn.hidden ? [UIImage imageNamed:@"ic_clear_3x"] : nil forSearchBarIcon:UISearchBarIconClear state:UIControlStateHighlighted];

    
}

@end
