//
//  ProductAdanceSearchTableViewCell.h
//  Ztore
//
//  Created by ken on 27/7/16.
//  Copyright © ken. All rights reserved.
//

#import "BEMCheckBox.h"
#import <UIKit/UIKit.h>
#import "SLExpandableTableView.h"

@interface ProductAdanceSearchTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet BEMCheckBox *checkBoxView;
@property(weak, nonatomic) IBOutlet UILabel *label_option;

- (void)setUpTick;
- (void)checkAnimationSelected:(BOOL)selected animated:(BOOL)animated;

@end
