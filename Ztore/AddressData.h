//
//  Address.h
//  Ztore
//
//  Created by ken on 10/5/16.
//  Copyright © ken. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AddressDetail

@end

@interface AddressDetail : REObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, copy) NSString *name;

@property(nonatomic, copy) NSString *code;

@end

@interface AddressData : REObject

@property(nonatomic, strong) NSArray<AddressDetail> *data;

- (NSString *)getNameFromAddressId:(NSInteger)addressId;

@end
